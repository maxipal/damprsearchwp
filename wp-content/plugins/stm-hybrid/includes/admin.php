<?php

add_action( 'admin_menu', function () {
	add_menu_page( __( 'Listings', 'motors' ), __( 'Listings', 'motors' ), 'edit_pages', 'listing-admin', 'listing_admin', 'dashicons-location-alt', 5 );
} );

add_action( 'admin_footer', function () {
	// Make the link to listings admin
	// console open in new tab
	echo <<<HTML
<script type="text/javascript">
jQuery('#toplevel_page_listing-admin a').attr('target', '_blank');
</script>
HTML;

} );

function listing_admin() {
	$token = stm_hybrid_user_token( null, true );
	if ( ! isset( $token['token'] ) ) {
		echo "Authentication error. Couldn't retrieve token.";
		return;
	}

	$url = site_url( '/console/' );

	echo <<<HTML
<script type="text/javascript">
localStorage.setItem('stm-hybrid-admin.user-token', '{$token['token']}');
localStorage.setItem('stm-hybrid-admin.user-token-expires', {$token['expires']} * 1000);
location.href = '$url';

// jQuery(function($) {
// 	function setSize() {
// 	    $('#listing-admin').height($(window).height() - 50);
// 	}
// 	$(window).on('resize', setSize);
// 	setSize();
// })
</script>
<!--
<div style="margin: 15px 15px 0 0">
<iframe id="listing-admin" src="/console" width="100%" height="600px"></iframe>
</div>
-->
HTML;
}

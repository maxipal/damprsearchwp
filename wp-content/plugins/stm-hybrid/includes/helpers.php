<?php
/**
 * Get current aircraft result
 *
 * @param string $part
 *
 * @return mixed
 */
function listings($part = 'data' ) {
	return isset( $GLOBALS['listings'] ) ? $GLOBALS['listings'][$part] : null;
}

/**
 * Register current aircraft
 *
 * @param $listing
 */
function the_listing($listing ) {
	$GLOBALS['listing'] = $listing;
}

/**
 * Get current aircraft or load by id
 *
 * @param int $id
 *
 * @return \Stm_Hybrid\Listing
 */
function listing($id = null ) {
	if ( $id ) {
	    $listing = listing();

		if ( is_object($listing) && $listing && $listing->id == $id ) {
			return listing();
		}

		return \Stm_Hybrid\Listing::find( $id );
	}

	return isset( $GLOBALS['listing'] ) ? $GLOBALS['listing'] : null;
}

function get_listing_image($id = null) {
    $image_url = false;
    $listing = listing($id);
    if (!empty($listing->gallery)) {
        $image_url = $listing->gallery[0];
    }

    return $image_url;
}

/**
 * Get list of attributes used in filter
 *
 * @param string $context
 *
 * @return \Stm_Hybrid\Filter[]
 */
function stm_hybrid_filters($context = '' ) {
	$filters = [];
	$res = stm_hybrid_api_transient( 180, 'listings/filters?context=' . $context );

	if ( is_wp_error( $res ) ) {
		wp_die('503: Listing service is unavailable', '', array( 'response' => 503 ) );
	}

	$json = $res->json();
	foreach ( $json as $filter ) {
		$filter = new \Stm_Hybrid\Filter( $filter );
		$filter->value = stm_listings_input( 'filter.' . $filter->key );

		$filters[ $filter->key ] = $filter;
	}

	global $landing;
	if ( ! empty( $landing ) ) {
		foreach ( $landing as $key => $value ) {
			$filters[ $key ]->value = $value;
		}
	}

	return apply_filters('stm_hybrid_filters', $filters, $context);
}

/**
 * Get listing filter query parameters for API request
 *
 * @param string $context
 *
 * @return array
 */
function stm_hybrid_filter_params( $context = '' ) {
	return wp_list_pluck( stm_hybrid_filters( $context ), 'value' );
}

function stm_hybrid_api_url() {
	if ( defined( 'LISTING_API_URL' ) ) {
		$url = LISTING_API_URL;
	} else {
		$url = get_option( 'listing_base_url' );
	}

	return rtrim( $url, '/' ) . '/';
}

function stm_hybrid_api_auth() {
	$token = get_option( 'stm_hybrid_auth', [ 'token' => null, 'expires' => null ] );

	if ( ! $token['token'] || $token['expires'] < time() + 5 ) {
		$res = \Stm_Hybrid\Api::make()->post('auth/login', stm_hybrid_api_credentials() );
		if ( ! is_wp_error( $res ) && $res->isOk() ) {
			$token = $res->json();
			$token['expires'] = time() + $token['expires_in'];
			update_option( 'stm_hybrid_auth', $token );
		}
	}

	return $token['token'];
}

function stm_hybrid_api_credentials() {
	return [
		'email' => get_option( 'stm_hybrid_api_email', 'wordpress@damprsearch.com' ),
		'password' => get_option( 'stm_hybrid_api_password', '7$kQWA' )
	];
}

function stm_hybrid_api_transient_key( $uri ) {
	return 'stm-hybrid-api-' . md5( $uri );
}

function stm_hybrid_api_transient($ttl, $uri, $options = [] ) {
	$transient_key  = stm_hybrid_api_transient_key( $uri );
	$use_cache = defined( 'WP_CACHE' ) && WP_CACHE;

	if ( $use_cache && false !== ( $cached = get_transient( $transient_key ) ) ) {
		$response = new \Stm_Hybrid\Response( new Requests_Response() );
		$response->set_status( $cached['response']['code'] );
		$response->set_headers( $cached['headers'] );
		$response->set_data( $cached['body'] );
	}
	else {
		$response = \Stm_Hybrid\Api::make()->get( $uri, $options );

		if ( ! is_wp_error( $response ) && $response->isOk() ) {
			$cache = $response->to_array();
			$cache['headers'] = $cache['headers']->getAll();
			set_transient( $transient_key, $cache, $ttl );
		}
	}

	return $response;
}

function stm_get_listings_base_url() {
    $stm_post_type_options = get_option('stm_post_types_options',
	['listings' => array(
		'title' => __( 'Listings', STM_POST_TYPE ),
		'plural_title' => __( 'Listings', STM_POST_TYPE ),
		'rewrite' => 'listings'
	)]);
    $listing_base_url = $stm_post_type_options['listings']['rewrite'];

    return $listing_base_url;
}

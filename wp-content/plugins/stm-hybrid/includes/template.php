<?php

add_filter( 'template_include', function ( $template ) {
	if ( is_post_type_archive( stm_listings_post_type() ) ) {
		return stm_hybrid_locate_template( 'inventory' );
	}

	return $template;
}, 1000 );

/**`
 * Include template in plugin scope
 *
 * @param $template
 * @param array $vars
 */
function stm_hybrid_include($template, $vars = array() ) {
    if (!empty($template)) {
        if ( $located = stm_hybrid_locate_template( $template ) ) {
            stm_hybdrid_load_template( $located, $vars );
        }
    }
}

/**
 * Load a template part into a template.
 *
 * The same as core WordPress get_template_part(), but also includes plugins scope
 *
 * @param string $template
 * @param string $name
 * @param array $vars
 */
function stm_hybrid_template_part($template, $name = '', $vars = array() ) {
	$templates = array();
	$name      = (string) $name;

	if ( '' !== $name ) {
		$templates[] = "{$template}-{$name}.php";
	}

	$templates[] = "{$template}.php";

	if ( $located = stm_hybrid_locate_template( $templates ) ) {
		stm_hybdrid_load_template( $located, $vars );
	}
}

/**
 * Locate template in plugin scope
 *
 * @param string|array $templates Single or array of template files
 *
 * @return string
 */
function stm_hybrid_locate_template($templates ) {
	$located = false;

	foreach ( (array) $templates as $template ) {
		if ( substr( $template, - 4 ) !== '.php' ) {
			$template .= '.php';
		}

		// Development tweak
		if ( isset( $_COOKIE['XDEBUG_TRACE'] ) ) {
			$located = STM_HYBRID_PATH . '/templates/' . $template;
		}
		elseif ( ! ( $located = locate_template( 'stm-hybrid/' . $template ) ) ) {
			$located = STM_HYBRID_PATH . '/templates/' . $template;
		}

		if ( file_exists( $located ) ) {
			break;
		}

	}

	return apply_filters( 'stm_hybrid_locate_template', $located, $templates );
}

/**
 * Load template in plugin scope
 *
 * @param $__template
 * @param array $__vars
 */
function stm_hybdrid_load_template($__template, $__vars = array() ) {
	extract( $__vars );
	include $__template;
}

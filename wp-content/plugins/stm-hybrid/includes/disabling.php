<?php

add_filter( 'register_post_type_args', function ( $args, $type ) {
	if ( $type !== stm_listings_post_type() ) {
		return $args;
	}

	$args['show_in_menu'] = false;

	return $args;
}, 10, 2 );

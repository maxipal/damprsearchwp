<?php
add_action( 'wp_enqueue_scripts', 'listing_auth_scripts' );
add_action( 'admin_enqueue_scripts', 'listing_auth_scripts' );
function listing_auth_scripts() {
	if ( ! is_user_logged_in() ) {
		return;
	}

	if ( is_admin() && 'toplevel_page_listing-admin' !== get_current_screen()->base ) {
		return;
	}

	wp_enqueue_script( 'stm-hybrid-auth', STM_HYBRID_URL . '/assets/js/auth.js', array( 'jquery' ), STM_HYBRID_VERSION, true );
	$token = stm_hybrid_user_token( wp_get_current_user() );
	if(!$token && is_user_logged_in()){
		stm_hybrid_sync_user( get_current_user_id() );
		$token = stm_hybrid_user_token( wp_get_current_user() );
	}
	$token['expires_in'] = intval($token['expires'] - time());
	wp_localize_script( 'stm-hybrid-auth', 'stm_hybrid_auth', $token );
}

add_action( 'init', function () {
	if ( ! isset( $_GET['stm-hybrid-refresh-token'] ) || ! is_user_logged_in() ) {
		return;
	}

	$token = stm_hybrid_user_token( null, true );
	wp_send_json( $token );
	exit;
} );

function stm_hybrid_user_token(WP_User $user = null, $refresh = false ) {
	if ( is_null( $user ) ) {
		$user = wp_get_current_user();
	}

	$token = get_user_meta( $user->ID, 'stm_hybrid_token', true );

	if ( $refresh || ! $token || ! $token['token'] || $token['expires'] < time() + 30 ) {
		$sync_id = get_user_meta( $user->ID, 'sync_id', true );
		if ( ! $sync_id ) {
			stm_hybrid_sync_user( $user->ID );
			$sync_id = get_user_meta( $user->ID, 'sync_id', true );
		}

		$res = \Stm_Hybrid\Api::make()->get('users/' . $sync_id . '/token', [], [ 'auth' => stm_hybrid_api_auth() ] );

		if ( ! is_wp_error( $res ) && $res->isOk() ) {
			$token = $res->json();
			$token['expires'] = time() + $token['expires_in'];
			update_user_meta( $user->ID, 'stm_hybrid_token', $token );
		}
	}

	return $token;
}


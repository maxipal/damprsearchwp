<?php
add_action( 'init', function () {
	remove_filter( 'request', 'stm_listings_query_vars' );
} );


add_filter( 'request', 'stm_hybrid_request_filter');

function stm_hybrid_request_filter( $query_vars ) {
	if ( is_admin() ) {
		return $query_vars;
	}

	// For single aircraft we just make query vars to request Sample post
	// Further that Sample post will be filled with post_title, post_name and etc from API
	if ( isset( $query_vars[ stm_listings_post_type() ] ) ) {
        $GLOBALS['stm_hybrid_query'] = [
			'slug' => $query_vars[ stm_listings_post_type() ],
		];

		$query_vars[ stm_listings_post_type() ] = 'sample';
		$query_vars[ 'name' ] = 'sample';

		return $query_vars;
	}

	$is_listing = isset( $query_vars['post_type'] )
	              && in_array( stm_listings_post_type(), (array) $query_vars['post_type'] );

	/*Include search*/
	if ( stm_listings_search_inventory() and ! empty( $_GET['s'] ) ) {
		$is_listing = true;
	}

	if ( isset( $query_vars['pagename'] ) ) {
		$listing_id = stm_listings_user_defined_filter_page();
		if ( $listing_id ) {
			$requested = get_page_by_path( $query_vars['pagename'] );
			if ( ! empty( $requested ) and $is_listing = $listing_id == $requested->ID ) {
				unset( $query_vars['pagename'] );
			}
		}
	}

	global $wp, $landing, $landing_breadcrumbs;
	if ( _stm_hybrid_should_match_request( $wp->request ) ) {
		$res = \Stm_Hybrid\Api::make()->post( 'listings/matches', [ 'path' => $wp->request ] );

		if ( is_wp_error( $res ) ) {
			wp_die('503: Listing service is unavailable', '', array( 'response' => 503 ) );
		}

		if ( $matched = $res->json( 'matches' ) ) {
			$is_listing = true;
			$landing = $matched;
		}


		if ( $breadcrumbs = $res->json('breadcrumbs')) {
		    $landing_breadcrumbs = $breadcrumbs;
	    }
	}

	if ( $is_listing and ! isset( $query_vars['listings'] ) ) {
		$query_vars = [ 'post_type' => stm_listings_post_type() ];
	}

	return $query_vars;
}

add_action( 'wp', function ( WP $wp ) {
	global $wp_query;

	if ( apply_filters( 'stm_hybrid_perform_listings_query', false )
	     && $wp_query->is_post_type_archive( stm_listings_post_type() ) ) {
		$query_params = [
			'filter' => array_filter( stm_hybrid_filter_params() ),
			'sort_order' => stm_listings_input( 'sort_order', 'date_high' ),
			'per_page' => get_theme_mod( 'listing_grid_choice', 9 ),
			'page' => stm_listings_input( 'page', 1 ),
		];

		$listings = \Stm_Hybrid\Listing::query( apply_filters( 'stm_hybrid_listings_query', $query_params ) );
		if ( is_wp_error( $listings ) ) {
			do_action( 'stm_hybrid_inventory_error', $listings );
			wp_die( '503: Listing service is unavailable', '', array( 'response' => 503 ) );
			return;
		}

		$GLOBALS['listings'] = $listings;
		return;
	}

	if ( $wp_query->is_singular( stm_listings_post_type() ) ) {
		$listing = \Stm_Hybrid\Listing::findBySlug( $GLOBALS['stm_hybrid_query']['slug'], [ 'context' => 'single', 'similar' => 1 ] );
		if ( is_wp_error( $listing ) ) {
			if ( WP_DEBUG && WP_DEBUG_DISPLAY ) {
				print '<pre>' . print_r( $listing, 1 ) . '</pre>'; die;
			}
			do_action( 'stm_hybrid_singular_error', $listing );
			wp_die( '503: Listing service is unavailable', '', array( 'response' => 503 ) );
			return;
		}
		$GLOBALS['listing'] = $listing;
		$wp_query->post->ID = $listing->id;
		$wp_query->post->post_title = $listing->title;
		$wp_query->post->post_name = $listing->slug;
		$wp_query->post->post_content = $listing->content;
		$wp_query->post->post_excerpt = $listing->content;
		return;
	}

} );


function _stm_hybrid_should_match_request( $path ) {
	// No need to match path with no sections or empty at all
	$path = trim( $path, '/' );
	if ( $path === '' ) {
		return false;
	}

	// Only GET requests are supported
	if ( $_SERVER['REQUEST_METHOD'] !== 'GET' ) {
		return false;
	}

	// Ignore requests to WP files
	if ( preg_match( '/wp\-(admin|content|includes|json)/', $path ) ) {
		return false;
	}

	// Ignore requests to static files by extension
	if ( preg_match( '/\.(js|css|ico|txt|htm)/', $path ) ) {
		return false;
	}

	$author_base = get_theme_mod( 'author_base', 'author' );
	if ( preg_match( '/^' . preg_quote( $author_base ) . '/', $path ) ) {
		return false;
	}

	if ( $page_for_posts = get_option( 'page_for_posts' ) ) {
		$post = get_post( $page_for_posts );
		if ( preg_match( '/^' . $post->post_name . '/', $path ) ) {
			return false;
		}
	}

	return true;
}

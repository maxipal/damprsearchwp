<?php

add_action( 'user_register', function ( $user_id ) {
	stm_hybrid_sync_user( $user_id );
} );

add_action( 'profile_update', function ( $user_id ) {
	stm_hybrid_sync_user( $user_id );
} );

add_action( 'delete_user', function ( $user_id ) {
	$sync_id = get_user_meta( $user_id, 'sync_id', true );
	if ( ! $sync_id ) {
		return;
	}

	\Stm_Hybrid\Api::make()->delete( 'users/' . $sync_id, [ 'auth' => stm_hybrid_api_auth() ] );
} );

function stm_hybrid_sync_user($user_id ) {
	$user = get_userdata( $user_id );
	if ( ! $user ) {
		return false;
	}

	$location = array_filter( [
		get_user_meta( $user_id, 'stm_dealer_location_lat', true ),
		get_user_meta( $user_id, 'stm_dealer_location_lng', true ),
	] );
	$location = count( $location ) == 2 ? join( ',', $location ) : null;

	$favorites = explode( ',', get_user_meta( $user_id, 'stm_user_favourites', true ) );
	$favorites = array_filter( array_map( 'intval', $favorites ) );

	ob_start();
	stm_display_user_name( $user_id );
	$name = ob_get_clean();

	$user_email = !empty($user->user_email) ? $user->user_email : $user->user_login . '@damprsearch.com';

	$data    = [
		'import_id' => $user->ID,
		'name'  => $name,
		'first_name'  => $user->first_name,
		'last_name'  => $user->last_name,
		'email' => $user_email,
		'roles' => array_values($user->roles),
		'is_dealer' => stm_get_user_role( $user_id ),
		'link'  => str_replace( rtrim( site_url(), '/' ), '', get_author_posts_url( $user_id ) ),
		'avatar' => get_user_meta( $user_id, 'stm_user_avatar', true ),
		'logo' => get_user_meta( $user_id, 'stm_dealer_logo', true ),
		'phone' => get_user_meta( $user_id, 'stm_phone', true ),
		'address' => get_user_meta( $user_id, 'stm_dealer_location', true ),
		'location' => $location,
		'rating' => array_intersect_key( stm_get_dealer_marks( $user_id ), array_flip( [ 'average', 'rate1', 'rate2', 'rate3', 'likes', 'dislikes', 'count' ] )),
		'show_email' => get_user_meta( $user_id, 'stm_show_email', true ) == 'show',
		'created_at' => date( 'Y-m-d\TH:i:s',  strtotime( $user->user_registered ) ),
		'favorites' => $favorites,
	];

	$res = \Stm_Hybrid\Api::make()->post( 'users', $data, [ 'auth' => stm_hybrid_api_auth() ] );

	if ( is_wp_error( $res ) ) {
		stm_hybrid_user_sync_failed( $user, $res );
		return false;
	}

	$result = $res->json( 'data', [] );
	if ( ! isset( $result['id'] ) ) {
		stm_hybrid_user_sync_failed( $user, $res );
		return false;
	}

	update_user_meta( $user_id, 'sync_id', $result['id'] );
	update_user_meta( $user_id, 'synced_at', current_time( 'timestamp' ) );
	delete_user_meta( $user->ID, 'sync_error' );

	return true;
}

function stm_hybrid_user_sync_failed($user, $result ) {
	delete_user_meta( $user->ID, 'synced_at' );
	update_user_meta( $user->ID, 'sync_error', $result );
}



add_action('stm_register_new_user', function($user_id){
	$user = get_user_by('id', $user_id);
	$product_id = 4988;
	stmAddProductToUser($user, $product_id);
});
function stmAddProductToUser($user, $product_id, $coupon_code = null) {
	ini_set('memory_limit', '-1');
	ini_set('max_execution_time', 1800);
	ini_set('display_errors', 0);
	ini_set('display_startup_errors', 0);
	error_reporting(E_ERROR);
	$product = wc_get_product( $product_id );
	$order = new WC_Order();
	$order->set_status('processing');
	$order->set_payment_method('gift');
	$order->set_customer_id($user->ID);
	$order->add_product($product , 1); // This is an existing SIMPLE product
	$order->set_currency( get_woocommerce_currency() );
	$order->set_prices_include_tax( 'yes' === get_option( 'woocommerce_prices_include_tax' ) );
	$order->set_customer_ip_address( WC_Geolocation::get_ip_address() );
	$order->set_customer_user_agent( wc_get_user_agent() );
	$order->set_created_via('checkout');
	$order->set_address([
		'first_name' => $user->first_name,
		'email'      => $user->user_email,
	], 'billing' );
	if($coupon_code) $order->apply_coupon($coupon_code);
	do_action( 'woocommerce_checkout_create_order', $order, [] );
	$order->save();

	do_action( 'woocommerce_checkout_update_order_meta', $order->get_id(), [] );
	update_post_meta($order->get_id(), 'on_register_create', $user->ID);
	WC()->cart->empty_cart();
	WC()->cart->add_to_cart( $product_id );
	if($coupon_code) WC()->cart->add_discount( $coupon_code );
	do_action('woocommerce_new_order', $order->get_id());
	do_action( 'woocommerce_checkout_order_processed', $order->get_id(), [], $order );
	do_action( 'woocommerce_payment_complete', $order->get_id() );
	do_action( 'woocommerce_order_status_processing', $order->get_id() );
	do_action( 'woocommerce_order_status_completed', $order->get_id() );
	$order->set_status('completed');
	$order->save();
	do_action( 'woocommerce_order_status_changed', $order->get_id(), 'processing', 'completed', $order );
	WC()->cart->empty_cart();
}

// Activate subscription and generate access code
add_action('subscriptio_subscription_created_via_checkout', function($subscription, $order){
	if(get_post_meta($order->get_id(), 'on_register_create', true)){
		$old_status = 'pending';
		$new_status = 'active';
		do_action('subscriptio_subscription_status_changing', $subscription, $old_status, $new_status);
		do_action('subscriptio_subscription_status_changing_from_' . $old_status, $subscription, $new_status);
		do_action('subscriptio_subscription_status_changing_to_' . $new_status, $subscription, $old_status);
		do_action('subscriptio_subscription_status_changing_from_' . $old_status . '_to_' . $new_status, $subscription);

		$subscription->get_suborder()->set_status($new_status);
		$subscription->set_status_since(time());
		$subscription->set_status_by('system');
		$subscription->set_previous_status($old_status);
		$subscription->save();
		$subscription->add_log_entry_note(sprintf(__('Subscription status changed from %1$s to %2$s.', 'subscriptio'), 'Pending', 'Active'));

		do_action('subscriptio_subscription_status_changed', $subscription, $old_status, $new_status);
		do_action('subscriptio_subscription_status_changed_from_' . $old_status, $subscription, $new_status);
		do_action('subscriptio_subscription_status_changed_to_' . $new_status, $subscription, $old_status);
		do_action('subscriptio_subscription_status_changed_from_' . $old_status . '_to_' . $new_status, $subscription);
	}
},10,2);

// Disable notifications on buy product of user after registration
add_action( 'woocommerce_email', 'stmDisableSendNotifyEmail' );
function stmDisableSendNotifyEmail( $email_class ) {
	if(!empty($_REQUEST['action']) && $_REQUEST['action'] == 'stm_custom_register'){
		remove_action('woocommerce_email_classes', array('RP_SUB_Mailer', 'register_email_types'));
		add_filter('woocommerce_email_actions', function($actions){
			return false;
		}, 10);
		add_filter( 'woocommerce_email_enabled_new_order', function($yesno, $object){
			return false;
		}, 10, 2);
		add_filter( 'woocommerce_email_enabled_cancelled_order', function($yesno, $object){
			return false;
		}, 10, 2);
		add_filter( 'woocommerce_email_enabled_failed_order', function($yesno, $object){
			return false;
		}, 10, 2);
		add_filter( 'woocommerce_email_enabled_customer_on_hold_order', function($yesno, $object){
			return false;
		}, 10, 2);
		add_filter( 'woocommerce_email_enabled_customer_processing_order', function($yesno, $object){
			return false;
		}, 10, 2);
		add_filter( 'woocommerce_email_enabled_customer_completed_order', function($yesno, $object){
			return false;
		}, 10, 2);
		add_filter( 'woocommerce_email_enabled_customer_refunded_order', function($yesno, $object){
			return false;
		}, 10, 2);
		add_filter( 'woocommerce_email_enabled_customer_invoice', function($yesno, $object){
			return false;
		}, 10, 2);
		add_filter( 'woocommerce_email_enabled_customer_note', function($yesno, $object){
			return false;
		}, 10, 2);
		add_filter( 'woocommerce_email_enabled_customer_reset_password', function($yesno, $object){
			return false;
		}, 10, 2);
		add_filter( 'woocommerce_email_enabled_customer_new_account', function($yesno, $object){
			return false;
		}, 10, 2);

	}
}

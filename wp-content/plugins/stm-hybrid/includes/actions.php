<?php

// Action handlers over aircraft in My inventory
add_action( 'init', function () {
	if ( ! is_user_logged_in() ) {
		return;
	}

	$id     = null;
	$action = null;

	if ( ! empty( $_GET['stm_disable_user_car'] ) ) {
		$id     = $_GET['stm_disable_user_car'];
		$action = 'disable';
	}

	if ( ! empty( $_GET['stm_enable_user_car'] ) ) {
		$id     = $_GET['stm_enable_user_car'];
		$action = 'enable';
	}

	if ( ! empty( $_GET['stm_mark_as_sold_car'] ) ) {
		$id     = $_GET['stm_mark_as_sold_car'];
		$action = 'sold';
	}

	if ( ! empty( $_GET['stm_unmark_as_sold_car'] ) ) {
		$id     = $_GET['stm_unmark_as_sold_car'];
		$action = 'unsold';
	}

	if ( ! empty( $_GET['stm_move_trash_car'] ) ) {
		$id     = $_GET['stm_move_trash_car'];
		$action = 'trash';
	}

	if ( ! $action || ! $id ) {
		return;
	}

	$listing = \Stm_Hybrid\Listing::find( $id );

	if ( ! $listing || $listing->author_id != get_current_user_id() ) {
		return;
	}

	switch ( $action ) {
		case 'disable':
			\Stm_Hybrid\Listing::update( $listing, [ 'status' => 'draft' ] );
			break;
		case 'enable':
			\Stm_Hybrid\Listing::update( $listing, [ 'status' => 'publish' ] );
			break;
		case 'sold':
			\Stm_Hybrid\Listing::update( $listing, [ 'sold' => true ] );
			break;
		case 'unsold':
			\Stm_Hybrid\Listing::update( $listing, [ 'sold' => false ] );
			break;
		case 'trash':
			\Stm_Hybrid\Listing::destroy( $listing );
			break;
	}
} );


// Favourites ajax handler
function stm_hybrid_add_to_favourites() {
	$response = array();

	if ( empty( $_POST['car_id'] ) ) {
		return;
	}

	$car_id      = intval( $_POST['car_id'] );
	$car = listing($car_id);
	$post_status = $car->status;

	if ( ! $post_status ) {
		$post_status = 'deleted';
	}

	if ( is_user_logged_in() and $post_status == 'publish') {
		$user           = wp_get_current_user();
		$user_id        = $user->ID;
		$user_added_fav = get_the_author_meta( 'stm_user_favourites', $user_id );
		if ( empty( $user_added_fav ) ) {
			update_user_meta( $user_id, 'stm_user_favourites', $car_id );
		} else {
			$user_added_fav = array_filter( explode( ',', $user_added_fav ) );
			if ( in_array( strval( $car_id ), $user_added_fav ) ) {
				$user_added_fav = array_diff( $user_added_fav, array( $car_id ) );
			} else {
				$user_added_fav[] = $car_id;
			}
			$user_added_fav = implode( ',', $user_added_fav );

			update_user_meta( $user_id, 'stm_user_favourites', $user_added_fav );
		}

		//$user_added_fav = get_the_author_meta('stm_user_favourites', $user_id);
		$user_added_fav    = array_filter( explode( ',', $user_added_fav ) );
		$response['fil']   = $user_added_fav;
		$response['id']    = $car_id;
		$response['count'] = count( $user_added_fav );
		stm_hybrid_sync_user( $user_id );
	}

	$response = json_encode( $response );
	echo $response;
	exit;
}

add_action( 'init', function () {
	remove_action( 'wp_ajax_stm_ajax_add_to_favourites', 'stm_ajax_add_to_favourites' );
	remove_action( 'wp_ajax_nopriv_stm_ajax_add_to_favourites', 'stm_ajax_add_to_favourites' );
	add_action( 'wp_ajax_stm_ajax_add_to_favourites', 'stm_hybrid_add_to_favourites');
	add_action( 'wp_ajax_nopriv_stm_ajax_add_to_favourites', 'stm_hybrid_add_to_favourites');
} );

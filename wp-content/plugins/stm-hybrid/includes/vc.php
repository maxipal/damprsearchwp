<?php

add_filter( 'vc_shortcode_set_template_stm_classic_filter', function () {
	return stm_hybrid_locate_template( 'vc_templates/stm_classic_filter' );
} );

add_filter( 'vc_shortcode_set_template_stm_add_a_car', function () {
	return stm_hybrid_locate_template( 'vc_templates/stm_add_a_car' );
} );

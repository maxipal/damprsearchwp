<?php

add_action( 'init', function () {
	if ( ! isset( $_REQUEST['laravel-notification'] ) ) {
		return;
	}

	$event = $_REQUEST['laravel-notification'];

	timer_start();
	stm_logging_notification(print_r($_REQUEST, 1));

	do_action( 'webhook-' . $event );
	exit;
} );

add_action( 'webhook-listing-created', 'stm_hybrid_flush_caches', 100 );
add_action( 'webhook-listing-updated', 'stm_hybrid_flush_caches', 100 );
add_action( 'webhook-listing-deleted', 'stm_hybrid_flush_caches', 100 );

/**
 * Flush caches where listings may appear
 */
function stm_hybrid_flush_caches() {
	if ( function_exists( 'w3tc_flush_url' ) ) {
		$base_url = site_url();

		$request_args = array(
			'timeout'		=> 60,
		);

		// Front page
		// $front_id = get_option( 'page_on_front' );
		// w3tc_flush_post( $front_id );
		// Prime caching
		// stm_preload_page( get_permalink( $front_id ) );

		// Listing archive
		$listing_id = get_theme_mod('listing_archive', false);
		if($listing_id){
			$listing_url = get_permalink( $listing_id );
			w3tc_flush_url( $listing_url );
			// Prime caching
			stm_preload_page( $listing_url, $request_args );
		}

		stm_logging_notification("Time Inventory");

		// Relevant pages
		if(!empty($_REQUEST['relevants']) && is_array($_REQUEST['relevants'])){

			foreach ($_REQUEST['relevants'] as $relevant) {
				$url = "$base_url/$relevant";
				w3tc_flush_url( $url );
				// Prime caching
				stm_preload_page( $url, $request_args );
			}
		}

		stm_logging_notification("Time Relevant");

		// Single inventory
		w3tc_flush_url( "$base_url/".$_REQUEST['listing']['link'] );
		// Prime caching
		stm_preload_page( "$base_url/".$_REQUEST['listing']['link'], $request_args );
	}


}

add_action( 'webhook-listing-attribute-created', 'stm_hybrid_flush_api_caches', 100 );
add_action( 'webhook-listing-attribute-updated', 'stm_hybrid_flush_api_caches', 100 );
add_action( 'webhook-listing-attribute-deleted', 'stm_hybrid_flush_api_caches', 100 );

/**
 * Flush caches for listing create form
 */
function stm_hybrid_flush_api_caches() {
	if ( function_exists( 'w3tc_flush_post' ) ) {
		w3tc_flush_post( get_theme_mod( 'user_add_car_page' ) );
	}

	delete_transient( stm_hybrid_api_transient_key( 'listings/create?context=front' ) );
}


function stm_preload_page($url, $args){
	$args['user-agent'] = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36';
	wp_remote_get( $url, $args );
	$args['user-agent'] = 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1';
	wp_remote_get( $url, $args );
}

function stm_logging_notification($text, $clear = false){
	if (!WP_DEBUG_LOG) return;
	file_put_contents(WP_CONTENT_DIR . '/laravel-notification.log', timer_stop( 0 ) . ": " . $text . "\n\n", ($clear ? null : FILE_APPEND));
}

<?php

namespace Stm_Hybrid;

class Model implements \ArrayAccess, \Iterator, \JsonSerializable {

	/**
	 * All of the attributes set on the container.
	 *
	 * @var array
	 */
	protected $attributes = [];

	/**
	 * Create a new fluent container instance.
	 *
	 * @param  array|object $attributes
	 *
	 * @return void
	 */
	public function __construct( $attributes = [] ) {
		if ( is_object( $attributes ) ) {
			$attributes = (array) $attributes;
		}

		foreach ( $attributes as $key => $value ) {
			$this->attributes[ $key ] = $value;
		}
	}

	/**
	 * Get an attribute from the container.
	 *
	 * @param  string $key
	 * @param  mixed $default
	 *
	 * @return mixed
	 */
	public function get( $key, $default = null ) {
		if ( array_key_exists( $key, $this->attributes ) ) {
			return $this->attributes[ $key ];
		}

		return $default;
	}

	/**
	 * Get the attributes from the container.
	 *
	 * @return array
	 */
	public function getAttributes() {
		return $this->attributes;
	}

	/**
	 * Convert the Fluent instance to an array.
	 *
	 * @return array
	 */
	public function toArray() {
		return $this->attributes;
	}

	/**
	 * Convert the object into something JSON serializable.
	 *
	 * @return array
	 */
	public function jsonSerialize() {
		return $this->toArray();
	}

	/**
	 * Convert the Fluent instance to JSON.
	 *
	 * @param  int $options
	 *
	 * @return string
	 */
	public function toJson( $options = 0 ) {
		return json_encode( $this->jsonSerialize(), $options );
	}

	/**
	 * Determine if the given offset exists.
	 *
	 * @param  string $offset
	 *
	 * @return bool
	 */
	public function offsetExists( $offset ) {
		return isset( $this->{$offset} );
	}

	/**
	 * Get the value for a given offset.
	 *
	 * @param  string $offset
	 *
	 * @return mixed
	 */
	public function offsetGet( $offset ) {
		return $this->{$offset};
	}

	/**
	 * Set the value at the given offset.
	 *
	 * @param  string $offset
	 * @param  mixed $value
	 *
	 * @return void
	 */
	public function offsetSet( $offset, $value ) {
		$this->{$offset} = $value;
	}

	/**
	 * Unset the value at the given offset.
	 *
	 * @param  string $offset
	 *
	 * @return void
	 */
	public function offsetUnset( $offset ) {
		unset( $this->{$offset} );
	}

	/**
	 * Dynamically retrieve the value of an attribute.
	 *
	 * @param  string $key
	 *
	 * @return mixed
	 */
	public function __get( $key ) {
		return $this->get( $key );
	}

	/**
	 * Dynamically set the value of an attribute.
	 *
	 * @param  string $key
	 * @param  mixed $value
	 *
	 * @return void
	 */
	public function __set( $key, $value ) {
		$this->attributes[ $key ] = $value;
	}

	/**
	 * Dynamically check if an attribute is set.
	 *
	 * @param  string $key
	 *
	 * @return bool
	 */
	public function __isset( $key ) {
		return isset( $this->attributes[ $key ] );
	}

	/**
	 * Dynamically unset an attribute.
	 *
	 * @param  string $key
	 *
	 * @return void
	 */
	public function __unset( $key ) {
		unset( $this->attributes[ $key ] );
	}


	/**
	 * Static method for new instance creation
	 *
	 * @param array $attributes
	 *
	 * @return static
	 */
	public static function make( $attributes = array() ) {
		return new static( $attributes );
	}

	/**
	 * Return the current element
	 * @link http://php.net/manual/en/iterator.current.php
	 * @return mixed Can return any type.
	 * @since 5.0.0
	 */
	public function current() {
		return current($this->attributes);
	}

	/**
	 * Move forward to next element
	 * @link http://php.net/manual/en/iterator.next.php
	 * @return void Any returned value is ignored.
	 * @since 5.0.0
	 */
	public function next() {
		next($this->attributes);
	}

	/**
	 * Return the key of the current element
	 * @link http://php.net/manual/en/iterator.key.php
	 * @return mixed scalar on success, or null on failure.
	 * @since 5.0.0
	 */
	public function key() {
		return key($this->attributes);
	}

	/**
	 * Checks if current position is valid
	 * @link http://php.net/manual/en/iterator.valid.php
	 * @return boolean The return value will be casted to boolean and then evaluated.
	 * Returns true on success or false on failure.
	 * @since 5.0.0
	 */
	public function valid() {
		return array_key_exists($this->key(), $this->attributes);
	}

	/**
	 * Rewind the Iterator to the first element
	 * @link http://php.net/manual/en/iterator.rewind.php
	 * @return void Any returned value is ignored.
	 * @since 5.0.0
	 */
	public function rewind() {
		reset($this->attributes);
	}
}

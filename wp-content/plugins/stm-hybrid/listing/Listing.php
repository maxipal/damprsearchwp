<?php

namespace Stm_Hybrid;

use WP_Error;

class Listing extends Model {

	public function __construct( $attributes = [] ) {
		parent::__construct( $attributes );
		if ( $this->get( 'attributes' ) ) {
			$this->attributes['attributes'] = array_map( function ( $item ) {
				return (object) $item;
			}, $this->get( 'attributes' ) );
		}
	}

	public function link() {
		$listing_base_url = stm_get_listings_base_url();

		return site_url( $listing_base_url . '/' . $this->slug . '/' );
	}

	public function image() {
		return count( $this->gallery ) ? $this->gallery[0] : null;
	}

	public function sortValue( $key ) {
		return isset( $this->_sort[ $key ] ) ? $this->_sort[ $key ] : null;
	}

	public static function query( $request = [], $options = [] ) {
		$request_time = microtime( true );

		$options += [
			'uri' => 'listings',
		];

		$response = Api::make()->get( $options['uri'], $request, $options );

		if ( is_wp_error( $response ) ) {
			return $response;
		}

		if ( ! $response->isOk() ) {
			return new WP_Error( $response->get_status(), $response->json( 'message', 'Unknown error' ), $response );
		}

		$return = $response->json();

		if ( isset( $return['data'] ) ) {
			$return['data'] = self::makeCollection( $return['data'] );
		}

		$return['request_time'] = microtime( true ) - $request_time;

		return $return;
	}

	/**
	 * Create new collection from raw results
	 *
	 * @param $results
	 *
	 * @return array
	 */
	public static function makeCollection( $results ) {
		return array_map( function ( $raw ) {
			return new self( $raw );
		}, $results );
	}

	/**
	 * @param Response|WP_Error $response
	 *
	 * @return Listing|WP_Error
	 */
	public static function fromResponse( $response ) {
		if ( is_wp_error( $response ) ) {
			return $response;
		}

		if ( ! $response->isOk() ) {
			return new WP_Error( $response->get_status(), $response->json( 'message', 'Unknown error' ), $response );
		}

		$listing = new self( $response->json( 'data' ) );

		if ( $similar = $response->json( 'similar' ) ) {
			$listing->similar = self::makeCollection( $similar );
		}
        if ( $seo = $response->json( 'seo' ) ) {
            $listing->seo = $seo;
        }

		return $listing;
	}

	public static function find( $id ) {
		$response = Api::make()->get( 'listings/' . $id );

		return self::fromResponse( $response );
	}

	public static function findBySlug( $slug, $query = [] ) {
		$query = $query ? '?' . http_build_query( $query ) : '';

		$response = Api::make()->get( 'listings/slug/' . $slug . $query );

		return self::fromResponse( $response );
	}

	/**
	 * @param array $args
	 *
	 * @return Attribute[]
	 */
	public static function attributes( $args = [] ) {
		if ( false === ( $attributes = get_transient( 'aircraft_attributes' ) ) ) {
			$response   = Api::make()->get( 'listings/attributes' );
			$attributes = $response->json();
			set_transient( 'aircraft_attributes', $attributes, 1 );
		}

		if ( $args ) {
			$attributes = wp_list_filter( $attributes, $args );
		}

		$attributes = array_map( function ( $item ) {
			return new Attribute( $item );
		}, $attributes );

		return $attributes;
	}

	public static function update( $aircraft, $data ) {
		$aircraft = $aircraft instanceof Listing ? $aircraft->id : $aircraft;

		$res = Api::make()->put( 'listings/' . $aircraft, $data, [
			'auth' => stm_hybrid_api_auth(),
		] );

		return self::fromResponse( $res );
	}

	public static function destroy( $aircraft ) {
		$aircraft = $aircraft instanceof Listing ? $aircraft->id : $aircraft;

		$res = Api::make()->delete( 'listings/' . $aircraft, [
			'auth' => stm_hybrid_api_auth(),
		] );

		return $res->isOk();
	}
}

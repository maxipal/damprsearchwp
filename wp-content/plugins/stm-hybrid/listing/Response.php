<?php

namespace Stm_Hybrid;


class Response extends \WP_HTTP_Requests_Response {

	public function isOk() {
		return $this->get_status() >= 200 && $this->get_status() < 300;
	}

	public function json( $key = null, $default = null ) {
		$json = json_decode( $this->get_data(), true );
		if ( is_null( $key ) ) {
			return $json;
		} elseif ( array_key_exists( $key, $json ) ) {
			return $json[ $key ];
		} else {
			return $default;
		}
	}
}

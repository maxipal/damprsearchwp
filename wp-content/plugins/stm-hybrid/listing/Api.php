<?php

namespace Stm_Hybrid;

class Api {

	public $request;

	/** @var Response */
	public $response;

	protected $baseUrl;

	public function __construct() {
		$this->baseUrl = stm_hybrid_api_url();
	}

	/**
	 * Get a new Api instance
	 *
	 * @return Api
	 */
	public static function make() {
		return new Api();
	}

	/**
	 * Perform a request
	 *
	 * @param string $method
	 * @param string $uri
	 * @param mixed $data
	 * @param array $options
	 *
	 * @return array|Response|\WP_Error
	 */
	public function request( $method, $uri, $data = null, $options = [] ) {
		$options += [
			'method'    => strtoupper( $method ),
			'sslverify' => false,
			'timeout'   => 30,
			'headers'   => [],
			'auth'      => null,
			'body'      => ! is_null( $data ) ? json_encode( $data ) : null,
		];

		$options['headers'] += [
			'Accept'       => 'application/json',
			'Content-Type' => 'application/json',
			'Referer'      => $_SERVER['REQUEST_URI'],
		];

		// Basic auth
		if ( is_array( $options['auth'] ) ) {
			$options['headers']['Authorization'] = 'Basic ' . base64_encode( join( ':', $options['auth'] ) );
		}

		// Token auth
		if ( is_string( $options['auth'] ) ) {
			$options['headers']['Authorization'] = 'Bearer ' . $options['auth'];
		}

		if ( $options['auth'] instanceof \WP_User ) {
			$token = stm_hybrid_user_token( $options['auth'] );
			$options['headers']['Authorization'] = 'Bearer ' . $token['token'];
		}

		$this->request = $options;

		$url = $this->baseUrl . ltrim( $uri, '/' );
		$url = add_query_arg( 'nocache', '1', $url );

		$res = _wp_http_get_object()->request( $url, $options );

		if ( is_wp_error( $res ) ) {
			return $res;
		}

		/** @var \WP_HTTP_Requests_Response $resObject */
		$resObject = $res['http_response'];
		$this->response = new Response( $resObject->get_response_object(), $res['filename'] );

		if ( ! $this->isSuccess( $res ) ) {
			if ( $this->isCritical( $res ) ) {
				//wp_mail( 'dev@stylemix.net', 'Listing api request error', 'Request: ' . $url . "\n" . print_r( $args, true ) . "\n\nResponse:" . print_r( $res, true ) );
			}
		}

		return $this->response;
	}

	/**
	 * Perform a GET request
	 *
	 * @param string $uri
	 * @param array $query
	 * @param array $options
	 *
	 * @return array|Response|\WP_Error
	 */
	public function get( $uri, $query = [], $options = [] ) {
		$uri .= rtrim( '?' . http_build_query( $query ), '?' );
		return $this->request( 'GET', $uri, null, $options );
	}

	/**
	 * Perform a POST request
	 *
	 * @param string $uri
	 * @param mixed $data
	 * @param array $options
	 *
	 * @return array|Response|\WP_Error
	 */
	public function post( $uri, $data, $options = [] ) {
		return $this->request( 'POST', $uri, $data, $options );
	}

	/**
	 * Perform a PUT request
	 *
	 * @param string $uri
	 * @param mixed $data
	 * @param array $options
	 *
	 * @return array|Response|\WP_Error
	 */
	public function put( $uri, $data, $options = [] ) {
		return $this->request( 'PUT', $uri, $data, $options );
	}

	/**
	 * Perform a DELETE request
	 *
	 * @param string $uri
	 * @param array $options
	 *
	 * @return array|Response|\WP_Error
	 */
	public function delete( $uri, $options = [] ) {
		return $this->request( 'DELETE', $uri, null, $options );
	}

	protected function isSuccess( $response ) {
		if ( is_wp_error( $response ) || ! is_array( $response ) )
			return false;

		return $response['response']['code'] >= 200 && $response['response']['code'] < 300;
	}


	protected function isCritical( $response ) {
		if ( is_wp_error( $response ) || ! is_array( $response ) )
			return true;

		return $response['response']['code'] >= 500;
	}

}

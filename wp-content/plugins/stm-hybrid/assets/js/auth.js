(function ($) {
	"use strict";

	function refreshToken(after) {
		setTimeout(function () {
			$.ajax({
				url: '?stm-hybrid-refresh-token=1',
				dataType: 'json',
				success: function (data) {
					stm_hybrid_auth.token = data.token;
                    stm_hybrid_auth.expires_in = data.expires_in;
					refreshToken(data.expires_in * 1000);
				}
			});
		}, Math.max(0, after - 5000));
	}

	refreshToken( stm_hybrid_auth.expires_in * 1000);

})(jQuery);

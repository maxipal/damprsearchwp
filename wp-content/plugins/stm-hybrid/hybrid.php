<?php
/**
 * Plugin Name: STM Hybrid Listing
 * Description: Integration with listing api.
 * Version: 1.0
 */

define( 'STM_HYBRID_VERSION', '1.0' );
define( 'STM_HYBRID_PATH', dirname( __FILE__ ) );
define( 'STM_HYBRID_URL', plugins_url( '', __FILE__ ) );

require_once STM_HYBRID_PATH . '/includes/helpers.php';
require_once STM_HYBRID_PATH . '/includes/query.php';
require_once STM_HYBRID_PATH . '/includes/actions.php';
require_once STM_HYBRID_PATH . '/includes/webhooks.php';
require_once STM_HYBRID_PATH . '/includes/user.php';
require_once STM_HYBRID_PATH . '/includes/auth.php';
require_once STM_HYBRID_PATH . '/includes/template.php';
require_once STM_HYBRID_PATH . '/includes/vc.php';
require_once STM_HYBRID_PATH . '/includes/admin.php';
require_once STM_HYBRID_PATH . '/includes/disabling.php';

require_once STM_HYBRID_PATH . '/listing/Api.php';
require_once STM_HYBRID_PATH . '/listing/Response.php';
require_once STM_HYBRID_PATH . '/listing/Model.php';
require_once STM_HYBRID_PATH . '/listing/Attribute.php';
require_once STM_HYBRID_PATH . '/listing/Filter.php';
require_once STM_HYBRID_PATH . '/listing/Listing.php';

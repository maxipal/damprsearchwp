#### API

GET request:
```php
$res = \Stm_Hybrid\Api::make()->get( 'api/aircraft', $query = [] );
```

POST request:
```php
$res = \Stm_Hybrid\Api::make()->post( 'api/aircraft', $post_data = [] );
```

#### Hybrid class

Search for aircraft:
```php
$list = \Stm_Hybrid\Hybrid::query( [ 'page' => 2 ] );
```

Get list of featured aircraft
```php
$res = \Stm_Hybrid\Hybrid::query( [
    'where' => ['is_special' => true],
    'sort' => ['created_at' => 'desc']
] );
```

#### Helpers
listings() - Global listings request results
```php
foreach ( listings() as $item ) {
    echo $item->title;
}
```
the_listing( $listing ) - Set globally a single listing model
```php
foreach ( listings() as $item ) {
    the_listing( $item );
    stm_hybrid_include( 'loop/item' );
}

// In loop/item.php
echo listing()->title;
```

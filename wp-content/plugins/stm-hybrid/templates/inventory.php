<?php
$sidebar_pos = stm_get_sidebar_position();
$sidebar_id = get_theme_mod('listing_sidebar', 'primary_sidebar');
if( !empty($sidebar_id) ) {
	$blog_sidebar = get_post( $sidebar_id );
}

if($sidebar_id == 'no_sidebar') {
	$sidebar_id = false;
}
?>
<div class="archive-listing-page">

	<div class="container">

		<div class="row">

			<div class="col-md-3 col-sm-12 classic-filter-row sidebar-sm-mg-bt ">
				<?php stm_hybrid_include('inventory/sidebar'); ?>
			</div>

			<div class="col-md-9 col-sm-12 <?php echo $sidebar_pos['content'] ?>">
				<div class="stm-ajax-row">
					<?php //stm_listings_load_template('classified/filter/actions', array('filter' => $filter)); ?>
					<div id="listings-result">
						<?php stm_hybrid_include( 'results' ); ?>
					</div>
				</div>

			</div>

		</div>

	</div>


</div>


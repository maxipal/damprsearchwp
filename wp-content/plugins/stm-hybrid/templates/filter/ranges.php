<div class="col-md-12 col-sm-6 stm-filter_<?php echo esc_attr( $filter->key ) ?>">
	<div class="form-group">
		<select key="<?php echo esc_attr( $filter->key ) ?>"
				name="filter[<?php echo esc_attr( $filter->key ) . '][lte]';  ?>"
				class="form-control">
			<option value=""><?php echo __( 'Max', 'motors' ) . ' ' . esc_html( $filter['label'] ); ?></option>
			<?php foreach ( $filter->options['ranges'] as $number ) : ?>
				<option value="<?php echo esc_attr( $number ) ?>">
					<?php echo $filter->options['prefix'] . esc_html( $number ); ?>
				</option>
			<?php endforeach; ?>
		</select>
	</div>
</div>

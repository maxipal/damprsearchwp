<div class="col-md-12 col-sm-6 stm-filter_<?php echo esc_attr( $filter->key ) ?>">
	<div class="<?php echo $filter->multiple ? 'stm-multiple-select' : 'form-group' ?>">
		<select key="<?php echo esc_attr( $filter->key ) ?>"
				name="filter[<?php echo esc_attr( $filter->key ) . ']' . ($filter->multiple ? '[]' : '');  ?>"
				<?php echo $filter->multiple ? 'multiple' : '' ?> class="form-control">
			<?php foreach ( $filter->options as $value => $option ) : ?>
				<option value="<?php echo esc_attr( $option['value'] ) ?>"
						class="<?php echo esc_attr( $option['class'] ) ?>"
					<?php selected( $option['selected'] ) ?>
					<?php disabled( $option['disabled'] ) ?>>
					<?php echo esc_html( $option['label'] ); ?>
				</option>
			<?php endforeach; ?>
			<?php if ( $filter->value ) { ?>
				<option value="<?php echo esc_attr( $filter->value ) ?>" selected></option>
			<?php } ?>
		</select>
	</div>
</div>


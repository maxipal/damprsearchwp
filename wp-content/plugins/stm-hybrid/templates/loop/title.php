<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
?>

<div class="title">
    <a href="<?php echo listing()->link() ?>" class="rmv_txt_drctn">
        <?php echo listing()->title; ?>
    </a>
</div>

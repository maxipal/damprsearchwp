<div class="image">
	<a href="<?php //the_permalink() ?>" class="rmv_txt_drctn">
		<div class="image-inner">
			<?php if ( $image = listing()->image() ) { ?>
				<img src="<?php echo $image['url'] ?>" class="img-responsive">
			<?php } ?>
		</div>
	</a>
</div>

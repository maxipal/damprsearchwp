(function($) {

  $(document).ready(function() {
    // $('.filter.stm-vc-ajax-filter select:not(.hide)').off('change');

    // $('.filter.stm-vc-ajax-filter form').get(0).reset();

    $('#stm-car-location-stm_all_listing_tab').attr('placeholder', 'Zip Code');

    $('select[name=make]').change(function() {
      $('select[name=serie]').val('');
    });
  });
  /*  function get_bind_tax(){
          var dependencies = [];
          if(!$.isEmptyObject(stm_bind_taxes)){
              $.each(stm_bind_taxes, function(el, opt){
                  var {dependency} = opt;

                  if(dependency && !dependencies.includes(dependency)){
                      dependencies.push(dependency);
                  }
              });
          }
          return dependencies;
      }*/
  STMListings.Filter.prototype.disableOptions = function(res) {
    var form = this.form;

    if (typeof res.options != 'undefined') {
      $.each(res.options, function(key, options) {
        var value = $('select[name=' + key + ']', form).val();
        var {listing_taxonomy_parent, children} = res.filters[key];

        if ($('select[name=' + children + ']', form).val()) {
          return;
        }
        if (listing_taxonomy_parent) {
          if ($('select[name=' + listing_taxonomy_parent + ']', form).val() ===
              '') {

            $('select[name=' + key + '] > option', form).each(function() {
              var slug = $(this).val();
              $(this).prop('disabled', false);

            });

          } else {
            $('select[name=' + key + '] > option', form).each(function() {
              var slug = $(this).val();
              if (options.hasOwnProperty(slug) && value !== slug) {
                $(this).prop('disabled', options[slug].disabled);
              }
            });
          }
        }
      });
    }

    $('select', form).select2('destroy');
    $('select', form).select2();

    $('.stm-select-sorting select').select2('destroy');
    $('.stm-select-sorting select').html('');
    $('.stm-select-sorting select').select2({
      data: res.sorts,
    });

    $.each(res.filter_links, function(key, link) {
      $.each(res.options[link['slug']], function(key, linkOption) {
        if (key)
          $('#stm-filter-link-' + link['slug'] + ' li[data-value="' + key +
              '"] span').text('(' + linkOption['count'] + ')');
      });
    });

  };

  //stm_init_prices_carousel();

  function stm_init_prices_carousel() {
    let $owl = $('.stm-pricing__tables-row');
    if ($(window).width() > 767) {
      $owl.trigger('refresh.owl.carousel')
      return;
    }

    $owl.owlCarousel({
      smartSpeed: 800,
      dots: true,
      margin: 0,
      autoplay: false,
			loop: false,
      nav: true,
      navText: [
        '<i class=\'fa fa-chevron-left\'></i>',
        '<i class=\'fa fa-chevron-right\'></i>',
      ],
      responsive: {
        0: {
          items: 1,
        },
        500: {
          items: 1,
        },
        767: {
          items: 2,
        },
      },
    });


  }

}(jQuery));

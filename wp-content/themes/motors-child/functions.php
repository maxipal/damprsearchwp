<?php
define('LISTING_API_URL', 'http://listing.damprsearch.loc/api');
define('LISTING_URL', 'http://listing.damprsearch.loc/');
define('LISTING_ENABLE_AUCTION', false);
define('TRADE_IN_PAGE', false);
define('TESTDRIVE_PAGE', false);

require_once 'inc/hybrid.php';

require_once 'inc/modifications-options/modifications.php';
require_once 'inc/customizer.php';
require_once 'inc/inventory-filters.php';
require_once 'inc/custom.php';
include_once 'inc/widgets/car-contact-form.php';

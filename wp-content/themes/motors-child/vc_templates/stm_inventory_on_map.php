<?php

$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);

$map_style = [
	'width' => ' width: 100%;',
	'height' => ' height: 100%;'
];
$id = rand();

wp_enqueue_script( 'map-view-osm' );
wp_enqueue_script( 'MarkerCluster' );
// wp_enqueue_script('leaflet');
// wp_enqueue_script('osm_leaflet');

?>

	<div class="stm-inventory-map-wrap">
		<div <?php echo(($map_style) ? 'style="' . esc_attr(implode(' ', $map_style)) . ' margin: 0 auto; "' : ''); ?>
			data-trigger="map" id="stm_gmap" class="stm_gmap"></div>
		<div class="stm-inventory-map-filter-arrow-wrap">
			<div class="stm-filter-arrow stm-map-filter-open"></div>
			<div class="stm-inventory-map-filter-wrap">
				<div class="stm-filter-scrollbar">
					<form action="<?php echo stm_listings_current_url() ?>" method="get" data-trigger="inventory-map">
						<div class="filter filter-sidebar ajax-filter">
							<?php do_action('stm_listings_filter_before'); ?>

							<div class="sidebar-entry-header">
								<i class="stm-icon-car_search"></i>
								<span class="h4"><?php _e('Search Options', 'motors'); ?></span>
							</div>

							<div class="row row-pad-top-24 slide-sidebar side-filters 65656565">
								<?php
								$filters = stm_hybrid_filters('map');
								foreach ($filters as $key => $filter ) {
									if($filter->key == 'towns' || $filter->key == 'countries'){
										unset($filters[$key]);
										continue;
									}
									if($filter['type'])	stm_hybrid_include( 'filter/' . $filter['type'], compact( 'filter' ) );
								} ?>
							</div>

							<!--View type-->
							<input type="hidden" id="stm_view_type" name="view_type"
								   value="<?php echo esc_attr(stm_listings_input('view_type')); ?>"/>
							<!--Filter links-->
							<input type="hidden" id="stm-filter-links-input" name="stm_filter_link" value=""/>
							<!--Popular-->
							<input type="hidden" name="popular" value="<?php echo esc_attr(stm_listings_input('popular')); ?>"/>

							<input type="hidden" name="s" value="<?php echo esc_attr(stm_listings_input('s')); ?>"/>
							<input type="hidden" name="sort_order" value="<?php echo esc_attr(stm_listings_input('sort_order')); ?>"/>

							<div class="sidebar-action-units">
								<input id="stm-classic-filter-submit" class="hidden" type="submit"
									   value="<?php _e('Show cars', 'motors'); ?>"/>

								<a href="<?php echo esc_url(get_permalink()); ?>"
								   class="button"><span><?php _e('Reset all', 'motors'); ?></span></a>
							</div>

							<?php do_action('stm_listings_filter_after'); ?>
						</div>
					</form>
				</div>
				<div class="stm-inventory-map-btn">
					<div class="stm-inventory-map-cars-count" data-sprint="<?php echo esc_html__('%s matches', 'motors')?>"><?php echo sprintf(esc_html__('%s matches', 'motors'), '<span rel="count"></span>');?></div>
					<input class="button" type="submit" value="<?php echo esc_html__('Apply', 'motors');?>" />
				</div>
			</div>
		</div>
	</div>


	<script type="text/javascript">
		jQuery("body").addClass("stm-inventory-map-body");
		jQuery(document).ready(function ($) {
			$('.slide-sidebar').fadeIn('slow');

			var mapHeight = ((parseInt($(window).height()) - parseInt($("#top-bar").height())) - parseInt($("#header").height())) - parseInt($("#footer").height());

			if (mapHeight > 400) {
				$(".stm-inventory-map-wrap").height(mapHeight);
				$(".stm-filter-scrollbar").height(mapHeight - $(".stm-inventory-map-btn").outerHeight() + 2);
			}

			$(".stm-filter-scrollbar").mCustomScrollbar({
				theme:"dark"
			});

			if(stm_check_mobile()) {
				$(".stm-filter-scrollbar").mCustomScrollbar('destroy');
			}

			$(window).resize(function(){
				if(typeof map != 'undefined' && typeof center != 'undefined') {
					setTimeout(function () {
						map.setCenter(center);
					}, 1000);
				}
			});

			var $form = $("form[data-trigger=filter-map]");

			$('#ca_location_listing_filter').on('keydown', function() {
				$("form[data-trigger=filter-map]").submit(function (e) { e.preventDefault(); });
				buildUrl();
			});

			$(".stm-filter-arrow").on("click", function () {
				setTimeout(function () {
					google.maps.event.trigger(map, "resize");
				}, 400);
				if($(this).hasClass("stm-map-filter-open")) {
					$(this).removeClass("stm-map-filter-open").addClass("stm-map-filter-close");
				} else {
					$(this).removeClass("stm-map-filter-close").addClass("stm-map-filter-open");
				}
			});

			new STMCascadingSelect($form, stm_bindings);


		});




		function buildUrl() {
			var data = [],
				url = $("form[data-trigger=inventory-map]").attr('action'),
				sign = url.indexOf('?') < 0 ? '?' : '&';

			$.each($("form[data-trigger=filter-map]").serializeArray(), function (i, field) {
				if (field.value != '') {
					data.push(field.name + '=' + field.value)
				}
			});

			url = url + sign + data.join('&');
			window.history.pushState('', '', decodeURI(url));
		}




	</script>
<?php
function invMapScript()
{
	?>
	<script type="text/javascript">
		<?php echo 'var stm_bindings = ', json_encode(stm_data_binding(true)), ';' ?>
	</script>
	<?php
}
add_action('wp_footer', 'invMapScript');
?>

<?php
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class($css, ' '));
?>

<div class="<?php echo esc_attr($css_class); ?>">
	<?php if ( trim( $atts['title'] ) ) { ?>
		<div class="stm-border-bottom-unit"><div class="title heading-font"><?php echo $atts['title'] ?></div></div>
	<?php } ?>
	<?php get_template_part( 'partials/news/related-aircrafts' ); ?>
</div>

<?php
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
$vc_content = 'vc_content';
$atts += [$vc_content=>$content];
extract($atts);

$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class($css, ' '));

wp_enqueue_script('vue-icon_filter');
stm_hybrid_include( 'vc_templates/vue-icon_filter', $atts);


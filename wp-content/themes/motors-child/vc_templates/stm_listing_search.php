<?php
wp_enqueue_script('search-box');
$filters = stm_hybrid_filters('frontpage');
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);
$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class($css, ' '));

if (isset($atts['items']) && strlen($atts['items']) > 0) {
    $items = vc_param_group_parse_atts($atts['items']);
    if (!is_array($items)) {
        $temp = explode(',', $atts['items']);
        $paramValues = array();
        foreach ($temp as $value) {
            $data = explode('|', $value);
            $newLine = array();
            $newLine['title'] = isset($data[0]) ? $data[0] : 0;
            $newLine['sub_title'] = isset($data[1]) ? $data[1] : '';
            if (isset($data[1]) && preg_match('/^\d{1,3}\%$/', $data[1])) {
                $colorIndex += 1;
                $newLine['title'] = (float)str_replace('%', '', $data[1]);
                $newLine['sub_title'] = isset($data[2]) ? $data[2] : '';
            }
            $paramValues[] = $newLine;
        }
        $atts['items'] = urlencode(json_encode($paramValues));
    }
}

$active_taxonomy_tab = true;
$active_taxonomy_tab_active = 'active';
$active_taxonomy_tab_content = 'in active';

if (!empty($show_all) and $show_all == 'yes') {
    $active_taxonomy_tab = false;
    $active_taxonomy_tab_active = '';
    $active_taxonomy_tab_content = '';
}

if (empty($filter_all)) {
    $active_taxonomy_tab = true;
    $active_taxonomy_tab_active = 'active';
}

if (empty($show_amount)) {
    $show_amount = 'no';
}

$words = array();

if (!empty($select_prefix)) {
    $words['select_prefix'] = $select_prefix;
}

if (!empty($select_affix)) {
    $words['select_affix'] = $select_affix;
}

if (!empty($number_prefix)) {
    $words['number_prefix'] = $number_prefix;
}

if (!empty($number_affix)) {
    $words['number_affix'] = $number_affix;
}

$distance_search = get_theme_mod( 'distance_search', 10000 );


$disabled_tabs = !empty($disable_tab) ? (array)explode('|', $disable_tab) : [];
?>

<div
    class="stm_dynamic_listing_filter stm_hybrid_filter_listing filter-listing animated fadeIn <?php echo esc_attr($css_class); ?>">
    <!-- Nav tabs -->
    <ul class="stm_dynamic_listing_filter_nav clearfix heading-font" role="tablist">
        <?php if (is_array($items)):
            $i = 0;
            foreach ($items as $key => $item):
                $i++;
            	if(isset($item['taxonomy_tab'])){
					$item_tab = str_replace(array(',', ' '), '', $item['taxonomy_tab']);
					$data = explode('|', $item_tab);
				}

                if ($i > 1) {
                    $active_taxonomy_tab_active = '';
                }

                if(isset($item['tab_title_single']) && in_array($item['tab_title_single'], $disabled_tabs)) continue;
                ?>
                <?php if (!empty($item['taxonomy_tab']) and !empty($item['tab_title_single']) and !empty($item['filter_selected'])): ?>
                <?php $slug = (isset($item['tab_id_single'])) ? sanitize_title($item['tab_id_single']) : sanitize_title($item['tab_title_single']); ?>

                <li class="<?php echo esc_attr($active_taxonomy_tab_active); ?>">
                    <a href="#<?php echo esc_attr($slug); ?>" aria-controls="<?php echo esc_attr($slug); ?>"
                       role="tab" data-toggle="tab" data-value="<?php if(!empty($data[0])) echo esc_attr($data[0]) ?>"
                       data-slug="<?php if(!empty($data[0])) echo esc_attr($data[1]); ?>">
                        <?php echo (!empty($item['tab_title_single']) ? esc_attr($item['tab_title_single']) : 'Undef.'); ?>
                    </a>
                </li>

            <?php endif; ?>
            <?php endforeach;
            $i = 0; ?>
        <?php endif; ?>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">

		<?php if (!$active_taxonomy_tab): ?>
			<div role="presentation" class="active listing-search-label">
				<?php echo esc_attr($show_all_label); ?>
			</div>
		<?php endif; ?>

        <?php if (!$active_taxonomy_tab): ?>
            <div role="tabpanel" class="tab-pane fade in active" id="stm_all_listing_tab">
                <form data-trigger="search-box"
					  action="<?php echo esc_url(stm_get_listing_archive_link()); ?>"
					  onsubmit="return false"
                      method="GET">
                    <button type="submit" class="heading-font" disabled>
                        <i class="fa fa-search"></i><?php echo '<span>' . __('Search', 'motors-child') . '</span> '  ?>
                    </button>
                    <div class="stm-filter-tab-selects clearfix filter home_filter">
                        <div class="row">
							<?php foreach ($filters as $key => $filter) { ?>
								<div class="col-md-3 col-sm-6 col-xs-12 stm-select-col
								stm-filter_<?php echo esc_attr($filter->key) ?>">
                                <?php
								if($filter['type'] == 'location'){?>

									<div class="stm-location-search-unit">

										<div id="search_vue_app">
											<auto-complete-map
												placeholder="<?php echo esc_attr__( 'Zip Code', 'motors' ) ?>"
												location="<?php echo esc_attr($stm_location); ?>"></auto-complete-map>
										</div>
										<input type="hidden" name="stm_lat" value=""/>
										<input type="hidden" name="stm_lng" value=""/>

									</div>
									<?php
								}
								else{
									stm_hybrid_include('filter/' . $filter['type'], compact('filter'));
								}
								?>
                            </div>
							<?php } ?>
						</div>
                    </div>
                </form>
            </div>
        <?php endif; ?>

        <?php if (is_array($items)): ?>
            <?php foreach ($items as $key => $item): $i++;

                if ($i > 1) {
                    $active_taxonomy_tab_content = '';
                }
                ?>
                <?php if (!empty($item['taxonomy_tab']) and !empty($item['tab_title_single']) and !empty($item['filter_selected'])): ?>
                    <?php $slug = (isset($item['tab_id_single'])) ? sanitize_title($item['tab_id_single']) : sanitize_title($item['tab_title_single']); ?>
                    <div role="tabpanel" class="tab-pane fade <?php echo esc_attr($active_taxonomy_tab_content); ?>"
                         id="<?php echo esc_attr($slug); ?>">
                        <?php
                        $tax_term = explode(',', $item['taxonomy_tab']);
                        $tax_term = explode(' | ', $tax_term[0]);

                        $taxonomy_count = stm_get_custom_taxonomy_count($tax_term[0], $tax_term[1]);
                        ?>
                        <form data-trigger="search-box"
							  action="<?php echo esc_url(stm_get_listing_archive_link()); ?>"
							  onsubmit="return false"
                              method="GET">
                            <button type="submit" class="heading-font" disabled>
                                <i class="fa fa-search"></i>
								<?php echo '<span>' . $taxonomy_count . '</span> ' . $search_button_postfix; ?>
                            </button>
                            <div class="stm-filter-tab-selects filter stm-vc-ajax-filter home_filter">
                                <div class="row">
									<input type="hidden" name="filter[<?php echo esc_attr($tax_term[1]); ?>]"
										   value="<?php echo esc_attr($tax_term[0]); ?>"/>
									<?php foreach ($filters as $filter) { ?>
										<div class="col-md-3 col-sm-6 col-xs-12 stm-select-col stm-filter_<?php echo esc_attr($filter->key) ?>">
											<?php
											if($filter['type'] == 'location'){
												stm_hybrid_include(
													'filter/' . $filter['type'] . '_alt',
													compact('filter')
												);
											}
											else stm_hybrid_include('filter/' . $filter['type'], compact('filter'));
											?>
										</div>
									<?php } ?>
								</div>
                            </div>
                        </form>
                    </div>

                <?php endif; ?>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>


<script>
	jQuery('.stm-filter-tab-selects.filter.home_filter').on('change paste keyup', 'input[name="stm_location"]', function(){
		console.log(jQuery(this));
		if(jQuery(this).val()){
			jQuery(this).parents('.tab-pane').find('button[type="submit"]').removeAttr('disabled');
		}else{
			jQuery(this).parents('.tab-pane').find('button[type="submit"]').attr('disabled', 'disabled');
		}
	})
</script>

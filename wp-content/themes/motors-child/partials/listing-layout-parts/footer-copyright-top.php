<div class="stm-listing-layout-footer">
	<div class="container">
		<div class="clearfix">

			<div class="stm-footer-search-inventory">
				<div class="search-suggestions">
					<form method="get" action="<?php echo esc_url( stm_get_listing_archive_link() ); ?>">
						<input type="text" data-trigger="suggestions" class="stm-footer-search-name-input" name="s" placeholder="<?php esc_html_e('Search Inventory', 'motors') ?>"/>
						<button type="submit"><i class="fa fa-search"></i></button>
					</form>
				</div>
			</div>

			<div class="stm-footer-menu">
				<ul class="stm-listing-footer-menu clearfix">
					<?php
					wp_nav_menu( array(
							'menu'              => 'bottom_menu',
							'theme_location'    => 'bottom_menu',
							'depth'             => 1,
							'container'         => false,
							'menu_class'        => 'stm-listing-footer-menu clearfix',
							'items_wrap'        => '%3$s',
							'fallback_cb' => false
						)
					);
					?>
				</ul>
			</div>
		</div>
	</div>
</div>

<?php
$dealers = $GLOBALS['dealers'];

function dealerLabels($conditions) {
	$labels = [];
	if (!$conditions) return;
	foreach($conditions as $condition){
		$labels[] = $condition->title;
	}
	return implode('/', $labels);
}

?>
<div class="stm_dealer_list_dip">
	<table class="stm_dealer_list_table">
		<?php foreach( $dealers['data'] as $dealer ): ?>
			<tr class="stm-single-dealer animated fadeIn">
				<td class="image">
					<a href="<?php echo $dealer->link ?>" target="_blank">
						<?php if($dealer->logo): ?>
							<div>
								<img src="<?php echo $dealer->logo ?>" alt="<?php echo $dealer->name ?>" class="img-responsive"/>
							</div>
						<?php else: ?>
							<div>
								<img src="<?php stm_get_dealer_logo_placeholder(); ?>" alt="<?php echo $dealer->name ?>" class="no-logo"/>
							</div>
						<?php endif ?>
					</a>
				</td>

				<td class="dealer-info">
					<div class="title">
						<a class="h4" href="<?php echo $dealer->link ?>" target="_blank"><?php echo $dealer->name ?></a>
					</div>
					<div class="rating">
						<div class="dealer-rating">
							<div class="stm-rate-unit">
								<div class="stm-rate-inner">
									<div class="stm-rate-not-filled"></div>
									<?php if(!empty($dealer->rating['average'])): ?>
										<div>
											<div class="stm-rate-filled" style="width: <?php echo $dealer->rating['average']*20 ?>%"></div>
										</div>
									<?php else: ?>
										<div><div class="stm-rate-filled" style="width:0%"></div></div>
									<?php endif ?>
								</div>
							</div>
							<div class="stm-rate-sum">(<?php esc_html_e( 'Reviews', 'motors' ); ?> <?php echo $dealer->rating['count'] ?>)</div>
						</div>
					</div>
				</td>

				<td class="dealer-cars">
					<div class="inner">
						<a href="<?php echo $dealer->link ?>#stm_d_inv" target="_blank">
							<?php if($dealer->count_listings): ?>
								<div class="dealer-labels heading-font">
									<?php echo $dealer->count_listings.' '.dealerLabels($dealer->conditions) ?>
								</div>
							<?php else: ?>
								<div class="dealer-labels heading-font">0</div>
							<?php endif ?>
							<div class="dealer-cars-count">
								<i class="stm-service-icon-body_type"></i>
								<?php if($dealer->count_listings > 1): ?>
									<span><?php echo __( 'Cars in stock', 'motors' ); ?></span>
								<?php else: ?>
									<span><?php echo __('Car in stock', 'motors') ?></span>
								<?php endif ?>
							</div>
						</a>
					</div>
				</td>

				<td class="dealer-phone">
					<div class="inner">
						<?php if($dealer->phone): ?>
							<div>
								<i class="stm-service-icon-phone_2"></i>
								<div class="phone heading-font"><?php echo substr($dealer->phone, 0,3).'*******' ?></div>
								<span class="stm-show-number_custom" data-id="<?php echo $dealer->phone ?>">
								<?php echo esc_html__("Show number", "motors"); ?></span>
							</div>
						<?php endif ?>
					</div>
				</td>


				<td class="dealer-location">
					<div class="clearfix">
						<?php if($dealer->location): ?>
							<div>
								<a rel="nofollow"
								   href="https://maps.google.com?q=<?php echo $dealer->address ?>"
								   target="_blank"
								   class="map_link"
								>
									<i class="fa fa-external-link"></i>
									<?php esc_html_e( 'See map', 'motors' ); ?>
								</a>
							</div>
						<?php endif ?>
						<div class="dealer-location-label">
							<?php if(!empty($dealer->_sort['location'])): ?>
								<div>
									<div class="inner">
										<i class="stm-service-icon-pin_big"></i>
										<span class="heading-font">
											<?php if(!empty($dealer->_sort['location'])) echo $dealer->_sort['location'] ?>
										</span>
										<div class="stm-label"><?php esc_html_e( 'From', 'motors' ); ?>
											inputLoc
										</div>
									</div>
								</div>
							<?php endif ?>
							<?php if(!empty($dealer->address)): ?>
								<div>
									<div class="inner">
										<i class="stm-service-icon-pin_big"></i>
										<span class="heading-font"><?php echo $dealer->address ?></span>
									</div>
								</div>
							<?php else: ?>
								<div><?php esc_html_e( 'N/A', 'motors' ); ?></div>
							<?php endif ?>
						</div>
					</div>
				</td>

			</tr>
			<tr class="dealer-single-divider">
				<td colspan="5"></td>
			</tr>
		<?php endforeach ?>
	</table>
</div>

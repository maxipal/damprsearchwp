<div class="modal fade" id="save-search" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="margin-top: 140px;">
        <div class="modal-content">
            <div class="modal-header" style="padding: 0;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <div class="modal-header modal-header-iconed">
					<i class="stm-service-icon-compare-new" style="top: 24px;"></i>
					<h3 class="modal-title"><?php esc_html_e('Save your search', 'motors-child') ?></h3>
				</div>
            </div>
            <div class="modal-body">
				<form method="post">
					<div class="form-group">
						<input type="text" name="title" />
					</div>
					<div class="form-group">
						<button type="submit" style="display: inline-block;"><?php _e( 'Save', 'motors-child' ) ?></button><span style="display:inline-block; margin-left: 20px; color: #1bc744;"></span>
					</div>
				</form>
            </div>
        </div>
    </div>
</div>

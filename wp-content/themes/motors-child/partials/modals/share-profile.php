<div class="modal fade" id="shareModalProfile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="
    margin-top: 140px;
    ">
        <div class="modal-content">
            <div class="modal-header" style="padding: 0;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <div class="modal-header modal-header-iconed">
					<i class="stm-service-icon-compare-new" style="top: 24px;"></i>
					<h3 class="modal-title"><?php esc_html_e('Share your profile link', 'motors') ?></h3>
					<div class="test-drive-car-name"> </div>
				</div>
            </div>
            <div class="modal-body">
                <div class="compare-link-wrap">
                    <input type="text" class="compare-link" id="copyInput"/>
                    <button onclick="copyFunction()"><i class="fa fa-file"></i></button>
                </div>
                <div class="compare-email-wrap">
                    <span>or </span><a data-toggle="collapse" href="#collapseThree" class="send-compare" style="text-decoration: underline;">Send by email</a>
                    <div class="compare-email-form-wrap collapse" id="collapseThree">
                        <form method="post" class="profile-email-form">
                            <div class="form-group">
                                <input type="email" name="compare-email" placeholder="Recipient Email" required />
                            </div>

                            <div class="form-group">
                                <input type="email" name="sender-email" placeholder="Your Email" required />
                            </div>
                            <div class="form-group">
                                <input type="text" name="sender-phone" placeholder="Your phone (optional)" />
                            </div>
                            <div class="form-group">
                                <input type="text" name="sender-f-name" placeholder="Your First Name" required />
                            </div>
                            <div class="form-group">
                                <input type="text" name="sender-l-name" placeholder="Last Name" required />
                            </div>

                            <div class="form-group">
                                <textarea name="body-text" placeholder="Enter messege" required row="7" style="min-height: 140px;"></textarea>
                            </div>
                            <?php
                                $recaptcha_enabled = get_theme_mod('enable_recaptcha',0);
                                $recaptcha_public_key = get_theme_mod('recaptcha_public_key');
                                $recaptcha_secret_key = get_theme_mod('recaptcha_secret_key');
                                if(!empty($recaptcha_enabled) and $recaptcha_enabled and !empty($recaptcha_public_key) and !empty($recaptcha_secret_key)):
                            ?>
                                <div class="form-group">
                                    <div class="g-recaptcha" data-sitekey="<?php echo esc_attr($recaptcha_public_key); ?>" data-size="normal"></div>
                                </div>
                            <?php endif; ?>
                            <div class="form-group share-result">
                                <button type="submit" style="display: inline-block;">Send email</button><span style="display:inline-block; margin-left: 20px; color: #1bc744;"></span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

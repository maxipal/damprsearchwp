<div class="modal" id="valuation_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabelValuationModal">
	<form id="request-valuation-modal-form" action="<?php echo esc_url( home_url('/') ); ?>" method="post">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header modal-header-iconed">
					<i class="stm-moto-icon-cash"></i>
					<h3 class="modal-title" id="myModalLabelTestDrive"><?php esc_html_e('VREF Verified Report', 'motors-child') ?></h3>
					<div class="test-drive-car-name"><?php echo stm_generate_title_from_slugs(get_the_id()); ?></div>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 col-sm-12 pdf_notify_wait">
							<?php _e("The Valuation report will be made available in your account within the next 24 hours", "motors-child") ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>


<div class="modal" id="pre_valuation_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabelPreValuationModal">
	<form id="request-pre-valuation-modal-form" action="<?php echo esc_url( home_url('/') ); ?>" method="post">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header modal-header-iconed clearfix">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
					<img class="site_logo" src="<?php echo get_stylesheet_directory_uri() . '/assets/images/logo-black.png' ?>" alt="logo_main">
					<h3 class="modal-title" id="myModalLabelTestDrive"><?php esc_html_e('Valuation Request', 'motors') ?></h3>
					<img class="vref_logo" src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/valuation_sert.png" alt="valuation_sert">
				</div>
				<div class="modal-body">
					<?php $form_content = get_page_by_path( 'valuation-form', OBJECT, 'page' ); ?>
					<?php echo do_shortcode( $form_content->post_content ) ?>

				</div>

				<div class="modal-footer">
					<div class="wrap_valueation_img"></div>
					<div class="wrap_valueation_title"></div>
					<div class="wrap_valueation_action">
						<?php
						$plan_id = get_theme_mod( 'valuation_plan', false );
						$_product = wc_get_product( $plan_id );
						?>

						<span class="wrap_price"><?php echo $_product->get_price_html(); ?></span>
						<span class="valuation_description"><?php _e("For a full comprehensive VREF Verified Valuations report", "motors") ?></span>
						<a class="buy_valuation button"><?php _e("Get report", "motors") ?></a>
						<div style="text-align:center">
							<a target="_blank" href="<?php echo wp_get_attachment_url(get_post_meta($plan_id,'sample_pdf_report', true)) ?>"><?php _e("View sample report", "motors") ?></a>
						</div>

					</div>
				</div>
			</div>
		</div>
	</form>
</div>




<style>
.modal-body .cap-wrapper{
transform:scale(0.78);-webkit-transform:scale(0.78);transform-origin:0 0;-webkit-transform-origin:0 0; margin-top:13px;
}
.p-right{
	float: right!important;
}

#request-pre-valuation-modal-form{
	margin-top: 120px;
}

#request-pre-valuation-modal-form .modal-content{
	border-radius: 7px;
}
#request-pre-valuation-modal-form .modal-header-iconed{
	background-color: #fff;
    padding: 15px 10px !important;
    border-radius: 5px 5px 0 0;
}
#request-pre-valuation-modal-form .modal-header-iconed .modal-title{
	color: #333;
	text-align: center;
	width: 100%;
	position: absolute;
	top: 13px;
}
#request-pre-valuation-modal-form .modal-header-iconed img{
	height: 35px;
    width: auto;
}
#request-pre-valuation-modal-form .modal-header-iconed img.site_logo{
	float: left;
    padding: 8px;
}
#request-pre-valuation-modal-form .modal-header-iconed img.vref_logo{
	float: right;
}
#request-pre-valuation-modal-form .modal-header-iconed i{
	top: -35px;
	color: #dadada;
}
#request-pre-valuation-modal-form .modal-body{
	background-image: url(http://air.stylemix.biz/wp-content/uploads/2016/02/sr22.jpg);
    background-size: cover;
    background-repeat: no-repeat;
    background-position: top center;
    padding: 40px 25px;
    width: 103%;
    margin-left: -10px;
    box-shadow: 0 5px 15px rgba(0,0,0,.5);
    background-color: #fff;
}
#request-pre-valuation-modal-form .modal-body:after {
    position: absolute;
    content: '';
    display: block;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background: rgba(59, 87, 144, 0.7);
}

#request-pre-valuation-modal-form .modal-body .wpb_text_column{
	color: #fff;
	padding-left: 15px;
	padding-right: 15px;
}
#request-pre-valuation-modal-form .modal-body .wpb_text_column p{
	font-size: 12px;
    line-height: 18px;
}
#request-pre-valuation-modal-form .modal-body .wpb_text_column h5{
	font-size: 16px;
    margin-bottom: 25px;
    color: #fff;
}
#request-pre-valuation-modal-form .modal-body > .vc_row{
	position: relative;
    z-index: 10;
}
#request-pre-valuation-modal-form .modal-footer{
	padding: 25px 20px;
}

#request-pre-valuation-modal-form .modal-footer .wrap_valueation_img,
#request-pre-valuation-modal-form .modal-footer .wrap_valueation_title{
	float: left;
}
#request-pre-valuation-modal-form .modal-footer .wrap_valueation_img{
	height: 135px;
    width: 230px;
    margin-right: 21px;
    border: 1px solid #dadada;
    overflow: hidden;
}
#request-pre-valuation-modal-form .modal-footer .wrap_valueation_title{
	text-transform: uppercase;
    font-size: 17px;
    max-width: 240px;
    text-align: left;
    font-weight: bold;
    vertical-align: middle;
    height: 135px;
    line-height: 135px;
}
#request-pre-valuation-modal-form .modal-footer .wrap_valueation_title span{
	line-height: 20px;
    display: inline-block;
}

#request-pre-valuation-modal-form .modal-footer .wrap_valueation_action{
	float: right;
    width: 220px;
}
#request-pre-valuation-modal-form .modal-footer .wrap_price,
#request-pre-valuation-modal-form .modal-footer .valuation_description{
	display: block;
	text-align: left;
	font-weight: bold;
	padding-left: 15px;
    padding-right: 15px;
}
#request-pre-valuation-modal-form .modal-footer .valuation_description{
	line-height: 15px;
    font-size: 12px;
}
#request-pre-valuation-modal-form .modal-footer .buy_valuation{
	margin: 10px 0;
}
#request-pre-valuation-modal-form .modal-footer .wrap_price{
	margin-bottom: 10px;
	font-size: 18px;
}
#request-pre-valuation-modal-form .modal-footer .buy_valuation{
	background-color: #d97e2b;
	border-radius: 50px;
	box-shadow: 0 2px 0 #d97e2b;
}


.stm-valuation-btn{
	position: relative;
}
.stm-valuation-btn i{
	position: absolute;
    right: 11px;
    left: auto !important;
    color: #fff;
}

.pdf_notify_wait{
	text-align: center;
    font-size: 16px;
    padding: 60px 0;
    text-transform: uppercase;
    font-weight: 700;
}
@media (min-width:767px){
	#request-pre-valuation-modal-form .modal-dialog{
		width: 767px;
	}
}
@media(max-width:767px){
	#request-pre-valuation-modal-form .modal-body{
		width: 100%;
		margin-left: 0;
		padding: 20px 0px;
	}
	#request-pre-valuation-modal-form .modal-header-iconed img{
		float: none !important;
		margin: 0 auto;
		display: block;
	}
	#request-pre-valuation-modal-form .modal-header-iconed .modal-title{
		position: static;
		margin: 10px 0;
	}
	#request-pre-valuation-modal-form .modal-body .wpb_text_column h5{
		margin-bottom: 10px;
    	margin-top: 10px;
	}
	#request-pre-valuation-modal-form .modal-footer .wrap_valueation_img,
	#request-pre-valuation-modal-form .modal-footer .wrap_valueation_title{
		float: none;
	}
	#request-pre-valuation-modal-form .modal-footer .wrap_valueation_img{
		margin: 0 auto 15px;
		height: 100px;
    	width: 190px;
	}

	#request-pre-valuation-modal-form .modal-footer .wrap_valueation_title{
		max-width: none;
		width: 100%;
		height: auto;
		line-height: normal;
		text-align: center;
	}
	#request-pre-valuation-modal-form .modal-footer .wrap_valueation_action{
		float: none;
		width: 100%;
		margin-top: 15px;
	}
}
@media (max-width:400px){
	.modal-body .cap-wrapper{
	overflow: hidden;
	transform:scale(1);-webkit-transform:scale(1)
	}
	.p-right{
		float: none!important;
	}
}

.stm-valuation.active{
	border: 1px solid #1bc744;
	color: #fff;
	background-color: #1bc744
}

.stm-valuation.active i{
	color: #fff;
}




</style>


<?php

$first_name = get_user_meta( get_current_user_id(), 'billing_first_name', true );
$last_name = get_user_meta( get_current_user_id(), 'billing_last_name', true );
$stm_phone = get_user_meta( get_current_user_id(), 'billing_phone', true );
$user_email = get_user_meta( get_current_user_id(), 'billing_email', true );


if(empty($user_email)){
	$user_info = get_userdata(get_current_user_id());
	$user_email = $user_info->user_email;
}
if(empty($first_name)) $first_name = get_user_meta( get_current_user_id(), 'first_name', true );
if(empty($last_name)) $last_name = get_user_meta( get_current_user_id(), 'last_name', true );
if(empty($stm_phone)) $stm_phone = get_user_meta( get_current_user_id(), 'stm_phone', true );

?>


<script>
	// jQuery('#pre_valuation_modal').on('show.bs.modal', function(event) {
	// 	event.preventDefault();
	// 	console.log(event);
	//
	// 	console.log();
	// });
	jQuery('#request-valuation-modal-form').on('submit', function(e){
		e.preventDefault();
		var form = jQuery(this);
		var name = form.find('input[name=name]').val();
		var email = form.find('input[name=email]').val();
		var phone = form.find('input[name=phone]').val();
		var vid = form.find('input[name=vehicle_id]').val();

		var recaptcha = form.find('#g-recaptcha-response').val();

		jQuery.ajax({
			url: ajaxurl,
			type: "POST",
			dataType: 'json',
			data: {
				'email': email,
				'name': name,
				'recaptcha': recaptcha,
				'phone': phone,
				'vid' : vid,
				'action': 'send_valuation_form'
			},
			success: function (data) {
				form.find('.mg-bt-25px').text(data.messege);

				setTimeout(function(){
					window.location = data.redirect;
				},2000);

			}
		});
	});



	jQuery(document).on('click','.stm-valuation-btn:not([data-toggle]):not(.valuation_pdf_download)', function(event) {
		event.preventDefault();
		var self = jQuery(this);
		if(!jQuery(this).hasClass('active')){
			jQuery.ajax({
				type: 'POST',
				dataType: 'json',
				url: ajaxurl,
				data: {
					'action': 'send_valuation_form',
					'email': '<?php echo $user_email ?>',
					'name': '<?php echo $first_name.' '.$last_name ?>',
					'phone': '<?php echo $stm_phone ?>',
					'vid': self.data('id')
				},
				beforeSend:function(){
					self.find('i').attr('class','stm-preloader');
				},
				complete:function(){
					self.find('i').attr('class','fa fa-check');
				},
				success: function(data){
					if(data.redirect){
						//jQuery('.stm-valuation').addClass('active');
						setTimeout(function(){
							window.location = data.redirect;
						},200);
					}else if(data.modal){
						jQuery(data.modal).modal('show');

						var air_image = self.data('image');
						var air_title = self.data('title');
						var air_id = self.data('id');
						if(air_image) jQuery('.wrap_valueation_img').html('<img src="'+air_image+'" />');
						if(air_title) jQuery('.wrap_valueation_title').html('<span>'+air_title+'</span>');
						// if(air_id) jQuery('.wrap_valueation_action').html('<span>'+air_id+'</span>');

						jQuery(data.modal).find('.modal-footer a.buy_valuation').attr('href',data.redirect_modal);
					}
				}
			});
		}
	});
</script>


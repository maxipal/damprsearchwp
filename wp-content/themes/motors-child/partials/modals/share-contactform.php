<div class="modal fade" id="share-contactform" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="padding: 0;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <div class="modal-header modal-header-iconed" style="padding-left: 88px;">
					<i class="fa fa-paper-plane" style="top: 24px; font-size: 31px;"></i>
					<h3 class="modal-title"><?php esc_html_e('Contact seller', 'motors') ?></h3>
					<div class="test-drive-car-name"> </div>
				</div>
            </div>
            <div class="modal-body">


                <div class="stm-single-car-contact">
                        <?php $shortcode = '[contact-form-7 id="4506" title="Send message to dealer"]'; ?>
        				<?php if(!empty($shortcode)) {
        					echo do_shortcode($shortcode);
        				}?>

                        <script type="text/javascript">
                            jQuery(document).ready(function(){
                                var $ = jQuery;
                                var inputAuthor = '<input type="hidden" value="" name="stm_changed_recepient"/>';
                                $('#share-contactform form').append(inputAuthor);
                            })
                        </script>

        		</div>


            </div>
        </div>
    </div>
</div>

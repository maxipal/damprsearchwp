<div class="modal fade" id="login-required" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				You must be logged in to use this function.
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-link" style="display: inline-block;" data-dismiss="modal">Cancel</button>
				<a rel="login-link" class="btn btn-primary" href="<?php echo stm_get_author_link( 'register' ) ?>?return=<?php echo $_SERVER['REQUEST_URI'] ?>">Log in</a>
			</div>
		</div>
	</div>
</div>

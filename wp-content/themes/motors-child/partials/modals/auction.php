<div class="modal" id="auction_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabelAuction">
	<form id="request-auction-form" action="<?php echo esc_url( home_url('/') ); ?>" method="post">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header modal-header-iconed">
					<?php $modal_title = get_theme_mod( 'modal_title', 'Sälj bil' ); ?>
					<h3 class="modal-title" id="myModalLabelTestDrive"><?php echo $modal_title; ?></h3>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">


					<div class="stm_wrap_type_inventory">
						<div class="stm_type_inv_body">
							<?php
							$single_bg = get_theme_mod( 'single_bg', false );
							$single_title = get_theme_mod( 'single_title', false );
							$single_content = get_theme_mod( 'single_content', false );
							$single_btn = get_theme_mod( 'single_btn', false );
							$auction_bg = get_theme_mod( 'auction_bg', false );
							$auction_title = get_theme_mod( 'auction_title', false );
							$auction_content = get_theme_mod( 'auction_content', false );
							$auction_btn = get_theme_mod( 'auction_btn', false );
							$auction_page_id = get_theme_mod( 'sell_auction', null );
							$single_page_id = get_theme_mod( 'sell_single', null );
							?>
							<div>
								<div class="stm_type_inv_body_wrap_img">
									<?php stm_lazy_image( $single_bg ) ?>
								</div>
								<h4><?php echo $single_title ?></h4>
								<p><?php echo $single_content ?></p>
								<a href="<?php echo get_permalink( $single_page_id ) ?>" class="button"><?php echo $single_btn ?></a>
							</div>
							<div>
								<div class="stm_type_inv_body_wrap_img">
									<?php stm_lazy_image( $auction_bg ) ?>
								</div>
								<h4><?php echo $auction_title ?></h4>
								<p><?php echo $auction_content ?></p>
								<a href="<?php echo get_permalink( $auction_page_id ) ?>" class="button"><?php echo $auction_btn ?></a>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</form>
</div>

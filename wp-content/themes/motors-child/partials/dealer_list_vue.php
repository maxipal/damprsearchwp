<table v-if="dealers.length > 0 && !firstLoad" v-cloak class="stm_dealer_list_table">
	<tbody>
	<template v-for="dealer in dealers">
		<tr :key="dealer.id" class="stm-single-dealer animated fadeIn">

			<td class="image">
				<a :href="dealer.link" target="_blank">
					<div v-if="dealer.logo">
						<img v-lazy="dealer.logo" :key="dealer.logo" :alt="dealer.name" class="img-responsive"/>
					</div>
					<div v-else>
						<img src="<?php stm_get_dealer_logo_placeholder(); ?>" :alt="dealer.name" class="no-logo"/>
					</div>
				</a>
			</td>

			<td class="dealer-info">
				<div class="title">
					<a class="h4" :href="dealer.link" target="_blank">{{dealer.name}}</a>
				</div>
				<div class="rating">
					<div class="dealer-rating">
						<div class="stm-rate-unit">
							<div class="stm-rate-inner">
								<div class="stm-rate-not-filled"></div>
								<div v-if="dealer.rating && dealer.rating.average">
									<div class="stm-rate-filled"
										 :style="'width:' + dealer.rating.average*20 + '%'"></div>
								</div>
								<div v-else>
									<div class="stm-rate-filled" style="width:0%"></div>
								</div>
							</div>
						</div>
						<div v-if="dealer.rating" class="stm-rate-sum">(<?php esc_html_e( 'Reviews', 'motors' ); ?>
							{{dealer.rating.count}})
						</div>
					</div>
				</div>
			</td>

			<td class="dealer-cars">
				<div class="inner">
					<a :href="dealer.link + '#stm_d_inv'" target="_blank">
						<div class="dealer-labels heading-font" v-if="dealer.count_listings">
							{{dealer.count_listings + ' ' + dealerLabels(dealer.conditions)}}
						</div>
						<div class="dealer-labels heading-font" v-if="!dealer.count_listings">0</div>
						<div class="dealer-cars-count">
							<i class="stm-service-icon-body_type"></i>

							<span v-if="dealer.count_listings > 1">
								<?php echo __( 'Cars in stock', 'motors' ); ?>
							</span>
							<span v-else>
								<?php echo __('Car in stock', 'motors') ?>
							</span>
						</div>
					</a>
				</div>
			</td>

			<td class="dealer-phone">
				<div class="inner">
					<div v-if="dealer.phone">
						<i class="stm-service-icon-phone_2"></i>
						<div class="phone heading-font">{{dealer.phone.substring(3, 0) + '*******'}}</div>
                        <span class="stm-show-number_custom" :data-id="dealer.phone">
						<?php echo esc_html__("Show number", "motors"); ?></span>
					</div>
				</div>
			</td>


			<td class="dealer-location">
				<div class="clearfix">
					<div v-if="dealer.location">
						<a rel="nofollow"
						   :href="'https://maps.google.com?q=' + dealer.address"
						   target="_blank"
						   class="map_link"
						>
							<i class="fa fa-external-link"></i>
							<?php esc_html_e( 'See map', 'motors' ); ?>
						</a>
					</div>
					<div class="dealer-location-label">
						<div v-if="dealer._sort && dealer._sort.location">
							<div class="inner">
								<i class="stm-service-icon-pin_big"></i>
								<span class="heading-font">
                                    {{dealer._sort.location | formatDistance}} km
									<?php // echo esc_attr($dealer_info['fields']['distance']); ?>
                                </span>
								<div class="stm-label"><?php esc_html_e( 'From', 'motors' ); ?>
									{{inputLoc}}
								</div>
							</div>
						</div>
						<div v-else-if="dealer.address">
							<div class="inner">
								<i class="stm-service-icon-pin_big"></i>
								<span class="heading-font">{{dealer.address}}</span>
							</div>
						</div>
						<div v-else>
							<?php esc_html_e( 'N/A', 'motors' ); ?>
						</div>
					</div>
				</div>
			</td>

		</tr>
		<tr :key="'divider' + dealer.id" class="dealer-single-divider">
			<td colspan="5"></td>
		</tr>
	</template>
	</tbody>
</table>

<?php
$result = air_get_related_listings( [ 'per_page' => 3, 'sort_order' => 'random', 'context' => 'short' ] );

if ( ! count( $result['data'] ) ) {
	return;
}
?>
<div class="stm-similar-cars-units">
	<?php foreach ( $result['data'] as $aircraft ) : the_listing( $aircraft ); ?>
		<a href="<?php echo $aircraft->link(); ?>" class="stm-similar-car clearfix">
			<?php if ( $image = $aircraft->thumbnail ): ?>
				<div class="image">
					<img src="<?php echo stm_hybrid_image_size( $image, 350, 356, true ); ?>" class="img-responsive wp-post-image" alt="<?php echo $aircraft->title; ?>">
				</div>
			<?php endif; ?>
			<div class="right-unit">
				<div class="title"><?php echo $aircraft->title; ?></div>

				<?php
				$user_added_by = $aircraft->author_id;
				if ( ! empty( $user_added_by ) ) {
					$user_exist = get_userdata( $user_added_by );
				}
				?>
				<div class="stm-dealer-name">
					<?php if ( ! empty( $user_exist ) and $user_exist ): ?>
						<?php echo stm_display_user_name( $user_added_by ); ?>
					<?php endif; ?>
				</div>
				<div class="clearfix">
					<?php if ( ! empty( $aircraft->price_view ) ): ?>
						<div class="stm-price heading-font"><?php echo( $aircraft->price_view ); ?></div>
					<?php endif; ?>
					<?php if ( ! empty( $value = $aircraft->condition ) ): ?>
						<div class="stm-car-similar-meta">
							<span><?php echo esc_attr( $value ); ?></span>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</a>
	<?php endforeach; ?>
</div>

<?php
$vc_status = get_post_meta(get_the_ID(), '_wpb_vc_js_status', true);
$recaptcha_enabled = get_theme_mod('enable_recaptcha', 0);
$recaptcha_public_key = get_theme_mod('recaptcha_public_key');
$recaptcha_secret_key = get_theme_mod('recaptcha_secret_key');
$car_filters = false;
$car_filters_req = stm_hybrid_api_transient(180, 'listings/attributes?context=compare');

if (!is_wp_error($car_filters_req)) {
	$car_filters = json_decode($car_filters_req->get_data());
}

$temp = array();
foreach ($car_filters as $car_filter) {
	if(isset($car_filter->useInCompare)) $temp[] = $car_filter;
}

$car_filters = $temp;


if (!empty($recaptcha_enabled) and $recaptcha_enabled and !empty($recaptcha_public_key) and !empty($recaptcha_secret_key)) {
	wp_enqueue_script('stm_grecaptcha');
}

if (function_exists('mpdf_pdfbutton')) mpdf_pdfbutton(false, 'my link', 'my login text');

if ($vc_status != 'false' && $vc_status == true):
	if (have_posts()) :
		while (have_posts()) : the_post();
			the_content();
		endwhile;
	endif;

else:

	if (empty($_COOKIE['compare_ids'])) {
		$compare_ids = array();
	} else {
		$compare_ids = $_COOKIE['compare_ids'];
	}

	if (!empty($_GET['comp'])) {
		$comp = $_GET['comp'];
		$compare_ids = array_map('intval', explode("-", $comp));
	}
	$empty_cars = 3 - count($compare_ids);
	$counter = 0;


	$comp_title = '';
	if (!empty($_GET['comp_title'])) {
		$comp_title = $_GET['comp_title'];
	}

	$hidden_array = array();
	$hidden_count = array();

	$compares = \Stm_Hybrid\Listing::query(
		['ids' => join(',', $compare_ids)],
		['uri' => 'listings/compare']
	);

	$compare_url = site_url(add_query_arg(['comp' => join('-', $compare_ids), 'comp_title' => rawurlencode($comp_title)]));

	?>

	<div class="<?php if (count($compares['data'])) echo 'compare-scroll' ?>">
		<?php if (count($compares['data'])):
			if (count($compares['data'])): ?>
				<div class="row row-4 car-listing-row stm-car-compare-row <?php  ?>" id="compare-content">
					<div class="col-sm-3 col-xs-4 hidden-xs">
						<h1 class="compare-title h2"><?php echo esc_html__('Compare Vehicles', 'motors'); ?></h1>

						<div class="colored-separator text-left">
							<div class="first-long"></div>
							<div class="last-short"></div>
						</div>
						<?php if (!empty($comp_title)): ?>
							<h3 style="text-transform: uppercase"><?php echo $comp_title; ?></h3>
						<?php endif; ?>
					</div>
					<?php foreach ($compares['data'] as $listing) : the_listing($listing);
						$counter++; ?>
						<!--Compare car description-->
						<div
							class="col-sm-3 col-xs-4 compare-col-stm compare-col-stm-<?php echo esc_attr(listing()->id); ?>">

							<a href="<?php echo esc_url(listing()->link); ?>" class="rmv_txt_drctn">
								<div class="compare-col-stm-empty">
									<div class="image">
										<?php if (listing()->thumbnail['url']) { ?>
											<div class="stm-compare-car-img">
												<img style="width:255px; height:135px"
													 class="img-responsive wp-post-image"
													 src="<?php echo(stm_hybrid_image_size(listing()->thumbnail, 255, 135, true)); ?>"
													 alt="<?php esc_attr(listing()->title) ?>"/>
											</div>
										<?php } else { ?>
											<i class="stm-icon-add_car"></i>
											<img class="stm-compare-empty"
												 src="<?php echo esc_url(get_stylesheet_directory_uri() . '/assets/images/compare-empty.jpg'); ?>"
												 alt="<?php esc_html_e('Empty', 'motors'); ?>"/>
										<?php }; ?>
									</div>
								</div>
							</a>
							<div class="remove-compare-unlinkable">
									<span
										class="remove-from-compare"
										data-id="<?php echo esc_attr(listing()->id); ?>"
										data-action="remove">
										<i class="stm-service-icon-compare-new"></i>
										<span><?php esc_html_e('Remove from list', 'motors'); ?></span>
									</span>
							</div>
							<a href="<?php echo esc_url((listing()->link)) ?>" class="rmv_txt_drctn">
								<div class="listing-car-item-meta">
									<div class="car-meta-top heading-font clearfix">
										<?php $price = listing()->price; ?>
										<?php $sale_price = listing()->price_sale; ?>
										<?php $car_price_form_label = listing()->price_view; ?>
										<?php if (empty($car_price_form_label)): ?>
											<?php if (!empty($price) and !empty($sale_price)): ?>
												<div class="price discounted-price">
													<div
														class="regular-price"><?php echo esc_attr(stm_listing_price_view_custom($price, get_the_id())); ?></div>
													<div
														class="sale-price"><?php echo esc_attr(stm_listing_price_view_custom($sale_price, get_the_id())); ?></div>
												</div>
											<?php elseif (!empty($price)): ?>
												<div class="price">
													<div
														class="normal-price"><?php echo esc_attr(stm_listing_price_view_custom($price, get_the_id())); ?></div>
												</div>
											<?php endif; ?>
										<?php else: ?>
											<div class="price">
												<div
													class="normal-price"><?php echo esc_attr($car_price_form_label); ?></div>
											</div>
										<?php endif; ?>
										<div class="car-title"><?php echo listing()->title ?></div>

									</div>
								</div>
							</a>

							<span class="btn btn-default add-to-compare hidden" data-action="remove"
								  data-id="<?php echo esc_js(listing()->id); ?>">
									<?php esc_html_e('Remove from compare', 'motors'); ?>
                            </span>
						</div> <!--md-3-->
					<?php endforeach; ?>
					<?php for ($i = 0; $i < $empty_cars; $i++) { ?>
						<div class="col-sm-3 col-xs-4 compare-col-stm-empty">
							<a href="<?php echo esc_url(stm_get_listing_archive_link()); ?>">
								<div class="image">
									<i class="stm-icon-add_car"></i>
									<img class="stm-compare-empty"
										 src="<?php echo esc_url(get_stylesheet_directory_uri() . '/assets/images/compare-empty.jpg'); ?>"
										 alt="<?php esc_html_e('Empty', 'motors'); ?>"/>
								</div>
								<div class="h5"><?php esc_html_e('Add car to compare', 'motors'); ?></div>
							</a>
						</div>
					<?php } ?>
				</div> <!--row-->
			<?php endif; ?>

			<!-------------------- SPECS BLOCK ------------------------->
			<?php if (count($compares['data'])): ?>
				<div class="row row-4 stm-compare-row">
					<div class="col-sm-3 col-xs-4 hidden-xs">
						<?php if (!empty($compares['attributes'])): ?>
							<div class="compare-options">
								<table>
									<?php foreach ($car_filters as $attribute):	?>
										<tr>
											<td class="compare-value-hover <?php echo esc_attr('compare-value-' . $attribute->name) ?>"
												data-value="<?php echo esc_attr('compare-value-' . $attribute->name) ?>">
												<?php echo $attribute->label; ?>
											</td>
										</tr>
									<?php endforeach; ?>
								</table>
							</div>
						<?php endif; ?>
					</div>

					<?php foreach ($compares['data'] as $listing) : the_listing($listing); ?>
						<div class="col-sm-3 col-xs-4 compare-col-stm-<?php echo esc_attr(listing()->id); ?>">
							<?php if (!empty($compares['data'])): ?>
								<div class="compare-values">
									<?php if (listing()->thumbnail["url"]): ?>
										<div class="compare-car-visible">
											<?php $image = $listing->thumbnail ?>
											<img style="width:255px; height:135px"
												 class="stm-img-mobile-compare img-responsive wp-post-image"
												 src="<?php echo(stm_hybrid_image_size($image, 255, 135, true)); ?>"
												 alt="<?php esc_attr(listing()->title) ?>"/>
										</div>
									<?php else: ?>
										<div class="compare-car-visible">
											<img class="stm-compare-empty stm-img-mobile-compare"
												 src="<?php echo esc_url(get_stylesheet_directory_uri() . '/assets/images/compare-empty.jpg'); ?>"
												 alt="<?php esc_html_e('Empty', 'motors'); ?>"/>
										</div>
									<?php endif; ?>

									<h4 class="text-transform compare-car-visible"><?php the_title(); ?></h4>
									<table>
										<?php if (wp_is_mobile()): ?>
											<tr class="visible-xs">
												<td class="compare-value-hover">
													<div class="h5" data-option="<?php esc_html_e('Price', 'motors'); ?>">
														&nbsp;
														<?php $price = listing()->price; ?>
														<?php $sale_price = listing()->price_sale; ?>
														<?php $car_price_form_label = listing()->price_view; ?>
														<?php if (empty($car_price_form_label)): ?>
															<?php if (!empty($price) and !empty($sale_price)): ?>
																<span
																	class="regular-price"><?php echo esc_attr(stm_listing_price_view($price)); ?></span>
																<span
																	class="sale-price"><?php echo esc_attr(stm_listing_price_view($sale_price)); ?></span>
															<?php elseif (!empty($price)): ?>
																<span
																	class="normal-price"><?php echo esc_attr(stm_listing_price_view($price)); ?></span>
															<?php endif; ?>
														<?php else: ?>
															<span
																class="normal-price"><?php echo esc_attr($car_price_form_label); ?></span>
														<?php endif; ?>
													</div>
												</td>
											</tr>
										<?php endif; ?>



										<?php $data = listing()->attributes;
										foreach ($car_filters as $car_filter): ?>
											<tr>
												<td class="compare-value-hover"
													data-value="<?php echo esc_attr('compare-value-' . $car_filter->name); ?>">
													<div class="h5"
														 data-option="<?php esc_html_e($car_filter->label, 'motors'); ?>">
														<?php if (!empty($data[$car_filter->name]->value)) {
															echo $data[$car_filter->name]->value;
														} else {
															esc_html_e('-', 'motors');
															$hidden_count[$slug] += 1;
														} ?>
													</div>
												</td>
											</tr>
										<?php endforeach; ?>
									</table>
								</div>
							<?php endif; ?>
						</div> <!--md-3-->
					<?php endforeach; ?>
					<?php
					foreach ($hidden_count as $slug => $count) {
						if ($count == count($compare_ids)) {
							$hidden_array[] = 'compare-value-' . $slug;
						}
					}
					?>

					<?php for ($i = 0; $i < $empty_cars; $i++) { ?>
						<?php if (!empty($compares['attributes'])): ?>
							<div class="col-sm-3 col-xs-4">
								<div class="compare-options hidden">
									<table>
										<?php foreach ($compares['attributes'] as $attribute): ?>
											<?php if ($attribute['name'] != 'price') { ?>
												<tr>
													<td class="compare-value-hover">&nbsp;</td>
												</tr>
											<?php }; ?>
										<?php endforeach; ?>
									</table>
								</div>
							</div>
						<?php endif; ?>
					<?php } ?>
				</div> <!--row-->
			<?php endif; ?>

		<?php else: //If empty cars, just everything without cars =) ?>

			<div class="row row-4 car-listing-row stm-car-compare-row stm-no-cars">
				<div class="col-md-3 col-sm-3">
					<h1 class="compare-title h2"><?php echo esc_html__('Compare Vehicles', 'motors'); ?></h1>
					<div class="colored-separator text-left">
						<div class="first-long"></div>
						<div class="last-short"></div>
					</div>
				</div>
				<?php for ($i = 0; $i < $empty_cars; $i++) { ?>
					<div class="col-md-3 col-sm-3 compare-col-stm-empty">
						<a href="<?php echo esc_url(stm_get_listing_archive_link()); ?>">
							<div class="image">
								<i class="stm-icon-add_car"></i>
								<img class="stm-compare-empty"
									 src="<?php echo esc_url(get_stylesheet_directory_uri() . '/assets/images/compare-empty.jpg'); ?>"
									 alt="<?php esc_html_e('Empty', 'motors'); ?>"/>
							</div>
							<div class="h5"><?php esc_html_e('Add car to compare', 'motors'); ?></div>
						</a>
					</div>
				<?php } ?>
			</div> <!--row-->
			<div class="row row-4 stm-compare-row hidden-xs">
				<div class="col-md-3 col-sm-3 col-xs-4 hidden-xs">
					<?php if (!empty($car_filters)): ?>
						<div class="compare-options">
							<table>
								<?php foreach ($car_filters as $car_filter): ?>
									<tr>
										<td class="compare-value-hover"
											data-value="<?php echo esc_attr('compare-value-' . $car_filter->key) ?>">
											<?php esc_html_e($car_filter->label, 'motors'); ?>
										</td>
									</tr>
								<?php endforeach; ?>
							</table>
						</div>
					<?php endif; ?>
				</div>
				<?php for ($i = 0; $i < $empty_cars; $i++) { ?>
					<?php if (!empty($car_filters)): ?>
						<div class="col-md-3 col-sm-3 col-xs-4">
							<div class="compare-options">
								<table>
									<?php foreach ($car_filters as $car_filter): ?>
										<tr>
											<td>&nbsp;</td>
										</tr>
									<?php endforeach; ?>
								</table>
							</div>
						</div>
					<?php endif; ?>
				<?php } ?>
			</div> <!--row-->
		<?php endif; ?>

		<!--Additional features-->
		<?php if (!empty($compares)): ?>
			<?php if (count($compares['data'])): ?>
				<div class="row row-4 row-compare-features  hidden-xs">
					<div class="col-md-3 col-sm-3 col-xs-4">
						<h4 class="stm-compare-features"><?php esc_html_e('Additional features', 'motors'); ?></h4>
					</div>
					<?php foreach ($compares['data'] as $listing) : the_listing($listing); ?>
						<?php $features = listing()->features; ?>
						<?php if (!empty($features)): ?>
							<div class="col-md-3 col-sm-3 col-xs-4 compare-col-stm-<?php echo esc_attr(listing()->id); ?>">
								<ul class="list-style-2">
									<?php foreach ($features as $key => $feature):; ?>
										<li><?php echo esc_attr($feature['title']); ?></li>
									<?php endforeach; ?>
								</ul>
							</div>
						<?php endif; ?>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		<?php endif; ?>
	</div>


	<div class="compare-empty-car-top">
		<div class="col-sm-3 col-xs-4 compare-col-stm-empty">
			<a href="<?php echo esc_url(get_post_type_archive_link(stm_listings_post_type())); ?>">
				<div class="image">
					<i class="stm-icon-add_car"></i>
					<img class="stm-compare-empty"
						 src="<?php echo esc_url(get_stylesheet_directory_uri() . '/assets/images/compare-empty.jpg'); ?>"
						 alt="<?php esc_html_e('Empty', 'motors'); ?>"/>
				</div>
				<div class="h5"><?php esc_html_e('Add car to compare', 'motors'); ?></div>
			</a>
		</div>
	</div>


	<div class="compare-empty-car-bottom">
		<?php if (!empty($car_filters)): ?>
			<div class="col-md-3 col-sm-3 col-xs-4">
				<div class="compare-options">
					<table>
						<?php foreach ($car_filters as $car_filter): ?>
							<tr>
								<td class="compare-value-hover">&nbsp;</td>
							</tr>
						<?php endforeach; ?>
					</table>
				</div>
			</div>
		<?php endif; ?>
	</div>


	<script type="text/javascript">
		jQuery(document).ready(function ($) {
			$('.compare-value-hover').hover(function () {
				var dataValue = $(this).data('value');
				$('.compare-value-hover[data-value = ' + dataValue + ']').addClass('hovered');
			}, function () {
				$('.compare-value-hover').removeClass('hovered');
			})

			var inputAircaft = '<input type="hidden" value="" name="stm_changed_aircraft"/>';
			$('.stm-single-car-contact form').append(inputAircaft);
		})
	</script>

<?php endif; ?>

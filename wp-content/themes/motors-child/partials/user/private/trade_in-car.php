<?php //$vid = get_post_meta($request['ID'],'vid',true); ?>
<div class="listing-list-loop stm-listing-directory-list-loop stm-isotope-listing-item stm-listing-no-price-labels">
    <div class="image">
    	<a href="<?php echo listing($vid)->link() ?>" class="rmv_txt_drctn" target="_blank">
    		<div class="image-inner">
    			<?php
                $special_car = listing($vid)->is_special;
    			$badge_text = listing($vid)->badge_text;
    			$badge_bg_color = listing($vid)->badge_bg_color;
    			if (!empty($badge_bg_color)) {
    				$badge_bg_color = 'style="background-color:' . $badge_bg_color . '";';
    			} else {
    				$badge_bg_color = '';
    			}

    			if($special_car && !empty($badge_text)): ?>
                    <div class="stm-badge-directory heading-font <?php if(stm_is_car_dealer()) echo "stm-badge-dealer"?>" <?php echo sanitize_text_field($badge_bg_color); ?>>
    					<?php echo esc_attr($badge_text); ?>
                    </div>
    			<?php endif; ?>

    			<?php if ($image = listing($vid)->image()): ?>
    				<?php $plchldr = (stm_is_dealer_two()) ? "plchldr-275.jpg" : 'plchldr350.png'; ?>
                    <img
                        data-original="<?php echo stm_hybrid_image_size($image, 796, 466, true); ?>"
                        src="<?php echo esc_url(get_stylesheet_directory_uri().'/assets/images/' . $plchldr); ?>"
                        class="lazy img-responsive"
                        alt="<?php listing($vid)->title; ?>"
                    />

    			<?php else: $plchldr = (stm_is_dealer_two()) ? "plchldr-275.jpg" : 'plchldr350.png'; ?>
                    <img
                        src="<?php echo esc_url(get_stylesheet_directory_uri().'/assets/images/' . $plchldr); ?>"
                        class="img-responsive"
                        alt="<?php esc_html_e('Placeholder', 'motors'); ?>"
                    />
    			<?php endif; ?>

                <?php if (isset($carSold) && $carSold): ?>
                    <div class="stm-badge-directory heading-font" <?php echo sanitize_text_field($badge_bg_color); ?>>
                        <?php echo esc_html__('Sold', 'motors'); ?>
                    </div>
                <?php endif; ?>
    		</div>
    	</a>
    </div>

    <div class="content">
		<div class="meta-top">
			<?php
			$price = listing($vid)->price_label === "Inquire" ? "Inquire" : stm_listing_price_view(listing($vid)->price);
			$sale_price = stm_listing_price_view(listing($vid)->price_sale);
			?>

			<div class="price" style="z-index:1">
			    <div class="normal-price">
			    	<?php if(!empty($sale_price)): ?>
			    		<div class="regular-price"><?php echo $price; ?></div>
			    		<div class="sale-price"><?php echo $sale_price ?></div>
			    	<?php else: ?>
			        	<span class="heading-font"><?php echo $price; ?></span>
			        <?php endif; ?>
			    </div>
			</div>

			<div class="title heading-font">
			    <a href="<?php echo listing($vid)->link() ?>" class="rmv_txt_drctn" target="_blank">
			        <div><?php echo listing($vid)->title; ?></div>
			    </a>
			</div>
		</div>

		<!--Item parameters-->
		<div class="meta-middle">
			<?php
            $car = listing($vid);
            $attributes = \Stm_Hybrid\Listing::attributes( [ 'useInList' => true ] );
            $stm_car_location = $car->location;

            if (!empty($attributes)): ?>
                <div class="meta-middle-row clearfix">
                    <?php foreach ($attributes as $key => $attribute): ?>
                        <?php
                	    $dataValue = $attribute['type'] === 'term_relation' ? listing($vid)[$key]['title'] : listing($vid)[$key];
                	    if (empty($dataValue)) {
                	        continue;
                        }

                        if (!empty($attribute['icon'])) {
                            $iconClass = 'font-exists';
                        } else {
                            $iconClass = '';
                        } ?>
                        <div class="meta-middle-unit <?php echo $key . ' '; echo $iconClass; ?>">
                            <div class="meta-middle-unit-top">
                                <?php if (!empty($attribute['icon'])): ?>
                                    <div class="icon">
                                        <i class="<?php echo $attribute['icon']; ?>"></i>
                                    </div>
                                <?php endif; ?>
                                <div class="name"><?php echo $attribute['label']; ?></div>
                            </div>
                            <div class="value">
                                <?php if ($key === 'location'): ?>
                                    <div class="stm-tooltip-link"
                                         data-toggle="tooltip"
                                         data-placement="bottom"
                                         data-original-title="<?php echo $dataValue; ?>">
                                        <?php echo $dataValue; ?>
                                    </div>
                                <?php else: ?>
                	                <?php echo $dataValue; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="meta-middle-unit meta-middle-divider"></div>
                        <?php if ($attribute == 'location'): $data_value = ''; ?>
                            <?php if (!empty($stm_car_location) or !empty($distance)): ?>
                                <div class="meta-middle-unit font-exists location">
                                    <div class="meta-middle-unit-top">
                                        <div class="icon"><i class="stm-service-icon-pin_big"></i></div>
                                        <div class="name"><?php esc_html_e('Distance', 'motors'); ?></div>
                                    </div>

                                    <div class="value">
                                        <?php if (!empty($distance)): ?>
                                            <div
                                                class="stm-tooltip-link"
                                                data-toggle="tooltip"
                                                data-placement="bottom"
                                                title="<?php echo $distance; ?>">
                                                <?php echo $distance; ?>
                                            </div>

                                        <?php else: ?>
                                            <div
                                                class="stm-tooltip-link"
                                                data-toggle="tooltip"
                                                data-placement="bottom"
                                                title="<?php echo $stm_car_location; ?>">
                                                <?php echo $stm_car_location; ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>

    	<!--Item options-->
    	<div class="meta-bottom">
            <div class="single-car-actions">
                <ul class="list-unstyled clearfix">

                </ul>
            </div>
    	</div>
    </div>
</div>

<?php
$ratings = stm_get_dealer_marks( get_current_user_id() );
add_action( 'wp_footer', function () {
	require_once get_stylesheet_directory() . '/partials/modals/share-profile.php';
} );

 ?>

<div style="padding-left: 33px; color: white">
	<div class="stm-rate-unit" style="display: inline-block;">
		<div class="stm-rate-inner">
			<div class="stm-rate-not-filled"></div>
			<?php if(!empty($ratings['average_width'])): ?>
				<div class="stm-rate-filled" style="width:<?php echo esc_attr($ratings['average_width']); ?>"></div>
			<?php else: ?>
				<div class="stm-rate-filled" style="width:0%"></div>
			<?php endif; ?>
		</div>
	</div>
	<?php echo sprintf( '%0.1f', $ratings['average'] ) ?>
</div>

<?php $profile_url = get_bloginfo( 'home' ) . '/aircraft-dealer/' . get_the_author_meta('user_nicename',get_current_user_id()) . '/#stm_w_rev'; ?>
<button style="margin-top:20px" class="load-more-btn share-compare"
		data-toggle="modal"
		data-target="#shareModalProfile"
		data-url="<?php echo $profile_url ?>"
		data-message="Please, review my profile: <?php echo $profile_url ?>">
	<i class="fa fa-share-alt"></i> <?php _e("Rate my profile", "motors") ?>
</button>
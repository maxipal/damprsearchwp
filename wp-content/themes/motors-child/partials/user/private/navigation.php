<div class="stm-actions-list heading-font">

	<?php do_action( 'stm_before_account_navigation' ) ?>

	<?php foreach ( stm_account_navigation_items() as $key => $item ) { ?>
		<a href="<?php echo esc_url( $item['url'] ); ?>"
		   class="<?php echo esc_attr( $item['classes'] ) . esc_attr( $item['current'] ? ' active' : '' ); ?>">
			<?php if ( isset( $item['icon'] ) ) { ?><i class="<?php echo $item['icon'] ?>"></i><?php } ?>
			<?php //if ( isset( $item['svg'] ) ) { echo file_get_contents($item['svg'] ); } ?>
			<?php echo $item['label'] ?>
		</a>
	<?php } ?>

	<?php do_action( 'stm_after_account_navigation' ) ?>

</div>

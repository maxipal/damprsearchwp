<?php
$current = stm_account_current_page();
?>

<div class="stm-user-private-main">
	<?php switch ( $current ) {
		case 'inventory':
			stm_hybrid_include( 'private/inventory' );
			break;
		case 'myauction':
			stm_hybrid_include('private/myauction-list');
			break;
		case 'auction':
			stm_hybrid_include('private/auction-list');
			break;
		case 'favourite':
			get_template_part( 'partials/user/private/user-favourite' );
			break;
		case 'trade_in':
			get_template_part( 'partials/user/private/trade_in' );
			break;
		case 'testdrive':
			get_template_part( 'partials/user/private/test-drive' );
			break;
		case 'settings':
			get_template_part( 'partials/user/private/' . ( stm_is_dealer() ? 'dealer-settings' : 'user-settings' ) );
			break;
		case 'become-dealer':
			get_template_part( 'partials/user/private/become-dealer' );
			break;
		default:
			do_action( 'stm_account_custom_page', $current );
			break;
	} ?>
</div>

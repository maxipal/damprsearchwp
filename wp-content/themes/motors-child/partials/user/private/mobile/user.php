<div class="stm-user-mobile-info-wrapper" id="user-mobile-info">
	<div class="stm-login-form-mobile-unregistered">
		<form method="post">
			<input type="hidden" name="redirect_path" value="<?php echo esc_attr( stm_account_url() ) ?>"/>

			<div class="form-group">
				<h4><?php esc_html_e('Login or E-mail', 'motors'); ?></h4>
				<input type="text" name="stm_user_login" placeholder="<?php esc_html_e('Enter login or E-mail', 'motors') ?>"/>
			</div>

			<div class="form-group">
				<h4><?php esc_html_e('Password', 'motors'); ?></h4>
				<input type="password" name="stm_user_password"  placeholder="<?php esc_html_e('Enter password', 'motors') ?>"/>
			</div>

			<div class="form-group form-checker">
				<label>
					<input type="checkbox" name="stm_remember_me" />
					<span><?php esc_html_e('Remember me', 'motors'); ?></span>
				</label>
			</div>
			<?php if(stm_is_rental()): ?><input type="hidden" name="redirect_path" value="<?php echo sanitize_text_field(get_the_permalink( get_option('woocommerce_myaccount_page_id') )); ?>"/><?php endif; ?>
			<?php if(class_exists('PMXI_Plugin')) : ?><input type="hidden" name="current_lang" value="<?php echo ICL_LANGUAGE_CODE; ?>"/><?php endif; ?>
			<input type="submit" value="<?php esc_html_e('Login', 'motors'); ?>"/>
			<span class="stm-listing-loader"><i class="stm-icon-load1"></i></span>
			<a href="<?php echo esc_url(stm_get_author_link('register')); ?>" class="stm_label"><?php esc_html_e('Sign Up', 'motors'); ?></a>

			<?php do_action( 'wordpress_social_login' ); ?>
			<div class="stm-validation-message"></div>
		</form>
	</div>
</div>

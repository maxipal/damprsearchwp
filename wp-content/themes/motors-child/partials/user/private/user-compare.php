<?php ?>
<div class="stm-car-listing-sort-units stm-car-listing-directory-sort-units clearfix private-compare stm-template-listing">
	<div class="stm-listing-directory-title">
		<h4 class="stm-seller-title"><?php esc_html_e('My Compare', 'motors'); ?></h4>
	</div>
    <div class="container">
        <?php
		$user_id = get_current_user_id();
        $compare_meta = get_user_meta($user_id, 'compare', true);

        if(!empty($compare_meta)){
            foreach ($compare_meta as $compare => $plane_ids):
                $plane_link = str_replace(',', '-', $plane_ids);
                $planes = explode(",", $plane_ids);
                $compares = \Stm_Hybrid\Listing::query(
                ['where[id]' => join(',', $planes), 'context'=>'short']
                );
                $planes_link = 'compare/?comp=' . $plane_link . '&comp_title=' . $compare;
                $site_url = get_site_url(null, $planes_link);
                ?>
				<div class="row private-compare-row car-listing-row">
					<div class="col-md-3 private-compare-title">
						<a href="<?php echo $site_url; ?>" class="private-copmare-link"><h3 class="compare-title"><?php echo $compare; ?></h3></a>
						<div class="colored-separator text-left">
							<div class="first-long"></div>
							<div class="last-short"></div>
						</div>
						<span class="remove-compare-row load-more-btn" data-title="<?php echo $compare; ?>">Remove</span>
					</div>

                    <?php foreach ($compares['data'] as $plane): the_listing($plane) ?>
                        <?php if(!empty($plane)): ?>
						<div class="col-md-3 listing-car-item-meta">
							<div class="private-compare-img">
								<a href="<?php echo esc_url(listing()->link); ?>">
									<?php if(listing()->thumbnail): ?>
										<img
											data-original="<?php echo (stm_hybrid_image_size( listing()->thumbnail, 255, 135, true )); ?>"
											src="<?php echo esc_url(stm_hybrid_image_size( listing()->thumbnail, 255, 135, true )); ?>"
											class="lazy img-responsive"
											alt="<?php echo listing()->title ?>"
										/>
									<?php else: ?>
										<img
											src="<?php echo esc_url(get_stylesheet_directory_uri().'/assets/images/plchldr255.png'); ?>"
											class="img-responsive"
											alt="<?php esc_html_e('Placeholder', 'motors'); ?>"
										/>
									<?php endif; ?>
								</a>
								<div class="privar-remove-compare-unlinkable">
										<span
											class="remove-from-private-compare"
											data-id="<?php echo esc_attr(listing()->id); ?>"
											data-title="<?php echo $compare; ?>">
											<i class="stm-service-icon-compare-new"></i>
											<span><?php esc_html_e('Remove from list', 'motors'); ?></span>
										</span>
								</div>
							</div>

							<div class="car-meta-top heading-font clearfix">
                                <?php $price = listing()->price;
                                $sale_price = listing()->price_sale;
                                $car_price_form_label = listing()->price_view; ?>
								<?php if(empty($car_price_form_label)): ?>
									<?php if(!empty($price) and !empty($sale_price) and $price != $sale_price):?>
										<div class="price discounted-price">
											<div class="regular-price"><?php echo esc_attr(stm_listing_price_view($price)); ?></div>
											<div class="sale-price"><?php echo esc_attr(stm_listing_price_view($sale_price)); ?></div>
										</div>
									<?php elseif(!empty($price)): ?>
										<div class="price">
											<div class="normal-price"><?php echo esc_attr(stm_listing_price_view($price)); ?></div>
										</div>
									<?php endif; ?>
								<?php else: ?>
									<div class="price">
										<div class="normal-price"><?php echo esc_attr($car_price_form_label); ?></div>
									</div>
								<?php endif; ?>
							    <div class="car-title" style="overflow:unset;">
							        <?php echo listing()->title ?>
							    </div>
							</div>
						</div>
						<?php endif; ?>
					<?php endforeach; ?>

				</div>
				<?php
			endforeach;
		}
		?>
    </div>
</div>

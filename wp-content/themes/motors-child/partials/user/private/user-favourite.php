<?php
	$list = 'active';
	$grid = '';

	if(!empty($_GET['view']) and $_GET['view'] == 'grid') {
		$list = '';
		$grid = 'active';
	}
?>
<div class="archive-listing-page">
<div class="stm-car-listing-sort-units stm-car-listing-directory-sort-units clearfix">
	<div class="stm-listing-directory-title">
		<h4 class="stm-seller-title"><?php esc_html_e('My Favorites', 'motors'); ?></h4>
	</div>
	<div class="stm-directory-listing-top__right">
		<div class="clearfix">
			<div class="stm-view-by">
				<a href="<?php echo esc_url( add_query_arg( [ 'view' => 'grid' ], stm_account_url( 'favourite' ) ) ); ?>" class="view-grid view-type <?php echo esc_attr($grid); ?>">
					<i class="stm-icon-grid"></i>
				</a>
				<a href="<?php echo esc_url( add_query_arg( [ 'view' => 'list' ], stm_account_url( 'favourite' ) ) ); ?>" class="view-list view-type <?php echo esc_attr($list); ?>">
					<i class="stm-icon-list"></i>
				</a>
			</div>
		</div>
	</div>
</div>
</div>

<?php
$favourites = get_the_author_meta( 'stm_user_favourites', get_current_user_id() );
if ( empty( $favourites ) ) { ?>
	<h4><?php esc_html_e( 'You have not added favorites yet', 'motors' ); ?>.</h4>
	<?php
	return;
}

$favourites = array_unique( explode( ',', $favourites ) );

$results = \Stm_Hybrid\Listing::query([
	'where' => [
		'id' => $favourites,
	],
	'context' => 'list',
]);

$exist_adds = array();
?>

<?php if ( $results['data'] ): ?>
	<div class="<?php if ( $grid == 'active' ) echo 'row'; ?> car-listing-row clearfix">
		<?php
		foreach ( $results['data'] as $listing ):
			the_listing( $listing );
			$exist_adds[] = listing()->id;

			if ( $list == 'active' ) { ?>
				<div class="stm-listing-fav-loop">
					<?php
					if ( listing()->status == 'draft' ) { ?>
						<div class="stm-car-overlay-disabled"></div>
						<div class="stm_edit_pending_car">
							<h4><?php esc_html_e('Disabled', 'motors'); ?></h4>
							<div class="stm-dots"><span></span><span></span><span></span></div>
						</div>
					<?php } elseif ( listing()->status == 'pending' ) { ?>
						<div class="stm-car-overlay-disabled"></div>
						<div class="stm_edit_pending_car">
							<h4><?php esc_html_e('Under review', 'motors'); ?></h4>
							<div class="stm-dots"><span></span><span></span><span></span></div>
						</div>
					<?php }
					stm_hybrid_include( 'loop/list' ); ?>
				</div>
				<?php
			} else {
				stm_hybrid_include( 'loop/grid' );
			}

		endforeach; ?>
	</div>
<?php endif; ?>

<!--Get deleted adds-->
<?php
$deleted_adds = array_diff( $favourites, $exist_adds);
?>

<?php if(!empty($deleted_adds)): ?>
	<div class="stm-deleted-adds">
		<?php foreach($deleted_adds as $deleted_add): ?>
			<?php if($deleted_add != 0): ?>
				<div class="stm-deleted-add">
					<div class="heading-font">
						<i class="fa fa-close stm-listing-favorite" data-id="<?php echo esc_attr($deleted_add); ?>"></i>
						<?php esc_html_e('the Item was removed', 'motors'); ?>
					</div>
				</div>
			<?php endif; ?>
		<?php endforeach; ?>
	</div>
<?php endif; ?>

<script type="text/javascript">
	jQuery(document).ready(function(){
		var $ = jQuery;
		$('.stm-deleted-adds .stm-deleted-add .heading-font .fa-close').on('click', function(){
			$(this).closest('.stm-deleted-add').slideUp();
		});
	});
</script>

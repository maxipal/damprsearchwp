<?php

// $notify = new STMNotification;
// $notify->getAuctionsBids(false);
// $notify->saveType('auction_bids');


?>

<div class="stm-change-block stm-s-result">
	<div class="title">
		<h4 class="stm-seller-title"><?php esc_html_e( 'My Offers', 'motors' ); ?></h4>
	</div>

	<div class="row">
		<div class=" col-md-12 stm-ajax-row" id="accordion" role="tablist" aria-multiselectable="true">
			<ul id="mybits_list" class="panel-group cabinet_list mybits_list mybits_list_status"></ul>
		</div>
	</div>

</div>

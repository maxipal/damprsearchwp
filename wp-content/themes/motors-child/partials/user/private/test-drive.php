
<div class="stm-change-block stm-s-result">
	<div class="title">
		<h4 class="stm-seller-title"><?php esc_html_e( 'Testdrive Requests', 'motors' ); ?></h4>
	</div>

	<div class="row">
		<div class=" col-md-12 stm-ajax-row" id="accordion" role="tablist" aria-multiselectable="true">
			<ul class="panel-group testdrive_in_list">
				<?php
				$user = get_user_by('id',get_current_user_id());
				$from = stm_get_testdrive_from($user->user_email);
				$to = stm_get_testdrive_to($user->user_email);

				// Update view notification
				$notify = new STMNotification;
				$available = $notify->getTestDriveNotify(false);
				$new = $notify->notifies['testdrive'];
				$notify->saveType('testdrive');

				$requests = array();
				foreach ($from as $from_value)
					$requests[$from_value->ID] = array(
						'ID' => $from_value->ID,
						'title' => $from_value->post_title,
						'date' => $from_value->post_date,
					);
				foreach ($to as $to_value)
					$requests[$to_value->ID] = array(
						'ID' => $to_value->ID,
						'title' => $to_value->post_title,
						'date' => $to_value->post_date,
					);
				krsort($requests);

				$count = 0;
				?>
				<?php foreach($requests as $key => $request): ?>
					<?php
					$u_from = get_post_meta($request['ID'], 'u_from', true);
					$u_to = get_post_meta($request['ID'], 'u_to', true);
					$uname_from = '';
					$uname_to = '';
					$user_from = get_user_by('email',$u_from);
					$vid = get_post_meta($request['ID'], 'vid', true);
					if(!$user_from){
						$uname_from = __("Unregistered user","motors") . " ($u_from)";
					}else{
						$uname_from = stm_display_user_name_alt($user_from->ID);
					}
					$user_to = get_user_by('email',$u_to);
					if(!$user_to){
						$uname_to = __("Unregistered user","motors") . " ($u_to)";
					}else{
						$uname_to = stm_display_user_name_alt($user_to->ID);
					}
					$confirm = get_post_meta($request['ID'],'confirm', true);
					?>
					<li class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a
									class="testdrive_in_title"
									data-toggle="collapse"
									data-parent="#accordion"
									href="#collapse_<?php echo $request['ID'] ?>">
										<?php
										if($u_from == $user->user_email)
											printf( __( 'Request to %s', 'motors' ), $uname_to );
										else
											printf( __( 'Request from %s', 'motors' ), $uname_from );
										?>
									<?php if($confirm !== ''): ?>
										<?php if($confirm == '1'): ?>
											<span><?php _e("Accepted","motors") ?></span>
										<?php else: ?>
											<span class="decline"><?php _e("Cancelled","motors") ?></span>
										<?php endif ?>
									<?php endif; ?>
								</a>
								<?php if($available && $count < $available && $count !== 0) echo '<span class="notify">!</span>' ?>
							</h4>
						</div>
						<div id="collapse_<?php echo $request['ID'] ?>" class="panel-collapse collapse <?php if($count == 0) echo 'in' ?>" data-id="<?php echo $request['ID'] ?>">
							<div class="panel-body">
								<div class="tabs_information width50">

									<div>
										<h5><?php _e("Vehicle Information","motors") ?></h5>
										<div>
											<a target="_blank" href="<?php echo listing($vid)->link ?>"><?php echo listing($vid)->title ?></a>
										</div>
										<?php $confirm = get_post_meta($request['ID'], 'confirm', true); ?>
										<?php $reason = get_post_meta($request['ID'], 'reason', true); ?>
										<?php if($u_to == $user->user_email && $confirm == ''): ?>
											<div class="form_wrap">
												<span><?php _e("Best time","motors") ?>:&nbsp;</span>
												<strong><input class="stm-date-timepicker" type="text" value="<?php echo date("Y/m/d H:i",get_post_meta($request['ID'], 'best_time', true)) ?>"></strong>
												<br>
												<textarea placeholder="<?php _e("Reason","motors") ?>"><?php echo $reason ?></textarea>
											</div>
										<?php else: ?>
											<div>
												<span><?php _e("Best time","motors") ?>:&nbsp;</span>
												<strong><?php echo date("d/m/Y H:i",get_post_meta($request['ID'], 'best_time', true)) ?></strong>
											</div>
											<?php if(!empty($reason)): ?>
												<div>
													<span><?php _e("Message","motors") ?>:&nbsp;</span>
													<strong><?php echo $reason ?></strong>
												</div>
											<?php endif; ?>
										<?php endif ?>
									</div>

									<div>
										<h5><?php _e("Contact Details","motors") ?></h5>
											<div>
												<span><?php _e("User Name","motors") ?>:&nbsp;</span>
												<strong><?php echo get_post_meta($request['ID'], 'uname', true) ?></strong>
											</div>
											<div>
												<span><?php _e("User Phone","motors") ?>:&nbsp;</span>
												<strong><?php echo get_post_meta($request['ID'], 'phone', true) ?></strong>
											</div>
											<div>
												<span><?php _e("User Email","motors") ?>:&nbsp;</span>
												<strong><?php echo get_post_meta($request['ID'], 'uemail', true) ?></strong>
											</div>
									</div>


								</div>

								<div class="testdrive_responce"></div>


								<?php if($u_to == $user->user_email && $confirm == ''): ?>
									<div class="testdrive_btn_wrap">
										<a class="btn-action testdrive_accept" href="#" onclick="return false;"><span><?php _e("Accept", "motors") ?></span></a>
										<a class="btn-action testdrive_decline btn-danger" href="#" onclick="return false;"><span><?php _e("Decline", "motors") ?></span></a>
									</div>
								<?php endif; ?>
							</div>
						</div>
					</li>
					<?php $count++; ?>
				<?php endforeach ?>

				<?php if(!count($requests)): ?>
					<h4 style="text-align:center;margin-top:40px;"><?php _e("Requests not found.","motors") ?></h4>
				<?php endif; ?>

			</ul>
		</div>
	</div>

</div>





<style type="text/css">
	.testdrive_in_list{
		margin: 0;
		padding: 0;

	}
	.testdrive_in_list li{
		list-style: none;
		padding: 0;
		margin: 0;
		display: inline-block;
		width: 100%;
		position: relative;

	}
	.testdrive_in_list li + li{
		margin-top: 10px !important;
	}
	.testdrive_in_list li .testdrive_in_title{
		vertical-align: middle;
		text-transform: uppercase;
		font-size: 14px;
		color: #333;
		font-weight: 700;
	}
	.testdrive_in_list li .testdrive_in_title:hover{
		color: #1bc744;
	}
	.testdrive_in_list li a.btn-action{
		border: 1px solid #6c98e1;
		background-color: #6c98e1;
		text-align: center;
		display: inline-block;
		vertical-align: middle;
		color: #fff;
		min-width: 140px;
		text-transform: uppercase;
		font-weight: 700;
		font-size: 12px;
		padding: 6px 20px 6px 20px;
		box-shadow: 0 2px 0 #567ab4;
		border-radius: 3px;
		font-family: 'Montserrat';
	}
	.testdrive_in_list li a.btn-action.active{
		color: #fff;
		background-color: #1bc744;
		border: 1px solid #1bc744;
		box-shadow: 0 2px 0 #169f36;
	}
	.testdrive_in_list li a.btn-action.btn-danger{
		border: 1px solid #c30019;
	    background-color: #c30019;
	    box-shadow: 0 2px 0 #c30019;
	}
	.testdrive_in_list li a:hover,
	.testdrive_in_list li a:focus{
		text-decoration: none;
	}

	@media(max-width:767px){
		.testdrive_in_list li{
			padding: 15px 120px 5px 10px;
		}
		.testdrive_in_list li a.btn-action{
			min-width: 110px;
			padding: 6px 5px 6px 5px;
			top: 20px;
		}
	}
</style>

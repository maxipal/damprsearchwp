<?php wp_enqueue_script('trade-in') ?>

<div class="stm-change-block stm-s-result">
	<div class="title">
		<h4 class="stm-seller-title"><?php esc_html_e( 'Trade-In Requests', 'motors' ); ?></h4>
	</div>

	<div class="row">
		<div class=" col-md-12 stm-ajax-row" id="trade_in_app" role="tablist" aria-multiselectable="true">
			<ul class="panel-group trade_in_list">
				<?php

				// Update view notification
				$notify = new STMNotification;
				$available = $notify->getTradeInNotify(false);
				$mess = $notify->getTradeInMessages();
				$trade = $notify->notifies['tradein'];
				$notify->saveType('tradein');
				$notify->saveType('tradein_mess');

				$user = get_user_by('id',get_current_user_id());
				$from = stm_get_tradein_from($user->user_email);
				$to = stm_get_tradein_to($user->user_email);

				$requests = array();
				foreach ($from as $from_value)
					$requests[$from_value->ID] = array(
						'ID' => $from_value->ID,
						'title' => $from_value->post_title,
						'date' => $from_value->post_date,
					);
				foreach ($to as $to_value)
					$requests[$to_value->ID] = array(
						'ID' => $to_value->ID,
						'title' => $to_value->post_title,
						'date' => $to_value->post_date,
					);
				krsort($requests);

				$groups = stm_tradein_fields();
				$count = 0;
				?>

				<?php foreach($requests as $key => $request): ?>
					<?php
					$u_from = get_post_meta($request['ID'], 'u_from', true);
					$u_to = get_post_meta($request['ID'], 'u_to', true);

					$dialog = get_post_meta($request['ID'], 'dialog', true);
					$status = get_post_meta($request['ID'],'status', true);

					$uname_from = '';
					$uname_to = '';
					$user_from = get_user_by('email',$u_from);
					if(!$user_from){
						$uname_from = __("Unregistered user","motors") . " ($u_from)";
					}else{
						$uname_from = stm_display_user_name_alt($user_from->ID);
					}
					$user_to = get_user_by('email',$u_to);
					if(!$user_to){
						$uname_to = __("Unregistered user","motors") . " ($u_to)";
					}else{
						$uname_to = stm_display_user_name_alt($user_to->ID);
					}

					$vid = get_post_meta($request['ID'],'vid',true);
					?>
					<li class="panel panel-default" data-id="<?php echo $request['ID'] ?>">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a
									class="trade_in_title"
									data-toggle="collapse"
									data-parent="#accordion"
									href="#collapse_<?php echo $key ?>">
										<?php
										if($u_from == $user->user_email)
											printf( __( 'Request to %s', 'motors' ), $uname_to );
										else
											printf( __( 'Request from %s', 'motors' ), $uname_from );
										?>
										<?php if(!empty($status)): ?>
											<?php if($status == 'accept'): ?>
												<span><?php _e("Accepted","motors") ?></span>
											<?php else: ?>
												<span class="decline"><?php _e("Cancelled","motors") ?></span>
											<?php endif ?>
										<?php endif; ?>
								</a>

								<?php if($available && $count < $available && $count !== 0) echo '<span class="notify">!</span>' ?>

								<?php if(isset($mess[$request['ID']]) && count($mess[$request['ID']])): ?>
									<span class="notify"><?php echo count($mess[$request['ID']]) ?></span>
								<?php endif; ?>
							</h4>
						</div>
						<div id="collapse_<?php echo $key ?>" class="panel-collapse collapse <?php if($count == 0) echo 'in' ?>">
							<div class="panel-body">


								<?php if(!empty($vid)): ?>
									<?php include( locate_template( 'partials/user/private/trade_in-car.php', false, false ) ); ?>
								<?php endif; ?>


								<div class="tabs_information">
									<div>
										<h5><?php _e("Car Information","motors") ?></h5>
										<?php foreach($groups['group1'] as $fname => $fields): ?>
											<?php $val = get_post_meta($request['ID'], $fname, true); ?>
											<div>

												<?php if($fname == 'videos'): ?>
													<span><?php echo $fields ?>:&nbsp;</span>
													<strong>
														<?php if(!empty($val)): ?>
															<a href="<?php echo $val; ?>" target="_blank"><?php _e("Video","motors") ?></a>
														<?php else: ?>
															<?php _e("None","motors") ?>
														<?php endif; ?>
													</strong>
												<?php elseif($fname == 'images'): ?>

												<?php else: ?>
													<span><?php echo $fields ?>:&nbsp;</span>
													<strong><?php echo $val; ?></strong>
												<?php endif ?>
											</div>
										<?php endforeach ?>
									</div>

									<div>
										<h5><?php _e("Vehicle Condition","motors") ?></h5>
										<?php foreach($groups['group2'] as $fname => $fields): ?>
											<div>
												<?php $val = get_post_meta($request['ID'], $fname, true); ?>
												<span><?php echo $fields['title'] ?></span>
												<div><strong><?php if(isset($fields['options'][$val])) echo $fields['options'][$val]; ?></strong></div>
											</div>
										<?php endforeach ?>
									</div>

									<div>
										<h5><?php _e("Contact Details","motors") ?></h5>
										<?php foreach($groups['group3'] as $fname => $fields): ?>
											<div>
												<?php $val = get_post_meta($request['ID'], $fname, true); ?>
												<span><?php echo $fields ?>:&nbsp;</span>
												<strong><?php echo $val; ?></strong>
											</div>
										<?php endforeach ?>
									</div>
								</div>



								<?php if(!empty($groups['group1']['images'])): ?>
									<div class="stm_tradein_images_carousel">
										<?php $images = (array)get_post_meta($request['ID'], 'images', true); ?>
										<ul>
											<?php foreach($images as $image): ?>
												<?php $full = wp_get_attachment_image_src($image, 'full') ?>
												<li>
													<a href="<?php echo $full[0] ?>" class="stm_fancybox" rel="stm-car-gallery">
														<?php echo wp_get_attachment_image($image,'thumbnail') ?>
													</a>
												</li>
											<?php endforeach; ?>
										</ul>
									</div>
								<?php endif; ?>

								<?php if(!empty($dialog)): ?>
									<div class="tradein_dialog_wrap">
										<ul>
											<?php foreach($dialog as $row):
												if(empty($row['id'])) continue;
												if(!isset($row['date'])) continue;
												?>
												<li class="<?php if((int)$row['id'] == get_current_user_id()) echo 'self' ?>">
													<span title="<?php echo __('Date added','motors').': '.date('d.m.Y H:i:s',$row['date']) ?>"><?php echo $row['message'] ?></span>
												</li>
											<?php endforeach; ?>
										</ul>
									</div>
								<?php endif; ?>

								<div class="tradein_btn_wrap">
									<div class="wrap_dialog">
										<div class="wrap_dialog_input">
											<textarea name="dialog" class="form-control" v-model="typeMessage"></textarea>
										</div>
										<a class="btn-action" v-on:click="sendMessage" href="#" onclick="return false;">
											<span><?php _e("Reply", "motors") ?></span>
											<div class="spinner">
												<div class="rect1"></div>
												<div class="rect2"></div>
												<div class="rect3"></div>
												<div class="rect4"></div>
												<div class="rect5"></div>
											</div>
										</a>
									</div>


									<?php if($u_to == $user->user_email && $status == ''): ?>
										<a class="btn-action btn-accept" href="#" v-on:click="setAccept" onclick="return false;">
											<span><?php _e("Accept", "motors") ?></span>
											<div class="spinner">
												<div class="rect1"></div>
												<div class="rect2"></div>
												<div class="rect3"></div>
												<div class="rect4"></div>
												<div class="rect5"></div>
											</div>
										</a>
										<a class="btn-action btn-reject" href="#" v-on:click="setReject" onclick="return false;">
											<span><?php _e("Reject", "motors") ?></span>
											<div class="spinner">
												<div class="rect1"></div>
												<div class="rect2"></div>
												<div class="rect3"></div>
												<div class="rect4"></div>
												<div class="rect5"></div>
											</div>
										</a>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</li>
					<?php $count++ ?>
				<?php endforeach ?>

				<?php if(!count($requests)): ?>
					<h4 style="text-align:center;margin-top:40px;"><?php _e("Requests not found.","motors") ?></h4>
				<?php endif; ?>

			</ul>
		</div>
	</div>

</div>




<style type="text/css">
	.trade_in_list{
		margin: 0 0 40px;
		padding: 0;
	}
	.trade_in_list li{
		list-style: none;
		padding: 0;
		margin: 0;
		display: inline-block;
		width: 100%;
		position: relative;

	}
	.trade_in_list > li + li{
		margin-top: 10px !important;
	}
	.trade_in_list li .trade_in_title{
		vertical-align: middle;
		text-transform: uppercase;
		font-size: 14px;
		color: #333;
		font-weight: 700;
	}
	.trade_in_list li .trade_in_title:hover{
		color: #1bc744;
	}
	.trade_in_list li a.btn-action{
		border: 1px solid #6c98e1;
		background-color: #6c98e1;
		text-align: center;
		display: inline-block;
		vertical-align: middle;
		color: #fff;
		min-width: 140px;
		text-transform: uppercase;
		font-weight: 700;
		font-size: 12px;
		padding: 6px 20px 6px 20px;
		box-shadow: 0 2px 0 #567ab4;
		border-radius: 3px;
		font-family: 'Montserrat';
	}
	.trade_in_list li a.btn-action.active{
		color: #fff;
		background-color: #1bc744;
		border: 1px solid #1bc744;
		box-shadow: 0 2px 0 #169f36;
	}
	.trade_in_list li a:hover,
	.trade_in_list li a:focus{
		text-decoration: none;
	}
	.tradein_btn_wrap{
		margin-top: 15px;
	}
	.tradein_btn_wrap .wrap_dialog{
		position: relative;
		margin-bottom: 15px;
	}
	.tradein_btn_wrap .wrap_dialog .wrap_dialog_input{
		padding-right: 150px;
	}
	.tradein_btn_wrap .wrap_dialog textarea{
		height: 38px;
		font-size: 12px;
    	line-height: 15px;
	}
	.tradein_btn_wrap .wrap_dialog a{
		position: absolute;
		right: 0;
		top: 0;
	}

	.tradein_dialog_wrap ul{
		padding: 5px 10px;
		border: 1px solid #dadada;
		margin: 10px 0;
		max-height: 140px;
		overflow: auto;
		box-shadow: 0 0 4px rgba(0,0,0,0.1) inset;
	}
	.tradein_dialog_wrap ul li{
		padding: 4px 0;
	}
	.tradein_dialog_wrap ul li span{
		float: right;
		border: 1px solid #dadada;
		padding: 3px 10px;
		font-size: 12px;
		line-height: normal;
		border-radius: 7px 7px 0px 7px;
		display: inline-block;
		background-color: #f0f5f0;
	}
	.tradein_dialog_wrap ul li.self span{
		float: left;
		border-radius: 7px 7px 7px 0px;
		background-color: #ebeefd;
	}

	@media(max-width:767px){
		.trade_in_list li{
			padding: 0;
		}
		.trade_in_list li .panel-body{
			padding-left: 10px;
			padding-right: 10px;
		}
		.trade_in_list li a.btn-action{
			min-width: 110px;
			padding: 6px 5px 6px 5px;
			top: 0px;
		}
		.tradein_btn_wrap .wrap_dialog .wrap_dialog_input {
		    padding-right: 125px;
		}
	}
</style>

<?php
extract( stm_private_user_dropdown_data() );
/**
 * @var WP_User $user
 * @var string $user_name
 * @var string $iconpath
 * @var integer $count_listings
 * @var integer $count_bids
 * @var integer $count_auctions
 * @var integer $count_favorites
 * @var integer $notify_auctions
 * @var integer $notify_bids
 * @var integer $notify_tradein
 * @var integer $notify_testdrive
 * @var array $account_data
 */
?>
<div class="lOffer-account-dropdown">
	<a href="<?php echo esc_url(add_query_arg(array('page' => 'settings'), stm_get_author_link(''))); ?>" class="settings">
		<i class="stm-settings-icon stm-service-icon-cog"></i>
	</a>
	<div class="name">
		<a href="<?php echo esc_url( stm_account_url() ) ?>"><?php echo $user_name ?></a>
	</div>
	<ul class="account-list">
		<li>
			<a href="<?php echo esc_url( stm_account_url( 'inventory' ) ) ?>">
<!--				<img src="--><?php //echo $iconpath ?><!--/avatar-line.svg" />-->
				<?php _e( 'My items', 'motors' ) ?>
				(<span><?php echo $count_listings ?></span>)
			</a>
		</li>
		<?php if(LISTING_ENABLE_AUCTION): ?>
			<li class="notify_bids">
				<a href="<?php echo esc_url( stm_account_url( 'myauction' ) ) ?>">
<!--					<img src="--><?php //echo $iconpath ?><!--/features.svg" />-->
					<?php _e( 'My Auction', 'motors' ) ?>
					<?php if ( $count_bids ) { ?>(<span><?php echo $count_bids ?></span>)<?php } ?>
					<?php echo $notify_bids ? '<span class="notify">' . $notify_bids . '</span>' : '' ?>
				</a>
			</li>
			<?php if ( stm_is_dealer() ) :?>
				<li class="notify_auction">
					<a href="<?php echo esc_url( stm_account_url( 'auction' ) ) ?>">
<!--						<img src="--><?php //echo $iconpath ?><!--/tag-line.svg" />-->
						<?php _e( 'Auctions', 'motors' ) ?>
						<?php echo $notify_auctions ? '<span class="notify">' . $notify_auctions . '</span>' : '' ?>
					</a>
				</li>
			<?php endif ?>
		<?php endif ?>
		<li class="stm-my-favourites">
			<a href="<?php echo esc_url( stm_account_url( 'favourite' ) ) ?>">
<!--				<img src="--><?php //echo $iconpath ?><!--/carfront-line.svg" />-->
				<?php _e( 'Favorites', 'motors' ) ?>
				(<span><?php echo $count_favorites ?></span>)
			</a>
		</li>
		<?php if(TRADE_IN_PAGE): ?>
			<li class="notify_trade_in">
				<a href="<?php echo esc_url( stm_account_url( 'trade_in' ) ) ?>">
<!--					<img src="--><?php //echo $iconpath ?><!--/searchalt-line.svg" />-->
					<?php _e( 'Trade-In Requests', 'motors' ) ?>
					<?php echo $notify_tradein ? '<span class="notify">' . $notify_tradein . '</span>' : '' ?>
				</a>
			</li>
		<?php endif ?>
		<?php if(TESTDRIVE_PAGE): ?>
			<li class="notify_testdrive">
				<a href="<?php echo esc_url( stm_account_url( 'testdrive' ) ) ?>">
<!--					<img src="--><?php //echo $iconpath ?><!--/searchalt-line.svg" />-->
					<?php _e( 'Test-Drive Requests', 'motors' ) ?>
					<?php echo $notify_testdrive ? '<span class="notify">' . $notify_testdrive . '</span>' : '' ?>
				</a>
			</li>
		<?php endif ?>
	</ul>
	<a href="<?php echo esc_url( wp_logout_url( home_url() ) ) ?>" class="logout">
		<i class="fa fa-power-off"></i> <?php _e( 'Logout', 'motors' ) ?>
	</a>
</div>

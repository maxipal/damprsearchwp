<div class="stm-user-private <?php echo stm_is_dealer() ? 'stm-dealer-private' : '' ?>">
	<?php if ( stm_is_dealer() ) {
		get_template_part( 'partials/user/private/dealer-sidebar' );
	} else {
		get_template_part( 'partials/user/private/user-sidebar' );
	} ?>
</div>

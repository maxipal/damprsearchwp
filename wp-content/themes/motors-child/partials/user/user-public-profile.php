<?php
$user_page = get_queried_object();
$user_id = $user_page->data->ID;
$user_image = get_the_author_meta('stm_user_avatar', $user_id);
$image = '';
$user_show_mail = '';
$user_show_mail = get_the_author_meta('stm_show_email', $user_id);
$user_phone = get_the_author_meta('stm_phone', $user_id);

if (!empty($user_image)) {
	$image = $user_image;
}

$query = (function_exists('stm_user_listings_query')) ? stm_user_listings_query($user_id, 'publish') : null;

$sidebar = get_theme_mod('user_sidebar', '1725');
$sidebar_position = get_theme_mod('user_sidebar_position', 'right');

$layout = stm_sidebar_layout_mode($sidebar_position, $sidebar);
?>
<div class="container stm-user-public-profile">
	<div class="row">
		<?php echo stm_do_lmth($layout['content_before']); ?>
		<div class="clearfix stm-user-public-profile-top">
			<div class="stm-user-name">
				<div class="image">
					<?php if (!empty($image)): ?>
						<img src="<?php echo esc_url($image) ?>"/>
					<?php else: ?>
						<i class="stm-service-icon-user"></i>
					<?php endif; ?>
				</div>
				<div class="title">
					<h4><?php echo esc_attr(stm_display_user_name($user_page->ID)); ?></h4>
					<div class="stm-title-desc">
						<?php esc_html_e('Private Seller', 'motors'); ?>
					</div>
				</div>
			</div>

			<div class="stm-user-data-right">
				<?php if (!empty($user_page->data->user_email) and $user_show_mail == 'show'): ?>
					<div class="stm-user-email">
						<i class="fa fa-envelope-o"></i>
						<div class="mail-label"><?php esc_html_e('Seller email', 'motors'); ?></div>
						<a href="mailto:<?php echo esc_attr($user_page->data->user_email); ?>"
						class="mail h4"><?php echo esc_attr($user_page->data->user_email); ?></a>
					</div>
				<?php endif; ?>
				<?php if (!empty($user_phone)): ?>
					<div class="stm-user-phone">
						<div class="listing-archive-dealer-info">
							<div class="dealer-info-block">
								<div class="dealer-information">
									<?php if (!empty($user_phone)): ?>
										<div class="title" style="margin-bottom: 5px;">
											<i class="stm-service-icon-phone"></i>
											<span><?php esc_html_e('Phone Number', 'motors'); ?> </span>
										</div>
										<div class="phone">
											<?php echo substr_replace($user_phone, "*******", 3, strlen($user_phone)); ?>
										</div>
										<span class="stm-show-number"	data-id="<?php echo esc_attr($user_id); ?>">
											<?php echo esc_html__("Show number", "motors"); ?>
										</span>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
					<style>
						.stm-user-phone .phone a,
						.stm-user-phone .phone {
							color: #232628;
							font-size: 14px !important;
							font-weight: bold;
						}

						.stm-user-phone .phone {

						}
					</style>
				<?php endif; ?>
			</div>
		</div> <!-- top profile -->

		<div class="dealer-aircrafts stm-user-public-listing" data-view="list">
			<?php stm_hybrid_include('vue-dealer_inventory', array(
				'title' => esc_html__('Sellers Inventory', 'motors'),
			)); ?>
		</div>

		<?php echo stm_do_lmth($layout['content_after']); ?>

		<?php echo stm_do_lmth($layout['sidebar_before']);
		if (!empty($sidebar)):
			$user_sidebar = get_post($sidebar);

			if (!empty($user_sidebar) and !is_wp_error($user_sidebar)):

				?>
				<div class="stm-user-sidebar">
					<?php echo apply_filters('the_content', $user_sidebar->post_content); ?>
					<style type="text/css">
						<?php echo get_post_meta( $user_sidebar, '_wpb_shortcodes_custom_css', true ); ?>
				  	</style>

					<script type="text/javascript">
						jQuery(window).load(function() {
							var $ = jQuery;
							var inputAuthor = '<input type="hidden" value="<?php echo esc_attr($user_page->ID); ?>" name="stm_changed_recepient"/>';
							$('.stm_listing_car_form form').append(inputAuthor);
						});
					</script>
				</div>
			<?php endif; ?>
		<?php endif; ?>
		<?php echo stm_do_lmth($layout['sidebar_after']); ?>
	</div>
</div>

<?php
$similar = stm_hybrid_similar( listing() );

if ( empty( $similar ) ) {
	return;
} ?>

<div class="stm-similar-cars-units">
	<?php foreach ($similar as $listing ) : ?>
		<a href="<?php echo $listing->link(); ?>" class="stm-similar-car clearfix">
			<?php if ( $image = $listing->thumbnail ): ?>
				<div class="image">
					<img src="<?php echo stm_hybrid_image_size( $image, 350, 356, true ); ?>" class="img-responsive wp-post-image" alt="<?php echo $listing->title; ?>">
				</div>
			<?php endif; ?>
			<div class="right-unit">
				<div class="title"><?php echo $listing->title; ?></div>

				<?php
				$user_added_by = $listing->author_id;
				if ( ! empty( $user_added_by ) ) {
					$user_exist = get_userdata( $user_added_by );
				}
				?>
				<div class="stm-dealer-name">
					<?php if ( ! empty( $user_exist ) and $user_exist ): ?>
						<?php stm_display_user_name( $user_added_by ); ?>
					<?php endif; ?>
				</div>
				<div class="clearfix">
					<?php if ( ! empty( $listing->price ) ): ?>
						<div class="stm-price heading-font"><?php echo( $listing->price_view ); ?></div>
					<?php else: ?>
						<div class="stm-price heading-font"><?php _e("With agreement","motors"); ?></div>
					<?php endif; ?>
					<?php if ( ! empty( $value = $listing->body ) ): ?>
						<div class="stm-car-similar-meta">
							<i class="stm-service-icon-body_type"></i>
							<span><?php echo esc_attr( $value ); ?></span>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</a>
	<?php endforeach; ?>
</div>

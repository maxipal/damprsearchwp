<?php

$user_added_by = listing()->author_id;
$showNumber = get_theme_mod("stm_show_number", false);
$showMail = (int)get_option('show_emails_global', false);
$show_hide_email = esc_attr( get_the_author_meta( 'show_hide_email', $user_added_by ) );

if(!empty($user_added_by)):

	$user_data = get_userdata($user_added_by);


	if($user_data):

		$user_fields = stm_get_user_custom_fields($user_added_by);
		$is_dealer = stm_is_dealer($user_added_by);

		if($is_dealer):
			$ratings = stm_get_dealer_marks($user_added_by);
			?>

			<div class="stm-listing-car-dealer-info">
				<a class="stm-no-text-decoration" href="<?php echo esc_url(stm_get_author_link($user_added_by)); ?>"><h3 class="title"><?php stm_display_user_name($user_added_by); ?></h3></a>
				<div class="clearfix">
					<div class="dealer-image">
						<div class="stm-dealer-image-custom-view">
							<a href="<?php echo esc_url(stm_get_author_link($user_added_by)); ?>">
								<?php if(!empty($user_fields['logo'])): ?>
									<img src="<?php echo esc_url($user_fields['logo']); ?>" />
								<?php else: ?>
									<img src="<?php stm_get_dealer_logo_placeholder(); ?>" />
								<?php endif; ?>
							</a>
						</div>
					</div>
				</div>

				<div class="dealer-contacts">
					<?php if(!empty($user_fields['phone'])): ?>
						<div class="dealer-contact-unit phone">
							<i class="stm-service-icon-phone_2"></i>
                            <?php if($showNumber ) : ?>
                                <div class="phone heading-font">
                                    <a href="tel:<?php echo $user_fields['phone']; ?>"><?php echo $user_fields['phone']; ?></a>
                                </div>
                            <?php else : ?>
                                <div class="phone heading-font">
                                    <?php echo substr_replace($user_fields['phone'], "*******", 3, strlen($user_fields['phone'])); ?>
                                </div>
                                <span class="stm-show-number" data-id="<?php echo $user_fields['user_id']; ?>"><?php echo esc_html__("Show number", "motors"); ?></span>
                            <?php endif; ?>
						</div>
					<?php endif; ?>
					<?php if(!empty($user_fields['location'])): ?>
						<div class="dealer-contact-unit address">
							<i class="stm-service-icon-pin_2"></i>
							<div class="address"><?php echo esc_attr($user_fields['location']); ?></div>
						</div>
					<?php endif; ?>
					<?php if(!empty($user_fields['email']) and !empty($user_fields['show_mail']) and $user_fields['show_mail'] == 'show'): ?>
						<div class="dealer-contact-unit mail">
							<i class="fa fa-envelope-o"></i>
							<div class="stm-label"><?php esc_html_e('Seller Email', 'motors'); ?></div>
							<div class="address"><a class="heading-font" href="mailto:<?php echo esc_attr($user_fields['email']); ?>"><?php echo esc_attr($user_fields['email']); ?></a></div>
						</div>
					<?php endif; ?>
				</div>
			</div>

		<?php else: ?>
			<div class="stm-listing-car-dealer-info stm-common-user">

				<div class="clearfix stm-user-main-info-c">
					<div class="image">
						<a href="<?php echo esc_url(stm_get_author_link($user_added_by)); ?>">
							<?php if(!empty($user_fields['image'])): ?>
								<img src="<?php echo esc_url($user_fields['image']); ?>" />
							<?php else: ?>
								<div class="no-avatar">
									<i class="stm-service-icon-user"></i>
								</div>
							<?php endif; ?>
						</a>
					</div>
					<a class="stm-no-text-decoration" href="<?php echo esc_url(stm_get_author_link($user_added_by)); ?>">
						<h3 class="title"><?php stm_display_user_name($user_added_by); ?></h3>
						<div class="stm-label"><?php esc_html_e('Private Seller', 'motors'); ?></div>
					</a>
				</div>

				<div class="dealer-contacts">
					<?php if(!empty($user_fields['phone'])): ?>
						<div class="dealer-contact-unit phone">
							<i class="stm-service-icon-phone_2"></i>
                            <?php if($showNumber ) : ?>
                                <div class="phone heading-font">
                                    <a href="tel:<?php echo $user_fields['phone']; ?>"><?php echo $user_fields['phone']; ?></a>
                                </div>
                            <?php else : ?>
                                <div class="phone heading-font"><?php echo substr_replace($user_fields['phone'], "*******", 3, strlen($user_fields['phone'])); ?></div>
                                <span class="stm-show-number" data-id="<?php echo $user_fields['user_id']; ?>"><?php echo esc_html__("Show number", "motors"); ?></span>
                            <?php endif; ?>
						</div>
					<?php endif; ?>
					<?php if(!empty($user_fields['email']) and !empty($user_fields['show_mail']) and $user_fields['show_mail'] == 'show'): ?>
						<?php if($showMail || $show_hide_email): ?>
							<div class="dealer-contact-unit mail">
								<i class="fa fa-envelope-o"></i>
								<div class="stm-label"><?php esc_html_e('Seller Email', 'motors'); ?></div>
								<div class="address"><a class="heading-font" href="mailto:<?php echo esc_attr($user_fields['email']); ?>"><?php echo esc_attr($user_fields['email']); ?></a></div>
							</div>
						<?php endif; ?>
					<?php endif; ?>
				</div>
			</div>

		<?php endif; ?>
	<?php endif; ?>
<?php endif; ?>

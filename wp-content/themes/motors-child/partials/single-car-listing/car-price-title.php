<?php
$price = get_post_meta(get_the_ID(), 'price', true);
$sale_price = get_post_meta(get_the_ID(), 'sale_price', true);
$car_price_form_label = get_post_meta(get_the_ID(), 'car_price_form_label', true);

if(empty($price) and !empty($sale_price)) {
	$price = $sale_price;
}

if(!empty($price) and !empty($sale_price)) {
	$price = $sale_price;
}
?>

<div class="stm-listing-single-price-title heading-font clearfix">
	<?php if(!empty($price)): ?>
		<?php if(!empty($car_price_form_label)): ?>
			<div class="price"><?php echo esc_attr($car_price_form_label); ?></div>
		<?php else: ?>
			<div class="price"><?php echo stm_listing_price_view_custom($price, get_the_ID()); ?></div>
		<?php endif; ?>
	<?php endif; ?>
	<h1 class="title" style="margin-bottom: 0; line-height: 20px;" itemprop="name">
		<?php echo stm_generate_title_from_slugs(get_the_ID(), get_theme_mod('show_generated_title_as_label', false)); ?>
	</h1>

	<?php $price_desc = get_post_meta(get_the_id(), 'stm_price_description', true) ?>
	<?php if(!empty($price_desc)): ?>
		<div class="stm_wrap_pricedesc"><?php echo $price_desc ?></div>
	<?php endif; ?>
</div>

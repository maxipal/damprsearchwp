<?php
//update kpi
//	$dealerID = get_the_author_meta('ID');
//	$count = get_user_meta($dealerID, 'air_user_expose_views', true );
//	    if ( !isset($count) ) $count == 0;
//	    $count++;
//	update_user_meta( $dealerID, 'air_user_expose_views', $count );  //totals of stm_car_views
$content = get_the_content();
$content = str_replace( 'Seller Note', 'Seller\'s Notes', $content );
?>

<div class="row">
	<div class="col-md-9 col-sm-12 col-xs-12">
		<div
			class="single-listing-car-inner"
			data-dealer_ID="<?php echo get_the_author_meta('ID'); ?>"
			data-plane_ID="<?php echo $post->ID ?>">
			<?php //Title and price
				get_template_part('partials/single-car-listing/car-price-title');
			?>

			<?php //Action buttons
				get_template_part('partials/single-car-listing/car-actions');
			?>

			<?php //Gallery
				get_template_part('partials/single-car-listing/car-gallery');
			?>


			<?php if ( strpos( $content, 'Seller\'s Notes' ) === false ) { ?>
			<div class="stm-car-listing-data-single stm-border-top-unit ">
				<div class="title heading-font"><?php esc_html_e("Seller's Notes", 'motors'); ?></div>
			</div>
			<?php } ?>
			<div>
				<?php echo apply_filters( 'the_content', $content ) ?>
			</div>


			<?php //CAR DATA
				$data = stm_get_single_car_listings();
				if(!empty($data)):
			?>
				<div class="stm-car-listing-data-single stm-border-top-unit">
					<div class="title heading-font"><?php esc_html_e('Car Details','motors'); ?></div>
				</div>

				<?php get_template_part('partials/single-car-listing/car-data'); ?>
			<?php endif; ?>


			<?php
				$features = get_post_meta(get_the_id(), 'additional_features', true);
				if(!empty($features)):
			?>
					<div class="stm-car-listing-data-single stm-border-top-unit ">
						<div class="title heading-font"><?php esc_html_e('Features', 'motors'); ?></div>
					</div>
					<?php get_template_part('partials/single-car-listing/car-features'); ?>

				<?php endif; ?>

		</div>
	</div>

	<div class="col-md-3 col-sm-12 col-xs-12">

		<?php if ( is_active_sidebar( 'stm_listing_car' )) { ?>
			<div class="stm-single-listing-car-sidebar">
				<?php dynamic_sidebar( 'stm_listing_car' ); ?>
			</div>
		<?php }; ?>

	</div>
</div>

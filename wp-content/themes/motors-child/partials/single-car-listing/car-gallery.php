<?php

//Getting gallery list
$gallery = apply_filters( 'stm_listing_gallery', get_post_meta(get_the_id(), 'gallery', true), get_the_ID() );
$video_preview = get_post_meta(get_the_id(), 'video_preview', true);
$gallery_video = get_post_meta(get_the_id(), 'gallery_video', true);
$special_car = get_post_meta(get_the_id(),'special_car', true);

$badge_text = get_post_meta(get_the_ID(),'badge_text',true);
$badge_bg_color = get_post_meta(get_the_ID(),'badge_bg_color',true);

$image_limit = '';

if (stm_pricing_enabled() and !empty($gallery)) {
	$user_added = get_post_meta(get_the_id(), 'stm_car_user', true);
	if(!empty($user_added)) {
		$limits = stm_get_post_limits( $user_added );
		$image_limit = $limits['images'] - 1;
		$gallery = array_slice($gallery, 0, $image_limit);
	}
}

?>

<?php if(!has_post_thumbnail() and stm_check_if_car_imported(get_the_id())): ?>
	<img
		itemprop="image"
		src="<?php echo esc_url(get_stylesheet_directory_uri().'/assets/images/automanager_placeholders/plchldr798automanager.png'); ?>"
		class="img-responsive"
		alt="<?php esc_html_e('Placeholder', 'motors'); ?>"
		/>
<?php endif; ?>


<div class="stm-car-carousels stm-listing-car-gallery">
	<!--Actions-->
	<?php
		$show_print = get_theme_mod('show_print_btn', true);
		$show_compare = get_theme_mod('show_compare', true);
		$show_share = get_theme_mod('show_share', true);
		$show_featured_btn = get_theme_mod('show_featured_btn', true);
	?>
	<div class="stm-gallery-actions">
        <?php if(!empty($show_print)): ?>
            <div class="stm-gallery-action-unit stm-listing-print-action">
                <a href="javascript:window.print()" class="stm-car-print heading-font">
                    <i class="fa fa-print"></i>
                </a>
            </div>
        <?php endif; ?>
		<?php if(!empty($show_featured_btn)): ?>
			<div class="stm-gallery-action-unit stm-listing-favorite-action" data-id="<?php echo esc_attr(get_the_id()); ?>">
				<i class="stm-service-icon-staricon"></i>
			</div>
		<?php endif; ?>
		<?php if(!empty($show_compare)): ?>
			<div class="stm-gallery-action-unit compare" data-id="<?php echo esc_attr(get_the_ID()); ?>" data-title="<?php echo esc_attr(stm_generate_title_from_slugs(get_the_id())); ?>">
				<i class="stm-service-icon-compare-new"></i>
			</div>
		<?php endif; ?>
		<?php if(!empty($show_share)): ?>
			<div class="stm-gallery-action-unit"
				 data-toggle="modal"
				 data-target="#shareModal"
				 data-url="<?php echo site_url( add_query_arg() ) ?>">
				<i class="stm-icon-share"></i>
			</div>
		<?php endif; ?>
	</div>

	<?php $car_media = stm_get_car_medias(get_the_id()); ?>
	<?php if(!empty($car_media['car_videos_count'])): ?>
		<div class="stm-car-medias">
			<div class="stm-listing-videos-unit stm-car-videos-<?php echo get_the_id(); ?>">
				<i class="fa fa-film"></i>
				<span><?php echo $car_media['car_videos_count']; ?> <?php esc_html_e('Video', 'motors'); ?></span>
			</div>
		</div>

		<script type="text/javascript">
			jQuery(document).ready(function(){

				jQuery(".stm-car-videos-<?php echo get_the_id(); ?>").click(function() {
					jQuery.fancybox.open([
						<?php foreach($car_media['car_videos'] as $car_video): ?>
						{
							href  : "<?php echo esc_url($car_video); ?>"
						},
						<?php endforeach; ?>
					], {
						type: 'iframe',
						padding: 0
					}); //open
				}); //click
			}); //ready

		</script>
	<?php endif; ?>


	<?php
		if(function_exists('get_field'))
			$stm_tour = get_field('tour_iframe', get_the_id());
		if(!empty($stm_tour)): ?>

		<div class="stm-car-medias stm-car-tour <?php echo empty($car_media['car_videos_count']) ? 'no-video' : ''; ?>">
			<a href="#tour" class="stm-listing-videos-unit stm_fancybox stm-car-tour-<?php echo get_the_id(); ?>">

				<svg version="1.1" id="Layer_1" class="degree360" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					 width="170px" height="170px" viewBox="0 0 170 170" enable-background="new 0 0 170 170" xml:space="preserve" fill="#fff">
				<g>
					<defs>
						<rect id="SVGID_3_" width="170" height="170"/>
					</defs>
					<clipPath id="SVGID_1_">
						<use xlink:href="#SVGID_3_"  overflow="visible"/>
					</clipPath>
					<path clip-path="url(#SVGID_1_)" d="M136.283,68.383v3.586c12.225,3.65,19.75,8.518,19.75,13.658
						c0,8.259-19.39,15.297-46.563,17.986v8.147c32.633-3.226,56.28-12.734,56.28-23.961C165.75,79.979,154.275,72.994,136.283,68.383z"
						/>
					<path clip-path="url(#SVGID_1_)" d="M87.659,104.758c-0.886,0.01-1.766,0.022-2.659,0.022c-39.231,0-71.035-8.575-71.035-19.153
						c0-5.603,8.927-10.886,23.151-14.616v-3.453C17.183,72.137,4.25,79.498,4.25,87.8c0,13.883,36.153,25.138,80.75,25.138
						c0.892,0,1.773-0.013,2.659-0.022v10.92l15.347-15.347L87.659,93.141V104.758z"/>
					<path clip-path="url(#SVGID_1_)" d="M50.639,79.362c-3.084,0-5.14-1.516-7.142-3.518l-5.519,5.735
						c2.109,3.138,7.466,6.114,12.606,6.114c7.305,0,14.987-3.895,14.987-11.904c0-4.869-3.301-7.628-5.032-8.873
						c1.407-1.081,4.003-3.191,4.003-6.87c0-6.926-4.977-11.47-13.796-11.47c-4.923,0-9.63,3.029-11.578,5.14l5.249,6.331
						c1.893-2.002,4.273-3.247,6.329-3.247c2.273,0,4.762,1.352,4.762,4.274c0,2.436-2.001,3.734-4.599,3.734h-3.95v6.439h4.058
						c2.705,0,4.491,1.027,4.491,3.949C55.508,77.9,53.668,79.362,50.639,79.362z"/>
					<path clip-path="url(#SVGID_1_)" d="M82.991,87.478c8.819-0.272,14.012-6.439,14.012-13.688c0-7.359-5.735-12.336-12.66-12.066
						c-2.868,0.108-5.681,1.353-7.088,2.759c0.542-4.003,3.084-7.25,7.088-7.358c3.301,0,6.061,1.678,7.899,2.76l3.679-7.033
						c-3.191-2.705-9.143-4.383-12.768-4.274c-14.23,0.433-16.069,14.771-15.907,21.966C67.571,81.364,74.118,87.747,82.991,87.478z
						 M82.937,69.515c3.245-0.055,4.328,2.705,4.383,4.815c0.053,2.597-2.165,4.653-4.924,4.653c-3.462,0-5.031-3.68-5.031-6.386
						C78.229,71.516,80.394,69.623,82.937,69.515z"/>
					<path clip-path="url(#SVGID_1_)" d="M129.409,68.271c0-11.958-4.923-19.803-15.528-19.803c-8.981,0-15.258,7.954-15.258,19.425
						c0,13.309,6.493,19.747,15.528,19.747C123.944,87.639,129.409,80.28,129.409,68.271z M109.336,67.892
						c0-6.602,2.489-10.769,4.491-10.714c2.65,0,4.706,4.653,4.706,11.092c0,6.6-1.894,10.983-4.382,10.983
						C111.986,79.253,109.336,74.871,109.336,67.892z"/>
					<path clip-path="url(#SVGID_1_)" d="M137.291,60.065c3.837,0,6.949-3.112,6.949-6.95s-3.112-6.951-6.949-6.951
						c-3.84,0-6.951,3.113-6.951,6.951S133.451,60.065,137.291,60.065z M137.291,49.452c2.021,0,3.662,1.641,3.662,3.664
						c0,2.022-1.641,3.663-3.662,3.663c-2.024,0-3.664-1.641-3.664-3.663C133.627,51.092,135.267,49.452,137.291,49.452z"/>
				</g>
				</svg>


				<span><?php esc_html_e('Tours', 'motors'); ?></span>
			</a>
			<div id="tour" style="display: none;">
				<?php echo $stm_tour; ?>
			</div>
		</div>
	<?php endif; ?>


	<div class="stm-big-car-gallery">
		<?php if(has_post_thumbnail()):
			$full_src = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()),'full');
			//Post thumbnail first ?>
			<div class="stm-single-image" data-id="big-image-<?php echo esc_attr(get_post_thumbnail_id(get_the_id())); ?>">
				<a href="<?php echo esc_url($full_src[0]); ?>" class="stm_fancybox" rel="stm-car-gallery">
					<?php the_post_thumbnail('stm-img-796-466', array('class'=>'img-responsive')); ?>
				</a>
			</div>
		<?php endif; ?>

		<?php if(!empty($gallery)): ?>
			<?php foreach ( $gallery as $gallery_image ): ?>
				<?php $src = wp_get_attachment_image_src($gallery_image, 'stm-img-796-466'); ?>
				<?php $full_src = wp_get_attachment_image_src($gallery_image, 'full'); ?>
				<?php if(!empty($src[0])): ?>
					<div class="stm-single-image" data-id="big-image-<?php echo esc_attr($gallery_image); ?>">
						<a href="<?php echo esc_url($full_src[0]); ?>" class="stm_fancybox" rel="stm-car-gallery">
							<img src="<?php echo esc_url($src[0]); ?>" alt="<?php echo get_the_title(get_the_ID()).' '.esc_html__('full','motors'); ?>"/>
						</a>
					</div>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php endif; ?>

	</div>

	<?php if(has_post_thumbnail() and !empty($gallery) ): ?>
		<div class="stm-thumbs-car-gallery">
			<?php if(has_post_thumbnail()):
				//Post thumbnail first ?>
				<div class="stm-single-image" id="big-image-<?php echo esc_attr(get_post_thumbnail_id(get_the_id())); ?>">
					<?php the_post_thumbnail('stm-img-350-205', array('class'=>'img-responsive')); ?>
				</div>
			<?php endif; ?>
			<?php if(!empty($gallery) && count($gallery) > 0): ?>
				<?php foreach ( $gallery as $gallery_image ): ?>
					<?php $src = wp_get_attachment_image_src($gallery_image, 'stm-img-350-205'); ?>
					<?php if(!empty($src[0])): ?>
						<div class="stm-single-image" id="big-image-<?php echo esc_attr($gallery_image); ?>">
							<img src="<?php echo esc_url($src[0]); ?>" alt="<?php echo get_the_title(get_the_ID()).' '.esc_html__('full','motors'); ?>"/>
						</div>
					<?php endif; ?>
				<?php endforeach; ?>
			<?php endif; ?>

		</div>
	<?php endif; ?>
</div>


<!--Enable carousel-->
<script type="text/javascript">
	jQuery(document).ready(function($){
		var big = $('.stm-big-car-gallery');
		var small = $('.stm-thumbs-car-gallery');
		var flag = false;
		var duration = 800;

		var owlRtl = false;
		if( $('body').hasClass('rtl') ) {
			owlRtl = true;
		}

		big
			.owlCarousel({
				items: 1,
				rtl: owlRtl,
				smartSpeed: 800,
				dots: false,
				nav: false,
				margin:0,
				autoplay: false,
				loop: false,
				responsiveRefreshRate: 1000
			})
			.on('changed.owl.carousel', function (e) {
				$('.stm-thumbs-car-gallery .owl-item').removeClass('current');
				$('.stm-thumbs-car-gallery .owl-item').eq(e.item.index).addClass('current');
				if (!flag) {
					flag = true;
					small.trigger('to.owl.carousel', [e.item.index, duration, true]);
					flag = false;
				}
			});

		small
			.owlCarousel({
				items: 5,
				rtl: owlRtl,
				smartSpeed: 800,
				dots: false,
				margin: 22,
				autoplay: false,
				nav: true,
				loop: false,
				navText: [],
				responsiveRefreshRate: 1000,
				responsive:{
					0:{
						items:2
					},
					500:{
						items:4
					},
					768:{
						items:5
					},
					1000:{
						items:5
					}
				}
			})
			.on('click', '.owl-item', function(event) {
				big.trigger('to.owl.carousel', [$(this).index(), 400, true]);
			})
			.on('changed.owl.carousel', function (e) {
				if (!flag) {
					flag = true;
					big.trigger('to.owl.carousel', [e.item.index, duration, true]);
					flag = false;
				}
			});

		if($('.stm-thumbs-car-gallery .stm-single-image').length < 6) {
			$('.stm-single-car-page .owl-controls').hide();
			$('.stm-thumbs-car-gallery').css({'margin-top': '22px'});
		}
	})
</script>

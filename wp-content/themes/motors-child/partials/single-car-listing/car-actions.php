<?php
$show_pdf = get_theme_mod('show_listing_pdf', false);
$car_brochure = get_post_meta(get_the_ID(), 'car_brochure', true);

$show_certified_logo_1 = get_theme_mod('show_listing_certified_logo_1', true);
$show_certified_logo_2 = get_theme_mod('show_listing_certified_logo_2', true);

$certified_logo_1 = get_post_meta(get_the_ID(),'certified_logo_1',true);
$history_link_1 = get_post_meta(get_the_ID(),'history_link',true);

$certified_logo_2 = get_post_meta(get_the_ID(),'certified_logo_2',true);
$certified_logo_2_link = get_post_meta(get_the_ID(),'certified_logo_2_link',true);
?>
<div class="single-car-actions">
    <ul class="list-unstyled clearfix">
		<?php if (!empty($car_brochure) && !empty($show_pdf) and $show_pdf ): ?>
			<li>
				<a href="<?php echo esc_url(wp_get_attachment_url($car_brochure)); ?>"
				   class="car-action-unit stm-brochure"
				   title="<?php esc_html_e('Download brochure', 'stm_vehicles_listing'); ?>"
				   target="_blank" download>
					<i class="stm-icon-brochure"></i>
					<?php esc_html_e('Aircraft Spec Sheet', 'stm_vehicles_listing'); ?>
				</a>
			</li>
		<?php endif; ?>

		<?php $active = get_post_meta(get_the_ID(),'valuation_active', true); ?>
		<?php if ($active !== 'on'): ?>
			<?php

			$valuation_id = get_theme_mod( 'valuation_plan', false );
			$valuation_pdf = get_post_meta( get_the_id(), 'valuation_pdf', true );

			$link = '#';

			$valuations = get_user_meta( get_current_user_id(),'valuations', true );

			if(isset($valuations[get_the_ID()]) && $valuations[get_the_ID()]['status'] == 'processing'){
				if(!empty($valuation_pdf)) $link = wp_get_attachment_url($valuation_pdf);
				else $link = '#valuation_modal';
			}

			if(!has_post_thumbnail() and stm_check_if_car_imported(get_the_id())) $image_src = esc_url(get_stylesheet_directory_uri().'/assets/images/automanager_placeholders/plchldr798automanager.png');
			else{
				$image_src = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()),'stm-img-350-205');
				$image_src = $image_src[0];
			}
			?>

			<?php if(true)://if(!empty($valuation_pdf)): ?>
				<li class="certified-logo-1 certified-logo-alt">
					<a
					data-title="<?php the_title() ?>"
					data-id="<?php echo get_the_id() ?>"
					data-image="<?php echo $image_src ?>"
					class="stm-valuation-btn <?php if(isset($valuations[get_the_ID()]) && !empty($valuation_pdf) && $valuations[get_the_ID()]['status'] == 'processing') echo 'valuation_pdf_download' ?>>"
					href="<?php echo $link; ?>"
					<?php //if(!is_user_logged_in()) echo 'class="valuation_pdf_download"' ?>
					<?php //if(isset($valuations[get_the_ID()]) && $valuations[get_the_ID()]['status'] == 'processing' && empty($valuation_pdf)) echo 'data-toggle="modal"' ?>
					<?php if(isset($valuations[get_the_ID()]) && !empty($valuation_pdf) && $valuations[get_the_ID()]['status'] == 'processing') echo 'target="_blank"' ?>>
						<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/valuation_sert.png" alt="Image"/>
						<i class=""></i>
					</a>
				</li>
			<?php else: ?>
				<li>
					<a href="#"
					   class="car-action-unit stm-valuation <?php if(isset($valuations[get_the_ID()])) echo 'active' ?>"
					   title="<?php esc_html_e('Valuation of aircraft', 'stm_vehicles_listing'); ?>"
					   <?php if(isset($valuations[get_the_ID()]) && $valuations[get_the_ID()]['status'] == 'processing') echo 'download' ?>>
						<i class="fa fa-check"></i>
						<?php if(isset($valuations[get_the_ID()])): ?>
							<?php esc_html_e('Pending of Valuation', 'stm_vehicles_listing'); ?>
						<?php else: ?>
							<?php esc_html_e('Request of Valuation', 'stm_vehicles_listing'); ?>
						<?php endif; ?>
					</a>
				</li>
			<?php endif; ?>

		<?php endif; ?>



		<!--Certified Logo 1-->
		<?php if(!empty($certified_logo_1) and !empty($show_certified_logo_1) and $show_certified_logo_1): ?>
			<?php
			$certified_logo_1 = wp_get_attachment_image_src($certified_logo_1, 'stm-img-796-466');
			if(!empty($certified_logo_1[0])){
				$certified_logo_1 = $certified_logo_1[0]; ?>

				<li class="certified-logo-1">
					<?php if(!empty($history_link_1)): ?>
						<a href="<?php echo esc_url($history_link_1); ?>" target="_blank">
					<?php endif; ?>
						<img src="<?php echo esc_url($certified_logo_1); ?>" alt="<?php esc_html_e('Logo 1', 'motors'); ?>"/>
					<?php if(!empty($history_link_1)): ?>
						</a>
					<?php endif; ?>
				</li>



			<?php } ?>
		<?php endif; ?>

		<!--Certified Logo 2-->
		<?php if(!empty($certified_logo_2) and !empty($show_certified_logo_2) and $show_certified_logo_2): ?>
			<?php
			$certified_logo_2 = wp_get_attachment_image_src($certified_logo_2, 'full');
			if(!empty($certified_logo_2[0])){
				$certified_logo_2 = $certified_logo_2[0]; ?>


				<li class="certified-logo-2">
					<?php if(!empty($certified_logo_2_link)): ?>
						<a href="<?php echo esc_url($certified_logo_2_link); ?>" target="_blank">
					<?php endif; ?>
						<img src="<?php echo esc_url($certified_logo_2); ?>"  alt="<?php esc_html_e('Logo 2', 'motors'); ?>"/>
					<?php if(!empty($certified_logo_2_link)): ?>
						</a>
					<?php endif; ?>
				</li>

			<?php } ?>
		<?php endif; ?>
	</ul>
</div>



<?php if(!empty($_GET['pay']) && !empty($valuation_pdf) && isset($valuations[get_the_ID()]) && $valuations[get_the_ID()]['status'] == 'processing'): ?>
	<script type="text/javascript">
		document.getElementById('stm-valuation-btn').click();
	</script>
<?php endif; ?>

<?php if(!empty($_GET['pay']) && empty($valuation_pdf) && isset($valuations[get_the_ID()]) && $valuations[get_the_ID()]['status'] == 'processing'): ?>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			jQuery('#valuation_modal').modal('show');
		});
	</script>
<?php endif; ?>
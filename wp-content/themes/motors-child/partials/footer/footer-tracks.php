<?php

if(listing() && is_singular('listings')){

    $slv = listing();

    $id_sv = $slv->id;
    $country_sv = $slv->countries['title'];
    $make_sv = $slv->make['title'];
    $model_sv = $slv->serie['title'];
    $year_sv = $slv->years;
    $state_of_vehicle_sv = $slv->condition['title'];
    $exterior_color_sv = $slv->attributes['exterior-color']->label;
    $transmission_sv = $slv->attributes['transmission']->label;
    $body_style_sv = $slv->attributes['body']->label;
    $fuel_type_sv = $slv->attributes['fuel']->label;
    $price_sv = $slv->price_final;
    $currency_sv = $slv->currency;

    ?>
    <script type="text/javascript">

        //view listing page
        fbq('track', 'ViewContent', {
            content_type: 'vehicle',
            content_ids: ['<?php echo $id_sv; ?>'],
            postal_code: '',
            country: '<?php echo $country_sv; ?>',
            make: '<?php echo $make_sv; ?>',
            model: '<?php echo $model_sv; ?>',
            year: '<?php echo $year_sv; ?>',
            state_of_vehicle: '<?php echo $state_of_vehicle_sv; ?>',
            exterior_color: '<?php echo $exterior_color_sv; ?>',
            transmission: '<?php echo $transmission_sv; ?>',
            body_style: '<?php echo $body_style_sv; ?>',
            fuel_type: '<?php echo $fuel_type_sv; ?>',
            price: '<?php echo $price_sv; ?>',
            currency: '<?php echo $currency_sv; ?>',
        });

        var $ = jQuery;
        //lead listing page
        $("#stm_listing_car_form_auction-2 form").submit( function() {

            var zip = $('#stm_listing_car_form_auction-2 form input[name="zip"]').val();

            fbq('track', 'Lead', {
                content_type: 'vehicle',
                content_ids: ['<?php echo $id_sv; ?>'],
                postal_code: zip,
                country: '<?php echo $country_sv; ?>',
                make: '<?php echo $make_sv; ?>',
                model: '<?php echo $model_sv; ?>',
                year: '<?php echo $year_sv; ?>',
                state_of_vehicle: '<?php echo $state_of_vehicle_sv; ?>',
                exterior_color: '<?php echo $exterior_color_sv; ?>',
                transmission: '<?php echo $transmission_sv; ?>',
                body_style: '<?php echo $body_style_sv; ?>',
                fuel_type: '<?php echo $fuel_type_sv; ?>',
                price: '<?php echo $price_sv; ?>',
                currency: '<?php echo $currency_sv; ?>',
            });

        });

    </script>



    <?php
}


if(is_front_page()){

    ?>
    <script type="text/javascript">

        //search click btn

        jQuery('#stm_all_listing_tab form').on( "submit", function( event ) {
            fbq('trackCustom', 'Home Search', {
                'Search': 'true'
            });
        });

    </script>

    <?php

}

?>

<script type="text/javascript">

    //registrations clicked
    jQuery('.stm-register-form form').submit( function(){
        var email = jQuery('.stm-register-form input[name="stm_user_mail"]').val();
        fbq('trackCustom', 'Registration', {
            'email': email,
        });

    });

</script>

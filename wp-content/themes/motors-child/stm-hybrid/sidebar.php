<?php
$def_db = get_template_directory_uri() . '/assets/images/listing-directory-filter-bg.jpg';
$filter_bg = get_theme_mod('sidebar_filter_bg', $def_db);

if (!empty($filter_bg)): ?>
	<style type="text/css">
		.stm-template-listing .filter-sidebar:after {
			background-image: url("<?php echo esc_url($filter_bg); ?>");
		}
	</style>
<?php endif; ?>

<?php
$filters = stm_hybrid_filters();
global $landing;
$separate_filters = [];
foreach ($filters as $key => $filter) {
	$attribute = $filter->__get('attribute');
	$placeholder1 = $filter['label'];
	$attribute['placeholder1'] = $placeholder1;
	$filter->__set('attribute', $attribute);

	if (!empty($filter->separatePanel)) {
		$separate_filters[] = $filter;
		unset($filters[$key]);
	}
	if($filter->key == 'towns' || $filter->key == 'countries') unset($filters[$key]);
}

/*first 5 taxanomies*/
$first_five = array_slice($filters, 0, 6);
/* others taxonomies starting from 5*/
$others = array_slice($filters, 6, count($filters));

$sep_price = $location = [];
foreach ($separate_filters as $key => $separate) {
	if($separate->key == 'price'){
		$sep_price = $separate;
		unset($separate_filters[$key]);
		break;
	}
}

$other_exclude = ['mod', 'fuel-economy', 'coordinates', 'features'];
foreach ($others as $key => $other) {
	if($other->key == 'coordinates'){
		$location = $other;
	}
	if(in_array($other->key, $other_exclude) !== false){
		unset($others[$key]);
	}
}

if(!empty($location)) $first_five[] = $location;

?>

<form
	action="<?php echo stm_listings_current_url() ?>"
	method="get"
	data-trigger="inventory"
	data-target="listings">
	<div class="filter filter-sidebar ajax-filter">
		<?php do_action('stm_listings_filter_before'); ?>

		<?php if(!stm_is_dealer_two()) : ?>
			<div class="sidebar-entry-header">
				<i class="stm-icon-car_search"></i>
				<span class="h4"><?php _e( 'Search Options', 'motors' ); ?></span>
			</div>
		<?php else : ?>
			<div class="sidebar-entry-header">
                <span class="h4"><?php _e( 'Search', 'motors' ); ?></span>
                <a class="heading-font" href="<?php echo esc_url(stm_get_listing_archive_link()) ?>">
                    <?php esc_html_e('Reset All', 'motors'); ?>
                </a>
            </div>
		<?php endif; ?>

		<!-- FIRST FIVE TAXES && LOCATION -->
		<div class="row row-pad-top-24">
			<?php foreach ($first_five as $key => $filter) {?>
				<?php
				if($landing && count($landing)) if(!empty($landing[$key])) {
					$filter->value = $landing[$key];
				}
				?>
				<div class="col-md-12 col-sm-6 stm-filter_<?php echo esc_attr($filter->key) ?>">
					<?php if($filter->type) stm_hybrid_include('filter/' . $filter->type, compact('filter')); ?>
				</div>
			<?php } ?>

			<div class="col-md-12 col-sm-6">
				<a href="<?php echo esc_url( stm_get_listing_archive_link() ); ?>"
				   class="button"><span><?php _e( 'Clear all', 'stm_vehicles_listing' ); ?></span></a>
			</div>
		</div>
	</div>

	<!--Classified price-->
	<?php if(!empty($sep_price)): ?>
		<div style="margin-bottom: 20px;"
			class="filter ajax-filter stm-filter-<?php echo $sep_price['key'] ?> stm-filter stm-filter__<?php echo $sep_price['type']; ?>">
			<div class="stm-accordion-single-unit">
				<a class="title" data-toggle="collapse" href="#<?php echo $sep_price['type'] ?>"
				   aria-expanded="true">
					<h5><?php echo __("Select Price","motors") ?></h5>
					<span class="minus"></span>
				</a>
				<div class="stm-accordion-content">
					<div class="content collapse in" id="<?php echo $sep_price['type'] ?>" aria-expanded="true">
						<div class="stm-accordion-content-wrapper">
							<div class="stm-accordion-inner">
								<?php stm_hybrid_include('filter/' . $sep_price['type'], array(
									'filter' => $sep_price,
									'collapse' => true
								)); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif ?>

	<!-- OTHERS (SEPARATE)-->
	<?php if(!empty($separate_filters)): ?>
		<div class="filter filter-sidebar ajax-filter" id="others" style="overflow: hidden;">
			<?php foreach ($separate_filters as $separate_filter) : ?>
				<?php if($separate_filter['type'] == 'slider'): ?>
					<div class="col-md-12 col-sm-12 stm-filter_<?php echo esc_attr( $separate_filter->key ) ?>">
						<?php stm_hybrid_include('filter/' . $separate_filter['type'], array(
							'filter' => $separate_filter,
						)); ?>
					</div>
				<?php else: ?>
					<div class="col-md-12 col-sm-6 stm-filter_<?php echo esc_attr( $separate_filter->key ) ?>">
						<div class="form-group">
							<?php stm_hybrid_include('filter/' . $separate_filter['type'], array(
								'filter' => $separate_filter,
								'collapse' => true
							)); ?>
						</div>
					</div>
			<?php endif ?>
			<?php endforeach; ?>
		</div>
	<?php endif ?>

	<!-- FILTERS -->
	<div class="stm-filter-listing-directory-filters">
		<div class="stm-accordion-single-unit stm_filters">
			<a class="title collapsed" data-toggle="collapse" href="#filters" aria-expanded="true">
				<h5><?php _e('Filters', 'motors'); ?></h5>
				<span class="minus"></span>
			</a>
			<div class="stm-accordion-content">
				<div class="collapse content" id="filters">
					<div class="stm-accordion-content-wrapper">
						<div class="clearfix">
							<?php foreach ($others as $key => $filter) {?>
								<?php
								if($landing && count($landing)) if(!empty($landing[$key])) {
									$filter->value = $landing[$key];
								}
								?>
								<div class="col-md-12 col-sm-6 stm-filter_<?php echo esc_attr($filter->key) ?>">
									<?php if($filter->type) stm_hybrid_include('filter/' . $filter->type, compact('filter')); ?>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<input type="hidden" name="s" value="<?php echo esc_attr(stm_listings_input('s')); ?>"/>
	<input type="hidden" name="sort_order"
		   value="<?php echo esc_attr(stm_listings_input('sort_order', 'date_high')); ?>"/>

	<div class="sidebar-action-units">
		<input id="stm-classic-filter-submit" class="hidden" type="submit"
			   value="<?php _e('Show cars', 'motors'); ?>"/>
	</div>

	<?php do_action('stm_listings_filter_after'); ?>

</form>

<?php
$view_type = stm_listings_input( 'view_type', get_theme_mod( "listing_view_type", "list" ) );

if ( ! count( listings() ) ) {
	return;
}
?>
<div class="stm-isotope-sorting stm-isotope-sorting-<?php echo esc_attr( $view_type ); ?>">
	<?php if ( $view_type == 'grid' ) { ?>
	<div class="row row-3 car-listing-row car-listing-modern-grid">
		<?php }
		foreach ( listings() as $item ) {
			the_listing( $item );
			if ( $view_type == 'grid' ) {
				stm_hybrid_include( 'loop/grid' );
			} else {
				stm_hybrid_include( 'loop/list' );
			}
		}
		if ( $view_type == 'grid' ) { ?>
	</div>
<?php } ?>
</div>

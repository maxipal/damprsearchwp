<div class="stm-car_dealer-buttons heading-font">

	<a href="#trade-offer" data-toggle="modal" data-target="#trade-offer">
		<?php esc_html_e( 'Make an offer price', 'motors' ); ?>
		<i class="stm-moto-icon-cash"></i>
	</a>

</div>

<?php
$show_print_btn = get_theme_mod('show_print_btn', false);

$show_test_drive = (!stm_is_magazine()) ? get_theme_mod('show_test_drive', true) : false;
$show_compare = get_theme_mod('show_compare', true);
$show_share = get_theme_mod('show_share', true);

$show_pdf = get_theme_mod('show_listing_pdf', false);
$show_certified_logo_1 = get_theme_mod('show_listing_certified_logo_1', true);
$show_certified_logo_2 = get_theme_mod('show_listing_certified_logo_2', true);


$history_link_1 = listing()->history_link;
$car_brochure = listing()->brochure;

?>
<div class="single-car-actions">
	<ul class="list-unstyled clearfix">

		<!--Schedule-->
		<?php if (!empty($show_test_drive) and $show_test_drive): ?>
			<li>
				<a href="#" class="car-action-unit stm-schedule" data-toggle="modal" data-target="#test-drive">
					<i class="stm-icon-steering_wheel"></i>
					<?php esc_html_e('Schedule Test Drive', 'motors'); ?>
				</a>
			</li>
		<?php endif; ?>

		<!--COmpare-->
		<?php if (!empty($show_compare) and $show_compare): ?>
			<li data-compare-id="<?php echo esc_attr(get_the_ID()); ?>">
				<a href="#" class="car-action-unit add-to-compare stm-added" style="display: none;" data-id="<?php echo esc_attr(get_the_ID()); ?>" data-action="remove">
					<i class="stm-icon-added stm-unhover"></i>
					<span class="stm-unhover"><?php esc_html_e('in compare list', 'motors'); ?></span>
					<div class="stm-show-on-hover">
						<i class="stm-icon-remove"></i>
						<?php esc_html_e('Remove from list', 'motors'); ?>
					</div>
				</a>
				<a href="#" class="car-action-unit add-to-compare" data-id="<?php echo esc_attr(get_the_ID()); ?>" data-action="add">
					<i class="stm-icon-add"></i>
					<?php esc_html_e('Add to compare', 'motors'); ?>
				</a>
			</li>
		<?php endif; ?>

		<!--Add to favorite-->
		<li>
			<a href="#" class="car-action-unit stm-car-print heading-font stm-listing-favorite-action" data-id="<?php the_id() ?>">
				<i class="stm-service-icon-staricon"></i>
				<?php echo esc_html__('Add to favorites', 'motors'); ?>
			</a>
		</li>


		<!--Print button-->
		<?php if (!empty($show_print_btn) and $show_print_btn): ?>
			<li>
				<a href="javascript:window.print()" class="car-action-unit stm-car-print heading-font">
					<i class="fa fa-print"></i>
					<?php echo esc_html__('Print', 'motors'); ?>
				</a>
			</li>
		<?php endif; ?>


		<!--Share-->
		<?php if (!empty($show_share) and $show_share): ?>
			<li class="stm-shareble">

				<a
					href="#"
					class="car-action-unit stm-share"
					title="<?php esc_html_e('Share this Ad', 'motors'); ?>"
					download>
					<i class="stm-icon-share"></i>
					<?php esc_html_e('Share this Ad', 'motors'); ?>
				</a>

				<?php if( function_exists( 'ADDTOANY_SHARE_SAVE_KIT' ) ): ?>
				<div class="stm-a2a-popup">
					<?php echo do_shortcode('[addtoany url="'.get_the_permalink(get_the_ID()).'" title="'.get_the_title(get_the_ID()).'"]'); ?>
				</div>
				<?php endif; ?>
			</li>
		<?php endif; ?>


		<?php if (!empty($car_brochure) && !empty($show_pdf) and $show_pdf ): ?>
			<li>
				<a href="<?php echo esc_url($car_brochure['url']); ?>"
				   class="car-action-unit stm-brochure"
				   title="<?php esc_html_e('Download brochure', 'stm_vehicles_listing'); ?>"
				   target="_blank" download>
					<i class="stm-icon-brochure"></i>
					<?php esc_html_e('Aircraft Spec Sheet', 'stm_vehicles_listing'); ?>
				</a>
			</li>
		<?php endif; ?>



		<!--Certified Logo 1-->
		<?php if (!empty($certified_logo_1) and !empty($show_certified_logo_1) and $show_certified_logo_1):
			$certified_logo_1 = wp_get_attachment_image_src($certified_logo_1, 'stm-img-796-466');
			if (!empty($certified_logo_1[0])) {
				$certified_logo_1 = $certified_logo_1[0]; ?>
				<li class="certified-logo-1">
					<?php if(!empty($history_link_1)): ?>
						<a href="<?php echo esc_url($history_link_1); ?>" target="_blank">
					<?php endif; ?>
						<img src="<?php echo esc_url($certified_logo_1); ?>" alt="<?php esc_html_e('Logo 1', 'motors'); ?>"/>
					<?php if(!empty($history_link_1)): ?>
						</a>
					<?php endif; ?>
				</li>
			<?php } ?>
		<?php endif; ?>

		<!--Certified Logo 2-->
		<?php if (!empty($certified_logo_2) and !empty($show_certified_logo_2) and $show_certified_logo_2):
			$certified_logo_2 = wp_get_attachment_image_src($certified_logo_2, 'full');
			if (!empty($certified_logo_2[0])) {
				$certified_logo_2 = $certified_logo_2[0]; ?>
				<li class="certified-logo-2">
					<?php if(!empty($certified_logo_2_link)): ?>
						<a href="<?php echo esc_url($certified_logo_2_link); ?>" target="_blank">
					<?php endif; ?>
						<img src="<?php echo esc_url($certified_logo_2); ?>"  alt="<?php esc_html_e('Logo 2', 'motors'); ?>"/>
					<?php if(!empty($certified_logo_2_link)): ?>
						</a>
					<?php endif; ?>
				</li>
			<?php } ?>
		<?php endif; ?>
	</ul>
</div>



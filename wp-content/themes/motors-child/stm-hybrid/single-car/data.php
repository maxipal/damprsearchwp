<?php
$data = (array)listing()->attributes;

if (empty($data)) {
	return;
} ?>
<div class="stm-single-car-listing-data">
	<table class="stm-table-main">
		<tr>
			<?php
			$i = 1;
			foreach ($data as $key => $attribute):
				if ($attribute->value === null) {
//					continue;
					$attribute->value = esc_html__('N/A', 'motors');
				}
			?>
			<td>
				<table class="inner-table">
					<tr>
						<td class="label-td">
							<?php if ($attribute->icon): ?>
								<i class="<?php echo esc_attr($attribute->icon) ?>"></i>
							<?php endif; ?>
							<?php echo $attribute->label; ?>
						</td>
						<td class="heading-font">
							<?php
							if($attribute->label == 'Kilometrazha') echo number_format($attribute->value, 0, '.', ' ');
							else echo $attribute->value;
							if (!empty($attribute->affix)) {
								echo ' ' . $attribute->affix;
							}
							?>
						</td>
					</tr>
				</table>
			</td>
			<td class="divider-td"></td>
			<?php if ($i % 3 === 0) : ?>
		</tr>
		<tr>
			<?php
			endif;
			$i++;
			endforeach; ?>
		</tr>
	</table>
</div>

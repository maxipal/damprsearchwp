<?php
$breadcrumbs = listing()->breadcrumbs;

if( $breadcrumbs != 'hide' ): ?>
    <div class="title-box-disabled"></div>
<?php endif; ?>

<!-- Breads -->	
<?php
if ( $breadcrumbs != 'hide' ):
    if ( function_exists( 'bcn_display' ) ) { ?>
        <div class="stm_breadcrumbs_unit heading-font">
            <div class="container">
                <div class="navxtBreads">
                    <?php bcn_display(); ?>
                </div>
            </div>
        </div>
    <?php }
endif;

<?php
$price = listing()->price;
$sale_price = listing()->price_sale;
$car_price_form_label = listing()->price_label;


//if(empty($price) and !empty($sale_price)) {
//	$price = $sale_price;
//}
//
//if(!empty($price) and !empty($sale_price)) {
//	$price = $sale_price;
//}

?>
<div class="stm-listing-single-price-title heading-font clearfix">
	<?php if (!empty($car_price_form_label)): ?>
		<?php if(listing()->auction): ?>
			<?php if(false): ?>
				<div class="price"><?php _e("Auction","motors"); ?></div>
			<?php endif ?>
		<?php else: ?>
			<div class="price">
				<?php echo $car_price_form_label ?>
			</div>
		<?php endif; ?>
	<?php else: ?>
		<?php if(!listing()->auction): ?>
			<div class="price">
				<?php if(!empty($sale_price)): ?>
					<span><?php echo stm_listing_price_view_custom($price, listing()->id); ?></span>
					<strong><?php echo stm_listing_price_view_custom($sale_price, listing()->id); ?></strong>
				<?php else: ?>
					<?php if(!empty($price)): ?>
						<?php echo stm_listing_price_view_custom($price, listing()->id); ?>
					<?php else: ?>
						<?php _e("Contact Me","motors") ?>
					<?php endif; ?>
				<?php endif; ?>
			</div>
		<?php endif ?>
	<?php endif; ?>

	<div class="stm-single-title-wrap">
		<h1 class="title" itemprop="name">
			<?php // echo stm_generate_title_from_slugs(get_the_ID(), get_theme_mod('show_generated_title_as_label', false)); ?>
			<?php echo (!empty(listing()->seo['pageTitle']) ? listing()->seo['pageTitle'] : listing()->title); ?>
		</h1>
		<?php if ( get_theme_mod( 'show_added_date', false ) ) : ?>
			<span class="normal_font">
                <i class="fa fa-clock-o"></i>
				<?php printf( esc_html__( 'ADDED: %s', 'motors' ),
					date( 'F d, Y', strtotime(listing()->created_at) ) ); ?>
            </span>
		<?php endif; ?>
	</div>

</div>

<?php
//update kpi
//	$dealerID = get_the_author_meta('ID');
//	$count = get_user_meta($dealerID, 'air_user_expose_views', true );
//	    if ( !isset($count) ) $count == 0;
//	    $count++;
//	update_user_meta( $dealerID, 'air_user_expose_views', $count );  //totals of stm_car_views
$content = listing()->content;
$content = str_replace( 'Seller Note', 'Seller\'s Notes', $content );
$calc_enable = get_theme_mod( 'leasing_disable', false );
?>

<div class="row">
	<div class="col-md-9 col-sm-12 col-xs-12">

		<div class="single-listing-car-inner"
			 data-dealer_ID="<?php echo listing()->author_id; ?>"
			 data-plane_ID="<?php echo listing()->id; ?>">
			<?php //Title and price
				stm_hybrid_include('single-car/price-title');
			?>

			<?php //Action buttons
				stm_hybrid_include('single-car/actions');
			?>

			<?php //Gallery
				stm_hybrid_include('single-car/gallery');
			?>



			<div class="stm-car-listing-data-single stm-border-top-unit">
				<div class="title heading-font"><?php esc_html_e('Car Details','motors'); ?></div>
			</div>
			<?php stm_hybrid_include('single-car/data'); ?>
			<?php $features = listing()->features; ?>

			<?php if (!empty($features)): ?>
				<div class="stm-car-listing-data-single stm-border-top-unit ">
					<div class="title heading-font"><?php esc_html_e('Features', 'motors'); ?></div>
				</div>
				<?php stm_hybrid_include('single-car/features', compact('features')); ?>
			<?php endif; ?>

			<?php if ( strpos( $content, 'Seller\'s Notes' ) === false ) { ?>
				<div class="stm-car-listing-data-single stm-border-top-unit ">
					<div class="title heading-font">
						<?php esc_html_e("Seller Note", 'motors'); ?>
					</div>
				</div>
			<?php } ?>
			<?php if(empty($content)): ?>
				N/A
			<?php else: ?>
				<div class="stm-car-listing-data-single stm-border-top-unit">
					<?php echo $content ?>
				</div>
			<?php endif ?>

		</div>
	</div>

	<div class="col-md-3 col-sm-12 col-xs-12">
		<?php if ( is_active_sidebar( 'stm_listing_car' )) { ?>
			<div class="stm-single-listing-car-sidebar">
				<?php dynamic_sidebar( 'stm_listing_car' ); ?>
			</div>
		<?php }; ?>
	</div>
</div>

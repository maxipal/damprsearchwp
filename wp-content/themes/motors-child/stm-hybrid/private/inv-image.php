<?php
/**
 * Created by PhpStorm.
 * User: alish
 * Date: 23-Oct-18
 * Time: 5:27 PM
 */

$show_compare = get_theme_mod('show_listing_compare', false);
$show_favorite = get_theme_mod('enable_favorite_items', true);
$sellOnline = get_theme_mod( 'enable_woo_online', false );
$isSellOnline = ( $sellOnline ) ? !empty( get_post_meta( get_the_ID(), 'car_mark_woo_online', true ) ) : false;
$sellOnlineNonce    = wp_create_nonce( 'stm_ajax_sell_online_nonce' );//
stm_hybrid_include('vue-timer');
?>
<div class="image">
    <!--Hover blocks-->
    <!---Media-->
	<div
		v-if="listing.subscription_status === 'active'"
		:ref="'countdown-'+listing.id"
		:class="'days-left stm-start-countdown-'+listing.id"></div>
	<div v-else class="stm_edit_pending_car" style="top: 32px;background: transparent;">
		<a :href="stm_hybrid.price_url+'?lid='+listing.id">
			<?php esc_html_e('Activate ad', 'motors'); ?> <i class="fa fa-eye"></i></i>
		</a>
	</div>
    <div class="stm-car-medias">
        <div v-if="listing.gallery && listing.gallery.length" @click="openGallery(listing.gallery, 'image')"
             class="stm-listing-photos-unit">
            <i class="stm-service-icon-photo"></i>
            <span>{{ listing.gallery.length }}</span>
        </div>
        <div v-if="listing.videos && listing.videos.length" @click="openGallery(listing.videos, 'iframe')"
             class="stm-listing-videos-unit">
            <i class="fa fa-film"></i>
            <span>{{ listing.videos.length }}</span>
        </div>
    </div>

    <!--Compare-->
    <?php if (!empty($show_compare) and $show_compare): ?>
        <div
                class="stm-listing-compare"
                :data-id="listing.id"
                :data-title="listing.title"
                data-toggle="tooltip" data-placement="left" title="<?php esc_attr_e('Add to compare', 'motors') ?>">
            <i class="stm-service-icon-compare-new"></i>
        </div>
    <?php endif; ?>

    <!--Favorite-->
    <?php if (!empty($show_favorite) and $show_favorite): ?>
        <div
                class="stm-listing-favorite"
                :data-id="listing.id"
                data-toggle="tooltip" data-placement="right" title="<?php esc_attr_e('Add to favorites', 'motors') ?>">
            <i class="stm-service-icon-staricon"></i>
        </div>
    <?php endif; ?>


    <!-- SOLD/EDIT/DISABLE buttons if not pending section -->

    <div v-if="listing.status != 'pending'" class="stm_edit_disable_car heading-font">

		<div class="stm_sold_sell_wrap">
			<a v-if="listing.sold" v-bind:href="getUnSoldLink(listing)" class="as_sold">
				<?php esc_html_e('Unmark as sold', 'motors'); ?><i class="fa fa-check-square-o" aria-hidden="true"></i>
			</a>

			<a v-else v-bind:href="getSoldLink(listing)">
				<?php esc_html_e('Mark as sold', 'motors'); ?><i class="fa fa-check-square-o" aria-hidden="true"></i>
			</a>

			<?php if($sellOnline): ?>
				<?php if($isSellOnline == 'on'): ?>
					<a href="<?php echo esc_url(add_query_arg(array('stm_unmark_woo_online' => get_the_ID(), 'nonce' => $sellOnlineNonce), stm_get_author_link(''))); ?>" class="as_sold">
						<?php esc_html_e('Don\'t sell online', 'motors'); ?>
						<i class="fa fa-check-square-o" aria-hidden="true"></i>
					</a>
				<?php else : ?>
					<a href="<?php echo esc_url(add_query_arg(array('stm_mark_woo_online' => get_the_ID(), 'nonce' => $sellOnlineNonce), stm_get_author_link(''))); ?>">
						<?php esc_html_e('Sell online', 'motors'); ?>
						<i class="fa fa-check-square-o" aria-hidden="true"></i>
					</a>
				<?php endif; ?>
			<?php endif; ?>
		</div>

        <!-- EDIT BUTTON -->
        <a v-bind:href="getEditLink(listing)"
		   data-toggle="tooltip"
		   data-placement="top"
		   title="<?php esc_html_e('Edit', 'motors') ?>">
            <?php //esc_html_e('Edit', 'motors'); ?><i class="fa fa-pencil"></i>
        </a>
        <!-- ENABLE/DISABLE Button -->

        <a v-if="listing.status=='draft'" v-bind:href="getEnableLink(listing)"
		   data-toggle="tooltip"
		   data-placement="top"
		   title="<?php esc_html_e('Enable', 'motors') ?>"
           class="enable_list"><?php //esc_html_e('Enable', 'motors'); ?><i class="fa fa-eye"></i></a>


        <a v-else v-bind:href="getDisableLink(listing)"
           class="disable_list"
		   data-toggle="tooltip"
		   data-placement="top"
		   title="<?php esc_html_e('Disable', 'motors') ?>"
           v-bind:data-id="listing.id"><?php //esc_html_e('Disable', 'motors'); ?>
			<i class="fa fa-eye-slash"></i></a>
		<?php
		if(get_theme_mod('dealer_payments_for_featured_listing', false)) :?>
			<?php
			$featuredStatus = get_post_meta(get_the_ID(), 'car_make_featured_status', true);
			if(!$special_car && (empty($featuredStatus) || $featuredStatus == 'in_cart')) :
				?>
				<a href="<?php echo esc_url(add_query_arg(array('stm_make_featured' => get_the_ID()), stm_get_author_link(''))); ?>" class="make_featured"
				   data-toggle="tooltip"
				   data-placement="top"
				   title="<?php esc_html_e('Make Featured', 'motors') ?>">
					<i class="fa fa-star" aria-hidden="true"></i>
				</a>
			<?php else :
				$featuredText = (($special_car && ($featuredStatus == 'completed' || $featuredStatus == 'processing')) || $special_car && empty($featuredClass)) ? 'Featured' : 'Featured (pending)';
				$featuredClass = (($special_car && ($featuredStatus == 'completed' || $featuredStatus == 'processing')) || $special_car && empty($featuredClass)) ? 'featured' : 'featured_pending';
				?>
				<span class="<?php echo esc_attr($featuredClass); ?>"
					  data-toggle="tooltip"
					  data-placement="top"
					  title="<?php stm_dynamic_string_translation_e('Featured Text', $featuredText) ?>"
				><i class="fa fa-star" aria-hidden="true"></i></span>
			<?php endif; ?>
		<?php endif; ?>
    </div>

    <!-- EDIT/DELETE buttons if pending section -->

    <div v-else class="stm_edit_pending_car">
        <h4><?php esc_html_e('Pending', 'motors'); ?></h4>
        <div class="stm-dots"><span></span><span></span><span></span></div>
        <a v-bind:href="getEditLink(listing)">
            <?php esc_html_e('Edit', 'motors'); ?>
            <i class="fa fa-pencil"></i>
        </a>
        <a class="stm-delete-confirmation"
           v-bind:href="getTrashLink(listing)"
           v-bind:data-title="listing.title">
            <?php esc_html_e('Delete', 'motors'); ?>
            <i class="fa fa-trash-o"></i>
        </a>
    </div>


    <!-- MOVE TO TRASH BUTTON-->

    <div v-if="listing.status=='draft'" class="stm_car_move_to_trash">
        <a class="stm-delete-confirmation"
           v-bind:href="getTrashLink(listing)"
           v-bind:data-title="listing.title">
            <i class="fa fa-trash-o"></i>
        </a>
    </div>

    <a :href="'<?php echo site_url(stm_get_listings_base_url() . '/') ?>' + listing.slug" class="rmv_txt_drctn">
        <div class="image-inner">
            <div v-if="listing.is_special && listing.badge_text"
                 class="stm-badge-directory heading-font"
                 :style="'background-color: ' + listing.badge_bg_color"
            >
                {{ listing.badge_text }}
            </div>

            <?php $plchldr = 'plchldr350.png'; ?>

            <img
                    v-if="getFirstImage(listing.gallery)"
                    :src="getImageSize(getFirstImage(listing.gallery), 796, 466, true)"
                    src2="<?php echo esc_url(get_stylesheet_directory_uri() . '/assets/images/' . $plchldr); ?>"
                    class="lazy img-responsive"
                    :alt="listing.title"
            />


            <img
                    v-else
                    src="<?php echo esc_url(get_stylesheet_directory_uri() . '/assets/images/' . $plchldr); ?>"
                    class="img-responsive"
                    alt="<?php esc_html_e('Placeholder', 'motors'); ?>"
            />

            <div v-if="listing.sold" class="stm-badge-directory heading-font"
                 :style="'background-color: ' + listing.badge_bg_color">
                <?php echo esc_html__('Sold', 'motors'); ?>
            </div>
        </div>
    </a>
</div>

<!--  Timer Component  -->
<div v-if="listing.subscription_expires_r" class="stm_car_plan_expired" style="display:none">
	<Timer
		:starttime="new Date()"
		:endtime="new Date(listing.subscription_expires_r)"
		trans='{
			"day":"Day",
			"hours":"Hours",
			"minutes":"Minuts",
			"seconds":"Seconds",
			"expired":"Listing has been expired.",
			"running":"Till the end of plan.",
			"upcoming":"Till start of plan.",
			"status": {
			"expired":"Expired",
			"running":"Running",
			"upcoming":"Future"
		}}'
	></Timer>
</div>
<!--  End! Timer Component  -->

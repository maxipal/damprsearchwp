<?php
/**
 * File: inventory.php
 */


$notify = new STMNotification;
$cnt = $notify->getAuctionsBids(false);
$auction_bids = $notify->notifies['auction_bids'];
$notify->saveType('auction_bids');
?>
<div class="stm-delete-confirmation-popup stm-disabled">
    <i class="fa fa-close"></i>
    <div class="stm-confirmation-text heading-font">
        <span class="stm-danger"><?php esc_html_e('Delete', 'motors'); ?></span>
        <span class="stm-car-title"></span>
    </div>
    <div class="actions">
        <a href="#" class="button stm-red-btn"><?php esc_html_e('Delete', 'motors'); ?></a>
        <a href="#" class="button stm-grey-btn"><?php esc_html_e('Cancel', 'motors'); ?></a>
    </div>
</div>
<div class="stm-delete-confirmation-overlay stm-disabled"></div>
<h4 class="stm-seller-title stm-main-title"><?php esc_html_e('My Auction', 'motors'); ?></h4>

<form data-trigger="private-inventory"></form>
<div class="clearfix"></div>
<div id="private-inventory" v-cloak data-notify_bids="<?php if($cnt) echo htmlspecialchars(json_encode($auction_bids), ENT_QUOTES, 'UTF-8') ?>">
    <div class="archive-listing-page">
        <div v-for="(listing, index) in listings"
			 :key="index"
             :class="'listing-list-loop stm-listing-directory-list-loop stm-isotope-listing-item all listing-list-loop-edit stm-listing-no-price-labels status_' + listing.status">

            <?php
            $show_compare = get_theme_mod('show_listing_compare', true);
            $show_favorite = get_theme_mod('enable_favorite_items', true);
            ?>
            <div class="image">
                <!--Hover blocks-->
                <!---Media-->
                <div class="stm-car-medias">
                    <div v-if="listing.gallery.length" @click="openGallery(listing.gallery, 'image')"
                         class="stm-listing-photos-unit">
                        <i class="stm-service-icon-photo"></i>
                        <span>{{listing.gallery.length}}</span>
                    </div>
                    <div v-if="listing.videos.length" @click="openGallery(listing.videos, 'iframe')"
                         class="stm-listing-videos-unit">
                        <i class="fa fa-film"></i>
                        <span>{{listing.videos.length}}</span>
                    </div>
                </div>

                <!--Compare-->
                <?php if (!empty($show_compare) and $show_compare): ?>
                    <div
                            class="stm-listing-compare"
                            :data-id="listing.id"
                            :data-title="listing.title"
                            data-toggle="tooltip" data-placement="left" title="<?php esc_attr_e('Add to compare', 'motors') ?>">
                        <i class="stm-service-icon-compare-new"></i>
                    </div>
                <?php endif; ?>

                <!--Favorite-->
                <?php if (!empty($show_favorite) and $show_favorite): ?>
                    <div
                            class="stm-listing-favorite"
                            :data-id="listing.id"
                            data-toggle="tooltip" data-placement="right" title="<?php esc_attr_e('Add to favorites', 'motors') ?>">
                        <i class="stm-service-icon-staricon"></i>
                    </div>
                <?php endif; ?>


                <!-- SOLD/EDIT/DISABLE buttons if not pending section -->

                <div v-if="listing.status != 'pending'" class="stm_edit_disable_car heading-font">

                    <a v-if="listing.sold" v-bind:href="getUnSoldLink(listing)" class="as_sold">
                        <?php esc_html_e('Unmark as sold', 'motors'); ?><i class="fa fa-check-square-o" aria-hidden="true"></i>
                    </a>

                    <a v-else v-bind:href="getSoldLink(listing)">
                        <?php esc_html_e('Mark as sold', 'motors'); ?><i class="fa fa-check-square-o" aria-hidden="true"></i>
                    </a>


                    <!-- EDIT BUTTON -->
                    <a v-bind:href="getEditLink(listing)">
                        <?php esc_html_e('Edit', 'motors'); ?><i class="fa fa-pencil"></i>
                    </a>
                    <!-- ENABLE/DISABLE Button -->

                    <a v-if="listing.status=='draft'" v-bind:href="getEnableLink(listing)"
                       class="enable_list"><?php esc_html_e('Enable', 'motors'); ?><i class="fa fa-eye"></i></a>


                    <a v-else v-bind:href="getDisableLink(listing)"
                       class="disable_list"
                       v-bind:data-id="listing.id"><?php esc_html_e('Disable', 'motors'); ?><i
                                class="fa fa-eye-slash"></i></a>

                </div>

                <!-- EDIT/DELETE buttons if pending section -->

                <div v-else class="stm_edit_pending_car">
                    <h4 v-if="!listing.accepted"><?php esc_html_e('Pending', 'motors'); ?></h4>
                    <h4 v-else><?php esc_html_e('Closed', 'motors'); ?></h4>
                    <div class="stm-dots"><span></span><span></span><span></span></div>
                    <a v-bind:href="getEditLink(listing)">
                        <?php esc_html_e('Edit', 'motors'); ?>
                        <i class="fa fa-pencil"></i>
                    </a>
                    <a class="stm-delete-confirmation"
                       v-bind:href="getTrashLink(listing)"
                       v-bind:data-title="listing.title">
                        <?php esc_html_e('Delete', 'motors'); ?>
                        <i class="fa fa-trash-o"></i>
                    </a>
                </div>


                <!-- MOVE TO TRASH BUTTON-->

                <div v-if="listing.status=='draft'" class="stm_car_move_to_trash">
                    <a class="stm-delete-confirmation"
                       v-bind:href="getTrashLink(listing)"
                       v-bind:data-title="listing.title">
                        <i class="fa fa-trash-o"></i>
                    </a>
                </div>

                <a :href="'<?php echo site_url(stm_get_listings_base_url() . '/') ?>' + listing.slug" class="rmv_txt_drctn">
                    <div class="image-inner">
                        <div v-if="listing.is_special && listing.badge_text"
                             class="stm-badge-directory heading-font"
                             :style="'background-color: ' + listing.badge_bg_color"
                        >
                            {{listing.badge_text}}
                        </div>

                        <?php $plchldr = 'plchldr350.png'; ?>

                        <img
                            v-if="getFirstImage(listing.gallery)"
                            :src="getImageSize(getFirstImage(listing.gallery), 796, 466, true)"
                            src2="<?php echo esc_url(get_stylesheet_directory_uri() . '/assets/images/' . $plchldr); ?>"
                            class="lazy img-responsive"
                            :alt="listing.title"
                        />


                        <img
                            v-else
                            src="<?php echo esc_url(get_stylesheet_directory_uri() . '/assets/images/' . $plchldr); ?>"
                            class="img-responsive"
                            alt="<?php esc_html_e('Placeholder', 'motors'); ?>"
                        />

                        <div v-if="listing.sold" class="stm-badge-directory heading-font"
                             :style="'background-color: ' + listing.badge_bg_color">
                            <?php echo esc_html__('Sold', 'motors'); ?>
                        </div>
                    </div>
                </a>
            </div>



            <div class="content">
                <div class="meta-top">
                    <div class="price">
                        <?php if(false): ?>
                            <div class="normal-price">
                                <span class="heading-font"><?php _e("Auction","motors") ?></span>
                            </div>
                        <?php endif ?>
                    </div>
                    <div class="title heading-font">
                        <a :href="'<?php echo site_url(stm_get_listings_base_url() . '/') ?>' + listing.slug" class="rmv_txt_drctn">
                            {{listing.title}}
                        </a>
                    </div>

                </div>
                <div class="meta-middle">
                    <?php stm_hybrid_include('loop/vue-list/middle-options') ?>
                </div>
                <div class="meta-bottom">
                    <?php stm_hybrid_include('loop/vue-list/bottom-actions'); ?>
                </div>


            </div>

            <ul class="panel-group cabinet_list mybits_list">
                <li class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="" data-toggle="collapse" data-parent="#accordion" :href="'#collapse_'+index"><?php _e("Offers","motors") ?></a>
                            <span v-if="listing.notify" class="notify">{{listing.notify}}</span>
                        </h4>
                    </div>
                    <div :id="'collapse_'+index" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="tabs_information">
                                <table>
                                    <thead>
                                        <tr>
                                            <th style="width:30%"><?php _e("Dealer","motors") ?></th>
                                            <th style="width:50%"><?php _e("Offer","motors") ?></th>
                                            <th style="width:20%"><?php _e("Action","motors") ?></th>
                                        </tr>
                                    </thead>
                                    <tbody v-for="(bid, key) in listing.bids" :key="key">
                                        <tr>
                                            <td><a :href="bid.dealer_link">{{bid.dealer_name}}</a></td>
                                            <td>{{bid.offer}}</td>
                                            <td>
                                                <a v-if="(!listing.accepted)" :data-id="bid.id" class="confirm_accept button" href="#"><?php _e("Accept","motors") ?></a>
                                                <span class="accepted" v-if="(bid.accepted)"><?php _e("Accepted","motors") ?></span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

    </div>
    <?php stm_hybrid_include('vue-pagination'); ?>
</div>


<?php
/**
 * File: inventory.php
 */
?>
<div class="stm-delete-confirmation-popup stm-disabled">
    <i class="fa fa-close"></i>
    <div class="stm-confirmation-text heading-font">
        <span class="stm-danger"><?php esc_html_e('Delete', 'motors'); ?></span>
        <span class="stm-car-title"></span>
    </div>
    <div class="actions">
        <a href="#" class="button stm-red-btn"><?php esc_html_e('Delete', 'motors'); ?></a>
        <a href="#" class="button stm-grey-btn"><?php esc_html_e('Cancel', 'motors'); ?></a>
    </div>
</div>
<div class="stm-delete-confirmation-overlay stm-disabled"></div>
<h4 class="stm-seller-title stm-main-title"><?php esc_html_e('Auctions', 'motors'); ?></h4>

<form data-trigger="auctions-inventory"></form>


<?php
// Update view notification
$notify = new STMNotification;
$notify->getAuctions(false);
$notify->saveType('auctions');
?>


<div id="auctions-inventory" v-cloak>
    <div class="stm-sort-auction">
        <div class="select-type">
            <div class="stm-label-type"><?php esc_html_e('Sort by', 'motors'); ?></div>
            <select v-model="v_status" @change="onSelected">
                <option value=""><?php esc_html_e('All', 'motors'); ?></option>
                <option value="without_my_proposal"><?php esc_html_e('Show only cars without my proposal', 'motors'); ?></option>
                <option value="pending"><?php esc_html_e('Show deleted cars', 'motors'); ?></option>
                <option value="with_my_proposal"><?php esc_html_e('Show cars with my proposal', 'motors'); ?></option>
            </select>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="archive-listing-page">
        <div v-for="(auction, index) in listings"
			 :key="index"
             :data-id="auction.id"
             :class="'listing-list-loop stm-listing-directory-list-loop stm-isotope-listing-item all listing-list-loop-edit stm-listing-no-price-labels ' + auction.v_status">
            <div class="image">
                <!--Hover blocks-->
                <!---Media-->
                <div class="stm-car-medias">
                    <div v-if="auction.gallery.length" @click="openGallery(auction.gallery, 'image')" class="stm-listing-photos-unit">
                        <i class="stm-service-icon-photo"></i>
                        <span>{{auction.gallery.length}}</span>
                    </div>
                    <div v-if="auction.videos && auction.videos.length > 0" @click="openGallery(auction.videos, 'iframe')" class="stm-listing-videos-unit">
                        <i class="fa fa-film"></i>
                        <span>{{auction.videos.length}}</span>
                    </div>
                </div>

                <a :href="'<?php echo site_url(stm_get_listings_base_url() . '/') ?>' + auction.slug" class="rmv_txt_drctn">
                    <div class="image-inner">
                        <div v-if="auction.is_special && auction.badge_text"
                             class="stm-badge-directory heading-font"
                             :style="'background-color: ' + auction.badge_bg_color"
                        >
                            {{auction.badge_text}}
                        </div>

                        <?php $plchldr = 'plchldr350.png'; ?>

                        <img v-lazy="getImageSize(getFirstImage(auction.gallery), 796, 466, true)"
                             :key="getImageSize(getFirstImage(auction.gallery), 796, 466, true)"
                            v-if="getFirstImage(auction.gallery)"
                            class="lazy img-responsive"
                            :alt="auction.title"
                        />

                        <img
                            src="<?php echo esc_url(get_stylesheet_directory_uri().'/assets/images/' . $plchldr); ?>"
                            class="img-responsive placeholder"
                            alt="<?php esc_html_e('Placeholder', 'motors'); ?>"
                        />

                        <div v-if="auction.sold" class="stm-badge-directory heading-font" :style="'background-color: ' + auction.badge_bg_color">
                            <?php echo esc_html__('Sold', 'motors'); ?>
                        </div>
                    </div>
                </a>
            </div>
            <div class="content">
                <div class="meta-top">
                    <div class="price">
                        <?php if(false): ?>
                            <div class="normal-price">
                                <span class="heading-font"><?php _e("Auction","motors") ?></span>
                            </div>
                        <?php endif ?>
                    </div>
                    <div class="title heading-font">
                        <a :href="'<?php echo site_url(stm_get_listings_base_url() . '/') ?>' + auction.slug" class="rmv_txt_drctn">
                            {{auction.title}}
                        </a>
                    </div>

                </div>
                <div class="meta-middle">
                    <div class="meta-middle-row clearfix">
                        <template v-for="(attribute, key) in auction.attributes" v-if="attribute.useInList">
                            <div v-if="attribute.value !== null" class="meta-middle-unit" :class="{key: true, 'font-exists': attribute.icon}">
                                <div class="meta-middle-unit-top">
                                    <div v-if="attribute.icon" class="icon"><i :class="attribute.icon"></i></div>
                                    <div class="name">{{attribute.label}}</div>
                                </div>
                                <div class="value">
                                    <div
                                        v-if="key === 'location'"
                                        class="stm-tooltip-link"
                                        data-toggle="tooltip"
                                        data-placement="bottom"
                                        :title="attribute.value">
                                        {{attribute.value}}
                                    </div>
                                    <div v-else>{{attribute.value}}</div>
                                </div>
                            </div>
                            <div class="meta-middle-unit meta-middle-divider"></div>
                        </template>
                    </div>
                </div>
                <div class="meta-bottom">
                    <?php

                    $show_compare = false;
                    //Show car actions
                    $show_pdf = get_theme_mod('show_listing_pdf', false);
                    $show_stock = get_theme_mod('show_listing_stock', false);
                    $show_share = get_theme_mod('show_listing_share', false);
                    $show_test_drive = get_theme_mod('show_listing_test_drive', false);
                    $show_certified_logo_1 = get_theme_mod('show_listing_certified_logo_1', true);
                    $show_certified_logo_2 = get_theme_mod('show_listing_certified_logo_2', false);
                    $listing_directory_enable_dealer_info = get_theme_mod('listing_directory_enable_dealer_info', true);
                    ?>

                    <div class="single-car-actions">
                        <ul class="list-unstyled clearfix">

                            <?php if(!empty($listing_directory_enable_dealer_info) and !empty($listing_directory_enable_dealer_info) and $listing_directory_enable_dealer_info): ?>
                                <li class="car-action-dealer-info">
                                    <div v-if="auction.author" class="listing-archive-dealer-info clearfix">
                                        <div v-if="auction.author.is_dealer" class="dealer-img">
                                            <div class="stm-dealer-image-custom-view">
                                                <a :href="auction.author.link">
                                                    <img v-if="auction.author.logo" class="img-responsive" :src="auction.author.logo">
                                                    <img v-else class="img-responsive" src="<?php stm_get_dealer_logo_placeholder(); ?>">
                                                </a>
                                            </div>
                                        </div>
                                        <div v-else class="dealer-image">
                                            <a :href="auction.author.link">
                                                <img v-if="user_image" class="stm-user-image img-responsive" :src="user_image">
                                                <div v-else class="stm-user-image-empty">
                                                    <i class="stm-service-icon-user"></i>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="dealer-info-block" :class="{'stm_phone_disabled': !auction.author.phone}">
                                                <a v-if="auction.author.is_dealer" :href="auction.author.link" class="title">{{auction.author.name}}</a>
                                                <div v-else class="title">
                                                    <span><?php esc_html_e('Private Seller', 'motors'); ?>: </span>
                                                    <a :href="auction.author.link">{{auction.author.name}}</a>
                                                </div>
                                            <div v-if="auction.author.phone"  class="dealer-information">
                                                    <div class="phone">
                                                        <i class="stm-service-icon-phone"></i>
                                                       {{auction.author.phone.substring(3, 0) + '*******'}}
                                                    </div>
                                                <span class="stm-show-number_custom" :data-id="auction.author.phone"><?php echo esc_html__("Show number", "motors"); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                            <?php endif; ?>

                            <!--Stock num-->
                            <?php if(!empty($show_stock) and $show_stock): ?>
                                <li>
                                    <div class="stock-num heading-font">
                                        <span><?php esc_html_e('stock', 'motors'); ?># </span>
                                        {{auction.stock_number}}
                                    </div>
                                </li>
                            <?php endif; ?>

                            <!--Schedule-->
                            <?php if(!empty($show_test_drive) and $show_test_drive): ?>
                                <li>
                                    <a href="#" class="car-action-unit stm-schedule" data-toggle="modal" data-target="#test-drive"
                                       @click="stmTestDriveCarTitle(auction.id, auction.title)">
                                        <i class="stm-icon-steering_wheel"></i>
                                        <?php esc_html_e('Schedule Test Drive', 'motors'); ?>
                                    </a>
                                </li>
                            <?php endif; ?>
                        </ul>
                        <div class="single-car-actions-footer">
                            <!--PDF-->
                            <?php if(!empty($show_pdf) and $show_pdf): ?>
                                <div v-if="auction.brochure" class="stm-brochure-item">
                                    <a :href="auction.brochure.url" class="car-action-unit stm-brochure"
                                       title="<?php esc_html_e('Download brochure', 'motors'); ?>"
                                       target="_blank" download>
                                        <i class="stm-icon-brochure"></i>
                                        <?php esc_html_e('Car brochure', 'motors'); ?>
                                    </a>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>


            </div>

            <ul class="panel-group cabinet_list mybits_list">
                <li class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="" data-toggle="collapse" data-parent="#accordion" :href="'#collapse_'+index"><?php _e("Offers","motors") ?></a>
                        </h4>
                    </div>
                    <div :id="'collapse_'+index" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="">
                                <table>
                                    <thead>
                                        <tr>
                                            <th style="width:30%"><?php _e("Dealer","motors") ?></th>
                                            <th style="width:50%"><?php _e("Offer","motors") ?></th>
                                            <th style="width:20%"><?php _e("Status","motors") ?></th>
                                        </tr>
                                    </thead>
                                    <tbody v-for="(bid, key) in auction.bids" :key="key">
                                        <tr>
                                            <td><a :href="bid.dealer_link">{{bid.dealer_name}}</a></td>
                                            <td>{{bid.offer}}</td>
                                            <td>
                                                <span class="accepted" v-if="bid.accepted"><?php _e("Accepted","motors") ?></span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>



                                <div class="wrap_auction_offer_form">
                                    <div class="wrap_auction_offer_input">
                                        <textarea v-model="auction_offer" placeholder="<?php _e("Type your offer for this auction vehicle","motors") ?>"></textarea>
                                    </div>
                                    <div class="wrap_auction_offer_button">
                                        <button class="btn-action" v-on:click="sendBid(auction)">
                                            <span><?php _e("Submit","motors") ?></span>
                                            <div class="spinner">
                                                <div class="rect1"></div>
                                                <div class="rect2"></div>
                                                <div class="rect3"></div>
                                                <div class="rect4"></div>
                                                <div class="rect5"></div>
                                            </div>
                                        </button>
                                    </div>
                                    <button class="btn-action remove_auction" v-on:click="removeAuction(auction,$event)">
                                        <span><?php _e("Remove","motors") ?></span>
                                        <div class="spinner">
                                            <div class="rect1"></div>
                                            <div class="rect2"></div>
                                            <div class="rect3"></div>
                                            <div class="rect4"></div>
                                            <div class="rect5"></div>
                                        </div>
                                    </button>
                                    <div class="wpcf7-response-output wpcf7-display-none"></div>
                                </div>
                            </div>


                        </div>
                    </div>
                </li>
            </ul>
        </div>

    </div>
    <?php stm_hybrid_include('vue-pagination'); ?>
</div>


<script type="text/javascript">

</script>

<div class="stm-delete-confirmation-popup stm-disabled">
    <i class="fa fa-close"></i>
    <div class="stm-confirmation-text heading-font">
        <span class="stm-danger"><?php esc_html_e('Delete', 'motors'); ?></span>
        <span class="stm-car-title"></span>
    </div>
    <div class="actions">
        <a href="#" class="button stm-red-btn"><?php esc_html_e('Delete', 'motors'); ?></a>
        <a href="#" class="button stm-grey-btn"><?php esc_html_e('Cancel', 'motors'); ?></a>
    </div>
</div>
<div class="stm-delete-confirmation-overlay stm-disabled"></div>
<h4 class="stm-seller-title stm-main-title"><?php esc_html_e('My Inventory', 'motors'); ?></h4>

<form data-trigger="private-inventory"></form>
<div class="stm-sort-private-my-cars">
    <div class="select-type">
        <div class="stm-label-type"><?php esc_html_e('Sort by', 'motors'); ?></div>
        <select >
            <option value="all"><?php esc_html_e('All', 'motors'); ?></option>
            <option value="pending"><?php esc_html_e('Pending', 'motors'); ?></option>
            <option value="draft"><?php esc_html_e('Disabled', 'motors'); ?></option>
        </select>
    </div>
</div>
<div class="clearfix"></div>
<div id="private-inventory" v-cloak style="display: none">
    <div class="archive-listing-page">
        <div v-for="(listing, index) in listings"
			 :key="index"
             :class="'listing-list-loop stm-listing-directory-list-loop stm-isotope-listing-item all listing-list-loop-edit stm-listing-no-price-labels ' + listing.status">
            <?php stm_hybrid_include('private/inv-image'); ?>
            <div class="content">
                <div class="meta-top">
                    <div class="price" v-if="!listing.price_sale">
                        <div class="normal-price">
                            <span class="heading-font">{{ listing.price_view }}</span>
                        </div>
                    </div>
					<div class="price discounted-price" v-else-if="listing.price_sale && listing.price_regular_view">
						<div class="regular-price">{{listing.price_regular_view}}</div>
                    	<div class="sale-price">
							<span class="heading-font">{{listing.price_view}}</span>
						</div>
					</div>
					<div class="price" v-else>
                        <div class="normal-price">
                            <span class="heading-font">{{ listing.price_view }}</span>
                        </div>
                    </div>
                    <div class="title heading-font">
                        <a :href="'<?php echo site_url(stm_get_listings_base_url() . '/') ?>' + listing.slug + '/'" class="rmv_txt_drctn">
                            {{ listing.title }}
                        </a>
                    </div>
                </div>
                <div class="meta-middle">
                    <?php stm_hybrid_include('loop/vue-list/middle-options') ?>
                </div>
                <div class="meta-bottom">
                    <?php stm_hybrid_include('loop/vue-list/bottom-actions'); ?>
                </div>
            </div>
        </div>

    </div>
    <?php stm_hybrid_include('vue-pagination'); ?>
</div>

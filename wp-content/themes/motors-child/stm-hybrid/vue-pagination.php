<listing-pagination v-show="itemsLoaded" v-cloak inline-template ref="pagination">
	<div class="stm_ajax_pagination stm-blog-pagination">
        <ul v-if="last > 1" class="page-numbers">
            <li v-if="current > 1">
                <a @click.prevent="emitClick(current - 1)" class="prev page-numbers" :href="getUrl(current - 1)">
                    <i class="fa fa-angle-left"></i>
                </a>
            </li>
            <li v-for="(page, index) in pages" :key="index">
                <span v-if="current === page" aria-current="page" class="page-numbers current">{{page}}</span>
                <a @click.prevent="emitClick(page)" v-else-if="typeof page === 'number'" class="page-numbers" :href="getUrl(page)">{{page}}</a>
                <span v-else class="page-numbers dots">…</span>
            </li>
            <li v-if="current !== last">
                <a @click.prevent="emitClick(current + 1)" class="next page-numbers" :href="getUrl(current + 1)">
                    <i class="fa fa-angle-right"></i>
                </a>
            </li>
        </ul>
    </div>
</listing-pagination>

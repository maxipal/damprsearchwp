<div v-cloak v-if="badges.length" class="stm-filter-chosen-units">
    <ul class="stm-filter-chosen-units-list">
        <li v-for="(badge, index) in badges" :key="index">
            <span>{{badge.name}}: </span> {{badge.value}}
            <i @click="unsetBadge(badge)" class="fa fa-close"></i>
        </li>
    </ul>
</div>

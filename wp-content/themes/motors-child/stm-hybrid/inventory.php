<?php
wp_enqueue_script('listing-script');

$sidebar_pos = stm_get_sidebar_position();
?>

<div class="archive-listing-page">
	<div class="container">
		<div class="row">

			<div class="col-md-3 col-sm-12 classic-filter-row sidebar-sm-mg-bt ">
				<?php stm_hybrid_include('sidebar'); ?>
			</div>

			<div class="col-md-9 col-sm-12 <?php //echo $sidebar_pos['content'] ?>">
				<div id="listings" class="stm-ajax-row">
                    <div
						id="listings-result"
						data-per-page="<?php echo get_theme_mod( 'listing_grid_choice', 9 ) ?>">
                        <?php stm_hybrid_include( 'vue-results', compact( 'result' ) ); ?>
					</div>
					<?php
					// For better user experience we will render listings
					// in backend, then after Vue initialization this block will be hidden
					echo '<div data-trigger="vue-remove">';
						stm_hybrid_include( 'results-list' );
						stm_hybrid_include( 'pagination' );
					echo '</div>';
					?>
				</div>
				<div
                    class="col-md-12 custom-description"
                    <?php if(empty(listings('pageDescription'))) echo 'style="display: none"' ?>>
                        <?php echo listings('pageDescription'); ?>
                </div>
			</div>

		</div>
	</div>
</div>

<script type="text/javascript">
	var listing_query_append = <?php echo json_encode( stm_hybrid_listings_query_append() ) ?>;
	var listing_result = <?php echo json_encode( $GLOBALS['listings'] ) ?>;
</script>

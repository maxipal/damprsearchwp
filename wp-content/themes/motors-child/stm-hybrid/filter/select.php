<div class="<?php echo $filter->multiple ? 'stm-multiple-select' : 'form-group' ?>">

	<?php if ($filter->multiple) : ?>
		<div class="clearfix">
			<h5 class="pull-left"><?php echo $filter->label; ?></h5>
		</div>
	<?php endif; ?>

    <?php
    $temp = $filter->options;
    $attr = $filter->getAttributes();
    if(stm_is_add_car_page()){
        if(isset($attr['attribute']['placeholder2'])){
            $temp[0]['label'] = $attr['attribute']['placeholder2'];
            $filter->__set('options', $temp);
        }
    }else{
        if(isset($attr['attribute']['placeholder1'])){
            $temp[0]['label'] = $attr['attribute']['placeholder1'];
            $filter->__set('options', $temp);
        }
    }
    ?>

    <select key="<?php echo esc_attr($filter->key) ?>"
            name="filter[<?php echo esc_attr($filter->key) . ']' . ($filter->multiple ? '[]' : ''); ?>"
        <?php echo $filter->multiple ? 'multiple' : '' ?> class="form-control">
        <?php foreach ($filter->options as $value => $option) : ?>
            <?php if($filter->multiple && empty($option['label'])) continue; ?>
            <option value="<?php echo esc_attr($option['value']) ?>"
                    class="<?php echo !empty($option['class']) ? esc_attr($option['class']) : '' ?>"
                <?php selected($option['selected']) ?>
                <?php disabled($option['disabled']) ?>>
                <?php echo esc_html($option['label']); ?>
            </option>
        <?php endforeach; ?>
        <?php if ($filter->value) :
            if (is_array($filter->value)) :
                foreach ($filter->value as $value) : ?>
                    <option value="<?php echo esc_attr($value) ?>" selected></option>
                <?php endforeach; ?>
            <?php else : ?>
                <option value="<?php echo esc_attr($filter->value) ?>" selected></option>
            <?php endif; ?>
        <?php endif; ?>
    </select>
</div>

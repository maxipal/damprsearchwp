<div class="form-group">
	<select key="<?php echo esc_attr( $filter->key ) ?>"
			name="filter[<?php echo esc_attr( $filter->key ) . '][lte]'; ?>"
			class="form-control">
		<option value="">
			<?php
			if($filter->key == 'price') echo __( 'Max Select price', 'motors' );
			else echo __( 'Max '.$filter['label'], 'motors' );
			?>
		</option>
		<?php foreach ( $filter->options['ranges'] as $value => $label ) : ?>
			<option value="<?php echo esc_attr( $value ) ?>"><?php echo $label; ?></option>
		<?php endforeach; ?>
	</select>
</div>

<?php
$start_value = $filter->options['min'];
$end_value = $filter->options['max'];
$slug = $filter->key;
$affix = !empty($filter->attribute['affix']) ? $filter->attribute['affix'] : '';

//if (!empty($filter->value)) {
	$low_val = !empty($filter->value['gte']) ? $filter->value['gte'] : '';
	$high_val = !empty($filter->value['lte']) ? $filter->value['lte'] : '';
//}

$min_value = $start_value;
$max_value = $end_value;

if ($slug == "price" && isset($_COOKIE["stm_current_currency"])) {
	$cookie = explode("-", $_COOKIE["stm_current_currency"]);
	$start_value = ($start_value * $cookie[1]);
	$end_value = ($end_value * $cookie[1]);
	$min_value = $start_value;
	$max_value = $end_value;
}

if (!empty($_GET['min_' . $slug])) {
	$min_value = intval($_GET['min_' . $slug]);
}

if (!empty($_GET['max_' . $slug])) {
	$max_value = intval($_GET['max_' . $slug]);
}

$vars = array(
	'slug' => $slug,
	'affix' => $affix,
	'js_slug' => str_replace('-', 'stmdash', $slug),
	'start_value' => $start_value,
	'end_value' => $end_value,
	'min_value' => $min_value,
	'max_value' => $max_value,
	'low_val' => $low_val,
	'high_val' => $high_val
);

$label_affix = $vars['min_value'] . $affix . ' — ' . $vars['max_value'] . $affix;
if ($slug == 'price') {
	$label_affix = stm_listing_price_view($vars['min_value']) . ' — ' . stm_listing_price_view($vars['max_value']);
}

$vars['label'] = stripslashes($label_affix);

?>

<?php if (empty($collapse)) : ?>
	<div class="filter-<?php echo esc_attr($vars['slug']); ?> stm-slider-filter-type-unit">
		<div class="clearfix">
			<h5 class="pull-left"><?php _e($filter['label'], 'motors'); ?></h5>
			<div class="stm-current-slider-labels"><?php echo $vars['label']; ?></div>
		</div>

		<div class="stm-price-range-unit">
			<div key="<?php echo esc_attr($vars['slug']); ?>"
				 class="stm-<?php echo esc_attr($vars['slug']); ?>-range stm-filter-type-slider"></div>
		</div>
		<div class="row">
			<div class="col-md-6 col-sm-6 col-md-wider-right">
				<input type="text" name="filter[<?php echo esc_attr($vars['slug']); ?>][gte]"
					   placeholder="<?php echo $min_value; ?>"
					   id="stm_filter_min_<?php echo esc_attr($vars['slug']); ?>"
					   class="form-control" <?php if ($vars["slug"] == "search_radius") echo "readonly"; ?> />
			</div>
			<div class="col-md-6 col-sm-6 col-md-wider-left">
				<input type="text" name="filter[<?php echo esc_attr($vars['slug']); ?>][lte]"
					   placeholder="<?php echo $max_value; ?>"
					   id="stm_filter_max_<?php echo esc_attr($vars['slug']); ?>" class="form-control"/>
			</div>
		</div>
	</div>

<?php else : ?>
	<div class="stm-filter stm-filter__panel">

		<div class="stm-filter__content" id="filter_<?php echo $filter->key; ?>" aria-expanded="true">
			<div class="stm-price-range-unit">
				<div key="<?php echo esc_attr($vars['slug']); ?>"
					 class="stm-<?php echo esc_attr($vars['slug']); ?>-range stm-filter-type-slider"></div>
			</div>
			<div class="row">
				<div class="col-md-6 col-sm-6 col-md-wider-right">
					<input type="text" name="filter[<?php echo esc_attr($vars['slug']); ?>][gte]"
						   placeholder="<?php echo number_format($min_value, 0, '.', ' '); ?>"
						   id="stm_filter_min_<?php echo esc_attr($vars['slug']); ?>"
						   class="form-control" <?php if ($vars["slug"] == "search_radius") echo "readonly"; ?> />
				</div>
				<div class="col-md-6 col-sm-6 col-md-wider-left">
					<input type="text" name="filter[<?php echo esc_attr($vars['slug']); ?>][lte]"
						   placeholder="<?php echo number_format($max_value, 0, '.', ' '); ?>"
						   id="stm_filter_max_<?php echo esc_attr($vars['slug']); ?>" class="form-control"/>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>

<!--Init slider-->
<?php stm_listings_load_template('filter/types/slider-js', $vars); ?>


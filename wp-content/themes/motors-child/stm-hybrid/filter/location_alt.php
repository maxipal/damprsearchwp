<!--Location inputs-->
<?php
$stm_location = get_option('filter_default_location', '');
$lat_lng = explode(',', get_option('filter_default_location_latlng', '0,0'));
$stm_lat = !empty($lat_lng[0]) ? $lat_lng[0] : '';
$stm_lng = !empty($lat_lng[1]) ? $lat_lng[1] : '';
//$stm_lat = $stm_lng = $stm_location = '';

if (isset($_GET['ca_location'])) {
	$stm_location = $_GET['ca_location'];
}
if (isset($_GET['stm_lng'])) {
	$stm_lng = $_GET['stm_lng'];
}
if (isset($_GET['stm_lat'])) {
	$stm_lat = $_GET['stm_lat'];
}

$max_radius = get_theme_mod("distance_search", 100);
$radius = get_option('filter_default_search_radius', $max_radius);

if (empty($radius)) {
	$radius = $max_radius;
}

$radiusArr = array(
	'values' => array(),
	'default' => $radius,
);
for ($q = 1; $q <= $max_radius; $q++) {
	$radiusArr['values'][$q] = array("label" => $q);
}

?>

<div class="form-group boats-location">
	<div class="stm-location-search-unit">
		<?php
		echo stm_locations_dropdown(
			array(
				"lat" => esc_attr($stm_lat),
				"lng" => esc_attr($stm_lng),
				"placeholder" => __("Select location","motors")
			)
		); ?>
	</div>
</div>
<?php
if (!empty($filter->search_radius)) : ?>
	<div class="row">
		<?php
		stm_listings_load_template('filter/types/slide', array(
			'taxonomy' => array("slug" => "search_radius", "single_name" => esc_html__("Search radius", 'motors')),
			'options' => $radiusArr
		)); ?>
	</div>
<?php endif;


<?php
$filter_name = 'filter[' . $filter->key . ']';
$selected_filters = !empty($_GET['filter']) ? $_GET['filter'][$filter->key] : [];
?>
<?php if(isset($filter->options)): ?>
	<div class="stm-ajax-checkbox-instant">
		<?php foreach ($filter->options as $key => $option) :
			$checked = !empty($selected_filters) && in_array($option['value'], $selected_filters);
			?>
			<label class="stm-option-label">
				<?php if (!empty($option['image'])) : ?>
					<div class="stm-option-image">
						<img src="<?php echo esc_url($option['image']['url']) ?>">
					</div>
				<?php endif; ?>
				<input type="checkbox" <?php checked($checked); ?> name="filter[<?php echo $filter->key; ?>][]" value="<?php echo $option['value']; ?>">
				<span><?php echo $option['label'] ?></span>
			</label>
		<?php endforeach; ?>
	</div>
<?php endif ?>

<script type="x-template" id="form-features-field">
    <div>
        <div class="stm-single-feature" v-for="group in groups" :key="group.id">
            <div class="heading-font">{{ group.title }}</div>
            <div class="feature-single" v-for="(option, index) in group.options" :key="index">
                <stm-checkbox v-model="field.value" :value="option.value" :label="option.label"></stm-checkbox>
            </div>
        </div>
    </div>
</script>

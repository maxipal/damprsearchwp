<script type="x-template" id="form-attachment-field">
    <div>
        <div class="add_brochure">
            <div class="heat">
                <span class="__label" v-if="!field.value && !field.attached"><?php _e('Add an aircraft spec sheet', 'motors-child'); ?></span>
                <span class="__label" v-else>{{ field.value.name || field.attached.filename }}</span>

                <div class="input_wrap">
                    <form-file-field :field="field" :errors="errors"/>
                    <span class="input-button"><i class="fa fa-plus"></i></span>
                </div>
                <a
                    v-if="field.value"
                    href="#"
                    @click.stop.prevent="clear"
                    style="border-bottom: 1px dashed rgb(21, 62, 77); color: rgb(35, 38, 40); font-size: 14px; font-weight: normal; margin-left: 10px;">Remove</a>
            </div>
        </div>
    </div>
</script>

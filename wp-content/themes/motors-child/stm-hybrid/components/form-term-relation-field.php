<script type="x-template" id="form-term-relation-field">
	<component :is="layoutComponent" :field="field">
		<template slot="field">
			<select
					v-select
					:disabled="busy || !hasAvailableOptions"
					:id="field.name"
					v-model="field.value"
					:multiple="field.multiple || false"
					class="form-control"
					:class="errorClasses">
				<option value="" :selected="field.value === null" :disabled="field.required">
					{{ field.placeholder || 'Choose an option' }}
				</option>
				<option
						v-for="option in options"
						:class="option.class"
						:value="option.value"
						:disabled="option.disabled"
						:selected="option.value == field.value">
					{{ option.label }}
				</option>
			</select>
		</template>
		<template slot="errors">
			<div v-if="hasError" class="invalid-feedback">
				{{ firstError }}
			</div>
		</template>
	</component>
</script>

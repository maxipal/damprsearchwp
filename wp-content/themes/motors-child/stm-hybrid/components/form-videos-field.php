<script type="x-template" id="form-videos-field">
	<div>
		<div v-for="(url, index) in field.value" :key="index">
			<div class="heading-font"><span class="video-label"><?php _e('Video link ', 'motors-child') ?></span><span
					class="count">{{ index + 1 }}</span></div>
			<div class="stm-video-link-unit">
				<input ref="text" type="text" v-model="field.value[index]">
				<div @click.stop="modifyValue(index)" class="stm-after-video"
					 :class="inputControlClasses(index)"></div>
			</div>
		</div>
	</div>
</script>

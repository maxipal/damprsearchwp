<script type="x-template" id="form-select-field">
	<component :is="layoutComponent" :field="field" >
		<template slot="field">
			<select
				v-select
				:id="field.name"
				v-model="field.value"
				:multiple="field.multiple || false"
				class="form-control"
				:class="errorClasses"
				:disabled="inputDisabled">
				<option value="" :selected="field.value === null" :disabled="field.required">
					{{ field.placeholder || 'Choose an option' }}
				</option>
				<option
					v-for="option in field.options"
					:value="option.value"
					:selected="option.value == field.value">
					{{ option.label }}
				</option>
			</select>
		</template>
	</component>
</script>

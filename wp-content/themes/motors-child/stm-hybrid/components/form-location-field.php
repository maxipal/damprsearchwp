<script type="x-template" id="form-location-field">
    <component :is="layoutComponent" :field="field" class="ignore_grid_column">
        <template slot="field">
            <div class="stn-add-car-location-wrap">
                <div class="stm-car-listing-data-single">
                    <div class="title heading-font">
                        <?php esc_html_e('Car Location', 'motors'); ?>
                    </div>
                </div>
                <div class="stm-form-1-quarter stm_location stm-location-search-unit">
                    <div class="stm-location-input-wrap stm-location" :class="{required: field.required}">
                        <div class="stm-label">
                            <i :class="[field.icon]"></i>
                            <span>{{ field.label }}</span>
                        </div>
                        <auto-complete placeholder="<?php _e("Enter ZIP or Address","motors") ?>"></auto-complete>
                    </div>

                    <?php if(true): ?>
                        <div class="stm-location-input-wrap stm-lng">
                            <div class="stm-label">
                                <i :class="[field.icon]"></i>
                                <span><?php esc_html_e('Latitude', 'motors') ?></span>
                            </div>
                            <input v-model="lat" type="text" class="text_stm_lat" placeholder="<?php esc_html_e('Enter Latitude', 'motors') ?>">
                        </div>
                        <div class="stm-location-input-wrap stm-lng">
                            <div class="stm-label">
                                <i :class="[field.icon]"></i>
                                <span><?php esc_html_e('Longitude', 'motors') ?></span>
                            </div>
                            <input v-model="lng" type="text" class="text_stm_lng"  placeholder="<?php esc_html_e('Enter Longitude', 'motors') ?>">
                        </div>

                    <?php endif; ?>
                </div>

				<div class="stm-link-lat-lng-wrap bazuar">
					<a href="http://www.latlong.net/" target="_blank"><?php esc_html_e('Lat and Long Finder', 'motors') ?></a>
				</div>

            </div>
        </template>
        <template slot="errors">
            <div v-if="hasError" class="invalid-feedback">
                {{ firstError }}
            </div>
        </template>
    </component>
</script>

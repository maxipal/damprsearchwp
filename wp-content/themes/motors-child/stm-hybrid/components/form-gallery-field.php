<script type="x-template" id="form-gallery-field">
	<div>
		<div class="col-md-9 col-sm-12 col-md-pull-3">
			<div class="stm-add-media-car">
				<div class="stm-media-car-main-input">
					<input
						ref="fileInput"
						type="file"
						id="gallery_id"
						class="form-control"
						@change="onImageFilesPicked"
						multiple
						accept="image/x-png,image/gif,image/jpeg">

					<div class="stm-placeholder" :class="{hasPreviews: isImagesPicked}">
						<i class="stm-service-icon-photos"></i>
						<a href="#"
						   class="button stm_fake_button"><?php esc_html_e('Choose files', 'motors'); ?></a>
					</div>

					<div v-if="isImagesPicked" class="stm-image-preview"
						 :style="{'background-image': `url('${firstThumb}')`}">
					</div>
				</div>
				<draggable
					v-model="imagesData"
					class="stm-media-car-gallery clearfix"
					:options="{draggable:'.stm-placeholder'}"
					@end="prepareFieldValue">
					<div ref="sortable" v-for="(thumb, index) in thumbs" :key="index"
						 class="stm-placeholder sortable-item" :class="thumbClasses">
						<div class="inner" :class="{active: thumb.url}">
							<i v-if="!thumb.url" class="stm-service-icon-photos"></i>
							<div v-else class="stm-image-preview"
								 :style="{'background-image': `url('${thumb.url}')`}">
								<i class="fa fa-close" @click.stop="removeImage(index)"></i>
							</div>
						</div>
					</div>
				</draggable>
			</div>
		</div>
	</div>
</script>

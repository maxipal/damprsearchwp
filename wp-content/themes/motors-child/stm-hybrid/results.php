<?php
return;
$startTime = microtime(true);
?>

<?php if ( count( listings() ) ) { ?>

    <?php
	    $view_type = sanitize_file_name(stm_listings_input('view_type', get_theme_mod("listing_view_type", "list")));
	    stm_hybrid_include('badges');
        stm_hybrid_include('featured');
    ?>

    <div class="stm-isotope-sorting stm-isotope-sorting-<?php echo esc_attr($view_type); ?>">
        <?php stm_hybrid_include( 'results-list' ); ?>
    </div>

	<?php stm_hybrid_include( 'pagination' ); ?>

<?php } else { ?>

	<h3><?php esc_html_e('Sorry, No results', 'motors') ?></h3>
	<p><?php esc_html_e('You might be interested in the following aircraft:', 'motors') ?></p>

<?php } ?>

<?php
    $generalTime = microtime(true) - $startTime ;
?>
<p>
    <span style="font-size: 12px; margin-right: 20px;">
        <b>Elasticsearch:</b> <?php echo round(listings('took') / 1000, 2) ?>s
    </span>
    <span style="font-size: 12px; margin-right: 20px;">
        <b>Request time:</b> <?php echo round(listings('request_time'), 2) ?>s
    </span>
    <span style="font-size: 12px;">
        <b>Loop render time:</b> <?php echo round($generalTime, 2) ?>s
    </span>
</p>

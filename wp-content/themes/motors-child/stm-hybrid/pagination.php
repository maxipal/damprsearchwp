<div class="stm_ajax_pagination stm-blog-pagination">
	<?php
	$meta  = listings( 'meta' );
	$pages = (array)paginate_links( array(
		'total' => $meta['last_page'],
		'current' => $meta['current_page'],
		'format' => '?page=%#%',
		'type'      => 'array',
		'prev_text' => '<i class="fa fa-angle-left"></i>',
		'next_text' => '<i class="fa fa-angle-right"></i>',
	) );

	$pages = array_map( function ( $page ) {
		if ( preg_match( '/\?page=(\d+)/', $page, $m ) ) {
			$page = str_replace( '<a', '<a rel="' . $m[1] . '"', $page );
		}

		return $page;
	}, $pages);

	echo "<ul class='page-numbers'>\n\t<li>";
	echo join("</li>\n\t<li>", $pages);
	echo "</li>\n</ul>\n";
	?>
</div>

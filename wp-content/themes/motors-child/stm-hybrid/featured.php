<div v-cloak v-if="featured.length" class="stm-featured-top-cars-title">
    <div class="heading-font"><?php esc_html_e( 'Featured Classified', 'motors' ); ?></div>
    <a href="<?php echo esc_url( stm_get_listing_archive_link() . '?featured=only' ); ?>">
        <?php esc_html_e( 'Show all', 'motors' ); ?>
    </a>
</div>

<div v-cloak v-if="featured.length && viewType === 'grid'" class="row row-3 car-listing-row car-listing-modern-grid">
	<div class="stm-isotope-sorting stm-isotope-sorting-featured-top">
		<?php stm_hybrid_include('loop/vue-grid', ['dataSource' => 'featured']); ?>
	</div>
</div>

<div v-cloak v-if="featured.length && viewType === 'list'" class="stm-isotope-sorting stm-isotope-sorting-featured-top">
	<?php stm_hybrid_include('loop/vue-list', ['dataSource' => 'featured']); ?>
</div>


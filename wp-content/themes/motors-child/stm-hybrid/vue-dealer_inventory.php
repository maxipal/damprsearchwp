<?php
wp_enqueue_script('vue-dealer_inventory');


$row = 'row row-3';
$active = 'grid';
$list = '';
$grid = 'active';
if(!empty($_GET['view_type']) and $_GET['view_type'] == 'list') {
    $list = 'active';
    $grid = '';
    $active = 'list';
    $row = 'row-no-border-last';
}

?>
<div v-if="recent" v-cloak>
	<h4 class="stm-seller-title"><?php echo $title; ?></h4>

	<div class="car-listing-row <?php echo esc_attr($row); ?>" v-if="viewType==='grid'">
		<div>
			<?php stm_hybrid_include('loop/vue-grid', ['dataSource' => 'recent']); ?>
		</div>
	</div>
	<div class="car-listing-row" v-else>
		<div class="stm-isotope-sorting stm-isotope-sorting-list">
			<?php stm_hybrid_include('loop/vue-list', ['dataSource' => 'recent']); ?>
		</div>
	</div>

	<div
		class="text-center"
		v-if="last_page > pageRecentCounter"
		v-on:click.prevent="getRecent()">
		<a
		v-on:click="pageRecentCounter += 1"
		:data-page="pageRecentCounter"
		id="#more"
		class="stm-load-more-dealers getRecent button"
		href="#more" data-offset="12">
			<span><?php esc_html_e( 'Show more', 'motors' ) ?></span>
		</a>
	</div>
</div>
<div v-else v-cloak>
    <h2 class="stm-seller-title h4" style="color:#aaa; margin-top:44px">
		<?php esc_html_e('No Inventory added yet.', 'motors'); ?>
	</h2>
</div>





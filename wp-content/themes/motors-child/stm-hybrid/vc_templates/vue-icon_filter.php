<?php
if (empty($limit)) {
	$limit = 20;
}

$res = stm_hybrid_api_transient( 180, 'listings/get-terms?taxonomy=' . $filter_selected );

$asCarousel = (!empty($as_carousel)) ? $as_carousel : 'no';
$randId = 'owl' . rand(1, 10000000);

if (!is_wp_error($res)) {
	$data = $res->get_data();
	echo <<<SCRIPT
<script type="text/javascript">
if (typeof stm_hybrid_icon_filter === 'undefined') stm_hybrid_icon_filter = {}
stm_hybrid_icon_filter.$filter_selected = $data
</script>
SCRIPT;
}
?>
<div class="stm_icon_filter_unit" data-count="<?php echo count(json_decode($data, true)) ?>" data-taxonomy="<?php echo($filter_selected) ?>" data-per_page="<?php echo($limit) ?>"
	 v-cloak>
	<div class="clearfix">
		<div v-if="false" v-on:click="withImage = !withImage" class="stm_icon_filter_label">
			<?php echo esc_attr($duration); ?>
		</div>
		<div class="stm_icon_filter_title">
			<?php echo($vc_content); ?>
		</div>
	</div>
	<div id="<?php echo esc_attr($randId); ?>" class="stm_listing_icon_filter stm_listing_icon_filter_<?php echo($per_row); ?> text-<?php echo($align); ?> ">
		<a v-for="term in byImage(terms)" :href="term.link" class="stm_listing_icon_filter_single"
		   :title="term.title">
			<div class="inner">
				<div v-show="withImage" class="image">
					<img v-if="term.image"
						 v-lazy="listingImageUrl(term.image)"
						 class="no-wp-lazyload"
						 :alt="term.title"/>
				</div>
				<div class="name">
					{{term.title}}
					<span v-if="false" class="count">({{term.count}})</span>
				</div>
			</div>
		</a>
	</div>

</div>


<?php if($asCarousel == 'yes'): ?>
	<script>
        (function($) {
			$(document).ready(function () {
				var owlIcon = $('#<?php echo esc_attr($randId); ?>');
				var owlRtl = false;
				if( $('body').hasClass('rtl') ) {
					owlRtl = true;
				}

				owlIcon.owlCarousel({
					items: 5,
					smartSpeed: 800,
					dots: false,
					margin: 0,
					autoplay: false,
					nav: true,
					loop: false,
					responsiveRefreshRate: 1000,
					responsive:{
						0:{
							items:2
						},
						500:{
							items:2
						},
						768:{
							items:3
						},
						1000:{
							items:5
						}
					}
				})
			});
		})(jQuery);
    </script>
<?php endif; ?>

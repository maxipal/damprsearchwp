<?php
extract( $atts );
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ) );
$title = esc_html__('Displaying Local Car Dealerships', 'motors');
wp_enqueue_script('vue-dealer_list');

$stm_filter_dealers_by = explode(',', $stm_filter_dealers_by);
if(empty($taxonomy)) {
	$taxonomy = '';
}

$filters = array(
	'alphabet' => esc_html__('Alphabet', 'motors'),
	'reviews' => esc_html__('Reviews', 'motors'),
	'date' => esc_html__('Date', 'motors'),
	'cars' => esc_html__('Cars number', 'motors'),
	'watches' => esc_html__('Popularity', 'motors')
);

if ( ! empty( $_GET['stm_sort_by'] ) ) {
	$sort_by = sanitize_title( $_GET['stm_sort_by'] );
} else {
	$sort_by = 'reviews';
}

$GLOBALS['dealers'] = \Stm_Hybrid\Listing::query(
	array(),
	array(
		'uri' => 'users?where[is_dealer]=1&sort[rating.count]=desc&page=1',
	)
);

?>
<div class="dealer-list">
	<div class="stm_dynamic_listing_filter stm_dynamic_listing_dealer_filter animated fadeIn ">

		<div class="tab-content">
			<div role="tabpanel" class="tab-pane fade in active" id="stm_all_listing_tab">
				<form data-trigger="dealers-filter">
					<button class="findDealers" type="submit" class="heading-font"><i class="fa fa-search"></i><?php esc_html_e( 'Find Dealer', 'motors' ); ?></button>
					<div class="stm-filter-tab-selects">
						<div class="row">
							<?php if(count($stm_filter_dealers_by) > 0): ?>
								<?php foreach($stm_filter_dealers_by as $stm_filter_dealers): ?>
									<?php if($stm_filter_dealers != 'location' && $stm_filter_dealers != 'keyword'): ?>
										<div class="col-md-4 col-sm-6 col-xs-12 stm-select-col">
											<div class="stm-ajax-reloadable">
												<select name="<?php echo esc_attr($stm_filter_dealers); ?>" data-class="stm_select_overflowed stm_select_dealer">
													<option value=""><?php esc_html_e('Choose', 'motors'); echo esc_attr(' ' . stm_get_name_by_slug($stm_filter_dealers)); ?></option>
												</select>
											</div>
										</div>
									<?php endif; ?>
								<?php endforeach; ?>
							<?php endif; ?>

							<div class="col-md-4 col-sm-6 col-xs-12 stm-select-col">
								<div class="stm-location-search-unit">
									<?php
									echo stm_locations_dropdown(
										array(
											"searchby" => "latlng",
											"placeholder" => __("Select location","motors"),
										)
									); ?>
								</div>
							</div>

							<?php // endif; ?>
						</div>
					</div>
				</form>
			</div>
		</div>

		<div class="dealer-search-title">
			<div class="stm-car-listing-sort-units stm-car-listing-directory-sort-units clearfix">
				<div class="stm-listing-directory-title">
					<h2 class="title h3"><?php echo wp_kses_post( $title ); ?></h2>
				</div>
				<div class="stm-directory-listing-top__right">
					<div class="clearfix">
						<div class="stm-sort-by-options clearfix">
							<span><?php esc_html_e( 'Sort by:', 'motors' ); ?></span>
							<div class="stm-select-sorting">
								<select class="dealers_sort">
									<?php foreach($filters as $filter_name => $filter): ?>
										<?php
										$selected = '';
										if($sort_by == $filter_name) {
											$selected = 'selected';
										}
										?>
										<option value="<?php echo esc_attr($filter_name) ?>" <?php echo esc_attr($selected); ?>>
											<?php echo esc_attr($filter); ?>
										</option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


		<div class="dealer-search-results dealer_vue">
			<div v-if="busy" class="text-center"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></div>
			<div v-if="firstLoad" class="wrap_first_search">
				<?php get_template_part( 'partials/dealer_list_dip' ) ?>
			</div>
	        <?php get_template_part( 'partials/dealer_list_vue' ) ?>
			<div v-if="!total && !busy">
				<h4><?php esc_html_e( 'No dealers on your search parameters', 'motors' ); ?></h4>
			</div>
			<a v-if="last_page > page" v-on:click.prevent="getJson(true)" id="#more" class="stm-load-more-dealers button" href="#more" data-offset="12">
				<span><?php esc_html_e( 'Show more', 'motors' ) ?></span>
			</a>
		</div>
	</div>

</div>

<script type="text/javascript">
	var dealers_result = <?php echo json_encode( $GLOBALS['dealers'] ) ?>;
</script>

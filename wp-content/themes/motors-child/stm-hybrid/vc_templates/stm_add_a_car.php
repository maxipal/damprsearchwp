<?php
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
$items = vc_param_group_parse_atts($atts['items']);
extract($atts);
$css_class = (!empty($css)) ? apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class($css, ' ')) : '';

wp_enqueue_script('stm-hybrid-form');
wp_enqueue_script('vue-draggable');
wp_enqueue_script('leaflet');
wp_enqueue_script('osm_leaflet');
wp_enqueue_script('ui-droppable');

$auction_page_id = get_theme_mod( 'sell_auction', null );
$single_page_id = get_theme_mod( 'sell_single', null );

add_action('wp_footer', function () {
	stm_hybrid_include('components/form-term-relation-field');
	stm_hybrid_include('components/form-location-field');
	stm_hybrid_include('components/form-features-field');
	stm_hybrid_include('components/form-attachment-field');
	stm_hybrid_include('components/form-gallery-field');
	stm_hybrid_include('components/form-videos-field');
	stm_hybrid_include('components/form-select-field');
}, 100);

$restricted = false;

if (!empty($link)) {
	$link = vc_build_link($link);
} else {
	$link = array();
}

if (is_user_logged_in()) {
	$user = wp_get_current_user();
	$user_id = $user->ID;
	$restrictions = stm_get_post_limits($user_id);
} else {
	$restrictions = stm_get_post_limits('');
}

if ($restrictions['posts_allowed'] - $restrictions['posts'] < 1) {
	$restricted = true;
}

$login_page = get_theme_mod('login_page', 1718);
if (function_exists('icl_object_id')) {
	$page_id = icl_object_id($login_page, 'page', false, ICL_LANGUAGE_CODE);
	if (is_page($page_id)) {
		$login_page = $page_id;
	}
}

$id = stm_listings_input('item_id');

if ($id && $user) {
	$res = \Stm_Hybrid\Api::make()->get("listings/{$id}/edit",
		['context' => 'front',],
		['auth' => $user,]
	);
	$car_edit = true;
} else {
	$res = stm_hybrid_api_transient(180, 'listings/create?context=front');
}

if (!is_wp_error($res)) {
	$fields = $res->get_data();
	echo <<<SCRIPT
<script type="text/javascript">
var listing_form = $fields;
</script>
SCRIPT;

}


$vars = array(
	'id' => $id,
	'taxonomy' => $taxonomy,
	'link' => $link,
	//'data' => $data,
	'items' => $items,
	'stm_title_user' => $stm_title_user,
	'stm_text_user' => $stm_text_user,
	'stm_histories' => $stm_histories,
	'stm_phrases' => $stm_phrases,
	'show_car_title' => $show_car_title,
	'use_inputs' => $use_inputs,
	'show_price_label' => $show_price_label,
	'stm_title_desc' => $stm_title_desc,
	'content' => $content
);

$subscriptions = getSubscriptionsWithoutListing();
if(!empty($subscriptions)) $restricted = false;


if ($restricted and !$car_edit): ?>
	<div class="stm-no-available-adds-overlay"></div>
	<div class="stm-no-available-adds">
		<h3><?php esc_html_e('Posts Available', 'motors'); ?>: <span>0</span></h3>
		<p><?php esc_html_e('You ended the limit of free classified ads. Please select one of the following', 'motors'); ?></p>
		<div class="clearfix">
			<?php if (stm_pricing_enabled()): ?>
				<?php $stm_pricing_link = stm_pricing_link();
				if (!empty($stm_pricing_link)): ?>
					<a href="<?php echo esc_url($stm_pricing_link); ?>" class="button stm-green">
						<?php esc_html_e('Upgrade Plan', 'motors'); ?>
					</a>
				<?php endif; ?>
			<?php else: ?>
				<?php if ($restrictions['role'] == 'user' && false): ?>
					<a href="<?php echo esc_url( stm_account_url( 'become-dealer' ) ); ?>"
					   class="button stm-green"><?php esc_html_e('Become a Dealer', 'motors'); ?></a>
				<?php endif; ?>
			<?php endif; ?>
			<?php if (is_user_logged_in()): ?>
				<a href="<?php echo esc_url( stm_account_url( 'inventory' ) ); ?>"
				   class="button stm-green-dk"><?php esc_html_e('My inventory', 'motors'); ?></a>
				<div> or </div>
				<a href="<?php echo esc_url( get_permalink(get_theme_mod('pricing_link')) ); ?>"
				   class="button stm-green-dk"><?php echo get_the_title(get_theme_mod('pricing_link')); ?></a>

			<?php elseif ($login_page): ?>
				<a href="<?php echo esc_url(get_permalink($login_page)); ?>"
				   class="button stm-green-dk"><?php esc_html_e('Registration', 'motors'); ?></a>
			<?php endif; ?>
		</div>
	</div>
<?php endif; ?>

<?php
if(isset($_REQUEST['edit'])){
	$phases = explode(', ',listing($id)->phases);
}else{
	$phases = array();
}
?>

<div class="stm_add_car_form">
	<div id="listing-form" data-id="<?php echo $id ?>" v-cloak>
		<form method="post" @submit.prevent="submit">
			<?php do_action('stm_vin_auto_complete_require_template'); ?>
			<?php if(!empty($subscriptions)): ?>
				<input type="hidden" name="subscription_id" value="<?php echo $subscriptions[0]->id ?>">
				<input type="hidden" name="subscription_status" value="<?php echo $subscriptions[0]->status ?>">
				<input type="hidden" name="subscription_expires" value="<?php echo $subscriptions[0]->expires ?>">
				<input type="hidden" name="subscription_expires_r" value="<?php echo $subscriptions[0]->expires_readable ?>">
				<input type="hidden" name="subscription_title" value="<?php echo $subscriptions[0]->product_name ?>">
			<?php endif ?>

			<?php
			if(!empty($show_car_title) and $show_car_title == 'yes'):
				$value = '';
				if(!empty($id)) {
					$value = get_the_title($id);
				} ?>
				<div class="stm-car-listing-data-single stm-border-top-unit ">
					<div class="stm_add_car_title_form">
						<div class="title heading-font">
							<?php esc_html_e('Car title', 'motors'); ?>
						</div>
						<input
							type="text"
							name="stm_car_main_title"
							value="<?php echo esc_attr($value); ?>"
							placeholder="<?php esc_attr_e('Title', 'motors'); ?>">
					</div>
				</div>
			<?php endif; ?>


			<div class="stm_add_car_form_1">
				<div class="stm-car-listing-data-single stm-border-top-unit ">
					<div class="title heading-font"><?php esc_html_e('Car Details', 'motors'); ?></div>
					<span class="step_number step_number_1 heading-font"><?php esc_html_e('step', 'motors'); ?> 1</span>
				</div>
				<!--  STEP 1.1 -->
				<div class="stm-form1-intro-unit">
					<div class="row">
						<fields :fields="primary" :errors="errors" layout="step1-primary"></fields>
					</div>
				</div>
				<!-- STEP 1.2 -->
				<div class="stm-form-1-end-unit clearfix">
					<fields :fields="secondary" :errors="errors" layout="step1-secondary"></fields>
					<?php if ( get_theme_mod('show_history', true) ) {
						$data_value = get_post_meta($id, 'history', true);
						$data_value_link = get_post_meta($id, 'history_link', true);
						?>
						<div class="stm-history-popup stm-invisible">
							<div class="inner">
								<i class="fa fa-remove"></i>
								<h5><?php esc_html_e('Vehicle history', 'motors'); ?></h5>
								<?php
								$histories = explode(',', $stm_histories);
								if (!empty($histories)):
									echo '<div class="labels-units">';
									foreach ($histories as $history): ?>
										<stm-radio
											value="<?php echo esc_attr($history); ?>"
											label="<?php echo esc_attr($history); ?>"></stm-radio>
									<?php endforeach;
									echo '</div>';
								endif;
								?>
								<input type="text" name="stm_history_link" placeholder="<?php esc_attr_e('Insert link', 'motors') ?>"
									   value="<?php echo esc_attr($data_value_link); ?>"/>
								<a href="#" class="button"><?php esc_html_e('Apply', 'motors'); ?></a>
							</div>
						</div>
					<?php } ?>

					<!-- STEP 1.2.1 -->
					<div class="stm-car-listing-data-single stm-border-bottom-unit modification-wrap ">
						<div class="title heading-font">
							<?php esc_html_e('Modifications', 'motors-child') ?>
						</div>
					</div>
					<fields :fields="modifications" :errors="errors" layout="step1-secondary"></fields>

					<!-- STEP 1.3 -->
					<field :field="fields.get('location')"></field>
				</div>


				<div id="addcar_location_map" class="addcar_location_map" style="display:none;height:350px;"></div>
				<!-- FEATURES SECTION STEP 2 -->
				<div class="stm-form-2-features clearfix">
					<div class="stm-car-listing-data-single stm-border-top-unit ">
						<div
							class="title heading-font"><?php esc_html_e('Select Your Car Features', 'motors'); ?></div>
						<span class="step_number step_number_2 heading-font"><?php esc_html_e('step', 'motors'); ?>
							2</span>
					</div>
					<field :field="fields.get('features_id')"></field>
				</div>
				<!-- GALLERY SECTION STEP 3 -->
				<div class="stm-form-3-photos clearfix">
					<div class="stm-car-listing-data-single stm-border-top-unit ">
						<div class="title heading-font"><?php esc_html_e('Upload photo', 'motors'); ?></div>
						<span class="step_number step_number_3 heading-font"><?php esc_html_e('step', 'motors'); ?>
							3</span>
					</div>
					<div class="row">
						<div class="col-md-12">
							<!--Check if user not editing existing images-->
							<div class="row">
								<div class="col-md-3 col-sm-12 col-md-push-9">
									<div class="stm-media-car-add-nitofication">
										<?php if (!empty($content)) {
											echo wpb_js_remove_wpautop($content, true);
										} ?>
									</div>
								</div>
								<field :field="fields.get('gallery_id')"></field>
							</div>
						</div>
					</div>
				</div>
				<!-- VIDEOS SECTION STEP 4 -->
				<div class="stm-form-4-videos clearfix">
					<div class="stm-car-listing-data-single stm-border-top-unit ">
						<div class="title heading-font"><?php esc_html_e('Add Videos', 'motors'); ?></div>
						<span class="step_number step_number_4 heading-font"><?php esc_html_e('step', 'motors'); ?>
							4</span>
					</div>
					<div class="stm-add-videos-unit">
						<div class="row">
							<div class="col-md-6 col-sm-12">
								<div class="stm-video-units">
									<div class="stm-video-link-unit-wrap">
										<field :field="fields.get('videos')"></field>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-12">
								<div class="stm-simple-notice">
									<i class="fa fa-info-circle"></i>
									<?php esc_html_e('If you don\'t have the videos handy, don\'t worry. You can add or edit them after you complete your ad using the "Manage Your Ad" page.', 'motors'); ?>

								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- SELLERS NOTE SECTION STEP 5 -->
				<div class="stm-form-5-notes clearfix">
					<div class="stm-car-listing-data-single stm-border-top-unit ">
						<div class="title heading-font"><?php esc_html_e('Enter Seller\'s notes', 'motors'); ?></div>
						<span class="step_number step_number_5 heading-font"><?php esc_html_e('step', 'motors'); ?>
							5</span>
					</div>
					<div class="row stm-relative">
						<div class="col-md-9 col-sm-9 stm-non-relative">
							<div class="stm-phrases-unit">
								<?php if (!empty($stm_phrases)): $stm_phrases = explode(',', $stm_phrases); ?>
									<div class="stm_phrases">
										<div class="inner">
											<i class="fa fa-close"></i>
											<h5><?php esc_html_e('Select all the phrases that apply to your vehicle.', 'motors'); ?></h5>
											<?php if (!empty($stm_phrases)): ?>
												<div class="clearfix">
													<?php foreach ($stm_phrases as $phrase): ?>
														<stm-checkbox
															<?php if(in_array(trim($phrase), $phases) !== false) echo 'checked="true"' ?>
															value="<?php echo esc_attr($phrase); ?>"
															label="<?php echo esc_attr($phrase); ?>"></stm-checkbox>
													<?php endforeach; ?>
												</div>
												<a href="#" class="button"><?php esc_html_e('Apply', 'motors'); ?></a>
											<?php endif; ?>
										</div>
									</div>
								<?php endif; ?>
								<field :field="fields.get('content')"></field>
							</div>
						</div>
						<?php if (!empty($stm_phrases)): ?>
							<div class="col-md-3 col-sm-3 hidden-xs">
								<div class="stm-seller-notes-phrases heading-font">
									<span><?php esc_html_e('Add the Template Phrases', 'motors'); ?></span>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>

				<?php if(!isset($_REQUEST['item_id']) || ( isset($_REQUEST['item_id']) && !listing($_REQUEST['item_id'])->auction) ): ?>
					<?php if(get_the_id() !== (int)$auction_page_id): ?>
						<!-- PRICE SECTION STEP 6 -->
						<div class="stm-form-price-edit">
							<div class="stm-car-listing-data-single stm-border-top-unit ">
								<div class="title heading-font"><?php esc_html_e('Set Your Asking Price', 'motors'); ?></div>
								<span class="step_number step_number_5 heading-font"><?php esc_html_e('step', 'motors'); ?>
									6</span>
							</div>
							<?php if (!empty($show_price_label) and $show_price_label == 'yes'): ?>
								<div class="row stm-relative">
									<div class="col-md-12 col-sm-12 stm-prices-add">
										<div style="display:none">
											<stm-checkbox value="auction" label="Auction"></stm-checkbox>
										</div>

										<?php if (!empty($stm_title_price)): ?>
											<h4><?php echo esc_attr($stm_title_price); ?></h4>
										<?php endif; ?>
										<?php if (!empty($stm_title_desc)): ?>
											<p><?php echo esc_attr($stm_title_desc); ?></p>
										<?php endif; ?>
									</div>
									<div class="col-md-12 col-sm-12">
										<div class="row">

											<div class="col-md-4 col-sm-12">
												<div class="stm_price_input">


													<div class="stm_label heading-font"><?php esc_html_e('Price', 'motors'); ?>
														*(<?php echo stm_get_price_currency(); ?>)
													</div>
													<field :field="fields.get('price')"
														   class="heading-font stm_price_input_wrapper"
														   name="stm_car_price"></field>
												</div>
											</div>
											<div class="col-md-4 col-sm-12">
												<div class="stm_price_input custom-sale-price">
													<div class="stm_label heading-font">
														<?php esc_html_e('Sale Price', 'motors'); ?>(<?php echo stm_get_price_currency(); ?>)
													</div>
													<field :field="fields.get('price_sale')" class="heading-font"
														   name="stm_car_price"></field>
												</div>
											</div>
											<div class="col-md-4 col-sm-12">
												<div class="stm_price_input custom-sale-price">
													<div
														class="stm_label heading-font"><?php esc_html_e('Custom label instead of price', 'motors'); ?></div>
													<field :field="fields.get('price_label')" class="heading-font"
														   name="stm_car_price"></field>
												</div>
											</div>
										</div>
									</div>
								</div>
							<?php else: ?>
								<div class="row stm-relative">
									<div class="col-md-4 col-sm-6">
										<div class="stm_price_input">
											<div class="stm_label heading-font"><?php esc_html_e('Price', 'motors'); ?>*

											</div>
											<field :field="fields.get('price')" class="heading-font stm_price_input_wrapper"
												   name="stm_car_price"></field>
										</div>
									</div>
									<div class="col-md-8 col-sm-6">
										<?php if (!empty($stm_title_price)): ?>
											<h4><?php echo esc_attr($stm_title_price); ?></h4>
										<?php endif; ?>
										<?php if (!empty($stm_title_desc)): ?>
											<p><?php echo esc_attr($stm_title_desc); ?></p>
										<?php endif; ?>
									</div>
								</div>
							<?php endif; ?>
						</div>
						<!-- END OF PRICE SECTION -->
					<?php else: ?>
						<div style="display:none">
							<stm-checkbox value="auction" checked="true" label="Auction"></stm-checkbox>
						</div>
					<?php endif; ?>
				<?php else: ?>
					<div style="display:none">
						<stm-checkbox value="auction" <?php if(isset($_REQUEST['item_id']) && listing($_REQUEST['item_id'])->auction) echo 'checked="true"' ?> label="Auction"></stm-checkbox>
					</div>
				<?php endif ?>

			</div>
		</form>
	</div>
	<?php stm_listings_load_template('add_car/check_user', $vars); ?>
</div>

<script type="x-template" id="step1-primary-layout">
	<div class="col-md-3 col-sm-3 stm-form-1-selects">
		<div class="stm-label heading-font">{{ field.label || fieldLabel }}*</div>
		<slot name="field"/>
	</div>
</script>

<script type="x-template" id="step1-secondary-layout" class="base">
	<div class="stm-form-1-quarter" :class="field.required ? 'required' : ''">
		<slot name="field"/>
		<div class="stm-label">
			<i v-if="field.icon" :class="field.icon"></i>
			<span>{{ field.label || fieldLabel }}</span>
		</div>
	</div>
</script>

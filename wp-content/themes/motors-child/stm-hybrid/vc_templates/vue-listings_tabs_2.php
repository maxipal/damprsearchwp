<div class="stm_listing_tabs_style_2" data-per_page="<?php echo( $per_page ) ?>" v-cloak>
	<div class="clearfix">
		<h2 class="hidden-md hidden-lg hidden-sm h3"><?php echo esc_attr( $title ); ?></h2>
		<ul class="stm_listing_nav_list heading-font" role="tablist">
			<?php if ( ! empty( $popular ) and $popular == 'yes' ): ?>
				<li id="popularLink"><a data-toggle="tab" href="#popular"><span><?php echo esc_attr( $popular_label ); ?></span></a></li>
			<?php endif; ?>
			<?php if ( ! empty( $recent ) and $recent == 'yes' ): ?>
				<li class="active" id="recentLink"><a data-toggle="tab" href="#recent" aria-expanded="true"><span><?php echo esc_attr( $recent_label ); ?></span></a></li>
			<?php endif ?>
			<?php if ( ! empty( $featured ) and $featured == 'yes' ): ?>
				<li id="featuredLink" class="active"><a data-toggle="tab" href="#featured"><span><?php echo esc_attr( $featured_label ); ?></span></a></li>
			<?php endif; ?>
		</ul>
		<h3 class="hidden-xs"><?php echo esc_attr( $title ); ?></h3>
	</div>

	<div class="tab-content">
		<?php if ( ! empty( $popular ) and $popular == 'yes' ): ?>
			<div id="popular" class="vue-tabs tab-pane fade ">
				<div class="row row-4 car-listing-row">
					<?php stm_hybrid_include('loop/vue-grid', ['dataSource' => 'popular', 'colClass' => 'col-md-3  col-sm-4']); ?>
				</div>
				<?php if ( ! empty( $show_more ) and $show_more == 'yes' ): ?>
					<div class="row">
						<div class="col-xs-12 text-center">
							<div class="dp-in">
								<a class="load-more-btn" href="<?php echo esc_url( stm_get_listing_archive_link( [ 'sort_order' => 'popular' ] ) ); ?>">
									<?php esc_html_e( 'Show all', 'motors' ); ?>
								</a>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		<?php endif; ?>
		<?php if ( ! empty( $recent ) and $recent == 'yes' ): ?>
			<div id="recent" class="vue-tabs tab-pane fade active in">
				<div class="row row-4 car-listing-row">
					<?php stm_hybrid_include('loop/vue-grid', ['dataSource' => 'recent', 'colClass' => 'col-md-3  col-sm-4']); ?>
				</div>
				<?php if ( ! empty( $show_more ) and $show_more == 'yes' ): ?>
					<div class="row">
						<div class="col-xs-12 text-center">
							<div class="dp-in">
								<a class="load-more-btn" href="<?php echo esc_url( stm_get_listing_archive_link() ); ?>">
									<?php esc_html_e( 'Show all', 'motors' ); ?>
								</a>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		<?php endif; ?>
		<?php if ( ! empty( $featured ) and $featured == 'yes' ): ?>
			<div id="featured" class="vue-tabs tab-pane fade in active">
				<div class="row row-4 car-listing-row">
					<?php stm_hybrid_include('loop/vue-grid', ['dataSource' => 'featured', 'colClass' => 'col-md-3  col-sm-4']); ?>
				</div>
				<?php if ( ! empty( $show_more ) and $show_more == 'yes' ): ?>
					<div class="row">
						<div class="col-xs-12 text-center">
							<div class="dp-in">
								<a class="load-more-btn" href="<?php echo esc_url( stm_get_listing_archive_link( [ 'featured' => 'only' ] ) ); ?>">
									<?php esc_html_e( 'Show all', 'motors' ); ?>
								</a>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		<?php endif; ?>
	</div>
</div>

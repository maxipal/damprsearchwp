<div>
    <?php stm_hybrid_include('actions'); ?>
    <?php stm_hybrid_include('badges'); ?>
    <?php stm_hybrid_include('featured'); ?>

    <div v-cloak v-if="itemsLoaded" class="stm-isotope-sorting" :class="'stm-isotope-sorting-' + viewType">
        <div v-if="viewType === 'grid'" class="row row-3 car-listing-row car-listing-modern-grid">
            <?php stm_hybrid_include('loop/vue-grid', ['dataSource' => 'listings']); ?>
        </div>
        <div v-else>
            <?php stm_hybrid_include('loop/vue-list', ['dataSource' => 'listings']); ?>
        </div>
    </div>

</div>

<?php stm_hybrid_include( 'vue-pagination' ); ?>

<div v-cloak v-if="itemsLoaded && itemsTotal === 0">
    <h3><?php esc_html_e('Sorry, No results', 'motors') ?></h3>
    <p><?php esc_html_e('You might be interested in the following cars:', 'motors') ?></p>
    <?php stm_hybrid_include('badges'); ?>
    <div class="stm-isotope-sorting" :class="'stm-isotope-sorting-' + viewType">
        <div v-if="viewType === 'grid'" class="row row-3 car-listing-row car-listing-modern-grid">
			<?php stm_hybrid_include('loop/vue-grid', ['dataSource' => 'alternate']); ?>
        </div>
        <div v-else>
			<?php stm_hybrid_include('loop/vue-list', ['dataSource' => 'alternate']); ?>
        </div>
    </div>
</div>

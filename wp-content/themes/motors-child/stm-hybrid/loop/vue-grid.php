<div v-for="(listing, index) in <?php echo $dataSource; ?>" :key="index"
	class="<?php echo isset( $colClass ) ? $colClass : 'col-md-4' ?> stm-directory-grid-loop stm-isotope-listing-item"
	:class="{ 'car-as-sold': listing.sold }">
	<a :href="'<?php echo site_url(stm_get_listings_base_url() . '/'); ?>' + listing.slug + '/'" class="rmv_txt_drctn">
		<?php stm_hybrid_include( 'loop/vue-grid/image' ); ?>
		<div class="listing-car-item-meta">
			<div class="car-meta-top heading-font clearfix">
				<?php stm_hybrid_include( 'loop/vue-price' ); ?>
				<div class="car-title">{{ listing.title }}</div>
			</div>
			<div class="car-meta-bottom">
				<ul>
					<li v-for="(attribute, key) in listing.attributes" v-if="attribute.useInGrid && attribute.value !== null">
						<i v-if="attribute.icon" :class="attribute.icon"></i>
						<span>{{ attribute.value }}</span>
					</li>
				</ul>
			</div>
		</div>
	</a>
</div>


<?php if ( listing()->price_regular_view && listing()->price_regular_view != '0' ) { ?>
	<div class="price discounted-price">
		<div class="regular-price"><?php echo listing()->price_regular_view; ?></div>
		<div class="sale-price"><?php echo listing()->price_view; ?></div>
	</div>
<?php } else { ?>
	<div class="price">
		<?php if ( ! listing()->auction && ! listing()->price ) { ?>
			<div class="normal-price with-zero">
				<?php _e("With agreement", "motors") ?>
			</div>
		<?php } elseif ( ! listing()->auction && listing()->price ) { ?>
			<div class="normal-price heading-font"><?php echo listing()->price_view; ?></div>
		<?php } else { ?>
			<div class="normal-price"><?php _e( "Auction", "motors" ); ?></div>
		<?php } ?>
	</div>
<?php } ?>

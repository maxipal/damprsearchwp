<div v-if="listing.price_regular_view && listing.price_regular_view !== 0" class="price discounted-price">
	<div class="regular-price">{{ listing.price_regular_view }}</div>
	<div class="sale-price heading-font">{{ listing.price_view }}</span></div>
</div>
<div v-else class="price">
	<div class="normal-price with-zero" v-if="!listing.auction && !listing.price"><?php _e("With agreement", "motors") ?></div>
	<div class="normal-price heading-font" v-else-if="!listing.auction && listing.price">
		{{ listing.price_view }}
	</div>
	<div class="normal-price" v-else><?php _e("Auction","motors") ?></div>
</div>

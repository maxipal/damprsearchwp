<div v-for="(listing, index) in <?php echo $dataSource; ?>"
	 :key="listing.id"
	 class="listing-list-loop stm-listing-directory-list-loop stm-isotope-listing-item">
	<?php stm_hybrid_include( 'loop/vue-list/image' ); ?>
	<div class="content">
		<div class="meta-top">
			<?php stm_hybrid_include( 'loop/vue-price' ); ?>
			<div class="title heading-font">
				<a :href="'<?php echo site_url(stm_get_listings_base_url() . '/') ?>' + listing.slug + '/'" class="rmv_txt_drctn">
					<div
						v-if="stm_hybrid.title_as_label"
						v-html="stm_generate_title(listing, stm_hybrid.title_as_label)"></div>
					<div v-else>{{ listing.title }}</div>
				</a>
			</div>
		</div>
		<!--Item parameters-->
		<div class="meta-middle">
			<?php stm_hybrid_include('loop/vue-list/middle-options') ?>
		</div>
		<!--Item options-->
		<div class="meta-bottom">
			<?php stm_hybrid_include( 'loop/vue-list/bottom-actions' ); ?>
		</div>
		<a :href="'<?php echo site_url(stm_get_listings_base_url() . '/') ?>' + listing.slug" class="stm-car-view-more button visible-xs"><?php _e('View more', 'motors') ?></a>
	</div>
</div>


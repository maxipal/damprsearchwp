<div class="listing-list-loop stm-listing-directory-list-loop stm-isotope-listing-item">

	<?php stm_hybrid_include( 'loop/list/image' ); ?>

	<div class="content">
		<div class="meta-top">
			<?php stm_hybrid_include( 'loop/price' ); ?>
			<div class="title heading-font">
				<a href="<?php echo listing()->link() ?>" class="rmv_txt_drctn">
					<div>
						<?php
						$title = stm_generate_title_from_slugs_hybrid(listing());
						echo $title;
//						//echo listing()->title;
						?>
					</div>
				</a>
			</div>
		</div>

		<!--Item parameters-->
		<div class="meta-middle">
			<?php stm_hybrid_include( 'loop/list/middle-options' ) ?>
		</div>

		<!--Item options-->
		<div class="meta-bottom">
			<?php stm_hybrid_include( 'loop/list/bottom-actions' ); ?>
		</div>
		<a href="<?php echo listing()->link() ?>" class="stm-car-view-more button visible-xs">View more</a>
	</div>
</div>

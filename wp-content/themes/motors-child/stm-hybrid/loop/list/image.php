<?php
$show_compare = get_theme_mod('show_listing_compare', true);

$show_favorite = get_theme_mod('enable_favorite_items', true);

$carPhotos = listing()->gallery;

$carVideos = listing()->videos;

$carSold = listing()->sold;
?>

<div class="image">
    <!--Hover blocks-->
    <!---Media-->
    <div class="stm-car-medias">
        <?php if(count($carPhotos)): ?>
            <div class="stm-listing-photos-unit stm-car-photos-<?php echo listing()->id; ?>">
                <i class="stm-service-icon-photo"></i>
                <span><?php echo count($carPhotos); ?></span>
            </div>

            <script type="text/javascript">
                jQuery(document).ready(function(){

                    jQuery(".stm-car-photos-<?php echo listing()->id; ?>").click(function() {
                        jQuery.fancybox.open([
                            <?php foreach($carPhotos as $carPhoto): ?>
                            {
                                href  : "<?php echo $carPhoto['url']; ?>"
                            },
                            <?php endforeach; ?>
                        ], {
                            padding: 0
                        });
                    });
                });

            </script>
        <?php endif; ?>
        <?php if($carVideos): ?>
            <div class="stm-listing-videos-unit stm-car-videos-<?php echo listing()->id; ?>">
                <i class="fa fa-film"></i>
                <span><?php echo count($carVideos); ?></span>
            </div>

            <script type="text/javascript">
                jQuery(document).ready(function(){
                    jQuery(".stm-car-videos-<?php echo listing()->id; ?>").click(function() {
                        jQuery.fancybox.open([
                            <?php foreach($carVideos as $carVideo): ?>
                            {
                                href  : "<?php echo esc_url($carVideo); ?>"
                            },
                            <?php endforeach; ?>
                        ], {
                            type: 'iframe',
                            padding: 0
                        });
                    });
                });

            </script>
        <?php endif; ?>
    </div>
    <!--Compare-->
    <?php if(!empty($show_compare) and $show_compare): ?>
        <div
                class="stm-listing-compare"
                data-id="<?php echo listing()->id; ?>"
                data-title="<?php echo listing()->title; ?>"
                data-toggle="tooltip" data-placement="left" title="<?php esc_attr_e('Add to compare', 'motors') ?>">
            <i class="stm-service-icon-compare-new"></i>
        </div>
    <?php endif; ?>

    <!--Favorite-->
    <?php if(!empty($show_favorite) and $show_favorite): ?>
        <div
                class="stm-listing-favorite"
                data-id="<?php echo listing()->id; ?>"
                data-toggle="tooltip" data-placement="right" title="<?php esc_attr_e('Add to favorites', 'motors') ?>">
            <i class="stm-service-icon-staricon"></i>
        </div>
    <?php endif; ?>

	<a href="<?php echo listing()->link() ?>" class="rmv_txt_drctn">
		<div class="image-inner">
			<?php
            $special_car = listing()->is_special;
			$badge_text = listing()->badge_text;
			$badge_bg_color = listing()->badge_bg_color;
			if (!empty($badge_bg_color)) {
				$badge_bg_color = 'style="background-color:' . $badge_bg_color . '";';
			} else {
				$badge_bg_color = '';
			}

			if($special_car && !empty($badge_text)): ?>
                <div class="stm-badge-directory heading-font <?php if(stm_is_car_dealer()) echo "stm-badge-dealer"?>" <?php echo sanitize_text_field($badge_bg_color); ?>>
					<?php echo esc_attr($badge_text); ?>
                </div>
			<?php endif; ?>

			<?php
			if ($image = listing()->image()) :
				$plchldr = (stm_is_dealer_two()) ? "plchldr-275.jpg" : 'plchldr350.png';
				stm_lazy_image(
					stm_hybrid_image_size($image, 796, 466, true),
					[
						'placeholder' => get_stylesheet_directory_uri().'/assets/images/' . $plchldr,
						'alt' => listing()->title,
						'class' => 'no-wp-lazyload img-responsive',
					]
				);
			else: $plchldr = (stm_is_dealer_two()) ? "plchldr-275.jpg" : 'plchldr350.png'; ?>
                <img
                    src="<?php echo esc_url(get_stylesheet_directory_uri().'/assets/images/' . $plchldr); ?>"
                    class="img-responsive"
                    alt="<?php esc_html_e('Placeholder', 'motors'); ?>"
                />
			<?php endif; ?>

            <?php if ($carSold): ?>
                <div class="stm-badge-directory heading-font" <?php echo sanitize_text_field($badge_bg_color); ?>>
                    <?php echo esc_html__('Sold', 'motors'); ?>
                </div>
            <?php endif; ?>
		</div>
	</a>
</div>

<?php
if ( ! isset( listing()->attributes ) ) {
	return;
}

$attributes = array_filter( listing()->attributes, function ( $attribute ) {
	return $attribute->useInList && $attribute->value !== null;
} );

if ( empty( $attributes ) ) {
	return;
}
?>
<div class="meta-middle-row clearfix">
   <?php foreach ( $attributes as $key => $attribute ) {if($key == 'price') continue; ?>
		<div class="meta-middle-unit <?php if(!empty($attribute->icon)) echo 'font-exists' ?>">
            <div class="meta-middle-unit-top">
                <?php if (!empty($attribute->icon)): ?>
                    <div class="icon">
                        <i class="<?php echo $attribute->icon; ?>"></i>
                    </div>
                <?php endif; ?>
                <div class="name"><?php echo $attribute->label; ?></div>
            </div>
            <div class="value">
                <?php if ($key === 'location'): ?>
                    <div class="stm-tooltip-link"
                         data-toggle="tooltip"
                         data-placement="bottom"
                         data-original-title="<?php echo $attribute->value; ?>">
                        <?php echo $attribute->value; ?>
                    </div>
                <?php else: ?>
	                <?php echo $attribute->value; ?>
                <?php endif; ?>
            </div>
        </div>
		<div class="meta-middle-unit meta-middle-divider"></div>
    <?php } ?>
</div>

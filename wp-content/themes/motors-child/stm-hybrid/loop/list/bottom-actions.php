<?php

$show_compare = false;

$stock_number = listing()->stock_number;
$car_brochure = listing()->car_brochure;

$history_link_1 = listing()->history_link;
$certified_logo_1 = listing()->certified_logo_1;

$certified_logo_2 = listing()->certified_logo_2;
$certified_logo_2_link = listing()->certified_logo_2_link;

//Show car actions
$show_pdf = get_theme_mod('show_listing_pdf', false);
$show_stock = get_theme_mod('show_listing_stock', false);
$show_share = get_theme_mod('show_listing_share', false);
$show_test_drive = get_theme_mod('show_listing_test_drive', false);
$show_certified_logo_1 = get_theme_mod('show_listing_certified_logo_1', true);
$show_certified_logo_2 = get_theme_mod('show_listing_certified_logo_2', false);
$listing_directory_enable_dealer_info = get_theme_mod('listing_directory_enable_dealer_info', true);
?>

<div class="single-car-actions">
    <ul class="list-unstyled clearfix">

        <?php if(!empty($listing_directory_enable_dealer_info) and $listing_directory_enable_dealer_info): ?>
	        <?php stm_hybrid_include( 'loop/list/bottom-user-info' ); ?>
        <?php endif; ?>

        <!--Stock num-->
        <?php if(!empty($stock_number) and !empty($show_stock) and $show_stock): ?>
            <li>
                <div class="stock-num heading-font">
                    <span><?php esc_html_e('stock', 'motors'); ?># </span>
                    <?php echo $stock_number; ?>
                </div>
            </li>
        <?php endif; ?>

        <!--Schedule-->
        <?php if(!empty($show_test_drive) and $show_test_drive): ?>
            <li>
                <a href="#" class="car-action-unit stm-schedule" data-toggle="modal" data-target="#test-drive" onclick="stm_test_drive_car_title(<?php echo esc_js(listing()->id); ?>, '<?php echo esc_js(get_the_title(listing()->id)) ?>')">
                    <i class="stm-icon-steering_wheel"></i>
                    <?php esc_html_e('Schedule Test Drive', 'motors'); ?>
                </a>
            </li>
        <?php endif; ?>

        <!--PDF-->
        <?php if(!empty($show_pdf) and $show_pdf): ?>
            <?php if(!empty($car_brochure)): ?>
                <li>
                    <a
                        href="<?php echo esc_url(wp_get_attachment_url($car_brochure)); ?>"
                        class="car-action-unit stm-brochure"
                        title="<?php esc_html_e('Download brochure', 'motors'); ?>"
                        target="_blank"
                        download>
                        <i class="stm-icon-brochure"></i>
                        <?php esc_html_e('Car brochure', 'motors'); ?>
                    </a>
                </li>
            <?php endif; ?>
        <?php endif; ?>

        <!--Share-->
        <?php if(!empty($show_share) and $show_share): ?>
            <li class="stm-shareble">
                <a href="#" class="car-action-unit stm-share" >
                    <i class="stm-icon-share"></i>
                    <?php esc_html_e('Share this', 'motors'); ?>
                </a>
                <?php if( function_exists( 'ADDTOANY_SHARE_SAVE_KIT' ) ): ?>
                    <div class="stm-a2a-popup">
                        <?php echo do_shortcode('[addtoany url="'.get_the_permalink(listing()->id).'" title="'.get_the_title(listing()->id).'"]'); ?>
                    </div>
                <?php endif; ?>
            </li>
        <?php endif; ?>





        <!--Certified Logo 1-->
        <?php if(!empty($certified_logo_1) and !empty($show_certified_logo_1) and $show_certified_logo_1): ?>
            <?php
            $certified_logo_1 = wp_get_attachment_image_src($certified_logo_1, 'stm-img-796-466');
            if(!empty($certified_logo_1[0])){
                $certified_logo_1 = $certified_logo_1[0]; ?>

                <li class="certified-logo-1">
                    <?php if(!empty($history_link_1)): ?>
                    <a href="<?php echo esc_url($history_link_1); ?>" target="_blank">
                        <?php endif; ?>
                        <img src="<?php echo esc_url($certified_logo_1); ?>" alt="<?php esc_html_e('Logo 1', 'motors'); ?>"/>
                        <?php if(!empty($history_link_1)): ?>
                    </a>
                <?php endif; ?>
                </li>

            <?php } ?>
        <?php endif; ?>

        <!--Certified Logo 2-->
        <?php if(!empty($certified_logo_2) and !empty($show_certified_logo_2) and $show_certified_logo_2): ?>
            <?php
            $certified_logo_2 = wp_get_attachment_image_src($certified_logo_2, 'stm-img-796-466');
            if(!empty($certified_logo_2[0])){
                $certified_logo_2 = $certified_logo_2[0]; ?>

                <li class="certified-logo-2">
                    <?php if(!empty($certified_logo_2_link)): ?>
                    <a href="<?php echo esc_url($certified_logo_2_link); ?>" target="_blank">
                        <?php endif; ?>
                        <img src="<?php echo esc_url($certified_logo_2); ?>"  alt="<?php esc_html_e('Logo 2', 'motors'); ?>"/>
                        <?php if(!empty($certified_logo_2_link)): ?>
                    </a>
                <?php endif; ?>
                </li>

            <?php } ?>
        <?php endif; ?>

    </ul>
</div>

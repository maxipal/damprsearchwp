<?php
$show_compare = get_theme_mod('show_listing_compare', true);
$show_favorite = get_theme_mod('enable_favorite_items', true);
?>

<div class="image">
    <!--Hover blocks-->
    <!---Media-->
    <div class="stm-car-medias">
        <div
			v-if="listing.gallery && listing.gallery.length"
			@click="openGallery(listing.gallery, 'image')"
			class="stm-listing-photos-unit">
            <i class="stm-service-icon-photo"></i>
            <span>{{listing.gallery.length}}</span>
        </div>
        <div
			v-if="listing.videos && listing.videos.length > 0"
			@click="openGallery(listing.videos, 'iframe')"
			class="stm-listing-videos-unit">
            <i class="fa fa-film"></i>
            <span>{{listing.videos.length}}</span>
        </div>
    </div>
    <!--Compare-->
    <?php if(!empty($show_compare) and $show_compare): ?>
        <div
                class="stm-listing-compare"
                :data-id="listing.id"
                :data-title="listing.title"
                data-toggle="tooltip"
				data-placement="left"
				title="<?php esc_attr_e('Add to compare', 'motors') ?>">
            <i class="stm-service-icon-compare-new"></i>
        </div>
    <?php endif; ?>

    <!--Favorite-->
    <?php if(!empty($show_favorite) and $show_favorite): ?>
        <div
                class="stm-listing-favorite"
                :data-id="listing.id"
                data-toggle="tooltip"
				data-placement="right"
				title="<?php esc_attr_e('Add to favorites', 'motors') ?>">
            <i class="stm-service-icon-staricon"></i>
        </div>
    <?php endif; ?>

	<a :href="'<?php echo site_url(stm_get_listings_base_url() . '/') ?>' + listing.slug"
	   class="rmv_txt_drctn">
		<div class="image-inner">
            <div v-if="listing.is_special && listing.badge_text"
                 class="stm-badge-directory heading-font"
                 :style="'background-color: ' + listing.badge_bg_color"
            >
                {{listing.badge_text}}
            </div>

			<?php $plchldr = 'plchldr350.png'; ?>

            <img v-lazy="getImageSize(getFirstImage(listing.gallery), 796, 466, true)"
                 :key="getImageSize(getFirstImage(listing.gallery), 796, 466, true)"
                v-if="getFirstImage(listing.gallery)"
                class="no-wp-lazyload img-responsive"
                :alt="listing.title"
            />

            <img
                src="<?php echo esc_url(get_stylesheet_directory_uri().'/assets/images/' . $plchldr); ?>"
                class="no-wp-lazyload img-responsive placeholder"
                alt="<?php esc_html_e('Placeholder', 'motors'); ?>"
            />

            <div
				v-if="listing.sold" class="stm-badge-directory heading-font"
				:style="'background-color: ' + listing.badge_bg_color">
                <?php echo esc_html__('Sold', 'motors'); ?>
            </div>
		</div>
	</a>
</div>

<?php

$show_compare = false;
//Show car actions
$show_pdf = get_theme_mod('show_listing_pdf', false);
$show_stock = get_theme_mod('show_listing_stock', false);
$show_share = get_theme_mod('show_listing_share', false);
$show_test_drive = get_theme_mod('show_listing_test_drive', false);
$show_certified_logo_1 = get_theme_mod('show_listing_certified_logo_1', true);
$show_certified_logo_2 = get_theme_mod('show_listing_certified_logo_2', false);
$listing_directory_enable_dealer_info = get_theme_mod('listing_directory_enable_dealer_info', true);
?>

<div class="single-car-actions">
    <ul class="list-unstyled clearfix">

        <?php if(!empty($listing_directory_enable_dealer_info) and !empty($listing_directory_enable_dealer_info) and $listing_directory_enable_dealer_info): ?>
	        <?php stm_hybrid_include( 'loop/vue-list/bottom-user-info' ); ?>
        <?php endif; ?>

        <!--Stock num-->
        <?php if(!empty($show_stock) and $show_stock): ?>
            <li>
                <div class="stock-num heading-font">
                    <span><?php esc_html_e('stock', 'motors'); ?># </span>
                    {{ listing.stock_number }}
                </div>
            </li>
        <?php endif; ?>

        <!--Schedule-->
        <?php if(!empty($show_test_drive) and $show_test_drive): ?>
            <li>
                <a href="#" class="car-action-unit stm-schedule" data-toggle="modal" data-target="#test-drive"
                   @click="stmTestDriveCarTitle(listing.id, listing.title)">
                    <i class="stm-icon-steering_wheel"></i>
                    <?php esc_html_e('Schedule Test Drive', 'motors'); ?>
                </a>
            </li>
        <?php endif; ?>
    </ul>
    <div class="single-car-actions-footer">
        <!--PDF-->
        <?php if(!empty($show_pdf) and $show_pdf): ?>
            <div v-if="listing.brochure" class="stm-brochure-item">
                <a :href="listing.brochure.url" class="car-action-unit stm-brochure"
                   title="<?php esc_html_e('Download brochure', 'motors'); ?>"
                   target="_blank" download>
                    <i class="stm-icon-brochure"></i>
                    <?php esc_html_e('Car brochure', 'motors'); ?>
                </a>
            </div>
        <?php endif; ?>
    </div>
</div>

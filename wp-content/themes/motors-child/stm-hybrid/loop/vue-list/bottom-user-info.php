<li class="car-action-dealer-info">
    <div v-if="listing.author" class="listing-archive-dealer-info clearfix">
        <div v-if="listing.author.is_dealer" class="dealer-img">
            <div class="stm-dealer-image-custom-view">
                <a :href="listing.author.link">
                    <img v-if="listing.author.logo" class="no-wp-lazyload img-responsive" v-lazy="listing.author.logo">
                    <img v-else-if="listing.author.avatar" class="no-wp-lazyload img-responsive" v-lazy="listing.author.avatar">
                    <img v-else class="no-wp-lazyload img-responsive" v-lazy="'<?php stm_get_dealer_logo_placeholder(); ?>'">
                </a>
            </div>
        </div>
        <div v-else class="dealer-image">
            <a :href="listing.author.link">
                <img v-if="listing.author.logo" class="no-wp-lazyload stm-user-image img-responsive" v-lazy="listing.author.logo">
                <img v-else-if="listing.author.avatar" class="no-wp-lazyload stm-user-image img-responsive" v-lazy="listing.author.avatar">
                <div v-else class="stm-user-image-empty">
                    <i class="stm-service-icon-user"></i>
                </div>
            </a>
        </div>
        <div class="dealer-info-block" :class="{'stm_phone_disabled': !listing.author.phone}">
                <a v-if="listing.author.is_dealer" :href="listing.author.link" class="title">{{ listing.author.name }}</a>
                <div v-else class="title">
                    <span><?php esc_html_e('Private Seller', 'motors'); ?>: </span>
                    <a :href="listing.author.link">{{listing.author.name}}</a>
                </div>
            <div v-if="listing.author.phone"  class="dealer-information">
                    <div class="phone">
                        <i class="stm-service-icon-phone"></i>
                       {{listing.author.phone.substring(3, 0) + '*******'}}
                    </div>
                <span class="stm-show-number_custom" :data-id="listing.author.phone"><?php echo esc_html__("Show number", "motors"); ?></span>
            </div>
        </div>
    </div>
</li>

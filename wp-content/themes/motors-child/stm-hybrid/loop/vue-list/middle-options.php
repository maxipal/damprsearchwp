<div class="meta-middle-row clearfix">
	<template v-for="(attribute, key) in listing.attributes" v-if="attribute.useInList">
		<div v-if="attribute.value !== null && key !== 'price'" class="meta-middle-unit" :class="{key: true, 'font-exists': attribute.icon}">
			<div class="meta-middle-unit-top">
				<div v-if="attribute.icon" class="icon"><i :class="attribute.icon"></i></div>
				<div class="name">{{ attribute.label }}</div>
			</div>
			<div class="value">
				<div v-if="key === 'location'"
					 class="stm-tooltip-link"
					 data-toggle="tooltip"
					 data-placement="bottom"
					 :title="attribute.value">
					{{ attribute.value }}
				</div>
				<div v-else>{{ attribute.value }}</div>
			</div>
		</div>
		<div class="meta-middle-unit meta-middle-divider"></div>
	</template>
</div>

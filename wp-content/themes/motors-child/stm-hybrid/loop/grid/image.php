<?php
$show_favorite = get_theme_mod('enable_favorite_items', true);

//Compare
$show_compare = get_theme_mod('show_listing_compare', true);

/*Media*/
$carPhotos = listing()->gallery;
$carVideos = listing()->videos;

$asSold = listing()->sold;
$imgSize = 'stm-img-255-160';

?>

<div class="image">
	<?php
	$plchldr = 'plchldr255.png';
	if ($image = listing()->image()):
		stm_lazy_image(
			stm_hybrid_image_size($image, 255, 135, true),
			[
				'placeholder' => get_stylesheet_directory_uri().'/assets/images/' . $plchldr,
				'alt' => listing()->title,
				'class' => 'img-responsive',
			]
		);
	else: ?>
		<img
			src="<?php echo esc_url(get_stylesheet_directory_uri().'/assets/images/' . $plchldr); ?>"
			class="img-responsive"
			alt="<?php esc_html_e('Placeholder', 'motors'); ?>"
		/>
	<?php endif; ?>

	<?php if ($asSold): ?>
		<div class="stm-badge-directory heading-font" <?php echo sanitize_text_field(listing()->badge_bg_color); ?>>
			<?php echo esc_html__('Sold', 'motors'); ?>
		</div>
	<?php endif; ?>
	<!--Hover blocks-->
	<?php
        $special_car = listing()->is_special;
        $badge_text = listing()->badge_text;
        $badge_bg_color = listing()->badge_bg_color;

        if (!empty($badge_bg_color)) {
            $badge_bg_color = 'style="background-color:' . $badge_bg_color . '";';
        } else {
            $badge_bg_color = '';
        }

	    if ($special_car && !empty($badge_text)): ?>
        <div class="stm-badge-directory heading-font" <?php echo sanitize_text_field($badge_bg_color); ?>>
			<?php echo esc_attr($badge_text); ?>
        </div>
	<?php endif; ?>
	<!---Media-->
	<div class="stm-car-medias">
		<?php if(!empty($carPhotos)): ?>
			<div class="stm-listing-photos-unit stm-car-photos-<?php echo listing()->id; ?>">
				<i class="stm-service-icon-photo"></i>
				<span><?php echo count($carPhotos); ?></span>
			</div>

			<script type="text/javascript">
				jQuery(document).ready(function(){

					jQuery(".stm-car-photos-<?php echo listing()->id; ?>").click(function(e) {
						e.preventDefault();
						jQuery.fancybox.open([
							<?php foreach($carPhotos as $carPhoto): ?>
							{
								href  : "<?php echo esc_url($carPhoto['url']); ?>"
							},
							<?php endforeach; ?>
						], {
							padding: 0
						}); //open
					});
				});

			</script>
		<?php endif; ?>
		<?php if(!empty($carVideos)): ?>
			<div class="stm-listing-videos-unit stm-car-videos-<?php echo listing()->id; ?>">
				<i class="fa fa-film"></i>
				<span><?php echo count($carVideos); ?></span>
			</div>

			<script type="text/javascript">
				jQuery(document).ready(function(){

					jQuery(".stm-car-videos-<?php echo listing()->id; ?>").click(function(e) {
						e.preventDefault();
						jQuery.fancybox.open([
							<?php foreach($carVideos as $carVideo): ?>
							{
								href  : "<?php echo esc_url($carVideo); ?>"
							},
							<?php endforeach; ?>
						], {
							type: 'iframe',
							padding: 0
						}); //open
					}); //click
				}); //ready

			</script>
		<?php endif; ?>
	</div>

	<!--Favorite-->
	<?php if(!empty($show_favorite) and $show_favorite): ?>
		<div
			class="stm-listing-favorite"
			data-id="<?php echo esc_attr(listing()->id); ?>"
			data-toggle="tooltip" data-placement="right" title="<?php esc_attr_e('Add to favorites', 'motors') ?>"
		>
			<i class="stm-service-icon-staricon"></i>
		</div>
	<?php endif; ?>

	<!--Compare-->
	<?php if(!empty($show_compare) and $show_compare): ?>
		<div
			class="stm-listing-compare stm-compare-directory-new"
			data-id="<?php echo esc_attr(listing()->id); ?>"
			data-title="<?php listing()->title; ?>"
			data-toggle="tooltip" data-placement="left" title="<?php esc_attr_e('Add to compare', 'motors') ?>"
		>
			<i class="stm-service-icon-compare-new"></i>
		</div>
	<?php endif; ?>
</div>

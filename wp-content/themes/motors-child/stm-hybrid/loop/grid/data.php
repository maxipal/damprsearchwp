<?php
if ( ! isset( listing()->attributes ) ) {
	return;
}

$attributes = array_filter( listing()->attributes, function ( $attribute) {
	return $attribute->useInGrid && $attribute->value !== null;
} );
?>
<div class="car-meta-bottom">
	<ul>
		<?php foreach ( $attributes as $key => $attribute ) { ?>
			<li>
				<?php if ( ! empty( $attribute->icon ) ): ?>
					<i class="<?php echo esc_attr( $attribute->icon ) ?>"></i>
				<?php endif; ?>
				<span><?php echo esc_attr( $attribute->value ); ?></span>
			</li>
		<?php } ?>
	</ul>
</div>

<?php
$classes = [];
if ( listing()->sold ) {
	$classes[] = 'car-as-sold';
}
?>
<div class="col-md-4 col-sm-4 stm-directory-grid-loop stm-isotope-listing-item <?php echo implode(' ', $classes); ?>">
	<a href="<?php echo listing()->link() ?>" class="rmv_txt_drctn">
    	<?php stm_hybrid_include('loop/grid/image') ?>
        <div class="listing-car-item-meta">
			<div class="car-meta-top heading-font clearfix">
		        <?php stm_hybrid_include( 'loop/price' ); ?>
				<div class="car-title"><?php echo listing()->title ?></div>
			</div>
            <?php stm_hybrid_include('loop/grid/data'); ?>
        </div>
    </a>
</div>

<?php
$val = getLaravelMailTemplate('offer_notify');
$subject = $val['subject'];//(get_option('auction_notify_subject', '') != '') ? get_option('auction_notify_subject', '') : 'Auction Notify Subject';
$content = stripslashes($val['content']);
?>
<div class="etm-single-form">
    <h3>Auction New Offer</h3>
    <input type="text" name="auction_notify_subject" value="<?php echo $subject; ?>" class="full_width" />
    <div class="lr-wrap">
        <div class="left">
            <?php
            $sc_arg = array(
                'textarea_rows' => apply_filters( 'etm-ti-sce-row', 10 ),
                'wpautop' => true,
                'media_buttons' => apply_filters( 'etm-ti-sce-media_buttons', false ),
                'tinymce' => apply_filters( 'etm-ti-sce-tinymce', true ),
            );

            wp_editor( $content, 'auction_notify_template', $sc_arg );
            ?>
        </div>
        <div class="right">
            <h4>Shortcodes</h4>
            <ul>
                <?php
                foreach (getTemplateShortcodes('auctionNofify') as $k => $val) {
                    echo "<li id='{$k}'><input type='text' value='{$val}' class='auto_select' /></li>";
                }
                ?>
            </ul>
        </div>
    </div>
</div>

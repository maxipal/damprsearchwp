<?php
$val = (get_option('trade_in_status_template', '') != '') ? stripslashes(get_option('trade_in_status_template', '')) :
    '<table>
        <tr>
            <td>Car - </td>
            <td>[car_title]</td>
        </tr>
        <tr>
            <td>Name - </td>
            <td>[from_name]</td>
        </tr>
        <tr>
            <td>Email - </td>
            <td>[from_email]</td>
        </tr>
        <tr>
            <td>Status - </td>
            <td>[staus]</td>
        </tr>
         <tr>
            <td>Date added - </td>
            <td>[date_added]</td>
        </tr>

        <tr>
            <td>Replay - </td>
            <td>[tradein_url]</td>
        </tr>
    </table>';

$subject = (get_option('trade_in_status_subject', '') != '') ? get_option('trade_in_status_subject', '') : 'Car trade in message';
?>
<div class="etm-single-form">
    <h3>Trade In Status</h3>
    <input type="text" name="trade_in_status_subject" value="<?php echo $subject; ?>" class="full_width" />
    <div class="lr-wrap">
        <div class="left">
            <?php
            $sc_arg = array(
                'textarea_rows' => apply_filters( 'etm-ti-sce-row', 10 ),
                'wpautop' => true,
                'media_buttons' => apply_filters( 'etm-ti-sce-media_buttons', false ),
                'tinymce' => apply_filters( 'etm-ti-sce-tinymce', true ),
            );

            wp_editor( $val, 'trade_in_status_template', $sc_arg );
            ?>
        </div>
        <div class="right">
            <h4>Shortcodes</h4>
            <ul>
                <?php
                foreach (getTemplateShortcodes('tradeInStatus') as $k => $val) {
                    echo "<li id='{$k}'><input type='text' value='{$val}' class='auto_select' /></li>";
                }
                ?>
            </ul>
        </div>
    </div>
</div>

<?php

// Return true if current page auction
function stm_is_auction_page(){
	//if(!stm_is_dealer()) return false;// Only dealers may be see auction

	if(!isset($pid)) $pid = get_the_id();
	if(!$pid) return false;

	$auction_page = (int)get_theme_mod('auction_page', 0);
	$pid = (int)$pid;
	if($auction_page == $pid) return true;
	return false;
}

// Return auction page ID
function stm_get_auction_page(){
	$auction_page = (int)get_theme_mod('auction_page', 0);
	if($auction_page) return $auction_page;
	return false;
}



add_action('wp_ajax_stm_update_profile_exdended','stm_update_profile_exdended');
function stm_update_profile_exdended(){
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	$user_id = get_current_user_id();
	if(!$user_id) return;

	update_user_meta( $user_id, 'stm_first_name', esc_attr( $_REQUEST['first_name'] ) );
	update_user_meta( $user_id, 'stm_last_name', esc_attr( $_REQUEST['last_name'] ) );
	update_user_meta( $user_id, 'stm_phone', esc_attr( $_REQUEST['stm_phone'] ) );
	update_user_meta( $user_id, 'first_name', esc_attr( $_REQUEST['first_name'] ) );
	update_user_meta( $user_id, 'last_name', esc_attr( $_REQUEST['last_name'] ) );

	do_action('profile_update', $user_id, array('stm_first_name' => esc_attr( $_REQUEST['first_name'] )));

	wp_send_json( array('success' => true), 200 );
}

// Redirect not dealer to inventory
// add_action('template_redirect', function(){
// 	if(listing()->auction){
// 		if((is_user_logged_in() && listing()->author_id == get_current_user_id()) || stm_is_dealer()) return;
// 		wp_redirect( get_permalink( get_theme_mod('listing_archive', 0) ), 302 );
// 	}
// });

// Register Settings in customizer page
add_action('init', 'stm_register_auction_settings');
function stm_register_auction_settings(){
	// $temp = array();
	// $pages = get_posts(array(
	// 	'post_type' => 'page',
	// 	'post_status' => 'publish',
	// 	'posts_per_page' => -1,

	// ));
	// foreach ($pages as $page) {
	// 	$temp[$page->ID] = $page->post_title;
	// }
	STM_Customizer::setSection('auction_settings', array(
		'title' => esc_html__('Auction settings', 'motors'),
		'fields' => array(
			'sell_auction' => array(
				'label' => esc_html__('Sell auction page', 'motors'),
				'type' => 'stm-post-type',
				'post_type' => 'page',
				'default' => ''
			),

			'modal_title' => array(
				'label' => esc_html__('Modal Title', 'motors'),
				'type' => 'text'
			),


			'single_auction' => array(
				'type' => 'stm-separator',
			),


			'single_bg' => array(
				'label' => esc_html__('Single Bg Image', 'motors'),
				'type' => 'image'
			),
			'single_title' => array(
				'label' => esc_html__('Single Title', 'motors'),
				'type' => 'text',
			),
			'single_content' => array(
				'label' => '',
				'type' => 'stm-code',
				'description' => esc_html__("Type the text under title", 'motors')
			),
			'single_btn' => array(
				'label' => esc_html__('Button caption', 'motors'),
				'type' => 'text',
			),



			'single_auction' => array(
				'type' => 'stm-separator',
			),


			'sell_single' => array(
				'label' => esc_html__('Sell single car', 'motors'),
				'type' => 'stm-post-type',
				'post_type' => 'page',
				'default' => ''
			),
			'auction_bg' => array(
				'label' => esc_html__('Auction Bg Image', 'motors'),
				'type' => 'image'
			),
			'auction_title' => array(
				'label' => esc_html__('Auction Title', 'motors'),
				'type' => 'text',
			),
			'auction_content' => array(
				'label' => '',
				'type' => 'stm-code',
				'description' => esc_html__("Type the text under title", 'motors')
			),
			'auction_btn' => array(
				'label' => esc_html__('Button caption', 'motors'),
				'type' => 'text',
			),
		)
	));
}

// If this auction page than redefine variable "archive_path"
add_filter('stm_listings_inventory_page_id', 'stm_redefine_listing_archive',100,1);
function stm_redefine_listing_archive($listing_link){
	if(stm_is_auction_page()) return stm_get_auction_page();
	return $listing_link;
}


<?php
function stm_user_suggestions() {
	$users = get_users( array(
		'numbers'  => - 1,
		'role__in' => [ 'stm_dealer' ]
	) );

	$users_array = array();
	foreach ( $users as $user ) {
		/** @var WP_User $user */
		$users_array[] = [
			'id'   => $user->ID,
			'name' => $user->display_name,
			'url'  => get_author_posts_url( $user->ID, $user->user_nicename ),
		];
	}

	wp_send_json( $users_array );
	exit;
}

add_action( 'parse_request', function ( WP $wp ) {
	// respond on /user-suggestions/ request uri
	if ( $wp->request == 'user-suggestions' ) {
		define( 'DOING_AJAX', true );
		stm_user_suggestions();
	}
} );

<?php

/**
 * Overrides parent theme's function
 *
 * @return array
 */
function stm_account_navigation() {
	$notify           = new STMNotification;
	$notify_testdrive = $notify->getTestDriveNotify( false );
	$notify_tradein   = $notify->getTradeInNotify( false );
	$notify_bids      = $notify->getAuctionsBids( false );
	$notify_auctions  = $notify->getAuctions(false);
	$mess             = $notify->getTradeInMessages();
	$tradein_mess     = 0;
	foreach ( $mess as $tradein ) {
		if ( is_array( $tradein ) && count( $tradein ) ) {
			$tradein_mess = $tradein_mess + count( $tradein );
		}
	}
	$notify_tradein = $tradein_mess + $notify_tradein;

	$iconpath = get_stylesheet_directory_uri() . '/hybrid/images/icons.cars.com.light';

	$nav = array(
		'inventory' => array(
			'label' => esc_html__( 'My Inventory', 'motors' ),
			'menu'  => true,
			'svg'   => $iconpath . '/avatar-line.svg',
			'icon' => 'stm-service-icon-inventory',
		),
		'favourite' => array(
			'label' => esc_html__( 'My Favorites', 'motors' ),
			'menu'  => true,
			'svg'   => $iconpath . '/carfront-line.svg',
			'icon' => 'stm-service-icon-star-o',
		)
	);
	if(LISTING_ENABLE_AUCTION){
		$nav['myauction'] = array(
			'label' => esc_html__( 'My Auction', 'motors' ),
			'menu'  => true,
			'svg'   => $iconpath . '/features.svg',
			'badge' => $notify_bids,
		);
		$nav['auction'] = array(
			'label' => esc_html__( 'Auctions', 'motors' ),
			'menu'  => stm_is_dealer(),
			'svg'   => $iconpath . '/tag-line.svg',
			'badge' => $notify_auctions,
		);
	}
	if(TRADE_IN_PAGE){
		$nav['trade_in']  = array(
			'label' => esc_html__( 'Trade-In Requests', 'motors-child' ),
			'menu'  => true,
			'svg'   => $iconpath . '/searchalt-line.svg',
			'badge' => $notify_tradein,
		);
	}

	if(TESTDRIVE_PAGE){
		$nav['testdrive'] = array(
			'label' => esc_html__( 'Test-Drive Requests', 'motors-child' ),
			'menu'  => true,
			'svg'   => $iconpath . '/searchalt-line.svg',
			'badge' => $notify_testdrive,
		);
	}
	$nav['settings']  = array(
		'label' => esc_html__( 'Profile Settings', 'motors' ),
		'menu'  => true,
		'svg'   => $iconpath . '/settings-line.svg',
		'icon' => 'fa fa-cog',
	);
//	$nav['become-dealer'] = array(
//		'label' => esc_html__( 'Become a dealer', 'motors' ),
//		'menu'  => false,
//	);


	return apply_filters( 'stm_account_navigation', $nav );
}

function stm_account_navigation_items() {
	$items = [];
	foreach ( stm_account_navigation() as $key => $item ) {
		if ( empty( $item['menu'] ) ) {
			continue;
		}
		$item['url']     = ! empty( $item['url'] ) ? $item['url'] : stm_account_url( $key );
		$item['classes'] = ! empty( $item['classes'] ) ? $item['classes'] : '';
		$item['current'] = stm_account_current_page() == $key;
		$items[ $key ]   = $item;
	}

	return $items;
}

function stm_account_current_page() {
	global $wp;
	$page = '';

	if ( ! empty( $wp->query_vars['my_account'] ) ) {
		$page = $wp->query_vars['my_account'];
	}

	return apply_filters( 'stm_account_current_page', $page );
}

/******************************************
 * My account page handling via WP rewrite
 *****************************************/
add_action( 'init', function () {
	add_rewrite_rule( 'my-account(?:/(.*))?', 'index.php?my_account=$matches[1]', 'top' );
} );

add_filter( 'query_vars', function ( $query_vars ) {
	$query_vars[] = 'my_account';

	return $query_vars;
} );

add_filter( 'request', function ( $query_vars ) {
	if ( ! isset( $query_vars['my_account'] ) ) {
		return $query_vars;
	}

	// Display login/register page for non logged-in users
	// when accessing /my-account
	if ( ! is_user_logged_in() ) {
		return [
			'page_id' => get_theme_mod( 'login_page', 1718 ),
		];
	}

	return [
		'my_account' => $query_vars['my_account'],
		'author'     => get_current_user_id(),
	];
} );

add_filter( 'template_include', function ( $template ) {
	if ( is_my_account() ) {
		// Redirect for non-existent account pages
		if ( is_user_logged_in() && ! array_key_exists( stm_account_current_page(), stm_account_navigation() ) ) {
			wp_redirect( stm_account_url( stm_account_default_page() ) );
			exit;
		}
		// Prevent caching of private account pages
		header( 'Cache-Control: private, no-cache, no-store', true );

		return locate_template( 'my-account.php' );
	}

	return $template;
} );

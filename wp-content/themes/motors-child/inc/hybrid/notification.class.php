<?php

class STMNotification{

	private $notifyMeta = 'notifies';
	private $pages = array();
	private $user_id = null;
	public $notifies = array();
	private $notifyType = array(
		'tradein',
		'testdrive',
		'tradein_mess',
		'testdrive_mess',
		'auctions',
		'auction_bids',
	);

	public function __construct()
	{
		$this->user_id = get_current_user_id();//var_dump(date("d.m.Y H:i:s",get_user_meta($this->user_id,'synced_at', true)));var_dump(get_user_meta($this->user_id,'sync_error', true));
		$this->notifies = (array)get_user_meta($this->user_id,$this->notifyMeta, true);

		add_action( 'wp_ajax_get_notifies', array($this,'STMNotification') );

		add_action( 'wp_ajax_get_tradein', array($this,'getTradeInNotifyAJAX') );
		add_action( 'wp_ajax_get_tradein_messages', array($this,'getTradeInMessages') );

		add_action( 'wp_ajax_get_testdrive', array($this,'getTestDriveNotifyAJAX') );
		add_action( 'wp_ajax_get_testdrive_messages', array($this,'getTestDriveMessages') );

		add_action( 'wp_ajax_get_auctions', array($this,'getAuctionNotify') );
		add_action( 'wp_ajax_get_auction_bids', array($this,'getAuctionBids') );

		// for test only
		if(isset($_REQUEST['clear-notify'])){
			update_user_meta($this->user_id,$this->notifyMeta, []);
		}
	}

	private function _getUserPostList($postType = null){
		$user = get_user_by('id',$this->user_id);
		$args = array(
			'post_type' => $postType,
			'post_status' => 'any',
			'posts_per_page' => -1,
			'fields' => 'ids',
			'meta_query' => array(
				'relation' => 'OR',
				array(
					'key' => 'u_from',
					'value' => $user->user_email,
					'compare' => '=',
				),
				array(
					'key' => 'u_to',
					'value' => $user->user_email,
					'compare' => '=',
				)
			)
		);
		$requests = get_posts($args);
		return $requests;
	}


	private function getNotify($notify_name,$post_type, $ajax){
		if(!isset($this->notifies[$notify_name])) $this->notifies[$notify_name] = array();
		$available = (array)$this->notifies[$notify_name];
		$all = $this->_getUserPostList($post_type);
		$new = array_diff($all,$available);
		$cnt = count($new);
		if($cnt){
			$this->notifies[$notify_name] = array_merge($new,$this->notifies[$notify_name]);
		}
		if(!$ajax) return $cnt;
		$this->setJSONrequest($cnt);
	}

	private function _getAuctionsList(){
		$res = \Stm_Hybrid\Api::make()->get(
			'listings/',
			[
				'context' => 'list',
				'auction' => 1,
				'filters'=>'all',
				'per_page' => 100,
				'stm_lat' => 0,
				'stm_lng' => 0,
				'max_search_radius' => get_theme_mod( 'distance_search', 10000 ),
				'featured' => 'with',
			],
			[]
		);
		$auctions = json_decode( $res->get_data() );
		$temp = array();
		if($auctions->data){
			foreach ($auctions->data as $auction) {
				$temp[] = $auction->id;
			}
		}

		return $temp;
	}

	private function _getAuctionsBids(){
		$res = \Stm_Hybrid\Api::make()->post(
			'bits/my',
			[
				'uid' => $this->user_id,
				'status'=>'exclude'
			],
			['auth' => wp_get_current_user()]
		);
		$bids = json_decode( $res->get_data() );

		$temp = array();
		if(isset($bids->data)){
			foreach ($bids->data as $bid) {
				$temp[] = $bid->id;
			}
		}

		return $temp;
	}



	public function getTradeInNotifyAJAX(){
		$this->getTradeInNotify(true);
	}

	public function getTradeInNotify($ajax = false)
	{
		return $this->getNotify('tradein','stm_tradein', $ajax);
	}

	public function getTradeInMessages()
	{
		if(!isset($this->notifies['tradein_mess'])) $this->notifies['tradein_mess'] = array();
		$available = $this->notifies['tradein_mess'];
		// var_dump($available);
		$tradein_list = $this->_getUserPostList('stm_tradein');
		$mess = 0;
		$new = array();

		foreach ($tradein_list as $tradein) {
			$messages = get_post_meta($tradein,'dialog',true);
			if(!isset($available[$tradein])) $available[$tradein] = array();
			$temp = array();
			if(is_array($messages)){
				foreach ($messages as $message) {
					if(isset($message['date'])) $temp[] = $message['date'];
				}
			}
			$new[$tradein] = array_diff($temp,$available[$tradein]);
			$available[$tradein] = $available[$tradein]  + $new[$tradein];
		}
		$this->notifies['tradein_mess'] = $available;
		return $new;
	}

	public function addTradeInMessage($trade_id, $mess_id){
		if(!isset($this->notifies['tradein_mess'])) $this->notifies['tradein_mess'] = array();
		if(!isset($this->notifies['tradein_mess'][$trade_id])) $this->notifies['tradein_mess'][$trade_id] = array();
		$this->notifies['tradein_mess'][$trade_id][] = $mess_id;
		$this->saveType('tradein_mess');
	}




	public function getTestDriveNotifyAJAX(){
		$this->getTestDriveNotify(true);
	}

	public function getTestDriveNotify($ajax = false)
	{
		return $this->getNotify('testdrive','stm_testdrive',$ajax);
	}

	public function getTestDriveMessagesAJAX()
	{
		//return $this->getTestDriveMessages(true);
	}

	public function getTestDriveMessages()
	{
		//return $this->getNotify('testdrive_mess','stm_testdrive',$ajax);
	}






	public function getAuctionNotify($ajax = false)
	{

	}

	public function getAuctionsBids($ajax = false, $bids_list = false)
	{
		if(!isset($this->notifies['auction_bids'])) $this->notifies['auction_bids'] = array();
		$available = (array)$this->notifies['auction_bids'];
		$all = $bids_list ? $bids_list : $this->_getAuctionsBids();
		$new = array_diff($all,$available);

		$cnt = count($new);
		if($cnt){
			$this->notifies['auction_bids'] = array_merge($new,$available);
		}
		if(!$ajax) return $cnt;
		$this->setJSONrequest($cnt);
	}

	public function getAuctionBids($auction_id = null)
	{
		//return $this->setJSONrequest($this->notifies['auction_bids']);
	}

	public function getAuctions($ajax = false, $auctions_list = false)
	{
		if(!isset($this->notifies['auctions'])) $this->notifies['auctions'] = array();
		$available = (array)$this->notifies['auctions'];
		$all = $auctions_list ? $auctions_list : $this->_getAuctionsList();
		$new = array_diff($all,$available);
		$cnt = count($new);
		if($cnt){
			$this->notifies['auctions'] = array_merge($new,$this->notifies['auctions']);
		}
		if(!$ajax) return $cnt;
		$this->setJSONrequest($cnt);
	}


	private function save(){
		update_user_meta($this->user_id,$this->notifyMeta, $this->notifies);
	}

	public function saveType($type = false,$newVal = false){
		if($newVal === false){
			$notifies = (array)get_user_meta($this->user_id,$this->notifyMeta, true);
			if(!isset($notifies[$type])) $notifies[$type] = array();
			if(isset($this->notifies[$type])){
				$notifies[$type] = $this->notifies[$type];
			}
			update_user_meta($this->user_id,$this->notifyMeta, $notifies);
		}else{
			$notifies = (array)get_user_meta($this->user_id,$this->notifyMeta, true);
			$notifies[$type] = $newVal;
			update_user_meta($this->user_id,$this->notifyMeta, $notifies);

		}

	}

	private function setJSONrequest($cnt){
		$response['success'] = true;
		$response['cnt'] = $cnt;
		wp_send_json( $response, 200 );
	}

}

new STMNotification;

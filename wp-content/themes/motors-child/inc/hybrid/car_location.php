<?php

class Stm_Car_Location_Hybrid_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'car_location', // Base ID
			__('Car Location Hybrid', 'stm_motors_extends'), // Name
			array( 'description' => __( 'Use this widget on single listing page to display Google Map with car location.', 'stm_motors_extends' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );

		echo $args['before_widget'];
		if ( ! empty( $title ) ) {
			echo $args['before_title'] . esc_html( $title ) . $args['after_title'];
		}
		wp_enqueue_script( 'map-view-osm' );
		$id = get_the_ID();

		$attributes = listing()->getAttributes();
		$lat_lng = explode(',', $attributes['coordinates']);
		$car_lat = $lat_lng[0];
		$car_lng = $lat_lng[1];
		$car_location = $attributes['location'];
		$google_api_key = get_theme_mod('google_api_key', '');

		if(!empty($car_lat) and $car_lng and $car_location) {
			if(empty($google_api_key)){
				?>
				<div
					id="stm-dealer-gmap"
					data-lat="<?php echo $car_lat ?>"
					data-lng="<?php echo $car_lng ?>"></div>
				<?php
			}else{
				stm_dealer_gmap($car_lat, $car_lng);
			}
		}

		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {

		$title = '';

		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}else {
			$title = '';
		}

		?>
		<p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'stm_motors_extends' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
		<?php
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? esc_attr( $new_instance['title'] ) : '';

		return $instance;
	}

}

function stm_register_car_location_hybrid_widget() {
	register_widget( 'Stm_Car_Location_Hybrid_Widget' );
}
add_action( 'widgets_init', 'stm_register_car_location_hybrid_widget' );
remove_action( 'widgets_init', 'stm_register_car_location_widget' );

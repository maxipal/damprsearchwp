<?php

// Register Test Drive post type
add_action( 'init', 'register_stm_testdrive_init' );
function register_stm_testdrive_init() {
	$labels = array(
		'name'               => __( 'Test Drive', 'motors' ),
		'singular_name'      => __( 'Test Drive', 'motors' ),
		'menu_name'          => __( 'Test Drive', 'motors' ),
		'name_admin_bar'     => __( 'Test Drive', 'motors' ),
		'add_new'            => __( 'New Test Drive', 'motors' ),
		'add_new_item'       => __( 'Add New Test Drive', 'motors' ),
		'new_item'           => __( 'New Test Drive', 'motors' ),
		'edit_item'          => __( 'Edit Test Drive', 'motors' ),
		'view_item'          => __( 'View Test Drive', 'motors' ),
		'all_items'          => __( 'All Test Drive', 'motors' ),
		'search_items'       => __( 'Search Test Drive', 'motors' ),
		'parent_item_colon'  => __( 'Parent Test Drive:', 'motors' ),
		'not_found'          => __( 'No Test Drive found.', 'motors' ),
		'not_found_in_trash' => __( 'No Test Drive found in Trash.', 'motors' )
	);

	$args = array(
		'labels'             => $labels,
		'description'        => __( 'Description.', 'motors' ),
		'public'             => false,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'trade-in' ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'author', 'thumbnail', 'excerpt' )
	);

	register_post_type( 'stm_testdrive', $args );
}



// Add Section in Catalog
add_action('add_meta_boxes', 'stm_testdrive_add_custom_box');
function stm_testdrive_add_custom_box(){
	add_meta_box( 'stm_testdrive_single_page', __('Fields', 'intact-child'), 'stm_testdrive_meta_box_callback', array( 'stm_testdrive' ), 'normal', 'high' );
}


add_action('wp_ajax_testdrive_accept','stm_testdrive_accept');
function stm_testdrive_accept(){
	$time = esc_html( $_REQUEST['time'] );
	$reason = esc_html( $_REQUEST['reason'] );
	$accept = $_REQUEST['accept'] == 'true' ? 1 : 0;
	$pid = (int)$_REQUEST['test_id'];

	$testdrive = get_post($pid);
	$uid = $testdrive->post_author;

	$confirm = get_post_meta($pid, 'confirm', true);
	if($confirm){
		wp_send_json( array(
			'message' => __("Your request updated already","motors"),
		), 200 );
	}

	$args = array(
		'ID' => $pid,
		'meta_input' => array(
			'best_time' => strtotime($time),
			'reason' => $reason,
		)
	);
	update_post_meta($pid,'confirm', $accept);

	$res = wp_update_post( $args );

	if(is_wp_error( $res )){
		$out = __("Cant update request.","motors");

	}else{
		$out = __("Request updated! User sended notification.","motors");

		$fields = stm_get_user_custom_fields($uid);
		$username = stm_display_user_name_alt($uid);

		$u_from = get_post_meta($pid, 'u_from', true);
		$uname = get_post_meta($pid, 'uname', true);
		$phone = get_post_meta($pid, 'phone', true);
		$best_time = date('d/m/Y H:i',get_post_meta($pid, 'best_time', true) );
		$vehicle_id = get_post_meta($pid, 'vid', true);


		$args = array(
			'car' => listing($vehicle_id)->title,
			'name' => $uname,
			'email' => $u_from,
			'phone' => $phone,
			'best_time' => $best_time,
			'reason' => $reason,
		);


		add_filter('wp_mail_content_type', 'stm_set_html_content_type');

		if($accept){
			$subject = generateSubjectView('test_drive_accept', $args);
			$body = generateTemplateView('test_drive_accept', $args);
			// send message for success
			$res = wp_mail( $fields['email'], $subject, $body );
		}else{
			$subject = generateSubjectView('test_drive_cancel', $args);
			$body = generateTemplateView('test_drive_cancel', $args);
			// send message for unsuccess
			$res = wp_mail( $fields['email'],$subject, $body );
		}

		remove_filter('wp_mail_content_type', 'stm_set_html_content_type');
	}

	wp_send_json( array(
		'message' => $out,
		'mail_status' => var_export($res, true),
	), 200 );
}

// Custom Column in admin area
add_filter( 'manage_edit-stm_testdrive_columns', 'stm_testdrive_custom_columns',1 ) ;
function stm_testdrive_custom_columns( $columns ) {
	$columns['u_from'] = __("From","motors");
	$columns['u_to'] = __("To","motors");
	$columns['vehicle'] = __("Vehicle","motors");
	$columns['best_time'] = __("Best Time","motors");
	return $columns;
}

add_action( 'manage_stm_testdrive_posts_custom_column', 'stm_testdrive_custom_columns_call', 1, 2 );
function stm_testdrive_custom_columns_call( $column, $post_id ) {
	global $post;
	$uemail_f = get_post_meta($post_id, 'u_from', true);
	//var_dump($uemail_f);
	$user_f = get_user_by('email',$uemail_f);
	$uemail_t = get_post_meta($post_id, 'u_to', true);
	$user_t = get_user_by('email',$uemail_t);

	switch( $column ) {
	case 'u_from' :
		if(!empty($uemail_f)){
			if(!$user_f){
				echo __("Unregistered","motors") . "<div>({$uemail_f})</div>";
			}else{
				$fields = stm_get_user_custom_fields($user_f->ID);
				echo stm_display_user_name($user_f->ID) . "<div>({$fields['email']})</div>";
			}
		}
		break;
	case 'u_to' :
		if(!empty($uemail_t)){
			if(!$user_t){
				echo __("Unregistered","motors") . "<div>({$uemail_t})</div>";
			}else{
				$fields = stm_get_user_custom_fields($user_t->ID);
				echo stm_display_user_name($user_t->ID) . "<div>({$fields['email']})</div>";
			}
		}
		break;
	case 'make' :
		echo get_post_meta($post_id, 'make', true);
		break;
	case 'vehicle' :
		echo get_the_title($post_id);
		break;
	case 'best_time' :
		echo date("d/m/Y H:i:s", get_post_meta($post_id, 'best_time', true));
		break;
	default :
		break;
	}
}



//add_action('admin_enqueue_scripts', 'stm_testdrive_wp_admin_style');
function stm_testdrive_wp_admin_style(){
	wp_enqueue_style( 'stm-select2', get_theme_file_uri( '/assets/css/select2.min.css' ), null, STM_THEME_VERSION, 'all' );
	wp_enqueue_script( 'stm-select2-js', get_theme_file_uri( '/assets/js/select2.full.min.js' ), array('jquery'), STM_THEME_VERSION, false );
}



// Render admin page
function stm_testdrive_meta_box_callback( $post, $meta ){
	$screens = $meta['args'];
	//wp_nonce_field( plugin_basename(__FILE__), 'stm_tradein_noncename' );

	// Render
	$post_id = !empty($_GET['post']) ? $_GET['post'] : 0;
	$vehicle_id = get_post_meta($post_id, 'vid', true);

	$uemail_f = get_post_meta($post_id, 'u_from', true);
	$user_f = get_user_by('email',$uemail_f);
	$uemail_t = get_post_meta($post_id, 'u_to', true);
	$user_t = get_user_by('email',$uemail_t);
	$vehicle_title = listing($vehicle_id)->title;
	$vehicle_url = listing($vehicle_id)->link;;
	$best_time = date("d/m/Y H:i",get_post_meta($post_id, 'best_time', true));


	$args = array(
		"blog_id"	=> 1,
		"orderby"	=> "nicename",
		"number"	=> "10",
		"include"	=> array($user_f->ID,$user_t->ID),
		"fields"	=> array('ID','user_email'),
	);

	$blogusers = get_users($args);
	$users = array();
	foreach ($blogusers as $user)
		$users[] = array(
			'email' => $user->user_email,
			'id' => $user->ID,
		);
	?>


	<div class="clearfix"><h3><?php _e("Request Information","motors") ?></h3></div>
	<div class="container-group">
		<div class="field-group width100">
			<div class="field-label">
				<label for="u_from"><?php _e("Request from:","motors") ?></label>
			</div>
			<div class="field-area">
				<select name="u_from" id="u_from" style="width:320px">
					<?php foreach ($users as $user): ?>
						<option <?php if($uemail_f == $user['email']) echo 'selected' ?> value="<?php echo $user['email'] ?>"><?php echo $user['email'] ?></option>
					<?php endforeach ?>
				</select>
			</div>
		</div>
		<div class="field-group width100">
			<div class="field-label">
				<label for="u_to"><?php _e("Request to:","motors") ?></label>
			</div>
			<div class="field-area">
				<select name="u_to" id="u_to" style="width:320px">
					<?php foreach ($users as $user): ?>
						<option <?php if($uemail_t == $user['email']) echo 'selected' ?> value="<?php echo $user['email'] ?>"><?php echo $user['email'] ?></option>
					<?php endforeach ?>
				</select>
			</div>
		</div>
	</div>


	<div class="clearfix"><h3><?php _e("Vehicle Information","motors") ?></h3></div>
	<div class="container-group">
		<div class="field-group">
			<div class="field-label">
				<label for="vehicle_name"><?php _e("Vehicle Name","motors") ?></label>
			</div>

			<div class="field-area width100">
				<a id="vehicle_name" href="<?php echo $vehicle_url ?>"><?php echo $vehicle_title ?></a>
			</div>

		</div>

		<div class="field-group">
			<div class="field-label">
				<label for="best_time"><?php _e("Best time","motors") ?></label>
			</div>

			<div class="field-area">
				<input autocomplete="off" data-week="<?php echo htmlspecialchars(json_encode( stm_get_reserved_dates() )) ?>" class="stm-date-timepicker" value="<?php echo $best_time ?>" type="text" name="best_time" id="best_time">
			</div>

		</div>
	</div>

	<div class="clearfix"><h3><?php _e("Contact Details","motors") ?></h3></div>
	<div class="container-group clearfix">
		<div class="field-group">
			<div class="field-label">
				<label for="uname"><?php _e("User Name","motors") ?></label>
			</div>

			<div class="field-area width100">
				<input autocomplete="off" value="<?php echo get_post_meta($post_id, 'uname', true) ?>" type="text" name="uname" id="uname">
			</div>

		</div>


		<div class="field-group">
			<div class="field-label">
				<label for="uemail"><?php _e("Email","motors") ?></label>
			</div>

			<div class="field-area">
				<input autocomplete="off" value="<?php echo get_post_meta($post_id, 'uemail', true); ?>" type="text" name="uemail" id="uemail">
			</div>

		</div>

	</div>

	<div class="container-group clearfix">
		<div class="field-group">
			<div class="field-label">
				<label for="phone"><?php _e("Phone","motors") ?></label>
			</div>

			<div class="field-area width100">
				<input autocomplete="off" value="<?php echo get_post_meta($post_id, 'phone', true); ?>" type="text" name="phone" id="phone">
			</div>

		</div>

	</div>


	<style type="text/css">
		.container-group{
			margin-left: -15px;
			margin-right: -15px;
		}
		.field-group {
			float: left;
			width: 50%;
			padding: 5px 15px;
			position: relative;
			min-height: 1px;
		}
		.container-group:before,
		.container-group:after,
		.clearfix:before,
		.clearfix:after{
			display: table;
			content: " ";
		}
		.clearfix{
			display: inline-block;
			width: 100%;
		}
		.field-group input[type=text]{
			width: 100%;
		}
		.field-group input[type=radio]{
			margin-top: 5px;
			margin-bottom: 5px;
		}
		.width100{
			width: 100%;
		}
		:after, :before {
			-webkit-box-sizing: border-box;
			-moz-box-sizing: border-box;
			box-sizing: border-box;
		}
		* {
			-webkit-box-sizing: border-box;
			-moz-box-sizing: border-box;
			box-sizing: border-box;
		}
		#stm_tradein_single_page{
			display: inline-block;
			width: 100%;
			padding-bottom: 40px;
		}
		#wpfooter{
			position: static;
		}
	</style>


	<script type="text/javascript">
		jQuery("select[name=u_from],select[name=u_to]").select2({
			minimumInputLength: 2,
			tags: [],
			ajax: {
				url: '/wp-admin/admin-ajax.php',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,
				cache: true,
				data: function (opt) {
					return {
						action: 'stm_get_users_by_name',
						param: opt,
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
			}
		});

		jQuery(document).ready(function($) {

			function disabledWeekdays(date) {
				var day = date.getDate();
				var month = date.getMonth()+1;
				var year = date.getFullYear();
				var hour = date.getHours();
				var min = date.getMinutes();
				var days = $('.stm-date-timepicker').data('week');
				if(days){
					var str = year+'/'+month+'/'+day;
					//let found = days.find(element => element == str );//+' '+hour+':'+min);
					var found = days.find(function (element) {
						return element == str;
					});
					if(found) return [false];
					else return [true];
				}
			}

			$('.stm-date-timepicker').stm_datetimepicker({
				// format: 'Y/m/d H:m',
				beforeShowDay: disabledWeekdays,
				minDate: new Date(),
				maxDate:new Date('<?php echo date("Y/m/d H:i",strtotime("+1 year",time())) ?>'),
				//weekends: <?php echo json_encode( stm_get_reserved_dates() ) ?>,
				onChangeDateTime:function(dp,$input){
					$input.attr('value',$input.val());
				}
			});
		});
	</script>

	<?php
}




add_action( 'save_post', 'stm_testdrive_admin_save_postdata' );
function stm_testdrive_admin_save_postdata( $post_id ) {
	//if ( !wp_verify_nonce( $_POST['stm_tradein_noncename'], plugin_basename(__FILE__) ) ) return;
	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return;
	if( !current_user_can( 'edit_post', $post_id ) ) return;

	if(isset($_POST['u_from']))
		update_post_meta( $post_id, 'u_from', esc_attr( $_POST['u_from'] ) );
	if(isset($_POST['u_to']))
		update_post_meta( $post_id, 'u_to', esc_attr( $_POST['u_to'] ) );
	if(isset($_POST['uname']))
		update_post_meta( $post_id, 'uname', esc_attr( $_POST['uname'] ) );
	if(isset($_POST['uemail']))
		update_post_meta( $post_id, 'uemail', esc_attr( $_POST['uemail'] ) );
	if(isset($_POST['phone']))
		update_post_meta( $post_id, 'phone', esc_attr( $_POST['phone'] ) );
	if(isset($_POST['best_time'])){
		update_post_meta( $post_id, 'best_time', esc_attr( strtotime($_POST['best_time']) ) );
	}

}


function stm_get_reserved_dates(){
	$args = array(
		'post_type' => 'stm_testdrive',
		'post_status' => 'any',
		'posts_per_page' => -1,
		'meta_query' => array(
			'condition' => 'AND',
			array(
				'key' => 'best_time',
				'value' => '',
				'compare' => '!=',
			),
			array(
				'key' => 'best_time',
				'compare' => 'EXISTS',
			)
		)
	);
	$tds = get_posts($args);
	$temp = array();
	foreach ($tds as $td) {
		$date = get_post_meta( $td->ID, 'best_time', true );
		$temp[] = date('Y/n/j',$date);
	}
	return $temp;
}



function stm_get_testdrive_from($userEmail = ''){
	$args = array(
		'post_type' => 'stm_testdrive',
		'post_status' => 'any',
		'posts_per_page' => -1,
		'orderby' => 'date',
		'order'   => 'ASC',
		'suppress_filters' => true,
		'meta_query' => array(
			//'condition' => 'AND',
			array(
				'key' => 'u_from',
				'value' => $userEmail,
				'compare' => '=',
			)
		)
	);
	return get_posts($args);
}


function stm_get_testdrive_to($userEmail = ''){
	$args = array(
		'post_type' => 'stm_testdrive',
		'post_status' => 'any',
		'orderby' => 'date',
		'order'   => 'ASC',
		'suppress_filters' => true,
		'posts_per_page' => -1,
		'meta_query' => array(
			'condition' => 'AND',
			array(
				'key' => 'u_to',
				'value' => $userEmail,
				'compare' => '=',
			)
		)
	);
	return get_posts($args);
}



function stm_add_testdrive_form_request($args = false){
	if(!$args) return;

	$p_agrs = array(
		'post_type' => 'stm_testdrive',
		'post_status' => 'pending',
		'post_title' => $args['car'],
		'post_content' => '',
		'meta_input' => array(
			'u_from' => $args['u_from'],
			'u_to' => $args['u_to'],
			'uname' => $args['uname'],
			'uemail' => $args['uemail'],
			'phone' => $args['phone'],
			'best_time' => $args['best_time'],
			'vid' => $args['vid'],
		)
	);
	$res = wp_insert_post( $p_agrs );
}



remove_action('wp_ajax_stm_ajax_add_test_drive', 'stm_ajax_add_test_drive');
remove_action('wp_ajax_nopriv_stm_ajax_add_test_drive', 'stm_ajax_add_test_drive');
add_action('wp_ajax_stm_ajax_add_test_drive', 'stm_ajax_add_test_drive_notify');
add_action('wp_ajax_nopriv_stm_ajax_add_test_drive', 'stm_ajax_add_test_drive_notify');
function stm_ajax_add_test_drive_notify(){
	$response['errors'] = array();

	if (!filter_var($_POST['name'], FILTER_SANITIZE_STRING)) {
		$response['response'] = esc_html__('Please fill all fields', 'motors');
		$response['errors']['name'] = true;
	}
	if (empty($_POST['phone'])) {
		if(!is_email($_POST['email'])){
			$response['response'] = esc_html__('Please enter correct email', 'motors');
			$response['errors']['email'] = true;
		}
	}
	if (!is_numeric($_POST['phone'])) {
		$response['response'] = esc_html__('Please enter correct phone number', 'motors');
		$response['errors']['phone'] = true;
	}
	if (empty($_POST['date'])) {
		$response['response'] = esc_html__('Please fill all fields', 'motors');
		$response['errors']['date'] = true;
	}

	if(!filter_var($_POST['name'], FILTER_SANITIZE_STRING) && !is_email($_POST['email']) && !is_numeric($_POST['phone']) && empty($_POST['date'])) {
		$response['response'] = esc_html__('Please fill all fields', 'motors');
	}

	$vehicle_id = (isset($_POST['vehicle_id']) && !empty($_POST['vehicle_id'])) ? intval($_POST['vehicle_id']) : '';
	$user_added_by = listing($vehicle_id)->author_id;

	if(get_current_user_id() == $user_added_by){
		$response['status'] = 'danger';
		$response['response'] = esc_html__('You can not send request to self', 'motors');
		$response = json_encode($response);
		echo $response;
		exit;
	}

	$recaptcha = true;

	$recaptcha_enabled = get_theme_mod('enable_recaptcha', 0);
	$recaptcha_public_key = get_theme_mod('recaptcha_public_key');
	$recaptcha_secret_key = get_theme_mod('recaptcha_secret_key');
	if (!empty($recaptcha_enabled) and $recaptcha_enabled and !empty($recaptcha_public_key) and !empty($recaptcha_secret_key)) {
		$recaptcha = false;
		if (!empty($_POST['g-recaptcha-response'])) {
			$recaptcha = true;
		}
	}

	if ($recaptcha) {
		if (empty($response['errors']) and (!empty($_POST['vehicle_id']) || !empty($_POST['vehicle_name']))) {

			$title = (!empty($vehicle_id)) ? listing($vehicle_id)->title : '';
			$title = (isset($_POST['vehicle_name']) && !empty($_POST['vehicle_name'])) ? $_POST['vehicle_name'] : $title;



			$user_fields = stm_get_user_custom_fields($user_added_by);
			$test_drive['car'] = esc_html__('New request for test drive', 'motors') . ' ' . $title;
			$test_drive['u_from'] = esc_html__($_POST['email']);
			$test_drive['u_to'] = $user_fields['email'];
			$test_drive['uname'] = esc_html__($_POST['name']);
			$test_drive['uemail'] = esc_html__($_POST['email']);
			$test_drive['phone'] = esc_html__($_POST['phone']);
			$test_drive['best_time'] = esc_html__( strtotime($_POST['date']) );
			$test_drive['vid'] = esc_html__( $_POST['vehicle_id'] );


			stm_add_testdrive_form_request($test_drive);
			// $test_drive_id = wp_insert_post($test_drive);
			// update_post_meta($test_drive_id, 'name', $_POST['name']);
			// update_post_meta($test_drive_id, 'email', $_POST['email']);
			// update_post_meta($test_drive_id, 'phone', $_POST['phone']);
			// update_post_meta($test_drive_id, 'date', $_POST['date']);
			$response['response'] = esc_html__('Your request was sent', 'motors');
			$response['status'] = 'success';

			//Sending Mail to admin
			add_filter('wp_mail_content_type', 'stm_set_html_content_type');

			$to = esc_html__($_POST['email']);//get_bloginfo('admin_email');

			$args = array(
				'car' => $title,
				'name' => $_POST['name'],
				'email' => $_POST['email'],
				'phone' => $_POST['phone'],
				'best_time' => $_POST['date'],
			);

			$subject = generateSubjectView('test_drive', $args);
			$body = generateTemplateView('test_drive', $args);

			// wp_mail($to, $subject, $body);

			$res = 'mail';

			if (stm_is_listing()) {
				$car_owner = listing($vehicle_id)->author_id;

				if (!empty($car_owner)) {
					$user_fields = stm_get_user_custom_fields($car_owner);
					if (!empty($user_fields) and !empty($user_fields['email'])) {
						$res = wp_mail($user_fields['email'], $subject, $body);
					}
				}
			}

			remove_filter('wp_mail_content_type', 'stm_set_html_content_type');

		} else {
			//$response['response'] = esc_html__('Please fill all fields', 'motors');
			$response['status'] = 'danger';
		}

		$response['recaptcha'] = true;
	} else {
		$response['recaptcha'] = false;
		$response['status'] = 'danger';
		$response['response'] = esc_html__('Please prove you\'re not a robot', 'motors');
	}

	$response['mail_status'] = var_export($res, true);
	$response = json_encode($response);

	echo $response;
	exit;
}



add_action('wp_footer', function(){
	?>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			if (!$.stm_datetimepicker) {
				return;
			}

			function disabledWeekdays(date) {
				var day = date.getDate();
				var month = date.getMonth()+1;
				var year = date.getFullYear();
				var hour = date.getHours();
				var min = date.getMinutes();
				var days = $('.stm-date-timepicker').data('week');
				if(days){
					var str = year+'/'+month+'/'+day;
					var found = days.find(function (element) {
						return element == str;
					});
					if(found) return [false];
					else return [true];
				}
			}

			$('.stm-date-timepicker').stm_datetimepicker('destroy').stm_datetimepicker({
				// format: 'Y/m/d H:m',
				beforeShowDay: disabledWeekdays,
				minDate: new Date(),
				maxDate:new Date('<?php echo date("Y/m/d H:i",strtotime("+1 year",time())) ?>'),
				//weekends: <?php echo json_encode( stm_get_reserved_dates() ) ?>,
				onChangeDateTime:function(dp,$input){
					$input.attr('value',$input.val());
				}
			});
		});

	</script>
	<?php
},1000);

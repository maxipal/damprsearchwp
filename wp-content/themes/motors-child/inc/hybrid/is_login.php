<?php

add_action( 'wp_footer', function () {
	if ( is_user_logged_in() ) {
		return;
	}

	require_once get_stylesheet_directory() . '/partials/modals/login-required.php';
} );


function stm_logged_in_user_dropdown()
{
	if (!is_user_logged_in()) return;

	$result      = array();
	$user_fields = stm_get_user_custom_fields( '' );
	if ( ! empty( $user_fields['image'] ) ) {
		$result['avatar'] = '
			<div class="stm-dropdown-user-small-avatar">
				<img src="' . esc_url( $user_fields['image'] ) . '" class="im-responsive"/>
			</div>';
	} else {
		$result['avatar'] = null;
	}

	ob_start();
	if ( wp_is_mobile() ) {
		locate_template( 'partials/user/private-dropdown-mobile.php', true );
		$result['dropdown_mobile'] = ob_get_clean();
	} else {
		locate_template( 'partials/user/private-dropdown.php', true );
		$result['dropdown'] = ob_get_clean();
	}

	$result['topbar'] = '
		<a class="logout-link" href="' . esc_url(wp_logout_url(home_url())) . '" title="Log out">
			<i class="fa fa-icon-stm_icon_user"></i>'. __('Logout', 'motors') . '
		</a>';

	wp_send_json($result);
}

if ( isset( $_REQUEST['action'] ) && $_REQUEST['action'] == 'stm_is_login' ) {
	add_action( 'after_setup_theme', function () {
		define( 'DOING_AJAX', true );
		stm_logged_in_user_dropdown();
		exit;
	} );
}

function stm_private_user_dropdown_data() {
	function get_count( $value ) {
		if ( is_array( $value ) ) {
			return count( $value );
		} else {
			return 0;
		}
	}

	$user = wp_get_current_user();
	$token = stm_hybrid_user_token( $user );
	$token = $token['token'];

	$user_meta = \Stm_Hybrid\Listing::query(
		array(),
		array(
			'uri'  => 'user?with=cars,auctions,bids',
			'auth' => $token
		)
	);
	$auctions_ids 	  = !empty($user_meta['meta_res']['auctions_ids']) ? $user_meta['meta_res']['auctions_ids'] : null;
	$notify           = new STMNotification();
	$notify_tradein   = $notify->getTradeInNotify( false );
	$notify_testdrive = $notify->getTestDriveNotify( false );
	$notify_auctions  = $notify->getAuctions( false, $auctions_ids );
	$notify_bids      = $notify->getAuctionsBids( false, $user_meta['meta_res']['bids_ids'] );
	$mess             = $notify->getTradeInMessages();

	$tradein_mess = 0;
	foreach ( $mess as $tradein ) {
		$tradein_mess += get_count( $tradein );
	}
	$notify_tradein = $tradein_mess + $notify_tradein;

	$count_listings = $count_bids = 0;
	if ( ! empty( $user_meta['meta_res']['cars'] ) ) {
		$count_listings = $user_meta['meta_res']['cars'];
	}
	if ( ! empty( $user_auctions['meta_res']['auctions'] ) ) {
		$count_bids = $user_meta['meta_res']['auctions'];
	}

	$count_favorites = get_the_author_meta( 'stm_user_favourites', $user->ID );
	$count_favorites = get_count( array_filter( explode( ',', $count_favorites ) ) );

	$user_name = stm_display_user_name_alt( $user->ID );
	if ( empty( $user_name ) ) {
		$user_name = $user->display_name;
	}

	$iconpath = get_stylesheet_directory_uri() . '/hybrid/images/icons.cars.com.dark';

	return get_defined_vars();
}

remove_action('wp_ajax_stm_custom_register', 'stm_custom_register');
remove_action('wp_ajax_nopriv_stm_custom_register', 'stm_custom_register');
add_action('wp_ajax_stm_custom_register', 'stm_custom_register_alt');
add_action('wp_ajax_nopriv_stm_custom_register', 'stm_custom_register_alt');

function stm_custom_register_alt() {
	$response   = array();
	$errors     = array();
	$user_login = $user_email = $user_pass = $user_name = $user_lastname = $user_phone = '';
	$is_add_car = ! empty( $_POST['add_car'] );

	if (empty($_POST['stm_nickname'])) {
		$errors['stm_nickname'] = true;
	} else {
		$user_login = sanitize_text_field($_POST['stm_nickname']);
	}


	if ( empty( $_POST['stm_user_first_name'] ) ) {
		if ( $is_add_car ) {
			$errors['stm_user_first_name'] = true;
		}
	} else {
		$user_name = sanitize_text_field( $_POST['stm_user_first_name'] );
	}

	if ( empty( $_POST['stm_user_last_name'] ) ) {
		if ( $is_add_car ) {
			$errors['stm_user_last_name'] = true;
		}
	} else {
		$user_lastname = sanitize_text_field( $_POST['stm_user_last_name'] );
	}

	if ( ! is_email( $_POST['stm_user_mail'] ) ) {
		$errors['stm_user_mail'] = true;
	} else {
		$user_email = sanitize_email( $_POST['stm_user_mail'] );
		//$user_login = $user_email;
	}

	if ( empty( $_POST['stm_user_phone'] ) ) {
		if ( $is_add_car ) {
			$errors['stm_user_phone'] = true;
		}
	} else {
		$user_phone = sanitize_text_field( $_POST['stm_user_phone'] );
	}

	if ( empty( $_POST['stm_user_password'] ) ) {
		$errors['stm_user_password'] = true;
	} else {
		$user_pass = $_POST['stm_user_password'];
	}

	if ( ! empty( $_POST['redirect'] ) and $_POST['redirect'] == 'disable' ) {
		$redirect = false;
	} else {
		$redirect = true;
	}

	if ( ! empty( $errors ) ) {
		$response['errors']  = $errors;
		$response['message'] = esc_html__( 'Please fill required fields', 'stm_vehicles_listing' );
		wp_send_json( $response );

		return;
	}

	$user_data = array(
		'first_name' => $user_name,
		'last_name'  => $user_lastname,
		'user_login' => $user_login,
		'user_pass'  => $user_pass,
		'user_email' => $user_email
	);

	$user_id = wp_insert_user( $user_data );

	if ( is_wp_error( $user_id ) ) {
		$response['message'] = $user_id->get_error_message();
		$response['user']    = $user_id;
		wp_send_json( $response );

		return;
	}

	update_user_meta( $user_id, 'stm_phone', $user_phone );
	update_user_meta( $user_id, 'stm_show_email', '' );

	wp_set_current_user( $user_id, $user_login );
	wp_set_auth_cookie( $user_id );
	$user = new WP_User( $user_id );
	do_action( 'wp_login', $user_login, $user );

	if ( $redirect ) {
		$response['message']      = esc_html__( 'Congratulations! You have been successfully registered. Redirecting to your account profile page.', 'stm_vehicles_listing' );
		$response['redirect_url'] = stm_account_url();
	} else {
		ob_start();
		stm_add_a_car_user_info( $user_login, $user_name, $user_lastname, $user_id );
		$restricted   = false;
		$restrictions = stm_get_post_limits( $user_id );

		if ( $restrictions['posts'] < 1 && enablePPL() ) {
			$restricted = true;
		}

		$response['restricted'] = $restricted;
		$response['user_html']  = ob_get_clean();
	}

	//AUTH
	do_action( 'stm_register_new_user', $user_id );
	if ( (int) get_option( 'users_can_register' ) ) {
		$response['message'] = esc_html__( 'Congratulations! You have been successfully registered. Please, activate your account', 'stm_vehicles_listing' );
	} else {
		if ( $redirect ) {
			$response['message']      = esc_html__( 'Congratulations! You have been successfully registered. Redirecting to your account profile page.', 'stm_vehicles_listing' );
			$response['redirect_url'] = stm_account_url();
		} else {
			ob_start();
			stm_add_a_car_user_info( $user_login, $user_name, $user_lastname, $user_id );
			$restricted   = false;
			$restrictions = stm_get_post_limits( $user_id );

			if ( $restrictions['posts'] < 1 && enablePPL() ) {
				$restricted = true;
			}

			$response['restricted'] = $restricted;
			$response['user_html']  = ob_get_clean();
		}
	}

	// API token
	$response['token'] = stm_hybrid_user_token($user);

	add_filter( 'wp_mail_content_type', 'stm_set_html_content_type_mail' );

	/*Mail admin*/
	$to      = get_bloginfo( 'admin_email' );
	$subject = generateSubjectView( 'new_user', array( 'user_login' => $user_login ) );
	$body    = generateTemplateView( 'new_user', array( 'user_login' => $user_login ) );

	wp_mail( $to, $subject, $body );

	/*Mail user*/
	$subjectUser = generateSubjectView( 'welcome', array( 'user_login' => $user_login ) );
	$bodyUser    = generateTemplateView( 'welcome', array( 'user_login' => $user_login ) );
	wp_mail( $user_email, $subjectUser, $bodyUser );

	remove_filter( 'wp_mail_content_type', 'stm_set_html_content_type_mail' );

	$response['errors'] = $errors;
	wp_send_json( $response );
}

/**
 * Log in (overridden)
 */
function stm_custom_login_alt() {
	$errors = array();

	if ( empty( $_POST['stm_user_login'] ) ) {
		$errors['stm_user_login'] = true;
	} else {
		$username = sanitize_text_field( $_POST['stm_user_login'] );
	}

	if ( empty( $_POST['stm_user_password'] ) ) {
		$errors['stm_user_password'] = true;
	} else {
		$password = $_POST['stm_user_password'];
	}

	if ( isset( $_POST['redirect_path'] ) ) {
		$redirect_path = $_POST['redirect_path'];
	}

	$remember = false;

	if ( ! empty( $_POST['stm_remember_me'] ) and $_POST['stm_remember_me'] == 'on' ) {
		$remember = true;
	}

	if ( ! empty( $_POST['redirect'] ) and $_POST['redirect'] == 'disable' ) {
		$redirect = false;
	} else {
		$redirect = true;
	}

	//AUTH
	$errors = apply_filters( "stm_user_login", $errors, $username );

	if ( ! empty( $errors ) ) {
		$response['message'] = esc_html__( 'Please fill required fields', 'stm_vehicles_listing' );
		$response['errors']  = $errors;
		wp_send_json( $response );
		exit;
	}

	if ( filter_var( $username, FILTER_VALIDATE_EMAIL ) ) {
		$user = get_user_by( 'email', $username );
	} else {
		$user = get_user_by( 'login', $username );
	}

	if ( $user ) {
		$username = $user->user_login;
	}

	$creds                  = array();
	$creds['user_login']    = $username;
	$creds['user_password'] = $password;
	$creds['remember']      = $remember;
	$secure_cookie          = is_ssl() ? true : false;


	$user = wp_signon( $creds, $secure_cookie );
	if ( is_wp_error( $user ) ) {
		$response['message'] = esc_html__( 'Wrong Username or Password', 'stm_vehicles_listing' );
		wp_send_json( $response );
		exit;
	}

	$response['user'] = $user;

	if ( $redirect ) {
		$response['message'] = esc_html__( 'Successfully logged in. Redirecting...', 'stm_vehicles_listing' );

		$wpmlUrl = ( ! empty( $redirect_path ) ) ? $redirect_path : stm_account_url();

		if ( function_exists( 'icl_object_id' ) && isset( $_POST['current_lang'] ) ) {
			$wpmlUrl = apply_filters( 'wpml_permalink', $wpmlUrl, $_POST['current_lang'], true );
		}

		$response['redirect_url'] = $wpmlUrl;
	} else {
		ob_start();
		stm_add_a_car_user_info( '', '', '', $user->ID );
		$restricted   = false;
		$restrictions = stm_get_post_limits( $user->ID );

		if ( $restrictions['posts'] < 1 ) {
			$restricted = true;
		}

		$response['restricted'] = $restricted;
		$response['user_html']  = ob_get_clean();
	}

	$response['token'] = stm_hybrid_user_token( $user );

	wp_send_json( $response );
	exit;
}

remove_action('wp_ajax_stm_custom_login', 'stm_custom_login');
remove_action('wp_ajax_nopriv_stm_custom_login', 'stm_custom_login');
add_action('wp_ajax_stm_custom_login', 'stm_custom_login_alt');
add_action('wp_ajax_nopriv_stm_custom_login', 'stm_custom_login_alt');


// Remove the WordPress Admin
if ( ! current_user_can( 'manage_options' ) ) {
	add_filter( 'show_admin_bar', '__return_false' );
}

// Replace user login to email in social login
add_filter('wsl_hook_process_login_delegate_wp_insert_user', 'stm_process_social_login_email_login',100,3);
function stm_process_social_login_email_login($userdata, $provider, $hybridauth_user_profile){
	$userdata['user_login'] = $userdata['user_email'];
	return $userdata;
}


// Enable linked social login with axists user
add_filter('wsl_hook_process_login_delegate_wp_insert_user', 'stm_linked_exists_user_login',1000);
function stm_linked_exists_user_login($userdata){
	if(!empty($_REQUEST['action']) && $_REQUEST['action'] == 'wordpress_social_account_linking'){
		if(!empty($_REQUEST['user_login']) && !empty($_REQUEST['user_password'])){
			$user = get_user_by( 'login', $_REQUEST['user_login'] );
			if ( $user && wp_check_password( $_REQUEST['user_password'], $user->data->user_pass, $user->ID) ){
				return $user->ID;
			}
		}
	}
	return $userdata;
}

// Redirect from auth page to profile if user authrized
add_action( 'template_redirect', 'redirect_from_authpage' );
function redirect_from_authpage() {
	if ( is_user_logged_in() ) {
		$authpage    = (int) get_theme_mod( 'login_page' );
		if ( get_the_id() == $authpage ) {
			wp_redirect( stm_account_url(), 302 );
		}
	}
}

<?php

/**
 * Check if given user has a role
 *
 * @param integer|WP_User $user_id
 * @param string $role
 *
 * @return bool
 */
function stm_is_user_role( $user_id, $role ) {
	if ( is_object( $user_id ) ) {
		$user = $user_id;
	} elseif ( $user_id ) {
		$user = get_current_user_id() == $user_id ? wp_get_current_user() : get_userdata( $user_id );
	} elseif ( is_user_logged_in() ) {
		$user = wp_get_current_user();
	} else {
		return false;
	}

	if ( in_array( $role, (array) $user->roles ) ) {
		return true;
	}

	return false;
}

/**
 * Is user is dealer
 * @param integer|WP_User $user_id
 *
 * @return bool
 */
function stm_is_dealer( $user_id = null ) {
	return stm_is_user_role( $user_id, 'stm_dealer' );
}

/**
 * Overrides parent theme function
 * @param integer|WP_User $user_id
 *
 * @return bool
 */
function stm_get_user_role( $user_id = null ) {
	return stm_is_user_role( $user_id, 'stm_dealer' );
}

/**
 * Slug of default account page
 *
 * @return string
 */
function stm_account_default_page() {
	return 'inventory';
}

/**
 * Check if we are in my account page
 *
 * @param string $page Optionally check for specific account page
 *
 * @return bool
 */
function is_my_account( $page = '' ) {
	global $wp;
	if(!empty($wp->query_vars['author_name'])){
		$user = get_userdata(get_current_user_id());
		if($user->user_nicename == $wp->query_vars['author_name']){
			return $page ? $wp->query_vars['my_account'] == $page : true;
		}
	}

	if ( ! isset( $wp->query_vars['my_account'] ) ) {
		return false;
	}

	return $page ? $wp->query_vars['my_account'] == $page : true;
}

/**
 * Get current user's private account URL
 *
 * @param string $page Specific account page
 *
 * @return string
 */
function stm_account_url( $page = '' ) {
	return site_url( 'my-account/' . ( $page ? $page . '/' : '' ) );
}

function stm_hybrid_thumb_url() {
	if(defined('LISTING_URL')) $url = LISTING_URL;
	else $url = "//listing.{$_SERVER['HTTP_HOST']}/";
	$default_url = $url . 'thumbnails/get.php';
	return get_option( 'listing_thumb_url', $default_url );
}

/**
 * Detects imgix URL from options
 *
 * @return mixed|null
 */
function stm_hybrid_imgix_url() {
	if ( defined( 'LISTING_IMGIX_URL' ) ) {
		return LISTING_IMGIX_URL;
	}

	return get_option( 'listing_imgix_url' );
}

/**
 * Detects Thumbor URL from options
 *
 * @return string|null
 */
function stm_hybrid_thumbor_url() {
	if ( defined( 'LISTING_THUMBOR_URL' ) ) {
		return LISTING_THUMBOR_URL;
	}

	return get_option( 'listing_thumbor_url' );
}

function stm_hybrid_image_size( $image, $width, $height, $crop = false ) {
	if ( $thumbor_url = stm_hybrid_thumbor_url() ) {
		return sprintf(
			"%s/unsafe/%s%s/%s",
			rtrim($thumbor_url, '/'),
			$crop ? '' : 'fit-in/',
			$width . 'x' . $height,
			$image['directory'] . '/' . $image['filename']
		);
	}

	if ( $imgix_url = stm_hybrid_imgix_url() ) {
		return sprintf(
			"%s/%s/%s?auto=format&w=%s&h=%s%s",
			$imgix_url,
			$image['directory'],
			$image['filename'],
			$width,
			$height,
			$crop ? '&fit=crop' : ''
		);
	}

	return stm_hybrid_thumb_url() . "?src=/{$image['directory']}/{$image['filename']}&w={$width}&h={$height}&zc={$crop}&q=90";
}

function stm_hybrid_similar( $listing ) {
	return is_array( $listing->similar ) ? $listing->similar : [];
}

/**
 * Helper function to print image with lazy load if supported
 *
 * @param string $src Image source URL
 * @param array $attrs Attributes such as 'class', 'title'
 * @param bool $echo Echo right away
 *
 * @return string
 */
function stm_lazy_image( $src, $attrs = [], $echo = true ) {
	$attrs     = [ 'src' => $src ] + $attrs + [ 'placeholder' => null, 'class' => '' ];
	$attrs_str = '';
	$a3        = class_exists( 'A3_Lazy_Load' );

	if ( $a3 ) {
		$attrs = A3_Lazy_Load::get_attachment_image_attributes( $attrs );

		if ( $attrs['placeholder'] ) {
			$attrs['src'] = $attrs['placeholder'];
			unset( $attrs['placeholder'] );
		}
	}

	foreach ( $attrs as $key => $val ) {
		$attrs_str .= ' ' . $key . '="' . esc_attr( $val ) . '"';
	}

	$html = "<img$attrs_str>";

	if ( $echo ) {
		echo $html;

		return '';
	}

	return $html;
}

function stm_hybrid_get_taxes( $args = false ) {
	$ids = [];
	if ( ! empty( $args['body'] ) ) {
		$ids[] = $args['body'];
	}
	if ( ! empty( $args['make'] ) ) {
		$ids[] = $args['make'];
	}
	if ( ! empty( $args['serie'] ) ) {
		$ids[] = $args['serie'];
	}
	if ( ! empty( $args['countries'] ) ) {
		$ids[] = $args['countries'];
	}
	if ( ! empty( $args['towns'] ) ) {
		$ids[] = $args['towns'];
	}

	$tax  = \Stm_Hybrid\Api::make()->get( 'terms/taxterm?taxonomies=' . implode( ',', $ids ) );
	$taxs = $tax->json();

	return $taxs;
}

function stm_hybrid_listing_title( $args = false ) {
	$title = $locations = [];
	$taxs  = stm_hybrid_get_taxes( $args );

	if ( is_array( $taxs['data'] ) && count( $taxs['data'] ) ) {
		foreach ( $taxs['data'] as $tax ) {

			if ( ! empty( $args['body'] ) && $tax['taxonomy'] == 'body' ) {
				$title[0] = $tax['title'];
			}
			if ( ! empty( $args['make'] ) && $tax['taxonomy'] == 'make' ) {
				$title[1] = $tax['title'];
			}
			if ( ! empty( $args['serie'] ) && $tax['taxonomy'] == 'serie' ) {
				$title[2] = $tax['title'];
			}
			if ( ! empty( $args['countries'] ) && $tax['taxonomy'] == 'countries' ) {
				$locations[0] = $tax['title'];
			}
			if ( ! empty( $args['towns'] ) && $tax['taxonomy'] == 'towns' ) {
				$locations[1] = $tax['title'];
			}
		}
		ksort( $title );
		ksort( $locations );
	}


	$res_title = implode( ' ', $title );
	$res_title .= ' për shitje';

	if ( count( $locations ) ) {
		$res_title .= ' në ' . implode( ', ', $locations );
	}
	if ( count( $title ) ) {
		return apply_filters('stm_hybrid_listing_title', $res_title);
	} else {
		return apply_filters('stm_hybrid_listing_title', false);
	}
}

function stm_hybrid_listing_desc( $args = false ) {
	$desc = '';
	$taxs = stm_hybrid_get_taxes( $args );
	if ( is_array( $taxs['data'] ) && count( $taxs['data'] ) ) {
		foreach ( $taxs['data'] as $tax ) {
			if ( ! empty( $tax['description'] ) ) {
				$desc = stm_hybrid_truncate( htmlspecialchars_decode( strip_tags( $tax['description'] ) ), 200 );
			}
		}
	}

	return apply_filters('stm_hybrid_listing_desc', $desc);
}

add_filter( 'wpseo_title', 'stm_hybrid_yost_seo_title' );
function stm_hybrid_yost_seo_title( $title ) {
	global $landing;
	if ( is_array( $landing ) && count( $landing ) ) {
		$t = stm_hybrid_listing_title( $landing );
		if ( $t ) {
			return $t;
		}
	}

	return $title;
}

function stm_hybrid_truncate( $string, $limit, $break = ".", $pad = "..." ) {
	// return with no change if string is shorter than $limit
	if ( strlen( $string ) <= $limit ) {
		return $string;
	}

	// is $break present between $limit and the end of the string?
	if ( false !== ( $breakpoint = strpos( $string, $break, $limit ) ) ) {
		if ( $breakpoint < strlen( $string ) - 1 ) {
			$string = substr( $string, 0, $breakpoint ) . $pad;
		}
	}

	return $string;
}


add_filter('stm_filter_user_restrictions', 'stm_filter_user_restrictions_hybrid',100,3);
function stm_filter_user_restrictions_hybrid($restrictions, $user_id, $type) {
	$user_id = intval($user_id);

	$restrictions = array(
		'posts' => 0,
		'images' => 0,
	);

	if (!empty($user_id)) {

		$created_posts = 0;

		$post_status = array('publish', 'pending', 'draft');
//
//		$args = array(
//			'post_type' => 'listings',
//			'post_status' => $post_status,
//			'posts_per_page' => -1,
//			'meta_query' => array(
//				'relation' => 'AND',
//				array(
//					'key' => 'stm_car_user',
//					'value' => $user_id,
//					'compare' => '='
//				),
//				array(
//					'key' => 'pay_per_listing',
//					'compare' => 'NOT EXISTS',
//					'value' => ''
//				)
//			)
//		);
//
//		$query = new WP_Query($args);

		$res = \Stm_Hybrid\Api::make()->get(
			'listings/',
			[
				'author_id' => $user_id,
				'status' => $post_status,

			],
			[]
		);
		$query = json_decode( $res->get_data(), true );

		if (count($query)) {
			$created_posts = count($query);
		}


		$posts_allowed = get_theme_mod('user_post_limit', '3');
		$restrictions['posts_allowed'] = intval($posts_allowed);
		$restrictions['premoderation'] = get_theme_mod('user_premoderation', true);
		$restrictions['images'] = get_theme_mod('user_post_images_limit', '5');
		$restrictions['role'] = 'user';

		$user_can_create = intval($posts_allowed) - intval($created_posts);

		if ($user_can_create < 1) {
			$user_can_create = 0;
		}

		$restrictions['posts'] = intval($user_can_create);
	} else {
		$restrictions['premoderation'] = get_theme_mod('user_premoderation', true);
		$restrictions['posts'] = get_theme_mod('user_post_limit', '3');
		$restrictions['images'] = get_theme_mod('user_post_images_limit', '5');
		$restrictions['role'] = 'user';
	}

	/*IF is admin, set all */
	if (user_can($user_id, 'manage_options')) {
		$restrictions['premoderation'] = false;
		$restrictions['posts'] = '9999';
		$restrictions['images'] = '9999';
		$restrictions['role'] = 'user';
	}
	return $restrictions;
}


function getSubscriptionsWithoutListing() {
	$user_subscriptions =
		(class_exists("Subscriptio_User")) ?
			Subscriptio_User::find_subscriptions( true, get_current_user_id() ) :
			false;
	$subscriptions = [];
	foreach ($user_subscriptions as $user_subscription) {
		$order = wc_get_order($user_subscription->last_order_id);
		if($order){
			$items = $order->get_items();
			$t = false;
			foreach ( $items as $item_id => $item ) {
				$lid = wc_get_order_item_meta( $item_id, 'Listing ID' );
				if($lid) $t = true;
			}
			if(!$t) $subscriptions[] = $user_subscription;
		}
	}
	return $subscriptions;
}

<?php

function stm_ajax_get_seller_phone_ext()
{
	$user_added_by = get_post_meta($_GET["phone_owner_id"], 'stm_car_user', true);
	if (empty($user_added_by)) {
		$user_fields = stm_get_user_custom_fields($_GET["phone_owner_id"]);
	} else {
		$user_fields = stm_get_user_custom_fields($user_added_by);
	}


	wp_send_json($user_fields['phone']);
	exit;
}

remove_action('wp_ajax_stm_ajax_get_seller_phone', 'stm_ajax_get_seller_phone');
remove_action('wp_ajax_nopriv_stm_ajax_get_seller_phone', 'stm_ajax_get_seller_phone');

add_action('wp_ajax_stm_ajax_get_seller_phone', 'stm_ajax_get_seller_phone_ext');
add_action('wp_ajax_nopriv_stm_ajax_get_seller_phone', 'stm_ajax_get_seller_phone_ext');


/////// show number on dealer page
$showNumber = get_theme_mod("stm_show_number", false);
if (!function_exists('stm_get_single_dealer_custom')) {
	function stm_get_single_dealer_custom($dealer_info, $taxonomy = '')
	{

		$dealer_cars_count = $dealer_info['cars_count'];
		$cars_count_text = esc_html__('Cars in stock', 'motors');
		if ($dealer_cars_count == 1) {
			$cars_count_text = esc_html__('Car in stock', 'motors');
		}

		if (!empty($taxonomy)) {
			$taxonomy = $taxonomy;
		} elseif (!empty($_GET['stm_dealer_show_taxonomies'])) {
			$taxonomy = sanitize_text_field($_GET['stm_dealer_show_taxonomies']);
		} else {
			$taxonomy = '';
		}


		$taxonomy = array_filter(explode(', ', $taxonomy));
		$ratings = $dealer_info['ratings'];
		$tax_term = array();
		if (!empty($taxonomy)) {
			foreach ($taxonomy as $tax) {
				$term_tax = explode(' | ', $tax);
				$tax_term[$term_tax[0]] = sanitize_title($term_tax[1]);
			}
		}

		$dealer_category_labels = array();

		$dealer_cars = $dealer_info['cars'];
		if ($dealer_cars->have_posts()) {
			while ($dealer_cars->have_posts()) {
				$dealer_cars->the_post();
				foreach ($tax_term as $term => $tax) {
					$terms_all = wp_get_object_terms(get_the_ID(), $tax);
					if (!empty($terms_all)) {
						foreach ($terms_all as $term_single) {
							if ($term_single->slug == $term) {
								$dealer_category_labels[] = $term_single->name;
							}
						}
					}
				}
			}
		}

		wp_reset_postdata();

		$dealer_category_labels = array_unique($dealer_category_labels);

		?>
		<tr class="stm-single-dealer animated fadeIn">

			<td class="image">
				<a href="<?php echo esc_url(stm_get_author_link($dealer_info['id'])); ?>" target="_blank">
					<?php if (!empty($dealer_info['fields']['logo'])): ?>
						<img src="<?php echo esc_url($dealer_info['fields']['logo']); ?>" class="img-responsive"/>
					<?php else: ?>
						<img src="<?php stm_get_dealer_logo_placeholder(); ?>" class="no-logo"/>
					<?php endif; ?>
				</a>
			</td>

			<td class="dealer-info">
				<div class="title">
					<a class="h4" href="<?php echo esc_url(stm_get_author_link($dealer_info['id'])); ?>"
					   target="_blank"><?php stm_display_user_name($dealer_info['id']); ?></a>
				</div>
				<div class="rating">
					<div class="dealer-rating">
						<div class="stm-rate-unit">
							<div class="stm-rate-inner">
								<div class="stm-rate-not-filled"></div>
								<?php if (!empty($ratings['average_width'])): ?>
									<div class="stm-rate-filled"
										 style="width:<?php echo esc_attr($ratings['average_width']); ?>"></div>
								<?php else: ?>
									<div class="stm-rate-filled" style="width:0%"></div>
								<?php endif; ?>
							</div>
						</div>
						<div class="stm-rate-sum">
							(<?php esc_html_e('Reviews', 'motors'); ?> <?php echo esc_attr($ratings['count']); ?>)
						</div>
					</div>
				</div>
			</td>

			<td class="dealer-cars">
				<div class="inner">
					<a href="<?php echo esc_url(stm_get_author_link($dealer_info['id'])); ?>#stm_d_inv" target="_blank">
						<div class="dealer-labels heading-font">
							<?php echo intval($dealer_cars_count); ?>
							<?php if (!empty($dealer_category_labels)):
								echo esc_attr(implode('/', $dealer_category_labels));
							endif; ?>
						</div>
						<div class="dealer-cars-count">
							<i class="stm-service-icon-body_type"></i>
							<?php echo esc_attr($cars_count_text); ?>
						</div>
					</a>
				</div>
			</td>

			<td class="dealer-phone">
				<div class="inner">
					<?php if (!empty($dealer_info['fields']['phone'])): ?>
						<!-- <div class="phone heading-font">
								<i class="stm-service-icon-phone_2"></i>
								<?php /* echo esc_attr($dealer_info['fields']['phone']); */ ?>
							</div> -->
						<i class="stm-service-icon-phone_2"></i>
						<?php if ($showNumber) : ?>
							<div class="phone heading-font"><?php echo $dealer_info['fields']['phone']; ?></div>
						<?php else : ?>
							<div class="phone heading-font"><?php echo substr_replace($dealer_info['fields']['phone'], "*******", 3, strlen($dealer_info['fields']['phone'])); ?></div>
							<span class="stm-show-number_custom"
								  data-id="<?php echo $dealer_info['fields']['phone']; ?>"><?php echo esc_html__("Show number", "motors"); ?></span>
							<?php /*<div class="phone-label"><?php esc_html_e('Call Free 24/7', 'motors'); ?></div>*/ ?>
						<?php endif; ?>
					<?php endif; ?>
				</div>
			</td>


			<td class="dealer-location">
				<div class="clearfix">
					<?php if (!empty($dealer_info['fields']['location']) and !empty($dealer_info['fields']['location_lat']) and !empty($dealer_info['fields']['location_lng'])): ?>
						<a rel="nofollow"
						   href="https://maps.google.com?q=<?php echo esc_attr($dealer_info['fields']['location']); ?>"
						   target="_blank"
						   class="map_link"
						>
							<i class="fa fa-external-link"></i>
							<?php esc_html_e('See map', 'motors'); ?>
						</a>
					<?php endif; ?>
					<div class="dealer-location-label">
						<?php if (!empty($dealer_info['fields']['distance'])): ?>
							<div class="inner">
								<i class="stm-service-icon-pin_big"></i>
								<span class="heading-font"><?php echo esc_attr($dealer_info['fields']['distance']); ?></span>
								<?php if (!empty($dealer_info['fields']['user_location'])): ?>
									<div class="stm-label"><?php esc_html_e('From', 'motors');
										echo ' ' . $dealer_info['fields']['user_location']; ?></div>
								<?php endif; ?>
							</div>
						<?php elseif (!empty($dealer_info['fields']['location'])): ?>
							<div class="inner">
								<i class="stm-service-icon-pin_big"></i>
								<span class="heading-font"><?php echo esc_attr($dealer_info['fields']['location']); ?></span>
							</div>
						<?php else: ?>
							<?php esc_html_e('N/A', 'motors'); ?>
						<?php endif; ?>
					</div>
				</div>
			</td>

		</tr>
		<tr class="dealer-single-divider">
			<td colspan="5"></td>
		</tr>
		<?php
	}
}


function stm_get_currency_prop($post_id)
{
	$stm_currency = get_post_meta($post_id, 'stm_currency', true);
	$multipleCurrencyList = json_decode(get_theme_mod("currency_list", ""));
	$currencies_name = explode(',', $multipleCurrencyList->currency);
	$currencies_value = explode(',', $multipleCurrencyList->to);

	foreach ($currencies_name as $key => $value) {
		if ($value == $stm_currency) {
			return $currencies_value[$key];
		}
	}
	return false;
}


add_action('wp', function () {
	if ($GLOBALS['wp_query']->get('post_type') == 'listings' && empty($_GET['max_search_radius'])) {
		$max = get_theme_mod("distance_search");
		$_GET['max_search_radius'] = $max ? $max : 100;
	}
});





add_action('wp_footer', 'share_modal', 99);
function share_modal()
{
	get_template_part('partials/modals/share', 'compare');
	get_template_part('partials/modals/share', 'contactform');
	get_template_part('partials/modals/trade-in');
	get_template_part('partials/modals/auction');

}


function stm_send_cf7_message_url($wpcf)
{
	if (!empty($_POST['stm_changed_aircraft'])) {

		$post_id = $_POST['stm_changed_aircraft'];
		$url = get_permalink($post_id);
		$title = get_the_title($post_id);
		$mail = $wpcf->prop('mail');
		$mail['body'] = str_replace('[_post_title]', $title, $mail['body']);
		$mail['body'] = str_replace('[_post_url]', $url, $mail['body']);
		$wpcf->set_properties(array('mail' => $mail));


	}

	return $wpcf;
}

add_action("wpcf7_before_send_mail", "stm_send_cf7_message_url", 8, 1);


add_filter('stm_listing_saved_email_body', function ($body, $post_id, $updating) {
	$user_id = get_post_meta($post_id, 'stm_car_user', true);
	$user = new WP_User($user_id);
	$title = get_the_title($post_id);

	if ($updating) {
		$body = esc_html__('User updated aircraft.', 'motors-child');
	} else {
		$body = esc_html__('User added aircraft.', 'stm_vehicles_listing');
	}

	$body .= ' ' . sprintf(
			esc_html__('User ID: %s, Username: %s, Aircraft ID: %s, Aircraft title: %s ', 'motors-child'),
			$user->ID,
			$user->user_email,
			$post_id,
			$title
		);

	return $body;
}, 10, 3);


function stm_sort_ratings_dealers($a, $b)
{
	if (floatval($a['ratings']['average']) == floatval($b['ratings']['average'])) {
		return 0;
	}
	return (floatval($a['ratings']['average']) < floatval($b['ratings']['average'])) ? 1 : -1;
}

add_filter('stm_filter_price_view', function ($html, $price) {
	if (empty($price)) {
		return $html;
	}

	$price_label = stm_get_price_currency();
	$custom_label = get_post_meta(get_the_ID(), 'air_currency', true);
	if (!empty($custom_label)) {
		$price_label = $custom_label;
	}
	$price_label_position = get_theme_mod('price_currency_position', 'left');
	$price_delimeter = get_theme_mod('price_delimeter', ' ');


	if ($price_label_position == 'left') {
		$response = ($price === '0') ? $price_label . '0' : $price_label . number_format(getConverPrice($price), 0, '', $price_delimeter);
	} else {
		$response = ($price === '0') ? '0' . $price_label : number_format(getConverPrice($price), 0, '', $price_delimeter) . $price_label;
	}

	return $response;
}, 10, 2);

if (!function_exists('stm_listing_price_view_custom')) {
	function stm_listing_price_view_custom($price, $post_id)
	{
		return stm_listing_price_view($price);
	}
}


function air_get_related_listings($source = [])
{
	$terms = get_field('aircraft_categories');

	if (empty($terms)) {
		return null;
	}

	$request = array_merge([
		'related' => [
			'types' => $terms,
		],
	], $source);

	return \Stm_Hybrid\Listing::query($request);
}


add_filter('theme_mod_show_offer_price', '__return_true');


// LIKE SEARCH FOR MODEL TAXONOMY
add_filter('stm_listings_build_query_args', function ($args) {
	if (isset($args['tax_query'])) {
		foreach ($args['tax_query'] as $key => $tax_query) {
			if ($tax_query['taxonomy'] == 'model') {
				$termIds = array_map(function (\WP_Term $term) {
					return $term->term_id;
				}, get_terms([
					'name__like' => $_REQUEST['model'],
				]));
				$args['tax_query'][$key]['field'] = 'id';
				$args['tax_query'][$key]['terms'] = $termIds;
			}
		}
	}
	return $args;
});


function send_profile_email()
{
	# Verify captcha
	$data = array();
	// //
	// $recaptcha_secret_key = get_theme_mod('recaptcha_secret_key');
	// $post_data = http_build_query(
	//     array(
	//         'secret' => $recaptcha_secret_key,
	//         'response' => (!empty($_POST['g-recaptcha-response']) ? $_POST['g-recaptcha-response'] : ''),
	//         'remoteip' => $_SERVER['REMOTE_ADDR']
	//     )
	// );
	// $opts = array('http' =>
	//     array(
	//         'method'  => 'POST',
	//         'header'  => 'Content-type: application/x-www-form-urlencoded',
	//         'content' => $post_data
	//     )
	// );
	// $context  = stream_context_create($opts);
	// $response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
	// $result = json_decode($response);
	// if (!$result->success) {
	// 	$data['messege'] = 'Error!';
	// }
	// else {
	if (!empty($_POST['email']) && !empty($_POST['body-text'])) {
		$email = $_POST['email'];
		$text = $_POST['body-text'];

		if (!empty($_POST['sender-phone'])) {
			$sender_phone = $_POST['sender-phone'];
			$text .= '<br />' . $sender_phone;
		}
		$sender_email = $_POST['sender-email'];
		$sender_f_name = $_POST['sender-f-name'];
		$sender_l_name = $_POST['sender-l-name'];
		$text .= '<br />' . $sender_email . '<br />' . $sender_f_name . '<br />' . $sender_l_name;
		wp_mail($email, 'Profile', $text, 'Content-Type: text/html');
		$data['messege'] = 'Success!';
	} else {
		$data['messege'] = 'Error!';
	}
	$data = json_encode($data);
	// }
	wp_die($data);
}

add_action('wp_ajax_send_profile_email', 'send_profile_email');


add_action('wp_ajax_stm_hybrid_get_current_user_token', 'stm_hybrid_get_current_user_token');
add_action('wp_ajax_nopriv_stm_hybrid_get_current_user_token', 'stm_hybrid_get_current_user_token');

function stm_hybrid_get_current_user_token()
{
	$user = wp_get_current_user();
	$token = false;
	if ($user) {
		$token = stm_hybrid_user_token($user);
	}

	wp_send_json_success($token);
}

add_action('wp', 'stm_hybrid_remove_disabled_or_deleted_from_compare');

function stm_hybrid_remove_disabled_or_deleted_from_compare () {
	$compare_page = (int) get_theme_mod('compare_page', 156);
	$queried_id = get_queried_object_id();
	if ($compare_page !== $queried_id) return;

	$compare_ids = $_COOKIE['compare_ids'];

	if(!$compare_ids) return;
	foreach ($compare_ids as $key => $listing_id) {
		$listing = \Stm_Hybrid\Listing::find($listing_id);

		if ($listing) {
			$listing_available = $listing->status === 'publish';
		} else {
			$listing_available = false;
		}

		if (!$listing_available) {
			setcookie("compare_ids[$key]", '', 0, '/');
			unset ($_COOKIE['compare_ids'][$key]);
		}
	}
}


//add_action('wpseo_head', 'stm_hybrid_listing_og_image', 50);
add_action('wpseo_head', 'stm_hybrid_listing_twitter_image', 55);
add_action('wpseo_metadesc', 'stm_hybrid_listing_meta_description', 10);
add_filter('wpseo_title', 'stm_hybrid_listing_meta_title');

add_filter('wpseo_opengraph_image', 'stm_hybrid_listing_og_image', 10, 1);
function stm_hybrid_listing_og_image ($og_image) {
	$queried_object = get_queried_object();
	if (!$queried_object || $queried_object->post_type !== 'listings') return $og_image;
	$listing = listing($queried_object->ID);
	if (!empty($listing->gallery)) {
		$og_image = $listing->gallery[0]['url'];
	}
	return $og_image;
}

function stm_hybrid_listing_twitter_image () {
	$queried_object = get_queried_object();
	if (!$queried_object || $queried_object->post_type !== 'listings') return;
	$listing = listing($queried_object->ID);
	if (!empty($listing->gallery)) {
		$image_url = $listing->gallery[0]['url'];
		echo '<meta name="twitter:image" content="'. $image_url .'">';
	}
}

function stm_hybrid_listing_meta_description ($description) {
	global $listing;
	$queried_object = get_queried_object();
	if ($queried_object && $queried_object->post_type === stm_listings_post_type()) {
		if (!empty($listing->meta_description)) {
			$description = $listing->meta_description;
		} else {
			$description = false;
		}
	}

	return $description;
}

function stm_hybrid_listing_meta_title($title) {
	global $listing;

	$obj = get_queried_object();
	if ($obj && $obj->post_type === stm_listings_post_type()) {
		if (empty($listing->meta_title)) {
			$title = $listing->title;
		} else {
			$title = $listing->meta_title;
		}
	}

	return $title;
}


function stm_hybrid_customize_add ($wp_customize) {
	/**
	 * @var $wp_customize WP_Customize_Manager
	 */
	$wp_customize->add_setting('filter_default_location', array(
		'type' => 'option',
	));

	$wp_customize->add_setting('filter_default_search_radius', array(
		'type' => 'option',
	));

	$wp_customize->add_control('filter_default_location_control', array(
		'label' => __('Default location in filter', 'motors'),
		'section' => 'listing_features',
		'type' => 'text',
		'settings' => 'filter_default_location'
	));

	$wp_customize->add_control('filter_default_search_radius_control', array(
		'label' => __('Default search radius in filter', 'motors'),
		'section' => 'listing_features',
		'type' => 'number',
		'settings' => 'filter_default_search_radius'
	));


	$wp_customize->add_setting('show_emails_global', array(
		'type' => 'option',
	));

	$wp_customize->add_control('show_emails_global', array(
		'label' => __('Show Emails in Frontend', 'motors'),
		'section' => 'listing_features',
		'type' => 'checkbox',
		'settings' => 'show_emails_global'
	));
}

add_action( 'customize_register', 'stm_hybrid_customize_add' );

add_action('customize_save_after', 'stm_hybrid_customize_check_default_filter_location', 50, 1);

function stm_hybrid_customize_check_default_filter_location() {
	$location = get_option('filter_default_location', false);

	if (empty($location)) {
		update_option('filter_default_location_latlng', '');
	}
}

add_action('wp_ajax_save_filter_default_location_latlng', 'stm_hybrid_save_filter_default_location_latlng');
add_action('wp_ajax_nopriv_save_filter_default_location_latlng', 'stm_hybrid_save_filter_default_location_latlng');

function stm_hybrid_save_filter_default_location_latlng() {
	if (!empty($_POST['lat']) && !empty($_POST['lng'])) {
		$lat = $_POST['lat'];
		$lng = $_POST['lng'];

		update_option('filter_default_location_latlng', $lat . ',' . $lng);
	}
}


if(!function_exists("getCurrencySelectorHtml")) {
	function getCurrencySelectorHtml() {
		$multipleCurrencyList = json_decode(get_theme_mod("currency_list", ""));
		if($multipleCurrencyList != null) {
			$currentCurrency = "";
			if(isset($_COOKIE["stm_current_currency"])){
				$mc = explode("-", $_COOKIE["stm_current_currency"]);
				$currentCurrency = $mc[0];
			}

			$currency[0] = get_theme_mod("price_currency_name", "USD");
			if($multipleCurrencyList->currency != null) $currency = array_merge($currency, explode(",", $multipleCurrencyList->currency));

			$symbol[0] = get_theme_mod("price_currency", "$");
			if($multipleCurrencyList->symbol != null) $symbol = array_merge($symbol, explode(",", $multipleCurrencyList->symbol));

			$to[0] = "1";
			$to = array_merge($to, explode(",", $multipleCurrencyList->to));

			$selectHtml = '<div class="pull-left currency-switcher">';
			$selectHtml .= "<div class='stm-multiple-currency-wrap'><select data-translate='" . esc_html__("Currency (%s)", "motors") . "' data-class='stm-multi-currency' name='stm-multi-currency'>";
			for($q=0;$q<count($currency);$q++) {
				$selected = ($symbol[$q] == $currentCurrency) ? "selected" : "";
				$val = html_entity_decode($symbol[$q]) . "-" . $to[$q];
				$currencyTitle = $currency[$q];

				if(!isset($_COOKIE["stm_current_currency"]) && $q == 0 || !empty($selected)) {
					$currencyTitle = sprintf(esc_html__("Currency (%s)", "motors"), $currency[$q]);
				}

				$selectHtml .= "<option value='{$val}' " . $selected . ">{$currencyTitle}</option>";
			}
			$selectHtml .= "</select></div>";
			$selectHtml .= '</div>';

			if(count($currency) > 1) {
				echo $selectHtml;
			}
		}
	}
}



//Function add to favourites
function stm_ajax_add_to_favourites_alt(){
	$response = array();
	$count = 0;

	if (!empty($_POST['car_id'])) {
		$car_id = intval($_POST['car_id']);
		$post_status = get_post_status($car_id);

		if (!$post_status) {
			$post_status = 'deleted';
		}

		if (is_user_logged_in() and $post_status == 'publish' or $post_status == 'pending' or $post_status == 'draft' or $post_status == 'deleted') {
			$user = wp_get_current_user();
			$user_id = $user->ID;
			$user_added_fav = get_the_author_meta('stm_user_favourites', $user_id);
			if (empty($user_added_fav)) {
				update_user_meta($user_id, 'stm_user_favourites', $car_id);
			} else {
				$user_added_fav = array_filter(explode(',', $user_added_fav));
				$response['fil'] = $user_added_fav;
				$response['id'] = $car_id;
				if (in_array(strval($car_id), $user_added_fav)) {
					$user_added_fav = array_diff($user_added_fav, array($car_id));
				} else {
					$user_added_fav[] = $car_id;
				}
				$user_added_fav = implode(',', $user_added_fav);

				update_user_meta($user_id, 'stm_user_favourites', $user_added_fav);
			}

			$user_added_fav = get_the_author_meta('stm_user_favourites', $user_id);
			$user_added_fav = count(array_filter(explode(',', $user_added_fav)));
			$response['count'] = intval($user_added_fav);
		}
	}

	$response = json_encode($response);
	echo $response;
	exit;
}

remove_action('wp_ajax_stm_ajax_add_to_favourites', 'stm_ajax_add_to_favourites');
remove_action('wp_ajax_nopriv_stm_ajax_add_to_favourites', 'stm_ajax_add_to_favourites');
add_action('wp_ajax_stm_ajax_add_to_favourites', 'stm_ajax_add_to_favourites_alt');
add_action('wp_ajax_nopriv_stm_ajax_add_to_favourites', 'stm_ajax_add_to_favourites_alt');


add_action( 'stm_account_custom_page', 'add_navigation_page_manager_export' );
function add_navigation_page_manager_export($current){
	if($current == 'stock-list'){
		include(STM_SLE_PATH. '/templates/stock_list.php');
	}
}


function stm_display_user_name_alt($user_id, $user_login = '', $f_name = '', $l_name = ''){
	$user = get_userdata($user_id);

	if (empty($user_login)) {
		$login = $user->data->user_login;
	} else {
		$login = $user_login;
	}
	if (empty($f_name)) {
		$first_name = get_the_author_meta('first_name', $user_id);
	} else {
		$first_name = $f_name;
	}

	if (empty($l_name)) {
		$last_name = get_the_author_meta('last_name', $user_id);
	} else {
		$last_name = $l_name;
	}

	$display_name = $login;

	if (!empty($first_name)) {
		$display_name = $first_name;
	}

	if (!empty($first_name) and !empty($last_name)) {
		$display_name .= ' ' . $last_name;
	}

	if (empty($first_name) and !empty($last_name)) {
		$display_name = $last_name;
	}


	return apply_filters('stm_filter_display_user_name', $display_name, $user_id, $user_login, $f_name, $l_name);

}

// Is add a car page (used for custom placeholders)
function stm_is_add_car_page($pid = 0){
	if(!$pid) $pid = get_the_id();
	if(!$pid) return false;

	$add_car = (int)get_theme_mod('user_add_car_page', 1755);
	$pid = (int)$pid;
	if($add_car == $pid) return true;
	return false;
}



// Add Show/Hide email in Profile page
add_action( 'show_user_profile', 'stm_show_hide_email_user_profile_field' );
add_action( 'edit_user_profile', 'stm_show_hide_email_user_profile_field' );
function stm_show_hide_email_user_profile_field( $user ) { ?>
	<h3><?php _e("Email Privilegies", "motors"); ?></h3>

	<table class="form-table">
		<tr>
			<th><label for="show_hide_email"><?php _e("Show/Hide email in public area","motors"); ?></label></th>
			<td>
				<?php $show_hide_email = esc_attr( get_the_author_meta( 'show_hide_email', $user->ID ) ) ?>
				<input type="checkbox" <?php if(!empty($show_hide_email)) echo 'checked' ?> name="show_hide_email"><span><?php _e("Show","motors") ?></span>
			</td>
		</tr>
	</table>
<?php }

// Save field
add_action( 'personal_options_update', 'stm_show_hide_email_save_user_profile_field' );
add_action( 'edit_user_profile_update', 'stm_show_hide_email_save_user_profile_field' );
function stm_show_hide_email_save_user_profile_field( $user_id ) {
	if ( !current_user_can( 'edit_user', $user_id ) ) return false;
	if(isset($_POST['show_hide_email'])) update_user_meta( $user_id, 'show_hide_email', 1 );
	else update_user_meta( $user_id, 'show_hide_email', '' );
}


remove_action('wp_ajax_stm_ajax_add_trade_offer', 'stm_ajax_add_trade_offer');
remove_action('wp_ajax_nopriv_stm_ajax_add_trade_offer', 'stm_ajax_add_trade_offer');
add_action('wp_ajax_stm_ajax_add_trade_offer', 'stm_ajax_add_trade_offer_no_admin');
add_action('wp_ajax_nopriv_stm_ajax_add_trade_offer', 'stm_ajax_add_trade_offer_no_admin');
//Ajax request trade offer
function stm_ajax_add_trade_offer_no_admin()
{
	$response['errors'] = array();

	if (!filter_var($_POST['name'], FILTER_SANITIZE_STRING)) {
		$response['errors']['name'] = true;
	}
	if (!is_email($_POST['email'])) {
		$response['errors']['email'] = true;
	}
	if (!is_numeric($_POST['phone'])) {
		$response['errors']['phone'] = true;
	}
	if (!is_numeric($_POST['trade_price'])) {
		$response['errors']['trade_price'] = true;
	}

	$recaptcha = true;

	$recaptcha_enabled = get_theme_mod('enable_recaptcha', 0);
	$recaptcha_public_key = get_theme_mod('recaptcha_public_key');
	$recaptcha_secret_key = get_theme_mod('recaptcha_secret_key');
	if (!empty($recaptcha_enabled) and $recaptcha_enabled and !empty($recaptcha_public_key) and !empty($recaptcha_secret_key)) {
		$recaptcha = false;
		if (!empty($_POST['g-recaptcha-response'])) {
			$recaptcha = true;
		}
	}

	if ($recaptcha) {
		if (empty($response['errors']) and !empty($_POST['vehicle_id'])) {
			$response['response'] = esc_html__('Your request was sent', 'motors');
			$response['status'] = 'success';

			//Sending Mail to admin
			add_filter('wp_mail_content_type', 'stm_set_html_content_type');

			$to = get_bloginfo('admin_email');

			$args = array(
				'car' => get_the_title($_POST['vehicle_id']),
				'name' => $_POST['name'],
				'email' => $_POST['email'],
				'phone' => $_POST['phone'],
				'price' => intval($_POST['trade_price'])
			);

			$owner_id = listing($_POST['vehicle_id'])->author_id;
			$user_fields = stm_get_user_custom_fields($owner_id);

			$subject = generateSubjectView('trade_offer', $args);
			$body = generateTemplateView('trade_offer', $args);

			wp_mail($user_fields['email'], $subject, $body);

			remove_filter('wp_mail_content_type', 'stm_set_html_content_type');
		} else {
			$response['response'] = esc_html__('Please fill all fields', 'motors');
			$response['status'] = 'danger';
		}

		$response['recaptcha'] = true;
	} else {
		$response['recaptcha'] = false;
		$response['status'] = 'danger';
		$response['response'] = esc_html__('Please prove you\'re not a robot', 'motors');
	}


	$response = json_encode($response);

	echo $response;
	exit;
}


// Rewrite author base slug
add_action('customize_register', function(){
	STM_Customizer::setSection('user_settings', array(
		'title' => esc_html__('Users', 'motors'),
		'panel' => 'listing',
		'priority' => 200,
		'fields' => array(
			'author_base' => array(
				'label' => esc_html__('Author Base', 'motors'),
				'type' => 'text'
			),

		)
	));
});

add_action("customize_save_after","stm_update_rewrite_author_base");
function stm_update_rewrite_author_base(){
	flush_rewrite_rules();
}

add_action("customize_save_after","stm_update_customize_hybrid");
function stm_update_customize_hybrid() {
	$options = [
		'user_premoderation',
		'dealer_premoderation',
		'user_post_limit',
		'user_post_images_limit',
		'dealer_post_limit',
		'dealer_post_images_limit',
		'user_image_size_limit',
		'dealer_pay_per_listing',
		'dealer_payments_for_featured_listing',
		'allow_dealer_add_new_category',
		'dealer_review_moderation',
		'pay_per_listing_price',
		'pay_per_listing_period',
		'featured_listing_price',
		'featured_listing_period',
		'enable_plans'
	];
	$customize = [];
	foreach ($options as $option) {
		$customize[$option] = get_theme_mod($option);
	}
	$res = \Stm_Hybrid\Api::make()->post( 'settings/customize', ['customize' => $customize], [ 'auth' => stm_hybrid_api_auth() ] );
}


add_action("init","stm_rewrite_author_base",1);
function stm_rewrite_author_base(){
	$author_base = get_theme_mod( 'author_base', '' );
	if(!empty($author_base)){
		global $wp_rewrite;
		$wp_rewrite->author_base = $author_base;
	}

	$uri = explode('/',$_SERVER['REQUEST_URI']);
	if(!empty($author_base) && isset($uri[1]) && $uri[1] == "author"){
		$uri[1] = $author_base;

		$location = home_url().implode('/',$uri);
		wp_redirect( $location, 302 );
		exit;
	}
}


// Increase per page YostSEO
add_filter('wpseo_sitemap_entries_per_page', function(){return 10000;});


add_filter('wpseo_sitemap_index','listing_sitemap_links');
function listing_sitemap_links($var){
	$listing_url = str_replace('api', '', get_option( 'listing_base_url', 'http://listing.makina.stylemix.biz/api' ));
	$filename = '/home/listing.makina.al/public/sitemap_index.xml';
	if(file_exists()){
		$modified = date("c", filemtime($filename));
	}else{
		$modified = date('c', time());
	}
	return '
	<sitemap>
		<loc>'.$listing_url.'listing_index.xml</loc>
		<lastmod>'.$modified.'</lastmod>
	</sitemap>
	<sitemap>
		<loc>'.$listing_url.'categories_index.xml</loc>
		<lastmod>'.$modified.'</lastmod>
	</sitemap>
	';
}

add_action( 'wp_footer', function () {
	/**
	 * Check if Facebook Official Pixel is active
	 **/
	if ( in_array( 'official-facebook-pixel/facebook-for-wordpress.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
		// if active, then include track template in footer
		include( get_stylesheet_directory() . '/partials/footer/footer-tracks.php' );
	}
} );

add_filter( 'wpseo_title', function ($title) {
	if(get_post_type() == 'listings' && is_single()){
		if(!empty( $GLOBALS['listing']->seo['title'] )){
			return $GLOBALS['listing']->seo['title'];
		}
		if(!empty( $GLOBALS['listings'] )){
			return $GLOBALS['listings']->title;
		}
	}
	if(!empty($GLOBALS['listings']['title'])){
		return $GLOBALS['listings']['title'];
	}
	return $title;
} );

add_filter('stm_hybrid_listing_title', 'stm_seo_extension_listing');
function stm_seo_extension_listing( $title ){
	if(function_exists('listings') && !empty(listings('title'))) return listings('title');
	return $title;
}

add_filter('stm_hybrid_listing_desc', function( $title ){
	if(function_exists('listings') && !empty(listings('description'))) return listings('description');
	return $title;
});

function stm_generate_title_from_slugs_hybrid($listing, $show_labels = true) {
	$title_from = get_theme_mod( 'listing_directory_title_frontend', '' );
	$post_id = $listing->id;
	$terms = $listing->get_title;
	$title_return = '';
	if ( true ) {

		if ( !empty( $title_from ) and is_listing() || stm_is_car_dealer() || stm_is_dealer_two() || stm_is_aircrafts() ) {
			$title = stm_replace_curly_brackets( $title_from );
			$title_counter = 0;

			if ( !empty( $title ) ) {
				foreach ( $title as $title_part ) {
					$title_counter++;
					if ( $title_counter == 1 ) {
						if ( $show_labels ) {
							$title_return .= '<div class="labels">';
						}
					}

					if ( !empty( $terms ) ) {
						if ( !empty( $terms[$title_part] ) ) {
							if ( !empty( $terms[$title_part]['value'] ) ) {
								if ( $title_counter == 1 ) {
									$title_return .= $terms[$title_part]['value'];
								} else {
									$title_return .= ' ' . $terms[$title_part]['value'];
								}
							} else {
//								$number_affix = get_post_meta( $post_id, strtolower( $title_part ), true );
//								if ( !empty( $number_affix ) ) {
//									$title_return .= ' ' . $number_affix . ' ';
//								}
							}
						}
					} else {
//						$number_affix = get_post_meta( $post_id, strtolower( $title_part ), true );
//						if ( !empty( $number_affix ) ) {
//							$title_return .= ' ' . $number_affix . ' ';
//						}
					}
					if ( $show_labels and $title_counter == 2 ) {
						$title_return .= '</div>';
					}
				}
			}
		} elseif ( !empty( $title_from ) and stm_is_boats() ) {
			$title = stm_replace_curly_brackets( $title_from );

			if ( !empty( $title ) ) {
				foreach ( $title as $title_part ) {
					$value = get_post_meta( $post_id, $title_part, true );
					if ( !empty( $value ) ) {
						$cat = get_term_by( 'slug', $value, $title_part );
						if ( !is_wp_error( $cat ) and !empty( $cat->name ) ) {
							$title_return .= $cat->name . ' ';
						} else {
							$title_return .= $value . ' ';
						}
					}
				}
			}
		} elseif ( !empty( $title_from ) and stm_is_motorcycle()) {
			$title = stm_replace_curly_brackets( $title_from );

			$title_counter = 0;

			if ( !empty( $title ) ) {
				foreach ( $title as $title_part ) {
					$value = get_post_meta( $post_id, $title_part, true );
					$title_counter++;

					if ( !empty( $value ) ) {
						$cat = get_term_by( 'slug', $value, $title_part );
						if ( !is_wp_error( $cat ) and !empty( $cat->name ) ) {
							if ( $title_counter == 1 and $show_labels ) {
								$title_return .= '<span class="stm-label-title">';
							}
							$title_return .= $cat->name . ' ';
							if ( $title_counter == 1 and $show_labels ) {
								$title_return .= '</span>';
							}
						} else {
							if ( $title_counter == 1 and $show_labels ) {
								$title_return .= '<span class="stm-label-title">';
							}
							$title_return .= $value . ' ';
							if ( $title_counter == 1 and $show_labels ) {
								$title_return .= '</span>';
							}
						}
					}
				}
			}
		} elseif ( !empty( $title_from ) && stm_is_listing_three()) {
			$title = stm_replace_curly_brackets( $title_from );
			$title_counter = 0;

			if ( !empty( $title ) ) {
				foreach ( $title as $title_part ) {
					$title_counter++;
					if ( $title_counter == 1 ) {
						if ( $show_labels ) {
							$title_return .= '<div class="labels">';
						}
					}

					$term = wp_get_post_terms( $post_id, strtolower( $title_part ), array( 'orderby' => 'none' ) );
					if ( !empty( $term ) and !is_wp_error( $term ) ) {
						if ( !empty( $term[0] ) ) {
							if ( !empty( $term[0]->name ) ) {
								if ( $title_counter == 1 ) {
									$title_return .= $term[0]->name;
								} else {
									$title_return .= ' ' . $term[0]->name;
								}
							} else {
								$number_affix = get_post_meta( $post_id, strtolower( $title_part ), true );
								if ( !empty( $number_affix ) ) {
									$title_return .= ' ' . $number_affix . ' ';
								}
							}
						}
					} else {
						$number_affix = get_post_meta( $post_id, strtolower( $title_part ), true );
						if ( !empty( $number_affix ) ) {
							$title_return .= ' ' . $number_affix . ' ';
						}
					}
					if ( $show_labels and $title_counter == 2 ) {
						$title_return .= '</div>';
					}
				}
			}
		} elseif ( !empty( $title_from ) && stm_is_equipment()) {
			$title = stm_replace_curly_brackets( $title_from );
			$title_counter = 0;

			if ( !empty( $title ) ) {
				foreach ( $title as $title_part ) {
					$title_counter++;
					if ( $title_counter == 1 ) {
						if ( $show_labels ) {
							$title_return .= '<div class="labels">';
						}
					}

					$term = wp_get_post_terms( $post_id, strtolower( $title_part ), array( 'orderby' => 'none' ) );

					if ( !empty( $term ) and !is_wp_error( $term ) ) {

						if ( !empty( $term[0] ) ) {

							if ( !empty( $term[0]->name ) ) {

								if ( $title_counter == 1 ) {
									$title_return .= $term[0]->name;
								} else {
									$title_return .= ' ' . $term[0]->name;
								}
							} else {
								$number_affix = get_post_meta( $post_id, strtolower( $title_part ), true );
								if ( !empty( $number_affix ) ) {
									$title_return .= ' ' . $number_affix . ' ';
								}
							}
						}
					} else {
						$number_affix = get_post_meta( $post_id, strtolower( $title_part ), true );
						if ( !empty( $number_affix ) ) {
							$title_return .= ' ' . $number_affix . ' ';
						}
					}

					if ( $show_labels and $title_counter == 2 ) {
						$title_return .= '</div>';
					}
				}
			}
		}
	}

	if ( empty( $title_return ) ) {
		$title_return = get_the_title( $post_id );
	}

	return $title_return;
}


function expired_listing() {
	$posts = \Stm_Hybrid\Listing::query([
		'where[status]' => 'publish',
	]);

	$gmt = time();
	foreach ($posts['data'] as $posts_info) {

		$author_id = $posts_info->post_author;
		$post_id = $posts_info->ID;
		$post_time = get_post_time($post_id);

		$expired_timestamp = strtotime($posts_info->created_at) + (30 * 24 * 3600);
		if ($expired_timestamp < $gmt) {
			\Stm_Hybrid\Listing::update( $posts_info, [ 'status' => 'draft' ] );
			//wp_update_post(['ID' => $post_id, 'post_status' => 'pending']);
		}
	}
	die;

}

// Expired cron
//add_action( 'wp', 'expired_activation' );
//function expired_activation() {
//    if( ! wp_next_scheduled( 'expired_listing' ) ) {
//        wp_schedule_event( time(), 'daily', 'expired_listing');
//    }
//}

add_action('wp_ajax_nopriv_register_listing', 'register_listing_subscribe');
add_action('wp_ajax_register_listing', 'register_listing_subscribe');
function register_listing_subscribe(){
	$listing_id = (int)$_REQUEST['lid'];
	$subscription_id = (int)$_REQUEST['sid'];
	$subscription = new Subscriptio_Subscription($subscription_id);
	$lid = 0;
	if(!empty($subscription->last_order_id)){
		$order = wc_get_order($subscription->last_order_id);
		$items = $order->get_items();
		foreach ( $items as $item_id => $item ) {
			wc_update_order_item_meta( $item_id, __( 'Listing ID', 'motors' ), $listing_id );
			wc_update_order_item_meta( $item_id, __( 'Listing Name', 'motors' ), $_REQUEST['l_name'] );
			clean_post_cache( $order->get_id() );
			wc_delete_shop_order_transients( $order );
			wp_cache_delete( 'order-items-' . $order->get_id(), 'orders' );
			$lid = wc_get_order_item_meta( $item_id, 'Listing ID' );
		}
	}

	wp_send_json(['listing_id' => $lid, 'listing_name' => $_REQUEST['l_name']]);
}


// Redirect WooCommerce checkout page to a custom thank you page
add_action( 'woocommerce_thankyou', 'redirect_to_add_car_page');
function redirect_to_add_car_page( $order_id ){
	$order = wc_get_order($order_id);
	if($order){
		$items = $order->get_items();
		$t = false;
		foreach ( $items as $item_id => $item ) {
			$lid = wc_get_order_item_meta( $item_id, 'Listing ID' );
			if($lid) $t = true;
		}
		if(!$t){
			$add_car = (int)get_theme_mod('user_add_car_page', 1755);
			wp_safe_redirect( get_permalink($add_car) );
		}else{
			wp_safe_redirect( stm_account_url( 'inventory' ) );
		}
		exit;
	}
}

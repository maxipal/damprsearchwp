<?php


// Add listing ID in cart meta
add_filter( 'woocommerce_add_cart_item_data', 'stm_add_listing_id_checkout', 10, 3 );
function stm_add_listing_id_checkout( $cart_item_data, $product_id, $variation_id ) {
	if( !empty( $_REQUEST['lid'] ) ) {
		$listing = \Stm_Hybrid\Listing::find((int)$_REQUEST['lid']);
		if($listing){
			$cart_item_data['listing_id'] = $listing->id;
			$cart_item_data['listing_title'] = $listing->title;
		}
	}
	return $cart_item_data;
}

// Add listing ID to order
add_action( 'woocommerce_checkout_create_order_line_item', 'stm_add_listing_id_order', 10, 4 );
function stm_add_listing_id_order( $item, $cart_item_key, $values, $order ) {
	if( isset( $values['listing_id'] ) ) {
		$item->add_meta_data( __( 'Listing ID', 'motors' ),	$values['listing_id'] );
		$item->add_meta_data( __( 'Listing Name', 'motors' ),	$values['listing_title'] );
		unset($values['listing_id']);
	}
}

add_filter( 'woocommerce_get_item_data', 'stm_add_listing_id_cart', 10, 2 );
function stm_add_listing_id_cart( $item_data, $cart_item_data ) {
	if( isset( $cart_item_data['listing_id'] ) ) {
		$item_data[] = array(
			'key' => __( 'Listing', 'motors' ),
			'value' => wc_clean( $cart_item_data['listing_title'] ),
			'display' => ''
		);
	}
	return $item_data;
}


add_action('subscriptio_status_changed', 'stm_subscriptio_status_changed_pay', 1000, 3);
function stm_subscriptio_status_changed_pay($subscription, $old_status, $new_status){
	if ( $new_status !== 'active' ) {
		stm_change_listing_status($subscription, false);
	}else{
		stm_change_listing_status($subscription, true);
	}
}

function stm_change_listing_status($subscription, $enable = true) {
	$args = [
		'subscription_id' => $subscription->id,
		'subscription_status' => $subscription->status,
		'subscription_expires' => $subscription->expires,
		'subscription_expires_r' => $subscription->expires_readable,
		'subscription_title' => $subscription->product_name,
	];
	$order_ids = $subscription->all_order_ids;
	foreach ($order_ids as $order_id) {
		$order = wc_get_order( $order_id );
		$items = $order->get_items();
		foreach ( $items as $item_id => $item ) {
			$lid = wc_get_order_item_meta( $item_id, 'Listing ID' );
			if($lid){
				if(!$enable){
					$args['status'] = 'pending';
				}else{
					$args['status'] = 'publish';
				}
				$listing = \Stm_Hybrid\Listing::update( $lid,  $args);
			}
		}
	}
	return;
}

add_action('template_redirect', function(){
	if(isset($_REQUEST['lofa'])){
		$lid = 5263;
		$subscription = new Subscriptio_Subscription(5415);
		var_dump($subscription);
		$listing = \Stm_Hybrid\Listing::find($lid);
//		var_dump($listing);
//		$listing = \Stm_Hybrid\Api::make()->put('listings/'.(int)$lid, $listing, ['auth' => stm_hybrid_api_auth()] );
//		$listing = json_decode($listing->get_data());
//		$args = [
//			'status' => 'draft',
//			'sold' => 1,
//			'subscription_id' => $subscription->id,
//			'subscription_status' => $subscription->status,
//			'subscription_due' => $subscription->payment_due,
//		];
//		$listing = \Stm_Hybrid\Listing::update( $lid,  $args);
//		var_dump($listing);
//		var_dump($args);
		die;
	}
});

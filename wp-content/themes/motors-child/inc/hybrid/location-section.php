<?php

define('STMLOCATION_DOMAIN', 'motors');



function stm_locations_dropdown($atts){
	$atts = array_merge( array(
		'placeholder'   => '',
		'lat'			=> '',
		'lng'			=> '',
		'location'		=> '',
		'searchby'		=> 'category'
	), $atts );
	global $landing;

	if(!$landing) $landing = [];
	$locations = get_option( 'wpuniq_theme_stm_location' );
	$res = \Stm_Hybrid\Api::make()->get( 'terms/locations', $landing );
	$locations = $res->json();

	if(!empty($locations)) $locations = htmlspecialchars(json_encode($locations), ENT_QUOTES, 'UTF-8');
	else $locations = '';
	$content = '
		<input autocomplete="off" data-searchby="'.$atts['searchby'].'" readonly data-el="'.$locations.'" value="'.$atts['location'].'" type="text" ref="stm_location" placeholder="'.$atts['placeholder'].'" class="stm_locations">
		<input type="hidden" name="stm_lat" value="'.$atts['lat'].'">
		<input type="hidden" name="stm_lng" value="'.$atts['lng'].'">
	';
	$content .= '<input type="hidden" name="filter[countries]" value="'.(!empty($landing['countries']) ? $landing['countries'] : '').'">';
	$content .= '<input type="hidden" name="filter[towns]" value="'.(!empty($landing['towns']) ? $landing['towns'] : '').'">';

	return $content;
}
// Very long country with several content
// Munchen with several rows

add_action('wp_footer','stm_locations_js_init');
function stm_locations_js_init(){
	?>
	<script type="text/javascript">
	(function ( $ ) {

		$.fn.stm_locations = function( options ) {

			function stm_title_location_popap(cid,tid){
				let name = '';
				if(cid){
					jQuery('.wrap_stm_locations_popap').find('li.country_item').each(function(index, el) {
						if(jQuery(el).data('id') == cid) name = jQuery(el).data('name');
					});
				}else if(tid){
					jQuery('.wrap_stm_locations_popap').find('li.town_item').each(function(index, el) {
						if(jQuery(el).data('id') == tid) name = jQuery(el).data('name');
					});
				}else return false;
				return name;
			}

			function show_rem_btn(self){
				if($(self).val() !== '' && !$(self).siblings('span.loc_rem').length){
					$(self).after('<span class="loc_rem"><i class="fa fa-times"></i></span>');
				}else{
					$(self).siblings('span.loc_rem').show();
				}
			}

			var settings = $.extend({
				data: null,
				autolocation: true,
				flags: true
			}, options );

			let items = $(this).data('el');
			if(settings.data) items = JSON.parse(settings.data);


			let li = '<li class="geo_cycle"><div><i class="fa fa-crosshairs"></i><span><?php _e("Current location","motors") ?></span></div></li>';

			for(var i in items){
				let towns = [];
				let pname = items[i].name;
				let plat = items[i].lat;
				let plng = items[i].lng;
				let cid = items[i].id;
				let pflag = '<span class="flag" style="background-image:url('+items[i].flag_url+')"></span>';
				if(items[i].towns){

					for(var j in items[i].towns){
						let name = '<span class="name">'+items[i].towns[j].name+'</span>';
						let flag = '<span class="flag" style="background-image:url('+items[i].towns[j].flag_url+')"></span>';
						let lat = items[i].towns[j].lat;
						let lng = items[i].towns[j].lng;
						let cname = items[i].towns[j].name;
						let id = items[i].towns[j].id;
						let html = '<li class="town_item" data-id="'+id+'" title="'+items[i].towns[j].name+'" data-name="'+pname+' - '+cname+'" data-lat="'+lat+'" data-lng="'+lng+'"><div>'+flag+name+'</div></li>';
						towns.push(html);
					}
				}
				if(towns.length) li = li + '<li class="country_item" data-id="'+cid+'" title="'+items[i].name+'" data-lat="'+plat+'" data-lng="'+plng+'" data-name="'+pname+'"><div class="has_child">'+pflag+'<span class="name">'+items[i].name+'</span><ul>'+towns.join('')+'</ul></div></li>';
				else li = li + '<li class="town_item" data-id="'+cid+'" title="'+items[i].name+'" data-lat="'+plat+'" data-lng="'+plng+'" data-name="'+pname+'"><div>'+pflag+'<span class="name">'+items[i].name+'</span></div></li>'
			}

			if($('body .wrap_stm_locations_popap').length){
				$('body').find('.wrap_stm_locations_popap').html('<ul>'+li+'</ul>');
			}else{
				$('body').append('<div class="wrap_stm_locations_popap"><ul>'+li+'</ul></div>');
			}


			return this.each(function() {
				var self = this;
				var reload = false;
				var el_data = $(self).data('el');
				var country = town = false;
				var searchby = $(self).data('searchby');

				if($(self).siblings('input[name="filter[countries]"]').val() !== ''){
					country = parseInt($(self).siblings('input[name="filter[countries]"]').val());
					$(self).val(stm_title_location_popap(country,false));
					show_rem_btn(self);
				}
				if($(self).siblings('input[name="filter[towns]"]').val() !== ''){
					town = parseInt($(self).siblings('input[name="filter[towns]"]').val());
					$(self).val(stm_title_location_popap(false,town));
					show_rem_btn(self);
				}

				if(typeof country !== 'undefined'){
					for(var i in el_data){
						if(el_data[i].id == country) country = el_data[i];
					}
				}

				if(typeof town !== 'undefined'){
					for(var c in el_data){
						for(var t in el_data[c].towns){
							if(typeof el_data[i].towns[t] !== 'undefined' && el_data[i].towns[t].id == town) town = el_data[i].towns[t];
						}
					}
				}

				$(document).off('click','.loc_rem').on('click', '.loc_rem', function(event) {
					$(self).val('');
					$(self).siblings('input[name=stm_lat]').val('');
					$(self).siblings('input[name=stm_lng]').val('');
					$(self).siblings('input[name="filter[countries]"]').val('');
					$(self).siblings('input[name="filter[towns]"]').val('');
					$(this).remove();
					$(self).parents('form').find('select:first').trigger('change');
					reload = false;
				});



				$(self).parents('form').on('change','input[name=max_search_radius]', function(event) {

					// Fix radius search
					let max_search_radius = $(self).parents('form').find('input[name=max_search_radius]').val();
					if(max_search_radius !== '100000' && max_search_radius !== stm_hybrid.distance_search){
						if(typeof town.lat !== 'undefined'){
							$(self).siblings('input[name=stm_lat]').val(town.lat);
							$(self).siblings('input[name=stm_lng]').val(town.lng);
						}else if(typeof country.lat !== 'undefined'){
							$(self).siblings('input[name=stm_lat]').val(country.lat);
							$(self).siblings('input[name=stm_lng]').val(country.lng);
						}

					}else{
						$(self).siblings('input[name=stm_lat]').val('');
						$(self).siblings('input[name=stm_lng]').val('');
					}
				});



				$(this).focus(function(){
					reload = false;
					$(document).off('click','.wrap_stm_locations_popap li.geo_cycle').on('click', '.wrap_stm_locations_popap li.geo_cycle', function(event) {
						reload = true;
						if (navigator.geolocation) {
							navigator.geolocation.getCurrentPosition(function(position) {
								let lat = position.coords.latitude;
								let lng = position.coords.longitude;

								$(self).siblings('input[name=stm_lat]').val(lat);
								$(self).siblings('input[name=stm_lng]').val(lng);

								$(self).addClass('fetch_content');
								fetch('https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat='+lat+'&lon='+lng).then(function(response){
									response.json().then(function(data) {
										let address = data.display_name;
										$(self).val(address);
										$(self).attr('title',address);
										$(self).removeClass('fetch_content');
										if(!$(self).siblings('span.loc_rem').length){
											$(self).after('<span class="loc_rem"><i class="fa fa-times"></i></span>');
										}
										$(self).parents('form').find('select:first').trigger('change');
									});
								});
							}, function(code, message){
								console.log({code:code,message:message});
								if(typeof code.message !== 'undefined') alert(code.message);
							});
						}else{
							alert("Enable geolocation in your brouser");
						}
					});


					$(document).off('click','.wrap_stm_locations_popap li').on('click', '.wrap_stm_locations_popap li', function(event) {
						let el = $(event.target);
						if(event.target.tagName !== 'LI') el = $(event.target).parents('li');
						if(el.length > 1) el = $(el[0]);
						let name = el.data('name');

						$(self).val(name).attr('title',name).attr('value',name);


						let cid = tid = lat = lng = '';
						if($(el).hasClass('country_item')){
							$(self).parents('form').find('input[name=max_search_radius]').val('100000');
							cid = $(el).data('id');
							tid = '';
							lat = $(el).data('lat');
							lng = $(el).data('lng');
						}else{
							cid = $(el).parents('.country_item').data('id');
							tid = $(el).data('id');
							lat = $(el).data('lat');
							lng = $(el).data('lng');
							$(self).parents('form').find('input[name=max_search_radius]').val(stm_hybrid.distance_search);
						}



						$(self).siblings('input[name="filter[countries]"]').val(cid);
						$(self).siblings('input[name="filter[towns]"]').val(tid);

						if(searchby == 'latlng'){
							$(self).siblings('input[name="stm_lat"]').val(lat);
							$(self).siblings('input[name="stm_lng"]').val(lng);
						}

						// $(self).parents('form').find('select:first').trigger('change');
						reload = true;
					});



					self.select();

					let offset = $(this).offset();
					var totalWidth = $(this).width();
					totalWidth += parseInt($(this).css("padding-left"), 10) + parseInt($(this).css("padding-right"), 10); //Total Padding Width
					totalWidth += parseInt($(this).css("margin-left"), 10) + parseInt($(this).css("margin-right"), 10); //Total Margin Width
					totalWidth += parseInt($(this).css("borderLeftWidth"), 10) + parseInt($(this).css("borderRightWidth"), 10); //Total Border Width


					let args = {
						left: offset.left,
						top: offset.top+$(this).height() + 4,
						width: totalWidth
					}
					if((($('body').width() - offset.left) - (totalWidth * 2)) < 0){
						$('body .wrap_stm_locations_popap').addClass('offset_popap');
					}else{
						$('body .wrap_stm_locations_popap').removeClass('offset_popap');
					}

					$('body .wrap_stm_locations_popap').css(args).show();
				}).focusout(function(){
					setTimeout(function(){
						show_rem_btn(self);
						$('body .wrap_stm_locations_popap').hide();
						if(reload) $(self).parents('form').find('select:first').trigger('change');
					},200);
				});

			});

		};
		$('.stm_locations').stm_locations();

	}( jQuery ));

	</script>


	<style type="text/css">
		.wrap_stm_locations_popap{
			position: absolute;
			display: none;
			background-color: #fff;
			z-index: 1000;
		}
		.wrap_stm_locations_popap ul{
			list-style: none;
			padding: 0;
			border: 1px solid #dadada;
			margin: 0;
			width: 100%;
			background-color: #fff;
		}
		.wrap_stm_locations_popap  ul li{
			margin: 0;
			display: inline-block;
			width: 100%;
		}
		.wrap_stm_locations_popap  ul li i.fa{
			margin-right: 10px;
			color: #417ae8;
			font-size: 20px;
			height: 15px;
			width: 20px;
			display: inline-block;
			position: absolute;
			top: 0;
			bottom: 0;
			margin: auto;
			left: 10px;
		}
		.wrap_stm_locations_popap  ul li i.fa.fa-crosshairs{
			height: 18px;
		}
		.wrap_stm_locations_popap  ul li div {
			padding: 3px 10px;
			margin: 3px 0 3px;
			padding-left: 40px;
			position: relative;
			cursor: pointer;
			font-size: 13px;
		}
		.wrap_stm_locations_popap  ul li div span.flag{
			background-position: center;
			background-size: cover;
			height: 15px;
			width: 20px;
			display: inline-block;
			position: absolute;
			top: 0;
			bottom: 0;
			margin: auto;
			left: 10px;
		}
		.wrap_stm_locations_popap  ul li div span.name{
			vertical-align: middle;
			text-overflow: ellipsis;
			overflow: hidden;
			display: block;
			white-space: nowrap;
		}
		.wrap_stm_locations_popap  ul li div.has_child{
			position: relative;
			padding-right: 20px;
		}
		.wrap_stm_locations_popap  ul li div.has_child:before {
			content: "▷";
			position: absolute;
			right: 5px;
			top: 0;
			bottom: 0;
			margin: auto;
			height: 13px;
			width: 13px;
			line-height: 12px;
			font-size: 12px;
			color: #ccc;
		}
		.wrap_stm_locations_popap > ul li + li {
			border-top: 1px solid #dadada;
		}
		.wrap_stm_locations_popap > ul > li ul{
			transition: all .4s ease-out;
			opacity: 0;
			visibility: hidden;
			position: absolute;
			left: 100%;
			top: -4px;
			max-height: 230px;
			overflow: auto;
		}
		.wrap_stm_locations_popap.offset_popap > ul > li ul{
			right: 100%;
			left: auto;
		}
		.wrap_stm_locations_popap  ul > li:hover{
			background-color: #e8e8e8;
		}
		.wrap_stm_locations_popap  ul > li:hover ul{
			opacity: 1;
			visibility: visible;
			transition: all .4s ease-out;
		}
		.stm-location-search-unit .stm_locations{
			padding-left: 15px;
			padding-right: 25px !important;
			text-overflow: ellipsis;
		}
		.stm-location-search-unit .fetch_content{
			opacity: 0.5;
		}

		.stm-location-search-unit{
			position: relative;
		}
		.stm_locations + span.loc_rem{
			position: absolute;
			top: 0;
			bottom: 0;
			margin: auto;
			right: 5px;
			height: 22px;
			width: 20px;
			font-size: 20px;
			font-weight: normal;
			cursor: pointer;
			line-height: 20px;
			display: inline-block;
			text-align: center;
		}
	</style>
	<?php
}

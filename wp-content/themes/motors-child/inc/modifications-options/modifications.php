<?php

add_filter('stm_listings_page_options_filter', 'add_modification_option');

function add_modification_option($options){

    $options['modifaction_option'] = [
        'label' => 'Use in Modification Section',
        'description' => 'Check if you want to see this in modification section',
        'value' => '',
        'type' => 'checkbox'
    ];

    return $options;
}

// Register settings
// Get list of parameters $params = stm_get_taxonomies_with_type('tax_name');
// Get params from all tax $params = get_option('stm_vehicle_listing_options');
add_filter('stm_listings_page_options_filter', 'pcm_dependencies_options_filter');
function pcm_dependencies_options_filter($options){
	$column = 2;
	$temp['filter_section'] = array(
		'label' => esc_html__('Put in "Filters" Section on inventory page', 'motors-child'),
		'value' => '',
		'type' => 'checkbox',
		'preview' => '',
	);
	$divider = array_search('divider_'.$column, array_keys($options));
	if($divider){
		$options = array_slice($options, 0, $divider, true) +
			$temp +
			array_slice($options, $divider, count($options) - 1, true) ;
	}
	return $options;
}


add_action('wp_footer', function() {
	?>
	<script>
		jQuery(document).ready(function() {

		});
		window.onpageshow = function() {
			if(jQuery('body.home .stm_dynamic_listing_filter.filter-listing form').length){
				let form = jQuery('.stm_dynamic_listing_filter.filter-listing form');
				form.get(0).reset();
				form.find('select').each(function(i,el){
					jQuery(el).val('').select2('destroy').select2();
				})
			}
		};
	</script>
	<?php
},1000);

<?php

add_action('customize_register', 'custom_settings');

function custom_settings($wp_customize)
{
    $wp_customize->add_section('cd_colors', array(
        'title' => 'Banner Tagline',
        'priority' => 30,
    ));
    $wp_customize->add_setting('banner_tagline', array(
        'default' => 'Car Shooping, for Car Enthusiasts',
        'type'      => 'theme_mod',
        'transport' => 'postMessage'
    ));
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'banner_tagline',
            array(
                'section' => 'cd_colors', // id секции
                'label' => 'Tagline Text',
                'type' => 'text' // текстовое поле
            ))
    );

    $wp_customize->add_setting('tagline_size', array(
        'default' => '18px',
        'type'      => 'theme_mod',
        'transport' => 'postMessage'
    ));
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'tagline_size',
            array(
                'section' => 'cd_colors', // id секции
                'label' => 'Tagline Font-size',
                'type' => 'text' // текстовое поле
            ))
    );

    $wp_customize->add_setting('tag_color', array(
        'default' => '#43C6E4',
        'type'      => 'theme_mod',
        'transport' => 'refresh',
    ));
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'tag_color', array(
        'label' => 'Tagline Color',
        'section' => 'cd_colors',
        'settings' => 'tag_color',
    )));

}
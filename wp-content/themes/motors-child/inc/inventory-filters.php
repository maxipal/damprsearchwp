<?php

add_filter( 'stm_listings_filter', function( $compact ) {

	foreach ( $compact['filters'] as $filter_name => $filter_opt ) {
		$parent_slug = $filter_opt['listing_taxonomy_parent'];

		if ( ! $parent_slug ) {
			continue;
		}
		$opt[] = $filter_opt;
		$compact['filters'][ $parent_slug ]['children'] = $filter_opt['slug'];
	}

	return $compact;
} );

function stm_get_parent_tax($child) {
	foreach (get_option('stm_vehicle_listing_options', array()) as $opt) {
		if($opt['slug'] == $child && !empty($opt['listing_taxonomy_parent'])) {
			return $opt['listing_taxonomy_parent'];
		}
	}
}

function _stm_child_listings_filter_attribute_options($taxonomy, $_terms)
{

	$attribute = stm_listings_attribute($taxonomy);
	$attribute = wp_parse_args($attribute, array(
		'slug' => $taxonomy,
		'single_name' => '',
		'numeric' => false,
		'slider' => false,
	));

	$options = array();

	if (!$attribute['numeric']) {

		$options[''] = array(
			'label' => apply_filters('stm_listings_default_tax_name', $attribute['single_name']),
			'selected' => stm_listings_input($attribute['slug']) == null,
			'disabled' => false,
		);

		foreach ($_terms as $_term) {
			$parent_tax =  get_term_meta($_term->term_id, 'stm_parent');
			$options[$_term->slug] = array(
				'label' => $_term->name,
				'selected' => stm_listings_input($attribute['slug']) == $_term->slug,
				'disabled' => false,
				'count' => $_term->count,
				'taxonomy' => $_term->taxonomy,
				'parent' => $parent_tax
			);
		}
	}
	else {
		$numbers = array();
		foreach ($_terms as $_term) {
			$numbers[intval($_term->slug)] = $_term->name;
		}
		ksort($numbers);

		if (!empty($attribute['slider'])) {
			foreach ($numbers as $_number => $_label) {
				$options[$_number] = array(
					'label' => $_label,
					'selected' => stm_listings_input($attribute['slug']) == $_label,
					'disabled' => false,
				);
			}
		} else {

			$options[''] = array(
				'label' => sprintf(__('Max %s', 'stm_vehicles_listing'), $attribute['single_name']),
				'selected' => stm_listings_input($attribute['slug']) == null,
				'disabled' => false,
			);

			$_prev = null;
			$_affix = empty($attribute['affix']) ? '' : __($attribute['affix'], 'stm_vehicles_listing');

			foreach ($numbers as $_number => $_label) {

				if ($_prev === null) {
					$_value = '<' . $_number;
					$_label = '< ' . $_label . ' ' . $_affix;
				} else {
					$_value = $_prev . '-' . $_number;
					$_label = $_prev . '-' . $_label . ' ' . $_affix;
				}

				$options[$_value] = array(
					'label' => $_label,
					'selected' => stm_listings_input($attribute['slug']) == $_value,
					'disabled' => false,
				);

				$_prev = $_number;
			}

			if ($_prev) {
				$_value = '>' . $_prev;
				$options[$_value] = array(
					'label' => '>' . $_prev . ' ' . $_affix,
					'selected' => stm_listings_input($attribute['slug']) == $_value,
					'disabled' => false,
				);
			}
		}
	}

	return $options;
}

function stm_child_listings_filter_options()
{
	static $options;

	if (isset($options)) {
		return $options;
	}

	$filters = stm_listings_attributes(array('where' => array('use_on_car_filter' => true), 'key_by' => 'slug'));
	$terms = stm_listings_filter_terms();
	$options = array();

	foreach ($terms as $tax => $_terms) {
		$_filter = isset($filters[$tax]) ? $filters[$tax] : array();
		$options[$tax] = _stm_child_listings_filter_attribute_options($tax, $_terms);

		if (empty($_filter['numeric']) || !empty($_filter['use_on_car_filter_links'])) {
			$_remaining = stm_listings_options_remaining($terms[$tax], stm_listings_query());


			foreach ($_terms as $_term) {
				$term_slug = $options[$tax][$_term->slug]['taxonomy'];
				$parent_tax = stm_get_parent_tax($term_slug);
				if( $_REQUEST[ $parent_tax ] && in_array($_REQUEST[ $parent_tax ],  $options[$tax][$_term->slug]['parent'] )){
					continue;
				} else if (isset($_remaining[$_term->term_taxonomy_id])) {
					$options[$tax][$_term->slug]['count'] = (int) $_remaining[$_term->term_taxonomy_id];
				} else {
					$options[$tax][$_term->slug]['count'] = 0;
					$options[$tax][$_term->slug]['disabled'] = true;
				}
			}
		}
	}

	$options = apply_filters('stm_listings_filter_options', $options);

	return $options;
}

add_filter('stm_listings_filter_options', 'stm_child_listings_filter_options');
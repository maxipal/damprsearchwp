<?php

add_action('wp_enqueue_scripts', 'stm_enqueue_parent_styles', 1000);
function stm_enqueue_parent_styles()
{
	wp_enqueue_style('child-style', get_stylesheet_directory_uri() . '/style.css', array('stm-theme-style'));
	wp_enqueue_script('custom-js', get_stylesheet_directory_uri() . '/assets/js/custom.js', ['stm-theme-script-filter'], '', true);
}

add_action('wp_footer', function () {
	$font_size = get_theme_mod('tagline_size', '20px');
	$tag_color = get_theme_mod('tag_color', '#0c70bb');
	$slogun = get_theme_mod('banner_tagline', false);
	?>
	<style>
        #hometopbg:before {
			content: '<?php esc_html_e($slogun, 'motors-child'); ?>';
			color: <?php echo $tag_color?>;
			font-size: <?php echo $font_size;?>;

		}
    </style>
<?php });

// Remove additional information
add_filter('woocommerce_enable_order_notes_field', '__return_false');

//wp_mail('alexeyhong10@gmail.com', 123, 123);

// Enable listings via hybrid
add_filter( 'stm_hybrid_perform_listings_query', '__return_true' );

<?php
require_once 'hybrid/location-section.php';
require_once 'hybrid/testdrive-notification.php';
require_once 'hybrid/helpers.php';
require_once 'hybrid/auction.php';
require_once 'hybrid/user-account.php';
require_once 'hybrid/notification.class.php';
require_once 'hybrid/is_login.php';
require_once 'hybrid/custom.php';
require_once 'hybrid/suggestions.php';
require_once 'email_template_manager/email_template_manager.php';
require_once 'hybrid/car_location.php';
require_once 'hybrid/pay_per_listing.php';

add_action( 'wp_enqueue_scripts', 'stm_enqueue_hybrid_styles', 1001 );
function stm_enqueue_hybrid_styles() {
	wp_enqueue_style( 'hybrid-style', get_stylesheet_directory_uri() . '/hybrid/style.css', array('stm-theme-style'), '1.1.22' );

	wp_deregister_script('stm-smooth-scroll');
	wp_deregister_script('stm-lazyload');
	wp_deregister_script('underscore');
	wp_enqueue_script( 'underscore', 'https://underscorejs.org/underscore-min.js', array(), null );

	register_mix_script( 'manifest', 'js/manifest.js' );
	register_mix_script( 'vendor', 'js/vendor.js', array( 'manifest' ) );
	wp_localize_script( 'vendor', 'thumbor_config', [
		'url' => stm_hybrid_thumbor_url(),
		'key' => get_option( 'listing_thumbor_key' ),
	] );

	wp_register_script( 'compare', get_stylesheet_directory_uri() . '/hybrid/js/compare.js', array( 'jquery' ), STM_THEME_VERSION, true );
	wp_register_script( 'favorites', get_stylesheet_directory_uri() . '/hybrid/js/favorites.js', array( 'jquery' ), STM_THEME_VERSION, true );
	wp_enqueue_script( 'stm-filter', get_stylesheet_directory_uri() . '/hybrid/js/filter-hybrid.js', array( 'jquery', 'compare', 'favorites' ), STM_THEME_VERSION, true );
	register_mix_script( 'vue', 'js/vue-init.js', array(), false );
	register_mix_script( 'vue-pagination', 'js/vue-pagination.js', array( 'vue' ), true );
	register_mix_script( 'vue-timer', 'js/vue-timer.js', array( 'vue' ), true );
	register_mix_script( 'listing-script', 'js/listing.js', array( 'vue', 'stm-filter', 'vue-pagination' ), true );

	wp_localize_script( 'listing-script', 'listing', array(
		'archive_path' => str_replace( site_url(), '', stm_get_listing_archive_link() ),
	) );


	wp_enqueue_script( 'login', get_stylesheet_directory_uri() . '/hybrid/js/login.js', array( 'jquery' ), STM_THEME_VERSION, true );
	wp_enqueue_script( 'new_custom', get_stylesheet_directory_uri() . '/hybrid/js/new_custom.js', array( 'jquery', 'stm-theme-scripts' ), STM_THEME_VERSION, true );
	wp_enqueue_script( 'option-groups', get_stylesheet_directory_uri() . '/hybrid/js/option-groups.js', array( 'jquery', 'stm-theme-scripts' ), STM_THEME_VERSION, true );
	wp_register_script( 'bloodhound', get_stylesheet_directory_uri() . '/hybrid/js/bloodhound.min.js', array(), STM_THEME_VERSION, true );
	wp_enqueue_script( 'search-suggestions', get_stylesheet_directory_uri() . '/hybrid/js/search-suggestions.js', array('jquery', 'bloodhound'), STM_THEME_VERSION, true );
	wp_enqueue_script('fancybox', get_stylesheet_directory_uri() . '/assets/js/jquery.fancybox.pack.js', array('jquery'), null, true);
	wp_enqueue_script( 'fancybox-media', get_stylesheet_directory_uri() . '/hybrid/js/jquery.fancybox-media.js', array('jquery'), STM_THEME_VERSION, true );
	register_mix_script( 'private-inventory', 'js/private-inventory.js', array( 'vue', 'stm-filter', 'vue-pagination', 'vue-timer' ), true );
	wp_register_script( 'search-box', get_stylesheet_directory_uri() . '/hybrid/js/search-box.js', array( 'vue', 'stm-filter' ), STM_THEME_VERSION, true );
	register_mix_script( 'vue-icon_filter', 'js/vue-icon_filter.js', array( 'vue' ), true );
	register_mix_script( 'vue-listings_tabs_2', 'js/vue-listings_tabs_2.js', array( 'vue' ), true );
	register_mix_script( 'vue-dealer_list', 'js/vue-dealer_list.js', array( 'vue' ), true );
	register_mix_script( 'vue-dealer_inventory', 'js/vue-dealer_inventory.js', array( 'vue' ), true );
	wp_register_script( 'Sortable', get_stylesheet_directory_uri() . '/hybrid/js/Sortable.js', array( 'vue' ), STM_THEME_VERSION, true );
	wp_enqueue_script( 'auction-js', get_stylesheet_directory_uri() . '/hybrid/js/auction.js', array('jquery'), STM_THEME_VERSION, true );
	wp_register_script( 'ui-droppable', get_stylesheet_directory_uri() . '/hybrid/js/jquery.ui.droppable.js', array('jquery'), STM_THEME_VERSION, true );

	wp_enqueue_style( 'fancybox-css', get_stylesheet_directory_uri() . '/assets/css/jquery.fancybox.css', array('hybrid-style'), STM_THEME_VERSION );

	$default_lat_lng = get_option('filter_default_location_latlng', '36.169941,-115.139830');
	if (empty($default_lat_lng)) {
		$default_lat_lng = '36.169941,-115.139830';
	}

	wp_register_script( 'vue-base', get_stylesheet_directory_uri() . '/hybrid/js/base.umd.js', array( 'vue' ), STM_THEME_VERSION, true );
	register_mix_script( 'stm-hybrid-form', 'js/listing-form.js', array('jquery'), true );
	wp_register_script( 'trade-in', get_stylesheet_directory_uri() . '/hybrid/js/trade-in.js', array( 'vue-base' ), STM_THEME_VERSION, true );

	enqueue_mix_script( 'notification-app', 'js/notification.js', array( 'vue-base' ), true );
	enqueue_mix_script( 'autocomplete-js', 'js/autocomplete.js', array( 'vue-base' ), true );

	if ( is_user_logged_in() && is_author( get_current_user_id() ) ) {
		wp_enqueue_script( 'private-inventory' );
		wp_localize_script( 'private-inventory', 'stm_hybrid_private', array(
			'user_id' => get_current_user_id(),
			'url' => stm_account_url(),
		) );
	}

	global $wp_rewrite;

	$uid = get_current_user_id();
	$user_fields = stm_get_user_custom_fields( $uid );
	$stm_hybrid_localize = array(
		'user_page_url' => stm_account_url(),
		'user_page_auction_url' => stm_account_url( 'myauction' ),
		'api_url' => stm_hybrid_api_url(),
		'cuser_avatar' => (!empty(get_user_meta($uid, 'wsl_current_user_image', true)) ? esc_url(get_user_meta($uid, 'wsl_current_user_image', true)): esc_url($user_fields['image'])),
		'thumb_url' => stm_hybrid_thumb_url(),
		'imgix_url' => stm_hybrid_imgix_url(),
		'distance_search' => get_theme_mod( 'distance_search', 10000 ),
		'listings_base_url' => stm_get_listings_base_url(),
		'author_id' => is_author() ? get_queried_object_id() : null,
		'add_car_page_url' => get_permalink(get_theme_mod('user_add_car_page', 1755)),
		'author_base' => $wp_rewrite->author_base,
		'translations' => array(
			'uploadingPhotos' => esc_html__('Car Added, uploading photos', 'motors'),
			'listingAddedRedirect' => esc_html__('Car added, redirecting to your account profile', 'stm_vehicles_listing'),
			'listingAddedPayRedirect' => __('Car added, redirecting to checkout', 'stm_vehicles_listing'),
			'photoRequired' => __('Please add at least 1 image', 'motors'),
			'addToFavorites' => __('Add to favorites', 'motors')
		)
	);

	$is_listing_archive = false;
	$listing_archive_id = apply_filters( 'stm_listings_inventory_page_id', get_theme_mod( 'listing_archive', false ));
	if(get_the_ID() == $listing_archive_id) $is_listing_archive = true;
	$stm_hybrid_localize['is_add_car_page'] = stm_is_add_car_page();
	$stm_hybrid_localize['is_listing_page'] = $is_listing_archive;
	$stm_hybrid_localize['show_registered'] = get_theme_mod('show_registered', true);
	$stm_hybrid_localize['show_vin'] = get_theme_mod('show_vin', true);
	$stm_hybrid_localize['show_history'] = get_theme_mod('show_history', true);
	$stm_hybrid_localize['is_auction'] = stm_is_auction_page();
	$stm_hybrid_localize['uid'] = $uid;
	$stm_hybrid_localize['title_as_label'] = get_theme_mod('show_generated_title_as_label', true);
	$stm_hybrid_localize['title_frontend'] = get_theme_mod( 'listing_directory_title_frontend', '' );

	$stm_hybrid_localize['dealer_pay_per_listing'] = get_theme_mod('dealer_pay_per_listing', false);
	$stm_hybrid_localize['pay_per_listing_price'] = get_theme_mod('pay_per_listing_price', 0);
	$stm_hybrid_localize['wc_get_checkout_url'] = wc_get_checkout_url() . '?add-to-cart=' . 4988;
	$stm_hybrid_localize['price_url'] = get_permalink(get_theme_mod('pricing_link'));

	if (is_user_logged_in()) {
		$limit = stm_filter_post_limits('images', get_current_user_id(), false);
		$limit = $limit['images'];
		if ($limit) {
			$stm_hybrid_localize['user_photo_upload_limit'] = $limit;
			$stm_hybrid_localize['translations']['uploadPhotoLimit'] = sprintf(__('You can\'t upload more than %s photos', 'motors'), $limit);
		}
	}

	wp_enqueue_script( 'jquery.countdown.js', get_theme_file_uri( '/assets/js/jquery.countdown.min.js' ), array('jquery'), STM_THEME_VERSION, true );

	wp_localize_script( 'stm-theme-scripts', 'stm_hybrid', $stm_hybrid_localize);

	wp_enqueue_style( 'leaflet', get_stylesheet_directory_uri() .'/hybrid/osm/leaflet.css', null, STM_THEME_VERSION );
	wp_register_script( 'leaflet', get_stylesheet_directory_uri() .'/hybrid/osm/leaflet.js', array('vue'), STM_THEME_VERSION, true );

	wp_enqueue_style( 'MarkerCluster', get_stylesheet_directory_uri() . '/hybrid/osm/MarkerCluster.css', null, STM_THEME_VERSION );
	wp_enqueue_style( 'MarkerCluster.Default', get_stylesheet_directory_uri() . '/hybrid/osm/MarkerCluster.Default.css', null, STM_THEME_VERSION );
	wp_register_script( 'MarkerCluster', get_stylesheet_directory_uri() . '/hybrid/osm/leaflet.markercluster.js', array('leaflet'), STM_THEME_VERSION, true );

	wp_register_script( 'osm_leaflet', get_stylesheet_directory_uri() . '/hybrid/osm/osm.js', array('leaflet'), STM_THEME_VERSION, true );
	wp_register_script( 'map-view-osm', get_stylesheet_directory_uri() . '/hybrid/osm/map-view-osm.js', array( 'leaflet', 'stm-filter' ), STM_THEME_VERSION, true );

	wp_localize_script( 'map-view-osm', 'stm_hybrid_map', array(
		'pin_url' => get_stylesheet_directory_uri() . '/hybrid/images/classified_inventory_pin.png',
		'cluster_url_path' => get_template_directory_uri() . '/hybrid/images/1.png',
		'default_lat_lng' => $default_lat_lng
	));
}


function register_mix_script( $handle, $path, $deps = [], $in_footer = true ) {
	$path = '/' . ltrim( $path );
	$version = mix_version( $path );
	if ( $handle != 'vendor' && $handle != 'manifest' ) {
		$deps[] = 'vendor';
	}

	wp_register_script( $handle, get_stylesheet_directory_uri() . '/dist' . $path, $deps, $version, $in_footer );
}

function enqueue_mix_script( $handle, $path, $deps = [], $in_footer = true ) {
	$path = '/' . ltrim( $path );
	$version = mix_version( $path );
	if ( $handle != 'vendor' && $handle != 'manifest' ) {
		$deps[] = 'vendor';
	}

	wp_enqueue_script( $handle, get_stylesheet_directory_uri() . '/dist' . $path, $deps, $version, $in_footer );
}

function register_mix_style( $handle, $path, $deps = [], $media = 'all' ) {
	$path = '/' . ltrim( $path );
	$version = mix_version( $path );

	wp_register_style( $handle, get_stylesheet_directory_uri() . '/dist' . $path, $deps, $version, $media );
}

function enqueue_mix_style( $handle, $path, $deps = [], $media = 'all' ) {
	$path = '/' . ltrim( $path );
	$version = mix_version( $path );

	wp_enqueue_style( $handle, get_stylesheet_directory_uri() . '/dist' . $path, $deps, $version, $media );
}

function mix_version( $path ) {
	$manifest = mix_manifest();
	if ( isset( $manifest[ $path ] ) ) {
		$version = str_replace( $path . '?id=', '', $manifest[ $path ] );
	} else {
		$version = STM_THEME_VERSION;
	}

	return $version;
}

function mix_manifest() {
	static $manifest;

	if ( is_null( $manifest ) ) {
		$manifest = json_decode( file_get_contents( get_stylesheet_directory() . '/dist/mix-manifest.json' ), true );
	}

	return $manifest;
}


 // QUERY
add_action( 'stm_hybrid_singular_error', function ( WP_Error $error ) {
	global $listing_error;
	// Make error global in order to make it accessible in templates
	$listing_error = $error;

	$status_code = $error->get_error_code();
	if ( ! in_array( $status_code, [ 401, 403, 404 ] ) ) {
		$status_code = 503;
	}

	// unexpected error
	if ( $error->get_error_code() >= 500 && $error->get_error_code() != 503 ) {
		$listing_error = new WP_Error(
			'api_error',
			__( 'Sorry, we are experiencing technical difficulties at this time', 'motors-child' )
		);
	}

	status_header( $status_code );
	$template = $status_code . '.php';

	add_filter( 'body_class', function ( $classes ) use ( $listing_error ) {
		$classes[] = 'error' . $listing_error->get_error_code();
		return $classes;
	} );
	add_filter( 'pre_get_document_title', function () use ( $listing_error ) {
		return $listing_error->get_error_message();
	} );
	locate_template( $template, true );
	exit;
} );

function stm_hybrid_listings_query_append() {
	return [
		'auction' => 0,
		'featured' => stm_listings_input('featured', 'with'),
		'filters' => 'all',
		'context' => 'list',
		'with' => 'badges,seo,alternate',
		'per_page' => get_theme_mod( 'listing_grid_choice', 9 ),
	];
}

add_filter( 'stm_hybrid_perform_listings_query', '__return_true' );

add_filter( 'stm_hybrid_listings_query', function ( $query ) {
	return stm_hybrid_listings_query_append() + $query;
} );


if(!function_exists("getCurrencySelectorHtml")) {
	function getCurrencySelectorHtml() {
		$multipleCurrencyList = json_decode(get_theme_mod("currency_list", ""));
		if($multipleCurrencyList != null) {
			$currentCurrency = "";
			if(isset($_COOKIE["stm_current_currency"])){
				$mc = explode("-", $_COOKIE["stm_current_currency"]);
				$currentCurrency = $mc[0];
			}

			$currency[0] = get_theme_mod("price_currency_name", "USD");
			if($multipleCurrencyList->currency != null) $currency = array_merge($currency, explode(",", $multipleCurrencyList->currency));

			$symbol[0] = get_theme_mod("price_currency", "$");
			if($multipleCurrencyList->symbol != null) $symbol = array_merge($symbol, explode(",", $multipleCurrencyList->symbol));

			$to[0] = "1";
			$to = array_merge($to, explode(",", $multipleCurrencyList->to));

			$selectHtml = '<div class="pull-left currency-switcher">';
			$selectHtml .= "<div class='stm-multiple-currency-wrap'><select data-translate='" . esc_html__("Currency (%s)", "motors") . "' data-class='stm-multi-currency' name='stm-multi-currency'>";
			for($q=0;$q<count($currency);$q++) {
				$selected = ($symbol[$q] == $currentCurrency) ? "selected" : "";
				$val = html_entity_decode($symbol[$q]) . "-" . $to[$q];
				$currencyTitle = $currency[$q];

				if(!isset($_COOKIE["stm_current_currency"]) && $q == 0 || !empty($selected)) {
					$currencyTitle = sprintf(esc_html__("Currency (%s)", "motors"), $currency[$q]);
				}

				$selectHtml .= "<option value='{$val}' " . $selected . ">{$currencyTitle}</option>";
			}
			$selectHtml .= "</select></div>";
			$selectHtml .= '</div>';

			if(count($currency) > 1) {
				echo $selectHtml;
			}
		}
	}
}

add_filter( 'woocommerce_default_address_fields' , 'custom_override_default_address_fields' );
// Our hooked in function - $address_fields is passed via the filter!
function custom_override_default_address_fields( $address_fields ) {
	$address_fields['address_1']['required'] = false;
	$address_fields['city']['required'] = false;
	$address_fields['state']['required'] = false;
	$address_fields['postcode']['required'] = false;
	return $address_fields;
}

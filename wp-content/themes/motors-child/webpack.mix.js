const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
	.setPublicPath('dist')
	.js('hybrid/js/vue-init.js', 'js')
	.js('hybrid/js/notification.js', 'js')
	.js('hybrid/js/autocomplete.js', 'js')
	.js('hybrid/js/vue-pagination.js', 'js')
	.js('hybrid/js/vue-timer.js', 'js')
	.js('hybrid/js/vue-icon_filter.js', 'js')
	.js('hybrid/js/vue-listings_tabs_2.js', 'js')
	.js('hybrid/js/listing.js', 'js')
	.js('hybrid/js/vue-dealer_list.js', 'js')
	.js('hybrid/js/vue-dealer_inventory.js', 'js')
	.js('hybrid/js/listing-form.js', 'js')
	.js('hybrid/js/private-inventory.js', 'js')
	.version()
	.extract([
		'lodash-es',
		'vue',
		'vue-lazyload',
		'url-polyfill'
	])
	.options({
		processCssUrls: false
	})
;

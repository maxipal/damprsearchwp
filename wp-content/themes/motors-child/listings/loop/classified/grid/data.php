<?php $labels = stm_get_car_listings();
?>

<?php if(!empty($labels)): ?>
    <div class="car-meta-bottom">


        <?php

        // $stm_range = get_field('stm_range_price');
        //
        // $details_amount = (int)get_post_meta(get_the_ID(),'details', true);
        //
        // $details = array();
        // if(!empty($details_amount)) {
        //     for ($i = 0; $i < $details_amount; $i++) {
        //         $item = get_post_meta(get_the_ID(),'details_'.$i.'_item', true);
        //         $val = get_post_meta(get_the_ID(),'details_'.$i.'_value', true);
        //         if(!empty($item) && !empty($val)) {
        //             $details[$item] = $val;
        //         }
        //     }
        // }


        $price = $stm_range = (int)get_post_meta(get_the_ID(),'stm_range_price',true);

        $details = array();
        $details['speed'] = (int)get_post_meta(get_the_ID(),'speed-nm',true);
        $details['no-of-seats'] = (int)get_post_meta(get_the_ID(),'seats-total',true);
        $details['range-nm'] = (int)get_post_meta(get_the_ID(),'range-nm',true);

        if(is_page('new-aircraft-showroom') || $ajax_template):?>

            <ul class="main-info">

                <?php if(isset($details['speed'])):?>
                <li><span class="speed">
                        <span>Speed :</span>
                        <i class="fa fa-fighter-jet" aria-hidden="true"></i>
                        <?php echo $stm_speed ?></span>
                </li>
                <?php endif;?>

                <?php if(isset($details['no-of-seats'])):?>

                    <li class="seats">
                    <i class="stm-rental-seats" style="float:left;"></i>
                        <span>Seats :</span>
                    <span><?php echo $details['no-of-seats'];?></span>
                </li>
                <?php endif;?>

				<?php if (empty($stm_range)) : ?>
					<li><div class="price"><span class="normal-price">Inquire</span></div></li>
				<?php else : ?>
					<li><div class="price"><span class="normal-price 999"><?php echo stm_listing_price_view($stm_range)?></span></div></li>
				<?php endif; ?>

            </ul>
        <?php endif;?>


        <ul>
            <?php foreach($labels as $label): ?>
                <?php $label_meta = get_post_meta(get_the_id(),$label['slug'],true); ?>
                <?php if($label_meta !== '' and $label_meta != 'none' and $label['slug'] != 'price'): ?>
                    <li>
                        <?php if(!empty($label['font'])): ?>
                            <i class="<?php echo esc_attr($label['font']) ?>"></i>
                        <?php endif; ?>

                        <?php if(!empty($label['numeric']) and $label['numeric']): ?>
                            <span><?php echo esc_attr($label_meta); ?></span>
                        <?php else: ?>

                            <?php
                            $data_meta_array = explode(',',$label_meta);
                            $datas = array();

                            if(!empty($data_meta_array)){
                                foreach($data_meta_array as $data_meta_single) {
                                    $data_meta = get_term_by('slug', $data_meta_single, $label['slug']);
                                    if(!empty($data_meta->name)) {
                                        $datas[] = esc_attr($data_meta->name);
                                    }
                                }
                            }
                            ?>

                            <?php if(!empty($datas)): ?>

                                <?php
                                if(count($datas) > 1) { ?>

                                    <span
                                        class="stm-tooltip-link"
                                        data-toggle="tooltip"
                                        data-placement="bottom"
                                        title="<?php echo esc_attr(implode(', ', $datas)); ?>">
														<?php echo $datas[0].'<span class="stm-dots dots-aligned">...</span>'; ?>
													</span>

                                <?php } else { ?>
                                    <span><?php echo implode(', ', $datas); ?></span>
                                <?php }
                                ?>
                            <?php endif; ?>

                        <?php endif; ?>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

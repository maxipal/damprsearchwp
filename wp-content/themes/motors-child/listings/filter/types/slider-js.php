<?php
if(empty($affix)) {$affix = "";}
if(empty($start_value)) {$start_value = 0;}
if(empty($end_value)) {$end_value = 0;}
if(empty($low_val) && $low_val !== 0) {$low_val = 'null';}
if(empty($high_val) && $high_val !== 0) {$high_val = 'null';}
?>

<script type="text/javascript">
    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    }

    var stmOptions_<?php echo esc_attr($js_slug); ?>;
    (function ($) {
        $(document).ready(function () {
            var affix = "<?php echo esc_js($affix); ?>";
            var stmMinValue = <?php echo esc_js($start_value); ?>;
            var stmMaxValue = <?php echo esc_js($end_value); ?>;
            var lowVal = <?php echo str_replace(' ', '', esc_js($low_val)); ?>;
            var highVal = <?php echo str_replace(' ', '', esc_js($high_val)); ?>;
            var sliderLow, sliderHigh;
            var inputLowVal = '';
            var inputHighVal = '';

            stmOptions_<?php echo esc_attr($js_slug); ?> = {
                range: true,
                min: <?php echo esc_js($start_value); ?>,
                max: <?php echo esc_js($end_value); ?>,
                values: [stmMinValue, stmMaxValue],
                step: 1,
                slide: function (event, ui) {
                    <?php if($slug == 'years'): ?>
                        $("#stm_filter_min_<?php echo esc_attr($slug); ?>").val(ui.values[0]);
                        $("#stm_filter_max_<?php echo esc_attr($slug); ?>").val(ui.values[1]);
                    <?php else: ?>
                        $("#stm_filter_min_<?php echo esc_attr($slug); ?>").val(numberWithCommas(ui.values[0]));
                        $("#stm_filter_max_<?php echo esc_attr($slug); ?>").val(numberWithCommas(ui.values[1]));
                    <?php endif ?>
                    <?php if($slug == 'price'): ?>
                    var stmCurrency = "<?php echo esc_js(stm_get_price_currency()); ?>";
                    var stmPriceDel = "<?php echo esc_js(get_theme_mod('price_delimeter',' ')); ?>";
                    var stmCurrencyPos = "<?php echo esc_js(get_theme_mod('price_currency_position', 'left')); ?>";
                    var stmText = stm_get_price_view(ui.values[0], stmCurrency, stmCurrencyPos, stmPriceDel ) + ' - ' + stm_get_price_view(ui.values[1], stmCurrency, stmCurrencyPos, stmPriceDel );
                    <?php else: ?>
                    var stmText = ui.values[0] + affix + ' — ' + ui.values[1] + affix;
                    <?php endif; ?>

                    $('.filter-<?php echo($slug) ?> .stm-current-slider-labels').html(stmText);
                }
            };
            $(".stm-<?php echo esc_attr($slug); ?>-range").slider(stmOptions_<?php echo esc_attr($js_slug); ?>);


            sliderLow = $(".stm-<?php echo esc_attr($slug); ?>-range").slider("values", 0)
            sliderHigh = $(".stm-<?php echo esc_attr($slug); ?>-range").slider("values", 1)

            if (sliderLow !== stmMinValue) {
                inputLowVal = sliderLow
            }
            if (sliderHigh !== stmMaxValue) {
                inputHighVal = sliderHigh
            }

            if (lowVal !== null) {
                inputLowVal = numberWithCommas(lowVal)
            }

            if (highVal !== null) {
                inputHighVal = numberWithCommas(highVal)
            }


            $("#stm_filter_min_<?php echo esc_attr($slug); ?>").val(inputLowVal);
            $("#stm_filter_max_<?php echo esc_attr($slug); ?>").val(inputHighVal);



        })
    })(jQuery);
</script>

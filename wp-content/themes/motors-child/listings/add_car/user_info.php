<?php
$user_id = get_current_user_id();
$user = stm_get_user_custom_fields( $user_id );
$user_login = '';
$f_name = '';
$l_name = '';

if ( is_wp_error( $user ) ) {
	return;
}
$dealer = stm_is_dealer( $user['user_id'] );
if ( $dealer ) :
	$ratings = stm_get_dealer_marks( $user_id ); ?>

	<div class="stm-add-a-car-user">
		<?php $user_fields = stm_get_user_custom_fields(''); ?>
        <?php if(false): //empty($user_fields['name']) || empty($user_fields['last_name']) || empty($user_fields['phone'])): ?>
            <div class="row form-group wrap_extended_fields">
                <div class="col-md-4">
                    <h4><?php esc_html_e('First name', 'motors'); ?>*</h4>
                    <input class="user_validated_field" type="text" name="stm_first_name"
                       value="<?php echo esc_attr($user_fields['name']); ?>"
                       placeholder="<?php esc_html_e('Enter First Name', 'motors') ?>"/>
                </div>
                <div class="col-md-4">
                    <h4><?php esc_html_e('Last name', 'motors'); ?>*</h4>
                    <input class="user_validated_field" type="text" name="stm_last_name"
                       value="<?php echo esc_attr($user_fields['last_name']); ?>"
                       placeholder="<?php esc_html_e('Enter Last Name', 'motors'); ?>"/>
                </div>
                <div class="col-md-4">
                    <h4><?php esc_html_e('Enter Phone', 'motors'); ?>*</h4>
                    <input class="user_validated_field" type="text" name="stm_phone"
                       value="<?php echo esc_attr($user_fields['phone']); ?>"
                       placeholder="<?php esc_html_e('Enter Phone', 'motors'); ?>"/>
                </div>
            </div>
        <?php endif; ?>


		<div class="clearfix">
			<div class="left-info left-dealer-info">
				<div class="stm-dealer-image-custom-view">
					<?php if ( ! empty( $user['logo'] ) ): ?>
						<img src="<?php echo esc_url( $user['logo'] ); ?>"/>
					<?php else: ?>
						<img src="<?php stm_get_dealer_logo_placeholder(); ?>"/>
					<?php endif; ?>
				</div>
				<h4><?php stm_display_user_name( $user['user_id'], $user_login, $f_name, $l_name ); ?></h4>

				<?php if ( ! empty( $ratings['average'] ) ): ?>
					<div class="stm-star-rating">
						<div class="inner">
							<div class="stm-star-rating-upper" style="width:<?php echo esc_attr( $ratings['average_width'] ); ?>"></div>
							<div class="stm-star-rating-lower"></div>
						</div>
						<div class="heading-font"><?php echo $ratings['average']; ?></div>
					</div>
				<?php endif; ?>

			</div>

			<div class="right-info">

				<a target="_blank" href="<?php echo esc_url( add_query_arg( array( 'view-myself' => 1 ), get_author_posts_url( $user_id ) ) ); ?>">
					<i class="fa fa-external-link"></i><?php esc_html_e( 'Show my Public Profile', 'motors' ); ?>
				</a>

				<div class="stm_logout">
					<a href="#"><?php esc_html_e( 'Log out', 'motors' ); ?></a>
					<?php esc_html_e( 'to choose a different account', 'motors' ); ?>
				</div>

			</div>

		</div>
	</div>

<?php else: ?>

	<div class="stm-add-a-car-user">
		<?php $user_fields = stm_get_user_custom_fields(''); ?>
        <?php if(false)://empty($user_fields['name']) || empty($user_fields['last_name']) || empty($user_fields['phone'])): ?>
            <div class="row form-group wrap_extended_fields">
                <div class="col-md-4">
                    <h4><?php esc_html_e('First name', 'motors'); ?>*</h4>
                    <input class="user_validated_field" type="text" name="stm_first_name"
                       value="<?php echo esc_attr($user_fields['name']); ?>"
                       placeholder="<?php esc_html_e('Enter First Name', 'motors') ?>"/>
                </div>
                <div class="col-md-4">
                    <h4><?php esc_html_e('Last name', 'motors'); ?>*</h4>
                    <input class="user_validated_field" type="text" name="stm_last_name"
                       value="<?php echo esc_attr($user_fields['last_name']); ?>"
                       placeholder="<?php esc_html_e('Enter Last Name', 'motors'); ?>"/>
                </div>
                <div class="col-md-4">
                    <h4><?php esc_html_e('Enter Phone', 'motors'); ?>*</h4>
                    <input class="user_validated_field" type="text" name="stm_phone"
                       value="<?php echo esc_attr($user_fields['phone']); ?>"
                       placeholder="<?php esc_html_e('Enter Phone', 'motors'); ?>"/>
                </div>
            </div>
        <?php endif; ?>


		<div class="clearfix">
			<div class="left-info">
				<div class="avatar">
					<?php if ( ! empty( $user['image'] ) ): ?>
						<img src="<?php echo esc_url( $user['image'] ); ?>"/>
					<?php else: ?>
						<i class="stm-service-icon-user"></i>
					<?php endif; ?>
				</div>
				<div class="user-info">
					<h4><?php stm_display_user_name( $user['user_id'], $user_login, $f_name, $l_name ); ?></h4>
					<div class="stm-label"><?php esc_html_e( 'Private Seller', 'motors' ); ?></div>
				</div>
			</div>

			<div class="right-info">
				<a target="_blank" href="<?php echo esc_url( add_query_arg( array( 'view-myself' => 1 ), get_author_posts_url( $user_id ) ) ); ?>">
					<i class="fa fa-external-link"></i><?php esc_html_e( 'Show my Public Profile', 'motors' ); ?>
				</a>
				<div class="stm_logout">
					<a href="#"><?php esc_html_e( 'Log out', 'motors' ); ?></a>
					<?php esc_html_e( 'to choose a different account', 'motors' ); ?>
				</div>
			</div>
		</div>
	</div>
<?php endif;

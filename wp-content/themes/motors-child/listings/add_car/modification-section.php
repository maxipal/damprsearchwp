<?php

$data = stm_get_single_car_listings();
$terms_args = array(
    'orderby'    => 'name',
    'order'      => 'ASC',
    'hide_empty' => false,
    'fields'     => 'all',
    'pad_counts' => true,
);
?>

<div class="modification-wrapper">

    <div class="stm-car-listing-data-single stm-border-bottom-unit ">
        <div class="title heading-font">
            <?php
            $mtitle = 'Modifications';
            esc_html_e($mtitle, 'motors-child')
            ?>
        </div>
    </div>

<?php
$car_id = $_GET['item_id'];
foreach ($data as $data_key => $data_unit):
    if ($data_unit['modifaction_option']) :
        $terms = get_terms($data_unit['slug'], $terms_args); ?>
        <div class="stm-form-1-quarter">
            <?php if (!empty($data_unit['numeric']) and $data_unit['numeric']): ?>
                <?php $value = '';
                if (!empty($car_id)) {
                    $value = get_post_meta($car_id, $data_unit['slug'], true);
                } ?>
                <input
                        type="number"
                        class="form-control <?php echo (!empty($value)) ? 'stm_has_value' : ''; ?>"
                        name="stm_s_s_<?php echo esc_attr($data_unit['slug']); ?>"
                        value="<?php echo esc_attr($value); ?>"
                        placeholder="<?php printf(esc_attr__('Enter %s %s', 'motors'), esc_attr__($data_unit['single_name'], 'motors'), (!empty($data_unit['number_field_affix'])) ? '(' . esc_attr__($data_unit['number_field_affix'], 'motors') . ')' : ''); ?>"
                />
            <?php else: ?>
                <select name="stm_s_s_<?php echo esc_attr($data_unit['slug']) ?>">
                    <?php $selected = '';
                    if (!empty($car_id)) {
                        $selected = get_post_meta($car_id, $data_unit['slug'], true);
                    }
                    ?>
                    <option value=""><?php printf(esc_html__('Select %s', 'motors'), esc_html__($data_unit['single_name'], 'motors')) ?></option>
                    <?php if (!empty($terms)):
                        foreach ($terms as $term): ?>
                            <?php
                            $selected_opt = '';
                            if ($selected == $term->slug) {
                                $selected_opt = 'selected';
                            } ?>
                            <option
                                    value="<?php echo esc_attr($term->slug); ?>" <?php echo esc_attr($selected_opt); ?>><?php echo esc_attr($term->name); ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            <?php endif; ?>
            <div class="stm-label">
                <?php if (!empty($data_unit['font'])): ?>
                    <i class="<?php echo esc_attr($data_unit['font']); ?>"></i>
                <?php endif; ?>
                <?php stm_dynamic_string_translation_e('Add A Car Step 1 Taxonomy Label', $data_unit['single_name']); ?>
            </div>
        </div>
    <?php endif; ?>
<?php endforeach; ?>

</div>

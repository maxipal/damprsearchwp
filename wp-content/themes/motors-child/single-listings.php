<?php
get_header();
stm_hybrid_include('single-car/page_bg');
stm_hybrid_include('single-car/title_box');
?>
<div class="stm-single-car-page">
	<?php
	$recaptcha_enabled = get_theme_mod('enable_recaptcha', 0);
	$recaptcha_public_key = get_theme_mod('recaptcha_public_key');
	$recaptcha_secret_key = get_theme_mod('recaptcha_secret_key');

	if (!empty($recaptcha_enabled) and $recaptcha_enabled and !empty($recaptcha_public_key) and !empty($recaptcha_secret_key)) {
		wp_enqueue_script('stm_grecaptcha');
	}
	?>

	<div class="container">
		<?php stm_hybrid_include('single-car/main'); ?>

		<div class="clearfix">
			<?php /*
				if ( comments_open() || get_comments_number() ) {
					comments_template();
				} */
			?>
		</div>
	</div> <!--cont-->
</div> <!--single car page-->
<?php get_footer(); ?>

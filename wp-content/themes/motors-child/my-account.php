<?php get_header(); ?>

<div class="stm-user-private <?php echo stm_is_dealer() ? 'stm-dealer-private' : '' ?>">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-3 hidden-sm hidden-xs stm-sticky-user-sidebar">
				<?php if ( stm_is_dealer() ) {
					get_template_part( 'partials/user/private/dealer-sidebar' );
				} else {
					get_template_part( 'partials/user/private/user-sidebar' );
				} ?>
			</div>
			<div class="col-md-9 col-sm-12">
				<?php get_template_part( 'partials/user/private/main' ) ?>
			</div>
		</div>
	</div>
</div>

	<style type="text/css">
		footer#footer {
			display: none;
		}
	</style>

	<script>
		jQuery(document).ready(function () {
			stm_private_user_height();

			<?php if(! empty( $_GET['stm_unmark_as_sold_car'] )): ?>
			window.history.pushState('', '', '<?php echo esc_url( stm_account_url( 'inventory' ) ); ?>');
			<?php endif; ?>

			<?php if(! empty( $_GET['stm_mark_as_sold_car'] )): ?>
			window.history.pushState('', '', '<?php echo esc_url( stm_account_url( 'inventory' ) ); ?>');
			<?php endif; ?>

			<?php if(! empty( $_GET['stm_disable_user_car'] )): ?>
			window.history.pushState('', '', '<?php echo esc_url( stm_account_url( 'inventory' ) ); ?>');
			<?php endif; ?>

			<?php if(! empty( $_GET['stm_enable_user_car'] )): ?>
			window.history.pushState('', '', '<?php echo esc_url( stm_account_url( 'inventory' ) ); ?>');
			<?php endif; ?>

			<?php if(! empty( $_GET['stm_move_trash_car'] )): ?>
			window.history.pushState('', '', '<?php echo esc_url( stm_account_url( 'inventory' ) ); ?>');
			<?php endif; ?>
		});

		jQuery(window).load(function () {
			stm_private_user_height();
		});

		jQuery(window).resize(function () {
			stm_private_user_height();
		});

		function stm_private_user_height() {
			var $ = jQuery;
			var windowH = $(window).outerHeight();
			var topBarH = $('#top-bar').outerHeight();
			var headerH = $('#header').outerHeight();

			var topH = 0;

			if (topBarH != null) {
				topH = topBarH;
			}

			if (headerH != null) {
				topH += headerH;
			}

			var minH = windowH - topH;

			$('.stm-user-private-sidebar').css({
				'min-height': minH + 'px'
			})
		}
	</script>

<?php get_footer(); ?>

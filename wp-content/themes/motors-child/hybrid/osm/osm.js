function createMap(elemId, centerLat, centerLng, zoom) {
	// Check if already initialized
	var container = L.DomUtil.get('map');
	if(container != null) container._leaflet_id = null;

	var map = new L.Map(elemId);

	// Data provider
	var osmUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
	var osmAttrib = 'Map data © <a href="https://openstreetmap.org">OpenStreetMap</a> contributors';

	// Layer
	var osmLayer = new L.TileLayer(osmUrl, {
		minZoom: 4,
		maxZoom: 20,
		attribution: osmAttrib
	});

	// Map
	map.setView(new L.LatLng(centerLat, centerLng), zoom);
	map.addLayer(osmLayer);
	return map;
}



function addMarker(map, latLng, onClick) {
	var marker = L.marker(latLng).addTo(map);
	if (onClick !== null) marker.on('click', onClick);
	return marker;
}





function osm_addcar_init_map(lat,lng,zoom){
	var map = L.DomUtil.get('addcar_location_map');
	if(map != null) map._leaflet_id = null;
	map = L.map('addcar_location_map').setView([lat, lng], zoom);
	map.scrollWheelZoom.disable();
	window.map = map;
	L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {
		attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
	}).addTo(map);
	var marker = L.marker([lat, lng]);
	marker.addTo(map);
	window.CenterMarker = marker;

	map.on('move', function (event) {
		if(window.CenterMarker){
			var cnt = map.getCenter();
			window.CenterMarker.setLatLng(cnt);
		}
	});

	map.on('moveend', function(event) {
		var lat = map.getCenter()['lat'];
		var lng = map.getCenter()['lng'];
		$('.stm-location-input-wrap input[name=lat]').val(lat).attr('value',lat);
		$('.stm-location-input-wrap input[name=lng]').val(lng).attr('value',lng);

		//Set address id moved pin
		fetch('https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat='+lat+'&lon='+lng).then(function(response){
			response.json().then(function(data) {
				$('.stm-location-input-wrap input[type=text]').val(data.display_name).attr('value',data.display_name);
				if(listing_form){
					_.each(listing_form.fields, function (value, key) {
						if(value.attribute == 'location'){
							listing_form.fields[key].value.address = data.display_name;
						}
					});
				}

			});
		});

		if(listing_form){
			_.each(listing_form.fields, function (value, key) {
				if(value.attribute == 'location'){
					listing_form.fields[key].value.coordinates = lat+','+lng;
				}
			});
		}

	});
}










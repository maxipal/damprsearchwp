(function ($) {
	"use strict";

	// All markers
	var markers = [];
	// Listing information grouped by coordinates
	var groups = {};
	// Listing IDs requested from server
	var requested = [];
	// current jqXHR requests
	var requests = [];

	var center = stm_hybrid_map.default_lat_lng.split(',');

	var popup = L.popup();
	var radius = stm_hybrid.distance_search;

	var content = '\
	<div class="stm_map_info_window_group_wrap stm_if_group_1 stm_if_group_no_scroll">\
		<div class="stm_if_scroll loading"></div>\
	</div>';


	function get_count_by_latlng(claster, res){
		var result = window.latlngSTR;

		if(claster._markers.length){
			for(var i in claster._markers){
				var latlng = Object.values(claster._markers[i]._latlng).join(',');
				res[latlng] = result[latlng];
			}
		}
		if(claster._childClusters.length){
			for(var cls in claster._childClusters){
				if(claster._childClusters[cls]._markers.length){
					for(var i in claster._childClusters[cls]._markers){
						var latlng = Object.values(claster._childClusters[cls]._markers[i]._latlng).join(',');
						res[latlng] = result[latlng];
					}
				}
				get_count_by_latlng(claster._childClusters[cls],res);
			}
		}
		return res;
	}

	function init() {
		$('form[data-trigger=inventory-map]').each(function () {
			var filter = new STMListings.Filter(this);
			filter.apiEndpoint = 'listings/coordinates';
			filter.ajaxDataAppend = {
				aggregations: 'all',
				context: 'map',
			};

			$(this).on('submit', function (event) {
				event.preventDefault();

				$(".stm_gmap").addClass("stm-loading");

				groups = {};
				markers = [];
				requested = [];
				resetRequests();
				filter.query();

			});

			$(this).on('result', function (event, data) {
				var latlng = [];
				var lcount = 1;
				var fdata = [];

				// Create array of filters
				$.each($(event.target).serializeArray(), function (i, field) {
					if (field.value !== '') {
						if (typeof fdata[field.name] === 'undefined') {
							fdata[field.name] = field.value
						} else if (typeof fdata[field.name] === 'object') {
							fdata[field.name].push(field.value)
						} else {
							fdata[field.name] = [fdata[field.name], field.value]
						}
					}
				});

				// Remove markers on change filter
				if(window.OSMMarkers) map.removeLayer(window.OSMMarkers);

				// Close opened popaps
				map.closePopup();


				// Create array with anly lat lng
				for(var i in data.data){
					latlng.push(data.data[i].latlng.split(','));
					lcount = lcount + data.data[i].count;
				}

				var markers = L.markerClusterGroup({
					maxClusterRadius: 40,
					//disableClusteringAtZoom: 15,

				});



				window.latlng = latlng;
				var latlngSTR = []; // List with received clasters

				for(var i in data.data){
					latlngSTR[data.data[i].latlng] = data.data[i].count;
					// Replace def marker with claster marker with label
					if(data.data[i].count > 1){
						var marker = L.marker(data.data[i].latlng.split(','),{
							icon: new L.DivIcon({
								className: 'marker-cluster marker-cluster-large marker-cluster-custom',
								html: '<div><span>'+data.data[i].count+'</span></div>'
							})
						});
					}else{ // Def marker
						var marker = L.marker(data.data[i].latlng.split(','));
					}

					// Add marker in claster
					markers.addLayer(marker);

					// Show popap in click marker
					marker.on('click', function(e){

						popup.setLatLng(e.latlng);
						var lat = e.latlng.lat;
						var lng = e.latlng.lng;

						var reqData = fdata
						reqData.stm_lat = lat;
						reqData.stm_lng = lng;
						reqData.max_search_radius = radius;
						reqData.featured = 'with';
						reqData.filters = 'all';
						reqData.context = 'list';
						reqData.per_page = '11';
						var zoom = window.map.getZoom();
						if(zoom > 7) reqData.max_search_radius = 10;
						var out = [];
						for (var key in reqData) {
						    if (reqData.hasOwnProperty(key)) {
						        out.push(key + '=' + encodeURIComponent(reqData[key]));
						    }
						}

						// Link on inventory
						var inv_url = '/'+stm_hybrid.listings_base_url+'/?'+out.join('&');
						// Get list of vehicles
						jQuery.ajax({
							type: 'GET',
							dataType: 'json',
							url: stm_hybrid.api_url+'listings',
							data: Object.assign({}, reqData),
							beforeSend:function(){
								jQuery(popup._wrapper).find('.stm_if_scroll').addClass('loading');
							},
							complete:function(){
								jQuery(popup._wrapper).find('.stm_if_scroll').removeClass('loading');
							},
							success: function(data){
								var content = '';
								for(var i in data.data){
									if(i == 10){
										content = content + '<div class="wrap_show_more"><a class="button" target="_blank" href="'+inv_url+'">Show more</a></div>';
										break;
									}
									// Image url
									if(data.data[i].gallery.length) var url = data.data[i].gallery[0].url;
									else var url = '';

									var price = '';

									if(data.data[i].price_sale){
										price = '<div class="regular-price">'+data.data[i].price_regular_view+'</div><div class="sale-price">'+data.data[i].price_view+'</div>';
									}else{
										if(typeof data.data[i].price == 'undefined') price = 'With agreement';
										else price = data.data[i].price_view;
									}
									// Popap html
									let mileage = data.data[i].attributes.mileage ? data.data[i].attributes.mileage.value : '';
									let make = data.data[i].attributes.make ? data.data[i].attributes.make.value : '';
									let transmission = data.data[i].attributes.transmission ? data.data[i].attributes.transmission.value : '';
									let year = data.data[i].attributes['ca-year'] ? data.data[i].attributes['ca-year'].value : '';
									let condition = data.data[i].attributes.condition ? data.data[i].attributes.condition.value : '';

									content = content + '\
									<a class="stm_iw_link" href="'+data.data[i].link+'">\
										<div class="stm_map_info_window_wrap">\
											<div class="stm_iw_condition">'+ condition +' '+year+'</div>\
											<div class="stm_iw_title">'+data.data[i].title+'</div>\
											<div class="stm_iw_car_data_wrap">\
												<div class="stm_iw_img_wrap">\
													<div style="background-image:url('+url+')"></div>\
												</div>\
												<div class="stm_iw_car_info">\
													<span class="stm_iw_car_opt stm_car_mlg">\
														<i class="stm-icon-road"></i>'+mileage+'\
													</span>\
													<span class="stm_iw_car_opt stm_car_engn">\
														<i class="stm-service-icon-body_type"></i>'+make+'\
													</span>\
													<span class="stm_iw_car_opt stm_car_trnsmsn">\
														<i class="stm-icon-transmission_fill"></i>'+transmission+'\
													</span>\
												</div>\
											</div>\
											<div class="stm_iw_car_price"><span class="stm_iw_price_trap"></span>'+price+'</div>\
										</div>\
									</a>';
								}
								jQuery(popup._wrapper).find('.stm_if_scroll').html(content);
							},
						});

						popup.setContent(content);
						popup.openOn(map);

						map.setView(e.latlng);
					});
				}


				window.latlngSTR = latlngSTR;

				markers.on('clusterclick', function (a) {

				});


				// Add clasters in map
				map.addLayer(markers);

				window.OSMMarkers = markers;

				// Recalculate vehicles on click claster
				for(var cl in markers._gridClusters){
					var cl_keys = Object.keys(markers._gridClusters[cl]._grid);

					for(var cl_key in cl_keys){
						var sub = markers._gridClusters[cl]._grid[cl_keys[cl_key]];
						var sub_keys = Object.keys(sub);
						// Zooms loop
						for(var sub_key in sub_keys){
							// One zoom
							var layer = sub[sub_keys[sub_key]][0];

							// calculate clasters in layer
							var count = get_count_by_latlng(layer,[]);
							var claster_count = 0;
							for(var i in count){
								claster_count = claster_count + count[i];
								if(count[i] == 0){
									claster_count = claster_count + latlngSTR[i];
								}

							}

							// Need optimize
							// if(claster_count == 0){
							// 	for(var i in layer._markers){
							// 		claster_count = claster_count + latlngSTR[Object.values(layer._markers[i]._latlng).join(',')];
							// 	}
							// }
							markers._gridClusters[cl]._grid[cl_keys[cl_key]][sub_keys[sub_key]][0]._childCount = claster_count;
						}

					}
				}



				// Update clasters
				markers.refreshClusters();


				// Set center on clasters
				// if(latlng && latlng.length){
				// 	var bounds = new L.LatLngBounds(latlng);
				// 	if(bounds) map.fitBounds(bounds);
				// }


				// setMarkers(data.data);
				$('.stm-inventory-map-cars-count [rel=count]').text(data.total);
				$(".stm_gmap").removeClass("stm-loading");


			});

			$(this).data('Filter', filter).submit();
		});
	}



	$('.stm-inventory-map-filter-wrap').on('click','input[type=submit]', function(event) {
		event.preventDefault();
		$('.stm-inventory-map-filter-wrap').find('form').submit();
	});


	jQuery(document).ready(function($) {
		init();
		if(jQuery('#stm_gmap').length){
			var map = L.map('stm_gmap').setView([40.98138838366699, 20.06588459014893], 3);
			map.scrollWheelZoom.disable();
			window.map = map;
			L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {
				minZoom: 2,
				maxZoom: 19,
				attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
			}).addTo(map);
		}


		if(jQuery('#stm-dealer-gmap').length){
			var map = L.map('stm-dealer-gmap').setView([0, 0], 4);
			map.scrollWheelZoom.disable();
			window.map = map;
			L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {
				attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
			}).addTo(map);

			var lat = jQuery('#stm-dealer-gmap').data('lat');
			var lng = jQuery('#stm-dealer-gmap').data('lng');
			var marker = L.marker([lat, lng]);
			map.addLayer(marker);
			map.setView(new L.LatLng(lat, lng), 15);

		}

		if(jQuery('.stm_vc_gmap').length){
			var map = L.map('stm_vc_gmap').setView([0, 0], 4);
			map.scrollWheelZoom.disable();
			window.map = map;
			L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {
				attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
			}).addTo(map);

			var lat = jQuery('#stm_vc_gmap').data('lat');
			var lng = jQuery('#stm_vc_gmap').data('lng');
			var text = jQuery('#stm_vc_gmap').data('popap');
			var zoom = jQuery('#stm_vc_gmap').data('zoom');
			var marker = L.marker([lat, lng]);
			map.addLayer(marker);
			map.setView(new L.LatLng(lat, lng), zoom);

			marker.on('click', function(e){
				popup
				.setLatLng(e.latlng)
				.setContent('<div class="stm_map_info_window_group_wrap stm_if_group_1 stm_if_group_no_scroll"><div class="stm_if_scroll">'+text+'</div></div>')
				.openOn(map);
			});

		}

	});


	function resetRequests() {
		requests.forEach(function (r) {
			r.abort();
		});

		requests = [];
	}



})(jQuery);

import { ListingItemsMixin } from "./includes";

(function ($) {
	$.urlParam = function(name){
		var results = new RegExp('\/?' + name + '\/?').exec(window.location.href);
		if (results==null) {
		   return null;
		}
		return decodeURI(results[1]) || 0;
	}


	if(jQuery('#private-inventory').length){
		window.privateInventory = new Vue({
			el: '#private-inventory',
			mixins: [ListingItemsMixin],
			data: {
				listings: [],
				status: 'all',
				itemsTotal: 1,
				itemsLoaded: false,
				user_image: null,
				notify_bids: [],
			},
			// props: ['notify_bids'],
			created: function () {
				this.getJson().then( (res) => {
					for(var i in this.listings){
						if(this.listings[i].subscription_expires_r){
							var date = new Date(this.listings[i].subscription_expires_r);
							var timezone = -1 * (date.getTimezoneOffset() / 60); // in hours
							date.setTime( date.getTime() + (timezone * 60 * 60 * 1000) + 5 * 60 * 60 * 1000 );
							$(this.$refs["countdown-"+this.listings[i].id])
								.countdown(date, function (event) {
									$(this).text(
										event.strftime('Left expired: %D days %H:%M:%S')
									);
								});
						}else{
							$(this.$refs["countdown-"+this.listings[i].id]).html("Listing expired!")
						}

					}
				}).catch((error) => {
					console.log(error)
				});
			},
			mounted: function () {
				var self = this;
				this.$refs.pagination.$on('page', function (page) {
					self.getJson(page);
				})
				this.user_image = stm_hybrid.cuser_avatar
				if(this.$el.getAttribute('data-notify_bids'))
					this.notify_bids = JSON.parse(this.$el.getAttribute('data-notify_bids'))
				$(self.$el).show();
			},
			methods: {
				getJson: function(page) {
					return new Promise((resolve, reject) => {
						var self = this;
						if($.urlParam('myauction')){
							var url = stm_hybrid.api_url + "listings/my?context=private&sort[updated_at]=desc&auction=1";
						}else{
							var url = stm_hybrid.api_url + "listings/my?context=private&sort[updated_at]=desc&auction=0";
						}

						if (page) {
							url += '&page=' + page;
						}

						if (this.status !== 'all') {
							url += '&where[status]=' + this.status;
						}

						$.ajax({
							url: url,
							dataType: 'json',
							headers: {
								Authorization: 'Bearer ' + stm_hybrid_auth.token
							},
							success: function (data) {
								self.listings = data.data;
								self.itemsTotal = data.meta.total;
								self.itemsLoaded = true;
								self.$refs.pagination.apply(data.meta.last_page, data.meta.current_page);

								for(var i in self.listings){
									if(typeof data.bids[self.listings[i].id] !== 'undefined'){
										self.listings[i].bids = data.bids[self.listings[i].id];

										for(var bid_index in data.bids[self.listings[i].id]){
											if(data.bids[self.listings[i].id][bid_index].accepted){
												self.listings[i].accepted = 1;
											}
										}
									}
								}

								for(var i in self.listings){
									self.listings[i].notify = 0;
									for(var j in self.listings[i].bids){
										var bid_id = self.listings[i].bids[j].id;
										if(self.notify_bids.find(bid => bid == bid_id)){
											self.listings[i].notify = self.listings[i].notify + 1;
										}
									}
								}
								self.itemsLoaded = true;
								resolve(data.data);
							},
							error: function (error) {
								reject(error)
							},
						});
					});

				},
				getUnSoldLink: function (listing) {
					return stm_hybrid_private.url+'?stm_unmark_as_sold_car='+listing.id;
				},
				getSoldLink: function (listing) {
					return stm_hybrid_private.url+'?stm_mark_as_sold_car='+listing.id;
				},
				getEnableLink: function (listing) {
					let expired = new Date(listing.subscription_expires_r);
					var timezone = -1 * (expired.getTimezoneOffset() / 60); // in hours
					expired.setTime(expired.getTime() + timezone * 60 * 60 * 1000 );

					let diff = expired.getTime() - new Date().getTime();
					if (diff < 0) diff = 0;
					if(diff){
						return stm_hybrid_private.url+'?stm_enable_user_car='+listing.id;
					}else{
						return stm_hybrid.price_url+'?lid='+listing.id;
					}
				},
				getDisableLink: function (listing) {
					return stm_hybrid_private.url+'?stm_disable_user_car='+listing.id;
				},
				getEditLink: function (listing) {
					return stm_hybrid.add_car_page_url + '?edit=1&item_id='+listing.id;
				},
				getTrashLink: function (listing) {
					return stm_hybrid_private.url+'?stm_move_trash_car='+listing.id;
				},
			}
		});
	}

	jQuery(document).ready(function () {
		$('.stm-sort-private-my-cars select').select2().on('change', function () {
			window.privateInventory.status = $(this).val();
			window.privateInventory.getJson();
		});


		$(document).on('click', 'a.confirm_accept', function(event) {
			event.preventDefault();
			var bid = $(this).data('id');
			var confirm = this;
			$.ajax({
				url: stm_hybrid.api_url + "bits/bitAccept",
				dataType: 'json',
				type: 'GET',
				data: {
					bit_id: bid
				},
				beforeSend: function(xhr){
					xhr.setRequestHeader('Authorization', 'Bearer ' + stm_hybrid_auth.token);
					$(confirm).parents('.panel-collapse').addClass('overlay');
				},
				complete:function(){
					$(confirm).parents('.panel-collapse').removeClass('overlay');
				},
				success: function (data) {
					$(confirm).parents('.panel-collapse').removeClass('overlay');
					$(confirm).parents('.panel-collapse').find('a.confirm_accept').hide();
					$(confirm).parents('td').html('<span class="accepted">Accepted</span>');
				}
			});
		});

		/*Stm confirmation before delete*/
		var urlToProceed = '';
		//Open confirmation
		$(document).on('click', '.stm-delete-confirmation', function (e) {
			e.preventDefault();

			urlToProceed = $(this).attr('href');
			var carTitle = $(this).data('title');

			$('.stm-delete-confirmation-popup').removeClass('stm-disabled');
			$('.stm-delete-confirmation-overlay').removeClass('stm-disabled');

			$('.stm-confirmation-text .stm-car-title').text(carTitle);
		});

		//Delete
		$('.stm-delete-confirmation-popup .actions .stm-red-btn').on('click', function (e) {
			e.preventDefault();
			window.location = urlToProceed;
		});

		//Cancel delete
		$('.stm-delete-confirmation-popup .actions .stm-grey-btn, .stm-delete-confirmation-overlay, .stm-delete-confirmation-popup .fa-close').on('click', function (e) {
			e.preventDefault();
			$('.stm-delete-confirmation-popup').addClass('stm-disabled');
			$('.stm-delete-confirmation-overlay').addClass('stm-disabled');
		});
	});



	if(jQuery('#auctions-inventory').length){

		let auctionInventory = new Vue({
			el: '#auctions-inventory',
			data: {
				listings: [],
				v_status: '',
				itemsTotal: 1,
				user_image: null,
				sortInventory: '',
				auction_offer: '',
				itemsLoaded: false,
			},
			created: function () {
				this.getJson();
			},
			mounted: function () {
				var self = this;
				this.$refs.pagination.$on('page', function (page) {
					self.getJson(page)
				});
				this.user_image = stm_hybrid.cuser_avatar
				$(".stm-sort-auction select").select2().on('select2:select', function (e) {
				    var event = new Event('change');
				    e.target.dispatchEvent(event);
				});
			},
			methods: {
				onSelected: function(event){
					this.getJson()
					this.v_status = $(event.target).val()
				},
				getJson: function(page) {
					var self = this;
					var url = stm_hybrid.api_url + "listings/auctions?stm_lat=0&stm_lng=0&max_search_radius=10000&sort_order=date_high&featured=with&filters=all&context=list&auction=1";

					if (page) {
						url += '&page=' + page;
					}

					if (this.v_status !== '') {
						url += '&v_status=' + this.v_status;
					}

					$.ajax({
						url: url,
						dataType: 'json',
						headers: {
							Authorization: 'Bearer ' + stm_hybrid_auth.token
						},
						success: function (data) {
							self.listings = data.data;
							self.itemsTotal = data.meta.total;
							self.$refs.pagination.apply(data.meta.last_page, data.meta.current_page);

							for(var i in self.listings){
								if(typeof data.bids[self.listings[i].id] !== 'undefined'){
									self.listings[i].bids = data.bids[self.listings[i].id];
									for(var bid_index in data.bids[self.listings[i].id]){
										if(data.bids[self.listings[i].id][bid_index].accepted){
											self.listings[i].accepted = 1;
										}
									}
								}
							}
							self.itemsLoaded = true;
						}
					});
				},
				getFirstImage: function (gallery) {
					return gallery[0] || false;
				},
				openGallery: function (gallery, boxType) {
					let urls = gallery.map(function (item) {
						return {
							href: item.url ? item.url : item
						};
					});

					jQuery.fancybox.open(urls, {
						padding: 0,
						type: boxType
					});
				},
				getImageSize: function (image, width, height, _crop) {
					let dir = image.directory;
					let name = image.filename;
					let crop = _crop ? 1 : 0;
					let url = stm_hybrid.thumb_url + '?';

					return url + 'src=/' + dir + '/' + name + '&w=' + width + '&h=' + height + '&zc=' + crop + '&q=90';
				},


				sendBid: function (auction) {
					let self = this;
					let data = {
						'vehicle': auction.id,
						'uid': stm_hybrid.uid,
						'offer': self.auction_offer,
					}
					let button = $(this.$el).find('.wrap_auction_offer_button button');
					let el = $(this.$el);
					$.ajax({
						type: 'POST',
						dataType: 'json',
						url: stm_hybrid.api_url + 'bits/create',
						data: data,
						headers: {
							Authorization: 'Bearer ' + stm_hybrid_auth.token
						},
						beforeSend:function(){
							button.addClass('loading');
						},
						complete:function(){
							button.removeClass('loading');
						},
						success: function(data){
							el.find('.wpcf7-response-output').html(data.message);
							el.find('.wpcf7-response-output').addClass('wpcf7-mail-sent-ok');
							el.find('.wpcf7-response-output').removeClass('wpcf7-display-none');
						},
						error: function(data){
							if(Object.keys(data.responseJSON.errors).length){
								el.find('.wpcf7-response-output').addClass('wpcf7-validation-errors');
								el.find('.wpcf7-response-output').removeClass('wpcf7-display-none');
								el.find('.wpcf7-response-output').html(Object.values(data.responseJSON.errors).join('<br>'));
							}
						}
					});
				},
				removeAuction: function (auction,event) {
					let self = this;
					let data = {
						'vehicle': auction.id,
						'uid': stm_hybrid.uid,
					}
					let button = event.target.tagName !== 'BUTTON' ? $(event.target).parent() : $(event.target);
					button.addClass('loading');
					let listings = self.listings;
					let el = $(self.$el);

					$.ajax({
						type: 'POST',
						dataType: 'json',
						url: stm_hybrid.api_url + 'bits/hide',
						data: data,
						headers: {
							Authorization: 'Bearer ' + stm_hybrid_auth.token
						},
						beforeSend:function(){
							button.addClass('loading');
						},
						complete:function(){
							button.removeClass('loading');
						},
						success: function(data){
							for(var i in self.listings){
								if(self.listings[i].id == auction.id){
									self.listings.splice(i,1);
								}
							}
						},
						error: function(data){
							if(Object.keys(data.responseJSON.errors).length){
								el.find('.wpcf7-response-output').addClass('wpcf7-validation-errors');
								el.find('.wpcf7-response-output').removeClass('wpcf7-display-none');
								el.find('.wpcf7-response-output').html(Object.values(data.responseJSON.errors).join('<br>'));
							}
						}
					});
				},
			}
		});
	}

})(jQuery);

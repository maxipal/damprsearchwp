Vue.component('listing-pagination', {
    data() {
        return {
            current: 0,
            last: 0,
        }
    },
    computed: {
        pages: function () {
            let c = this.current;
            let m = this.last;
            let current = c;
            let last = m;
            let delta = 2;
            let left = current - delta;
            let right = current + delta + 1;
            let range = [];
            let rangeWithDots = [];
            let l;

            range.push(1)
            for (let i = c - delta; i <= c + delta; i++) {
                if (i >= left && i < right && i < m && i > 1) {
                    range.push(i);
                }
            }
            range.push(m);

            for (let i of range) {
                if (l) {
                    if (i - l === 2) {
                        rangeWithDots.push(l + 1);
                    } else if (i - l !== 1) {
                        rangeWithDots.push('...');
                    }
                }
                rangeWithDots.push(i);
                l = i;
            }

            return rangeWithDots;
        }
    },
    methods: {
        getUrl: function(page) {
            return '?page=' + page;
        },
        apply: function(last, current) {
            this.current = current;
            this.last = last;
        },
        emitClick: function (page) {
            this.$emit('page', page);
        }
    }
});
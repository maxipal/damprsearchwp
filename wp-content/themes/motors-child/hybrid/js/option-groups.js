(function ($) {
	// Stores callback for ajax success event
	var ajaxCallbacks = [], doingAjax = false;

	STMCascadingSelect.prototype.selectbox = function (slug, config) {
		var parent = config.dependency, _this = this;

		if (!$(this.selector(slug), this.ctx).length || (parent && !$(this.selector(parent), this.ctx).length)) {
			return null;
		}

		if (config.hierarchical) {
			$(this.selector(slug), this.ctx).addClass('hierarchical')
		}

		$.each(config.options, function (i, option) {
			option.classes = [];
			if (typeof option.depth !== 'undefined') {
				option.classes.push('depth' + option.depth)
			}
			if (option.is_parent) {
				option.classes.push('is_parent')
			}
		});

		var requires = parent ? [this.selector(parent)] : null;
		if (slug === 'serie') {
			requires.push(this.selector('types'));
		}

		return {
			selector: this.selector(slug),
			paramName: slug,
			requires: requires,
			allowAll: config.allowAll,
			selected: $(this.selector(slug), this.ctx).data('selected'),
			source: function (request, response) {
				var options  = [];
				if (!parent) {
					response(options);
					return;
				}

				var selected = request[parent], category = null;
				if (slug === 'serie' && request.types) {
					selected = request.types + '-' + selected;
				}

				var remaining = false;

				function collectOptions() {
					$.each(config.options, function (i, option) {
						if ((config.allowAll && !selected) || (option.deps && option.deps.indexOf(selected) >= 0)) {
							// remove lonely parent option
							if (options.length && option.is_parent && options[options.length - 1].is_parent) {
								options.pop();
							}

							if (remaining && remaining.indexOf(option.value) < 0) {
								return;
							}

							options.push(option);
						}
					});

					// remove lonely parent option
					if (options.length && options[options.length - 1].is_parent) {
						options.pop();
					}

					response(options);
				}

				if ($('body').hasClass('home') && doingAjax) {
					ajaxCallbacks.push(function (data) {
						remaining = data.options[slug];
						collectOptions();
					});
				}
				else {
					collectOptions();
				}
			}
		};
	};

	var __fragments = STMListings.SearchBox.prototype.fragments;
	STMListings.SearchBox.prototype.fragments = function (data) {
		return __fragments.apply(this, arguments) + ',options,options-short';
	};

	var __beforeAjax = STMListings.SearchBox.prototype.beforeAjax;
	STMListings.SearchBox.prototype.beforeAjax = function () {
		__beforeAjax.apply(this, arguments);
		doingAjax = true;
	};


	var __success = STMListings.SearchBox.prototype.success;
	STMListings.SearchBox.prototype.success = function (data) {
		__success.apply(this, arguments);
		ajaxCallbacks.forEach(function (cb) {
			cb(data);
		});
		ajaxCallbacks = [];
		doingAjax = false;
	};

	 window.select2TemplateResult = function (data) {
		// We only really care if there is an element to pull classes from
		if (!data.element) {
			return data.text;
		}

		var $element = $(data.element);

		var $wrapper = $('<span></span>');
		$wrapper.addClass($element[0].className);

		$wrapper.text(data.text);

		return $wrapper;
	};

	$(function () {

		$("select:not(.hide)").select2({
			width: '100%',
			minimumResultsForSearch: Infinity,
			templateResult: select2TemplateResult
		});

		$('.classic-filter-row select').select2({
			minimumResultsForSearch: 1
		})
	});


})(jQuery);

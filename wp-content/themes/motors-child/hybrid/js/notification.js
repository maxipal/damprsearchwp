(function ($) {
	"use strict";

	class Notification{
		constructor(){
			// this.getNotifyTradeIn();
			this.timerUpdateUserCount();
			this.clearNotifyOnCollapse();
			this.clearNotifyOnPage();
		}

		timerUpdateUserCount(){
			var self = this;
			setInterval(function(){
				self.clearNotifyOnPage();

				let userCount = 0;
				$('.lOffer-account-dropdown .account-list').find('span.notify').each(function(index, el) {
					userCount = userCount + parseInt($(el).html());
				});

				if(userCount > 0){
					if($('.lOffer-account-unit .lOffer-account .notify').length){
						$('.lOffer-account-unit .lOffer-account .notify').html(userCount);
					}else{
						$('.lOffer-account-unit .lOffer-account').append('<span class="notify">'+userCount+'</span>');
					}
				}
			},1000);

			if($('.stm-actions-list a.active span.notify').length){
				$('.stm-actions-list a.active span.notify').animate(
					{opacity: 0},
					1000,
					function() {
						$(this).remove();
					}
				);
			}
		}
		getNotifyTradeIn(){
			fetch(ajaxurl+'?action=get_tradein').then(function(response){
				response.json().then(function(data) {
					// $('.account-list .notify_trade_in').append('<span class="notify">'+data.cnt+'</span>');
				});
			});
		}
		clearNotifyOnCollapse(){
			$(document).on('click', '.panel-group a[data-toggle=collapse]', function(event) {
				$(this).siblings('span.notify').animate({
					opacity: 0},
					1000, function() {
					$(this).remove();
				});
			});
		}
		clearNotifyOnPage(){
			if(window.location.search == '?myauction=1'){
				$('.lOffer-account-dropdown .notify_bids .notify').remove();
			}
			if(window.location.search == '?testdrive=1'){
				$('.lOffer-account-dropdown .notify_testdrive .notify').remove();
			}
			if(window.location.search == '?trade_in=1'){
				$('.lOffer-account-dropdown .notify_trade_in .notify').remove();
			}
			if(window.location.search == '?auction=1'){
				$('.lOffer-account-dropdown .notify_auction .notify').remove();
			}
		}
	}

	let notify = new Notification;



})(jQuery);
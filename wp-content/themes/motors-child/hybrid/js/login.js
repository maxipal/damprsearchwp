(function ($) {

	STMListings.stm_ajax_login = function () {
		$('.lOffer-account-unit').mouseout(function () {
			$('.stm-login-form-unregistered').removeClass('working');
		});
		$('.stm-forgot-password a').click(function (e) {
			e.preventDefault();
			$('.stm_forgot_password_send').slideToggle();
			$('.stm_forgot_password_send input[type=text]').focus();
			$(this).toggleClass('active');
		})

		$(".stm-login-form-mobile-unregistered form,.stm-login-form form:not(.stm_password_recovery), .stm-login-form-unregistered form").submit(function (e) {
			e.preventDefault();
			if (!$(this).hasClass('stm_forgot_password_send')) {
				$.ajax({
					type: "POST",
					url: ajaxurl,
					dataType: 'json',
					context: this,
					data: $(this).serialize() + '&action=stm_custom_login',
					beforeSend: function () {
						$(this).find('input').removeClass('form-error');
						$(this).find('.stm-listing-loader').addClass('visible');
						$('.stm-validation-message').empty();

						if ($(this).parent('.lOffer-account-unit').length > 0) {
							$('.stm-login-form-unregistered').addClass('working');
						}
					},
					success: function (data) {
						if (!_.isEmpty(data.token)) {
							window.stm_hybrid_auth = data.token;
						}
						if ($(this).parent('.lOffer-account-unit').length > 0) {
							$('.stm-login-form-unregistered').addClass('working');
						}
						if (data.user_html) {
							var $user_html = $(data.user_html).appendTo('#stm_user_info');
							$('.stm-not-disabled, .stm-not-enabled').slideUp('fast', function () {
								$('#stm_user_info').slideDown('fast');
							});

							$("html, body").animate({scrollTop: $('.stm-form-checking-user').offset().top}, "slow");
							$('.stm-add-a-car-login-overlay,.stm-add-a-car-login').toggleClass('visiblity');

							$('.stm-form-checking-user button[type="submit"]').removeClass('disabled').addClass('enabled');
						}

						if (data.restricted && data.restricted) {
							$('.btn-add-edit').remove();
						}

						$(this).find('.stm-listing-loader').removeClass('visible');
						for (var err in data.errors) {
							$(this).find('input[name=' + err + ']').addClass('form-error');
						}

						if (data.message) {
							var message = $('<div class="stm-message-ajax-validation heading-font">' + data.message + '</div>').hide();

							$(this).find('.stm-validation-message').append(message);
							message.slideDown('fast');
						}


						if (typeof (data.redirect_url) !== 'undefined') {
							window.location = data.redirect_url;
						}
					}
				});
			} else {
				/*Send passs*/
				$.ajax({
					type: "POST",
					url: ajaxurl,
					dataType: 'json',
					context: this,
					data: $(this).serialize() + '&action=stm_restore_password',
					beforeSend: function () {
						$(this).find('input').removeClass('form-error');
						$(this).find('.stm-listing-loader').addClass('visible');
						$('.stm-validation-message').empty();
					},
					success: function (data) {
						$(this).find('.stm-listing-loader').removeClass('visible');
						if (data.message) {
							var message = $('<div class="stm-message-ajax-validation heading-font">' + data.message + '</div>').hide();

							$(this).find('.stm-validation-message').append(message);
							message.slideDown('fast');
						}
						for (var err in data.errors) {
							$(this).find('input[name=' + err + ']').addClass('form-error');
						}
					}
				});
			}
		});

		$('.user_validated_field').hover(function () {
			$(this).removeClass('form-error');
		});

		$('input[name="stm_accept_terms"]').on('click', function () {
			if ($(this).is(':checked')) {
				$('.stm-login-register-form .stm-register-form form input[type="submit"]').removeAttr('disabled');
			} else {
				$('.stm-login-register-form .stm-register-form form input[type="submit"]').attr('disabled', '1');
			}
		});

		//disable motors stuff
		$(document).off('click', 'ul.stm-filter-chosen-units-list li > i')
	};

	STMListings.stm_ajax_registration = function () {
		$(".stm-register-form form").submit(function (e) {
			e.preventDefault();
			$.ajax({
				type: "POST",
				url: ajaxurl,
				dataType: 'json',
				context: this,
				data: $(this).serialize() + '&action=stm_custom_register',
				beforeSend: function () {
					$(this).find('input').removeClass('form-error');
					$(this).find('.stm-listing-loader').addClass('visible');
					$('.stm-validation-message').empty();
				},
				success: function (data) {
					if (!_.isEmpty(data.token)) {
						window.stm_hybrid_auth = data.token;
					}

					if (data.user_html) {
						var $user_html = $(data.user_html).appendTo('#stm_user_info');
						$('.stm-not-disabled, .stm-not-enabled').slideUp('fast', function () {
							$('#stm_user_info').slideDown('fast');
						});
						$("html, body").animate({scrollTop: $('.stm-form-checking-user').offset().top}, "slow");

						$('.stm-form-checking-user button[type="submit"]').removeClass('disabled').addClass('enabled');
					}

					if (data.restricted && data.restricted) {
						$('.btn-add-edit').remove();
					}

					$(this).find('.stm-listing-loader').removeClass('visible');
					for (var err in data.errors) {
						$(this).find('input[name=' + err + ']').addClass('form-error');
					}

					if (data.redirect_url) {
						window.location = data.redirect_url;
					}

					if (data.message) {
						var message = $('<div class="stm-message-ajax-validation heading-font">' + data.message + '</div>').hide();

						$(this).find('.stm-validation-message').append(message);
						message.slideDown('fast');
					}
				}
			});
		});
	};

	$.ajax({
		url: '/?action=stm_is_login',
		dataType: 'json',
		beforeSend: function () {
			$('.lOffer-account-dropdown').addClass('login_preloader');
		},
		success: function (data) {
			if (data) {
				if (data.avatar) {
					$('.lOffer-account-unit .lOffer-account').append(data.avatar);
				}
				$('.header-login-url').html(data.topbar);
				$('.lOffer-account-dropdown').remove();
				if (data.dropdown) {
					$('.lOffer-account-unit').append(data.dropdown);
				}
				if (data.dropdown_mobile) {
					$('#user-mobile-info').html(data.dropdown_mobile);
				}
			}

			$('.lOffer-account-dropdown').removeClass('login_preloader');
		}
	});

})(jQuery);

jQuery(document).ready(function ($) {
	$('#login-required').on('show.bs.modal', function () {
		$('[rel=login-link]', this).attr('href', function () {
			return $(this).attr('href').replace(/return=.*/, 'return=' + location.pathname)
		});
	});

	// make PS link goto page on desktop devices
	$('.lOffer-account').off('click').click(function (e) {
		if ($(this).hasClass('mobile')) {
			e.preventDefault();
		}
		$('.stm-opened-menu-listing').removeClass('opened');
		$('.stm-menu-trigger').removeClass('opened');
		$(this).toggleClass('active');
		$(this).closest('.lOffer-account-unit').find('.stm-user-mobile-info-wrapper').toggleClass('active');
	});
});

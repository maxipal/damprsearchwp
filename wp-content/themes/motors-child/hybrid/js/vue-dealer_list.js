jQuery(function ($) {
	// disable motors stuff
	$('.dealer-search-title select, .stm-sort-by-options select')
		.off('change')
		// when turning off change handlers
		// select2 will not update current value
		// re-applying select2 will fix this
		.select2();

	$(".dealer-search-results").each(function () {

		var dealerList = new Vue({
			el: this,
			data: {
				busy: false,
				dealers: dealers_result.data,
				sorting: "&sort[rating.count]=desc",
				replaceSort: true,
				page: 1,
				total: dealers_result.meta.total,
				last_page: dealers_result.meta.last_page,
				selectedManuf: '',
				location: '',
				keywordsRaw: '',
				inputLoc: '',
				firstLoad: true,
			},
			methods: {
				getJson: function (append) {
					this.busy = true;
					var self = this;
					if (this.selectedManuf === "") {
						var searchByManuf = ''
					} else {
						var searchByManuf = '&where[makes]=' + this.selectedManuf;
					}
					if (this.location === "") {
						var searchByLoc = ''
					} else {
						var searchByLoc = '&sort[location]=' + this.location + '|asc|mi';
						// this.sorting = ''
					}
					if (append) {
						this.page += 1;
					}
					else {
						this.page = 1;
					}

					self.firstLoad = false;

					$.getJSON(stm_hybrid.api_url + "users?where[is_dealer]=1" + this.sorting + searchByLoc + '&page=' + this.page + searchByManuf, function (result) {
						for(var i in result.data){
							var link = result.data[i].link;
							if(stm_hybrid.author_base && link.split('/')[2]){
								result.data[i].link = '/'+stm_hybrid.author_base+'/'+link.split('/')[2];
							}
						}
						if (append) {
							for(var i in result.data){
								self.dealers.push(result.data[i]);
							}
						} else {
							self.dealers = result.data;
						}
						self.total = result.meta.total;
						self.last_page = result.meta.last_page;
						self.busy = false
					});
				},
				dealerLabels(conditions) {
					var labels = [];
					if(!conditions) return;
					conditions.forEach(function (condition) {
						labels.push(condition.title)
					});

					return labels.join('/')
				}
			},
			filters: {
				formatDistance: function (value) {
					return parseFloat(value).toFixed(2);
				}
			}
		});

		$('.stm-select-sorting select').on('change', function (e) {
			var opt_val = $(this).val();

			switch (opt_val) {
				case 'alphabet':
					dealerList.sorting = "&sort[name]=asc";
					break;
				case 'reviews' :
					dealerList.sorting = "&sort[rating.average]=desc";
					break;
				case 'date':
					dealerList.sorting = "&sort[created_at]=asc";
					break;
				case 'cars' :
					dealerList.sorting = "&sort[count_listings]=desc";
					break;
				case 'watches':
					break;
			}

			dealerList.page = 1;
			dealerList.getJson();
		});

		$('[data-trigger=dealers-filter]').each(function () {
			var form = $(this);
			var filter = this;
			var selects = form.find('select');
			var $make = $('[name="make"]', filter);

			selects.each(function () {
				var select = $(this);
				var tax = select.attr('name');

				$.getJSON(stm_hybrid.api_url + "terms?where[taxonomy]=" + tax + "&source[]=title&source[]=slug&sort[title]=asc&per_page=999", function (result) {

					$.each(result.data, function (i, term) {
						var $option = $('<option />').attr('value', term.slug).html(term.title);
						select.append($option);
					});
				});
			});

			//selects.on('change', form.submit());

			$(this).on('submit', function (event) {
				event.preventDefault()
				event.stopPropagation()

				$('.wrap_first_search').html('');

				var latNum = $('input[name=stm_lat]', filter).val();
				var lngNum = $('input[name=stm_lng]', filter).val();

				dealerList.inputLoc = $('[ref=stm_location]', filter).val();

				if (latNum !== '' && lngNum !== '' && typeof latNum !== 'undefined' && typeof lngNum !== 'undefined') {
					dealerList.location = latNum + ',' + lngNum;
				} else {
					dealerList.location = '';
				}
				// dealerList.keywords = encodeURIComponent($('.stm_listing_search_keyword', filter).val());
				// typeof dealerList.keywords === 'undefined' ? dealerList.keywords = '' : dealerList.keywords;
				dealerList.sorting = '';
				dealerList.selectedManuf = $make.val();
				dealerList.getJson();
			});
		})
	});
});

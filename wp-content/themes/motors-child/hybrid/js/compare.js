if (typeof (STMListings) == 'undefined') {
	var STMListings = {};
}

(function ($) {
	"use strict";

	var Compare = STMListings.Compare = function () {
		this.init();
		this.setCount();
		this.activateLinks();
		this.bind();
	};

	Compare.prototype.init = function () {
		var ids = this.ids = [];
		$.each($.cookie(), function (key, value) {
			if (key.match(/^compare_ids/)) {
				ids.push(value);
			}
		});
	};

	Compare.prototype.setCount = function () {
		$('[data-contains=compare-count]').text(this.ids.length ? this.ids.length : '');
	};

	Compare.prototype.click1 = function (e) {
		e.preventDefault();

		var target = $(e.target).closest('[data-id]');
		var dataId = target.attr('data-id');
		var dataAction = target.attr('data-action');

		if (typeof dataAction === 'undefined') {
			dataAction = 'add';
		}

		if (typeof dataId === 'undefined') {
			return;
		}

		var stm_timeout;
		$.ajax({
			url: ajaxurl,
			type: "POST",
			dataType: 'json',
			data: '&post_id=' + dataId + '&post_action=' + dataAction + '&action=stm_ajax_add_to_compare',
			context: this,
			beforeSend: function (data) {
				target.addClass('disabled');
				clearTimeout(stm_timeout);
			},
			success: function (data) {
				$('.single-add-to-compare').addClass('single-add-to-compare-visible');

				if (typeof data.response != 'undefined') {
					$('.single-add-to-compare .stm-title').text(data.response);
				}

				if (typeof data.length != 'undefined') {
					$('.stm-current-cars-in-compare').text(data.length);
				}

				stm_timeout = setTimeout(function () {
					$('.single-add-to-compare').removeClass('single-add-to-compare-visible');
				}, 5000);

				if (data.status === 'danger') {
					return;
				}

				target.hide().siblings('[data-id]').show();
			},
			complete: function () {
				target.removeClass('disabled');
				clearTimeout(stm_timeout);
			}
		});
	};

	Compare.prototype.click2 = function (e) {
		e.preventDefault();

		var stm_timeout;
		var $el = $(e.currentTarget);
		var id = $el.data('id').toString();
		var stm_car_title = $el.data('title');

		if (this.ids.indexOf(id) === -1) {
			if (this.ids.length < 3) {
				$.cookie('compare_ids[' + id + ']', id, {expires: 7, path: '/'});
				this.ids.push(id);
				$el.addClass('active');

				//Added popup
				clearTimeout(stm_timeout);
				$('.single-add-to-compare .stm-title').text(stm_car_title + ' - ' + stm_added_to_compare_text);
				$('.single-add-to-compare').addClass('single-add-to-compare-visible');
				stm_timeout = setTimeout(function () {
					$('.single-add-to-compare').removeClass('single-add-to-compare-visible');
				}, 5000);
				//Added popup
			} else {
				//Already added 3 popup
				clearTimeout(stm_timeout);
				$('.single-add-to-compare .stm-title').text(stm_already_added_to_compare_text);
				$('.single-add-to-compare').addClass('single-add-to-compare-visible');
				stm_timeout = setTimeout(function () {
					$('.single-add-to-compare').removeClass('single-add-to-compare-visible');
					$('.single-add-to-compare').removeClass('overadded');
					$('.compare-remove-all').remove();
				}, 5000);
				//Already added 3 popup
				$('.single-add-to-compare').addClass('overadded');
				$('.compare-remove-all').remove();
				$('.single-add-to-compare .compare-fixed-link').before('<a href="#" class="compare-fixed-link compare-remove-all pull-right heading-font">' + resetAllTxt + '</a>');
			}
		} else {
			$.removeCookie('compare_ids[' + id + ']', {path: '/'});
			this.ids.splice(this.ids.indexOf(id), 1);
			$el.removeClass('active');

			//Deleted from compare text
			clearTimeout(stm_timeout);
			$('.single-add-to-compare .stm-title').text(stm_car_title + ' ' + stm_removed_from_compare_text);
			$('.single-add-to-compare').addClass('single-add-to-compare-visible');
			stm_timeout = setTimeout(function () {
				$('.single-add-to-compare').removeClass('single-add-to-compare-visible');
			}, 5000);
			//Deleted from compare text

			$('.single-add-to-compare').removeClass('overadded');
			$('.compare-remove-all').remove();
		}

		this.setCount();
	};

	Compare.prototype.remove = function (e) {
		e.preventDefault();
		var $el = $(e.currentTarget);
		var dataId = $el.attr('data-id');
		var dataAction = $el.attr('data-action');
		if (typeof dataId === 'undefined') {
			return;
		}

		$.ajax({
			url: ajaxurl,
			type: "POST",
			dataType: 'json',
			data: '&post_id=' + dataId + '&post_action=' + dataAction + '&action=stm_ajax_add_to_compare',
			context: this,
			beforeSend: function () {
				$el.addClass('loading');

				let $compare = $('.stm-current-cars-in-compare');
				if (parseFloat($compare.text()) > 0) {
					$compare.text(parseFloat($compare.text()) - 1);
				}

				$('.car-listing-row .compare-col-stm-' + dataId).hide('slide', {direction: 'left'}, function () {
					$('.car-listing-row .compare-col-stm-' + dataId).remove();
					$('.car-listing-row').append($('.compare-empty-car-top').html());
				});

				$('.stm-compare-row .compare-col-stm-' + dataId).hide('slide', {direction: 'left'}, function () {
					$('.stm-compare-row .compare-col-stm-' + dataId).remove();
					$('.stm-compare-row').append($('.compare-empty-car-bottom').html());
				});

				$('.row-compare-features .compare-col-stm-' + dataId).hide('slide', {direction: 'left'}, function () {
					$('.row-compare-features .compare-col-stm-' + dataId).remove();
					if ($('.row-compare-features .col-md-3').length < 2) {
						$('.row-compare-features').slideUp();
					}
				});
			}
		});
	};

	Compare.prototype.removeAll = function (e) {
		e.preventDefault();

		$.each(this.ids, function (i, id) {
			$.removeCookie('compare_ids[' + id + ']', {path: '/'});
		});

		this.ids = [];
		this.activateLinks();
		this.setCount();
		location.reload();
	};

	Compare.prototype.activateLinks = function (ctx) {
		$.each(this.ids, function (i, value) {
			if (!value) {
				return;
			}

			$('[data-compare-id=' + value + ']', ctx).each(function () {
				$('a', this).eq(0).show();
				$('a', this).eq(1).hide();
			});

			$('.stm-compare-directory-new, .stm-listing-compare, .stm-gallery-action-unit.compare', ctx)
				.filter('[data-id=' + value + ']')
				.addClass('active')
				.tooltip('destroy')
				.attr('title', stm_i18n.remove_from_compare)
				.tooltip()
			;
		});
	};

	Compare.prototype.bind = function () {
		$(document).on('click', '.add-to-compare', $.proxy(this.click1, this));
		$(document).on('click', '.stm-user-public-listing .stm-listing-compare, .image .stm-listing-compare, .listing-list-loop.stm-listing-directory-list-loop .stm-listing-compare, .stm-gallery-action-unit.compare, .stm-compare-directory-new', $.proxy(this.click2, this));
		$(document).on('click', '.remove-from-compare', $.proxy(this.remove, this));
		$(document).on('click', '.compare-remove-all', $.proxy(this.removeAll, this));
	};

	window.stm_compare = new Compare();

})(jQuery);

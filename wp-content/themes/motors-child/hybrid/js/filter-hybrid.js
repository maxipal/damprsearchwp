if (typeof (STMListings) == 'undefined') {
	var STMListings = {};
}

(function ($) {
	"use strict";

	function Filter(form) {
		this.ajax = null;
		this.form = form;
		this.target = document.getElementById($(form).data('target') || 'listings-result');
		this.apiEndpoint = 'listings';
		this.ajaxDataAppend = {};
		this.showCounts = false;
	}

	if($('select[name="filter[serie]"]').length){
        $(document).on('change','select[name="filter[make]"]', function(event) {
        	if($(this).val() !== ''){
        		$(this).parents('.filter').find('select[name="filter[serie]"]').prop("disabled", false);
        	}else{
        		$(this).parents('.filter').find('select[name="filter[serie]"]').prop("disabled", true);
        		$(this).parents('.filter').find('select[name="filter[serie]"]').val('');
        	}
        });
    }

	$('.stm-filter-listing-directory-filters select').on('change', function(){
		$(this).parents('form').submit();
	});

	$('select[name="filter[serie]"]').each(function(index, el) {
    	$(this).prop("disabled", false);
    });

	Filter.prototype.query = function (data) {
		this.performAjax(_.extend(this.formData(), this.ajaxDataAppend, data));
	};

	Filter.prototype.formData = function () {
		var data = {};

		$.each($(this.form).serializeArray(), function (i, field) {
			if (field.value !== '') {
				if (typeof data[field.name] === 'undefined') {
					data[field.name] = field.value
				} else if (typeof data[field.name] === 'object') {
					data[field.name].push(field.value)
				} else {
					data[field.name] = [data[field.name], field.value]
				}
			}
		});

		// When no lat,lng params is set
		// no reason to pass search radius
		if (!data.stm_lat && !data.stm_lat) {
			delete data.max_search_radius
		}
		if(typeof data['filter[price][gte]'] !== 'undefined'){
			data['filter[price][gte]'] = data['filter[price][gte]'].replace(' ','');
		}
		if(typeof data['filter[price][lte]'] !== 'undefined'){
			data['filter[price][lte]'] = data['filter[price][lte]'].replace(' ','');
		}
		// console.log(data);

		return data;
	};

	Filter.prototype.unsetFilter = function (filter) {
		let self = this;

		switch (filter.type) {
			case 'select':
				let select = $('select[key=' + filter.slug + ']');

				select.val('').change();

				break;
			case 'slider':
				let slider = $("div[key=" + filter.slug + "]", self.form);
				let minInput = $("input[name='filter[" + filter.slug + "][gte]']", self.form);
				let maxInput = $("input[name='filter[" + filter.slug + "][lte]']", self.form);
				let minVal = slider.slider("option", "min");
				let maxVal = slider.slider("option", "max");

				slider.slider("option", "values", [minVal, maxVal]);
				minInput.val('');
				maxInput.val('');
				self.performAjax(_.extend(this.formData(), this.ajaxDataAppend));

				break;
		}
	};

	Filter.prototype.paginationClick = function (page) {
		this.performAjax(_.extend(this.formData(), this.ajaxDataAppend, {page: page}));

		jQuery('html, body').animate({
			scrollTop: jQuery(".archive-listing-page").offset().top
		}, 500);
	};

	Filter.prototype.performAjax = function (data) {
		this.ajax && this.ajax.abort();

		var query = [];
		if(typeof data['filter[make]'] == 'undefined' && typeof data['filter[serie]'] !== 'undefined') delete data['filter[serie]'];

		if(typeof data['filter[mileage][gte]'] !== 'undefined'){
			data['filter[mileage][gte]'] = data['filter[mileage][gte]'].replace(/ /g,'');
		}
		if(typeof data['filter[mileage][lte]'] !== 'undefined'){
			data['filter[mileage][lte]'] = data['filter[mileage][lte]'].replace(/ /g,'');
		}

		_.each(data, function (value, key) {
			if (typeof value === 'object') {
				_.each(value, function (v) {
					query.push(key + '=' + v)
				})
			} else {
				query.push(key + '=' + value);
			}
		});


		if(stm_hybrid.is_auction){
			stm_hybrid.listings_base_url  = 'auction';
			query.push('auction=1');
		}else{
			query.push('auction=0');
		}
		query = query.join('&');
		this.ajax = $.ajax({
			url: stm_hybrid.api_url + this.apiEndpoint + '?' + query,
			dataType: 'json',
			context: this,
			beforeSend: this.ajaxBefore,
			success: this.ajaxSuccess,
			error: this.ajaxError,
			complete: this.ajaxComplete
		});
	};

	Filter.prototype.ajaxBefore = function () {
		/*Add filter preloader*/
		this.getTarget().addClass('stm-loading');

		/*Add selects preloader*/
		$('.select2-container--default .select2-selection--single .select2-selection__arrow b', this.form).addClass('stm-preloader');
	};

	Filter.prototype.ajaxSuccess = function (res) {
		/*Append new html*/
		this.appendData(res);

		/*Remove select preloaders*/
		$('.select2-container--default .select2-selection--single .select2-selection__arrow b', this.form).removeClass('stm-preloader');

		/*Disable useless selects*/
		this.processFilters(res);

		/*Reinit js functions*/
		this.reInitJs();
	};

	Filter.prototype.ajaxError = function () {
		this.getTarget().removeClass('stm-loading');
	};

	Filter.prototype.ajaxComplete = function () {
		this.getTarget().removeClass('stm-loading');
	};


	Filter.prototype.reInitJs = function () {
		//stButtons.locateElements();
		//$("img.lazy").lazyload();
		//$('.stm-tooltip-link, div[data-toggle="tooltip"]').tooltip();
		STMListings.initVideoIFrame();

		$('.stm-shareble').hover(function () {
			$(this).parent().find('.stm-a2a-popup').addClass('stm-a2a-popup-active');
		}, function () {
			$(this).parent().find('.stm-a2a-popup').removeClass('stm-a2a-popup-active');
		});

		$(".a2a_dd").each(function () {
			a2a.init('page');
		});

	};

	Filter.prototype.appendData = function (data) {
		$(this.form).trigger('result', data);
	};

	Filter.prototype.processFilters = function (res) {
		if ($('select[name="filter[serie]"]', this.form).length) {
			$('select[name="filter[make]"]', this.form).each(function (index, el) {
				$(el).parents('.filter').find('select[name="filter[serie]"]').prop("disabled", $(el).val() === '');
			});
		}

		if (typeof res.filters === 'undefined') {
			return;
		}

		var _this = this;
		$.each(res.filters, function (key, filter) {
			switch (filter.type) {
				case 'select':
					_this.processSelectFilters(key, filter);
			}
		});
	};

	Filter.prototype.processSelectFilters = function (key, filter) {
		var self = this,
			$select = $('select[key=' + filter.key + ']', this.form),
			multiple,
			selected = $select.val();

		multiple = $select.prop('multiple');

		$select.find('option').remove();

		// new collect to groups
		let parents = [];
		let childs = [];
		self.options = [];
		let placeholder = {};
		$.each(filter.options, function (value, option) {
			if(!option.value){
				let label = option.label;
				if(stm_hybrid.is_listing_page)
					label = label.replace('Choose ', '');
				option.label = label;
				placeholder = option;
				return true;
			}
			// childs
			if(typeof option.parent_id !== 'undefined' && option.parent_id){
				childs.push(option)
			}
			if(typeof option.parent_id == 'undefined' || !option.parent_id){ // groups
				parents.push(option)
			}
		});


		let arr = [];
		_.each(parents, function (parent) {
			let parent_id = parent.parent_id;
			let sub = []
			_.each(childs, function (child) {
				if(child.parent_id == parent.value) sub.push(child)
			})
			arr[parent.value] = {
				label: parent.label,
				value: parent.value,
				sub: sub,
				selected: parent.selected,
				disabled: parent.disabled,
				count: parent.count
			};
		})

		arr.sort(function(a,b){
			if (a.label > b.label) return 1;
			if (a.label < b.label) return -1;
			return 0;
		});

		self.options.push(placeholder);
		for(var i in arr){
			var temp = {
				label: arr[i].label,
				value: arr[i].value,
				class: 'is_parent',
				selected: arr[i].selected,
				disabled: arr[i].disabled,
				count: arr[i].count,
			}
			if(arr[i].sub.length){
				temp.class = temp.class + ' has_child';
			}
			self.options.push(temp);

			if(arr[i].sub.length){
				for(var j in arr[i].sub){
					var child = {
						label: arr[i].sub[j].label,
						value: arr[i].sub[j].value,
						class: 'child childof_'+arr[i].sub[j].parent_id,
						selected: arr[i].sub[j].selected,
						disabled: arr[i].sub[j].disabled,
						count: arr[i].sub[j].count,
					}
					self.options.push(child);
				}
			}
		}

		$.each(self.options, function (value, option) {
			if(!option.label) return true;
			var optionClass = '',
				label = option.label;
			if (self.showCounts && typeof option.count !== 'undefined') {
				label += ' (' + option.count + ')'
			}

			var $option = $('<option />').attr('value', option.value).addClass(option.class).html(label);

			if (multiple && null !== selected) {
				if (selected.indexOf(option.value) !== -1) {
					$option.prop('selected', 1)
				}
			} else {
				if (option.value === selected) {
					$option.prop('selected', 1)
				}
			}

			$select.append($option);
		});
		// end new collect to groups

		$select.val(selected).select2({dropdownCssClass: "showDisabledOption tax_simgle", templateResult: select2TemplateResult});
	};

	Filter.prototype.setViewType = function (type) {
		listingApp.viewType = type;
	};

	Filter.prototype.getTarget = function () {
		return $(this.target);
	};

	STMListings.Filter = Filter;

})(jQuery);

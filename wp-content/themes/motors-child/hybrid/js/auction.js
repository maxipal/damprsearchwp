(function ($) {
	"use strict";

	if($('#mybits_list').length){
		jQuery.ajax({
			type: 'POST',
			data: {
				'uid': stm_hybrid.uid,
			},
			dataType: 'json',
			url: stm_hybrid.api_url + 'bits/my',
			beforeSend: function(xhr){
				xhr.setRequestHeader('Authorization', 'Bearer ' + stm_hybrid_auth.token);
			},
			success: function(data){
				if(data.data.length){
					var bits = '';
					for(var i in data.data){
						var accepted = '';

						var offers = [];
						for(var index in data.data[i].offers){
							if(data.data[i].offers[index].accepted){
								offers.push('<span>'+data.data[i].offers[index].offer+'</span>');
								data.data[i].accepted = data.data[i].offers[index].accepted;
							}else{
								offers.push(data.data[i].offers[index].offer);
							}
						}

						if(data.data[i].accepted && data.data[i].accepted == data.data[i].myaccepted) accepted = '<span>Accepted</span>';
						else if(data.data[i].accepted && data.data[i].accepted !== data.data[i].myaccepted) accepted = '<span class="decline">Decline</span>';
						bits = bits + '\
						<li class="panel panel-default">\
							<div class="panel-heading">\
								<h4 class="panel-title">\
									<a class="'+data.data[i].vehicle_link+'"	data-toggle="collapse" data-parent="#accordion" href="#collapse_'+i+'">\
										Offer on '+data.data[i].vehicle_name+' '+accepted+'\
									</a>\
								</h4>\
							</div>\
							<div id="collapse_'+i+'" class="panel-collapse collapse">\
								<div class="panel-body">\
									<div class="tabs_information width50">\
										<div>\
											<h5>Vehicle Information</h5>\
											<a target="_blank" href="'+data.data[i].vehicle_link+'">'+data.data[i].vehicle_name+'</a>\
										</div>\
										<div>\
											<h5>Offer</h5>\
											<p>'+offers.join('<br>')+'</p>\
										</div>\
									</div>\
								</div>\
							</div>\
						</li>';
					}
					jQuery('.mybits_list').html(bits);
				}else{
					jQuery('.mybits_list').after('<h4 style="text-align:center;margin-top:40px;">Bits not found</h4>');
				}

			},
			error: function(data){
				console.log(data);
			}
		});
	}



})(jQuery);
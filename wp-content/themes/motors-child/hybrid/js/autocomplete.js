jQuery(document).ready(function($) {
	// Auto-complete location by OpenStreetMap
	Vue.component('auto-complete-map', {
		template: '\
			<div class="auto-complete complete-map">\
				<input v-model="keywords" :placeholder="placeholder" class="form-control" name="stm_location" type="text" v-on:keydown="debounceInput" @focus="focus" @blur="focus" autocomplete="off">\
				<div class="pac-container pac-logo" v-if="(focused && this.data.length > 0)">\
					<div class="pac-item" v-for="(person, i) in data" v-on:click="searchClicked" :data-lat="person.lat" :data-lng="person.lon">\
				    	<span class="pac-icon pac-icon-marker"></span>\
				    	<span class="pac-item-query">\
				    		<span class="pac-matched" :title="person.display_name">{{ person.display_name }}</span>\
				    	</span>\
				    </div>\
				    <div class="stm-map-location">\
						<span>Powered by <strong>Nominatim</strong></span>\
						<a data-toggle="modal" href="#stm_pickup_location">\
							<img src="https://nominatim.openstreetmap.org/ui/assets/images/osm_logo.120px.png" alt="logo">\
						</a>\
					</div>\
				</div>\
			</div>\
		',
		methods: {
			debounceInput: _.debounce(function (e) {
				this.filterKey = e.target.value;
				var self = this;
				fetch('https://nominatim.openstreetmap.org/search.php?q='+this.filterKey+'&format=json').then(function(response){
					response.json().then(function(data) {
						self.data = data;
						self.Olddata = data;
					});
				});
			}, 500),
			searchClicked(event) {
				if($(event.target).hasClass('pac-item')) var target = $(event.target);
				else var target = $(event.target).parents('.pac-item');
				var lat = target.data('lat');
				var lng = target.data('lng');
				var name = target.find('.pac-matched').text();

				$(event.target).parents('.stm-location-search-unit').find('input[name=stm_lat]').val(lat).attr('value',lat);
				$(event.target).parents('.stm-location-search-unit').find('input[name=stm_lng]').val(lng).attr('value',lng);
				$(event.target).parents('.auto-complete').find('input[type=text]').val(name).attr('value',name);
				this.keywords = name;

				if($("form[data-trigger=inventory]").length){
					$("form[data-trigger=inventory]").find('select:first').trigger('change');
				}
			},
			focus(event) {
				var self = this;
				// Click work before blur
				setTimeout(function(){
					if(event.type == 'focus'){
						self.focused = true;
						event.target.select();
					}else{
						self.focused = false;

						if($(event.target).val() == ''){
							$(event.target).parents('.stm-location-search-unit').find('input[name=stm_lat]').val('').attr('value','');
							$(event.target).parents('.stm-location-search-unit').find('input[name=stm_lng]').val('').attr('value','');
						}
					}

				}, 200);
			},

		},
		data() {
			// Get saved location
			var _address = null,
				_coordinates = '';

			if(_coordinates){
				if(window.CenterMarker){
					window.CenterMarker.setLatLng(_coordinates.split(','));
					window.map.setView(_coordinates.split(','),14);
				}
			}
			return {
				coordinates: _coordinates,
				input: '',
				focused: false,
				data: [],
				keywords: _address,
				location: '',
				placeholder: "Enter the address"
			}
		},
		mounted() {
			if(this.$attrs.location){
				$(this.$el).find('input[type=text]').val(this.$attrs.location)
			}
			if(this.$attrs.placeholder){
				this.placeholder = this.$attrs.placeholder;
			}
		},
		created() {
			this.input = this.value || ''
		},
	});

	if($('#search_vue_app').length){
		var ListingMapForm = new Vue({
			el: '#search_vue_app',
		});
	}


	// Native jQuery auotcomplete
	if($('.single_location').length){
		// Select text in focus input field
		$(".single_location").click(function () {
			$(this).select();
			$(this).siblings('input[name=stm_lat]').val('');
			$(this).siblings('input[name=stm_lng]').val('');
		});
		$( ".single_location" ).each(function(index, el) {
			var temp = $(el).autocomplete({
				source: function( request, response ) {
					fetch('https://nominatim.openstreetmap.org/search.php?q='+request.term+'&format=json').then(function(data){
						data.json().then(function(res) {
							var temp = [];
							for(var i in res){
								temp.push({
									id: res[i].place_id,
									label: res[i].display_name,
									value: res[i].display_name,
									lat: res[i].lat,
									lng: res[i].lon,
								})
							}
							response( temp );
						});
					});
				},
				minLength: 2,
				open: function() {

				},
				select: function( event, ui ) {
					$(event.target).siblings('input[name=stm_lat]').val(ui.item.lat)
					$(event.target).siblings('input[name=stm_lng]').val(ui.item.lng)
					$(event.target).parents('form').find('select:first').trigger('change');
				},
			});
			// Self render dropdown
			temp.data('ui-autocomplete')._renderItem = function(ul, item) {
				return $( '<div class="pac-item"><span class="pac-icon pac-icon-marker"></span><span class="pac-item-query"><span title="'+item.label+'" class="pac-matched">'+item.label+'</span></span></div>' ).appendTo( ul );
			}
			temp.data('ui-autocomplete')._renderMenu = function( ul, items ) {
				var that = this;
				$.each( items, function( index, item ) {
					that._renderItemData( ul, item );
				});
				$(ul).attr('class','pac-container pac-logo');
  				if(!$(ul).parent().hasClass('auto-complete')) $(ul).wrap('<div class="auto-complete"></div>');

			}
		});

	}

});// ready

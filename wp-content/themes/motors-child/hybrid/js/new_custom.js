jQuery(document).ready(function ($) {

    $('.stm_dynamic_listing select').select2({
        minimumResultsForSearch: 0
    });

    $(document).on('click', '.stm-show-number_custom', function () {
        // alert('click');
        $(this).addClass('.stm-show-number');
        var phone = $(this).attr('data-id');
        $(this).parent().find('.phone').html("<a href='tel:" + phone + "'>" + phone + "</a>");
        $(this).hide();
    });

});


jQuery(function ($) {

    $('.compare-buttons .save-compare').on('click', function (e) {
        e.preventDefault();
        var planeId = [];
        var title = $('.compareTitle').val();
        $('.compare-col-stm').each(function () {
            var id = $(this).find('span[data-action="remove"]').attr('data-id');
            planeId.push(id);
        });
        $.ajax({
            url: ajaxurl,
            type: "POST",
            dataType: 'json',
            data: 'planes_id=' + planeId + '&action=air_add_to_user_compare&compare_title=' + title,
            success: function (data) {
                if (data.title) {
                    $('.title-dropdown .messege').addClass('success');
                    window.location.href = data.url;
                }
                else {
                    $('.title-dropdown .messege').removeClass('success');
                }

                $('.title-dropdown .messege').text(data.messege);

                setTimeout(function () {
                    $('.title-dropdown .collapse').removeClass('in');
                }, 1000);
            }
        });
    });


    $('.remove-from-private-compare').on('click', function () {
        var planeID = $(this).attr('data-id');
        var compareName = $(this).attr('data-title');
        var button = $(this);
        $.ajax({
            url: ajaxurl,
            type: "POST",
            dataType: 'html',
            data: 'plane_id=' + planeID + '&action=air_remove_user_compare&compare_title=' + compareName,
            success: function (data) {
                if (data == 'empty') {
                    button.closest('.row').remove();
                }
                else {
                    button.closest('.col-md-3').remove();
                }

            }
        });
    });


    $('#shareModal, #shareModalProfile').on('show.bs.modal', function (event) {
        var $button = $(event.relatedTarget);
        var url = $button.data('url');
        $('#copyInput').val(url);
        var message;
        if (message = $button.data('message')) {
            $('.compare-email-form-wrap textarea').val(message);
        }
    });

    $('.remove-compare-row').on('click', function () {
        var compare = $(this).attr('data-title');
        var button = $(this);
        $.ajax({
            url: ajaxurl,
            type: "POST",
            dataType: 'html',
            data: 'compare=' + compare + '&action=air_remove_user_compare_row',
            success: function (data) {
                button.closest('.row').remove();
            }
        });
    });


    $('.compare-email-form').on('submit', function (e) {
        e.preventDefault();
        var form = $(this);
        var email = form.find('input[name=compare-email]').val();
        var text = form.find('textarea[name=body-text]').val();
        var recaptcha = form.find('#g-recaptcha-response').val();

        var senderEmail = form.find('input[name=sender-email]').val();
        var senderPhone = form.find('input[name=sender-phone]').val();
        var senderFirstName = form.find('input[name=sender-f-name]').val();
        var senderLastName = form.find('input[name=sender-l-name]').val();
        $.ajax({
            url: ajaxurl,
            type: "POST",
            dataType: 'json',
            data: {
                'email': email,
                'body-text': text,
                'recaptcha': recaptcha,
                'sender-email': senderEmail,
                'sender-phone': senderPhone,
                'sender-f-name': senderFirstName,
                'sender-l-name': senderLastName,
                'action': 'send_compare_email'
            },
            success: function (data) {
                form.find('.share-result span').text(data.messege);
                setTimeout(function () {
                    form.closest('.modal-dialog').find('.close').trigger('click');
                    form.find('.share-result span').text('');
                }, 1000);

            }
        });
    });

    $('.profile-email-form').on('submit', function (e) {
        e.preventDefault();
        var form = $(this);
        var email = form.find('input[name=compare-email]').val();
        var text = form.find('textarea[name=body-text]').val();
        var recaptcha = form.find('#g-recaptcha-response').val();

        var senderEmail = form.find('input[name=sender-email]').val();
        var senderPhone = form.find('input[name=sender-phone]').val();
        var senderFirstName = form.find('input[name=sender-f-name]').val();
        var senderLastName = form.find('input[name=sender-l-name]').val();
        $.ajax({
            url: ajaxurl,
            type: "POST",
            dataType: 'json',
            data: {
                'email': email,
                'body-text': text,
                'recaptcha': recaptcha,
                'sender-email': senderEmail,
                'sender-phone': senderPhone,
                'sender-f-name': senderFirstName,
                'sender-l-name': senderLastName,
                'action': 'send_profile_email'
            },
            success: function (data) {
                form.find('.share-result span').text(data.messege);
                setTimeout(function () {
                    form.closest('.modal-dialog').find('.close').trigger('click');
                    form.find('.share-result span').text('');
                }, 1000);

            }
        });
    });


    $('.compare-contact-seller button').on('click', function () {
        var userId = $(this).attr('user-id');
        var airId = $(this).attr('air-id');
        var modal = $('#share-contactform');
        modal.find('input[name=stm_changed_recepient]').val(userId);
        modal.find('input[name=stm_changed_aircraft]').val(airId);
    });

    $('#main').click(function (e) {
        if (!$(e.target).is('.save-sign-up')) {
            $('.lOffer-account-dropdown').css({
                "visibility": "hidden",
                "opacity": 0
            });
        }
    });
    $('.save-sign-up').on('click', function (e) {
        e.preventDefault();
        $('.lOffer-account-dropdown').css({
            "visibility": "visible",
            "opacity": 1
        });
    });

    $('.hide-show-sidebar').on('click', function () {
        $('.side-filters').slideToggle();
        $('.sidebar-action-units a.button').toggleClass('reset-filter');
        $(this).toggleClass('open');
        if ($(this).hasClass('open')) {
            $(this).text('Hide Filters');
        }
        else $(this).text('Show Filters');
    });


    $('.stm-footer-search-name-input').focus(function () {
        $(this).closest('.search-suggestions').addClass('active');
    });

    $('.stm-footer-search-name-input').blur(function () {
        $(this).closest('.search-suggestions').removeClass('active');
    });
});

function copyFunction() {
    /* Get the text field */
    var copyText = document.getElementById("copyInput");

    /* Select the text field */
    copyText.select();

    /* Copy the text inside the text field */
    document.execCommand("Copy");

}


(function ($) {
    // Testdrive
    $(document).on('click','.testdrive_accept,.testdrive_decline', function(event) {
        event.preventDefault();
        var btn = this;
        var ac_dec = ($(btn).hasClass('testdrive_accept') ? true: false);

        // <div id="spinner"><i class="fa fa-spinner"></i></div>
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/wp-admin/admin-ajax.php',
            data: {
                'action': 'testdrive_accept', //calls wp_ajax_nopriv_actionName
                'time': $(btn).parents('.panel-collapse').find('input.stm-date-timepicker').val(),
                'reason': $(btn).parents('.panel-collapse').find('textarea').val(),
                'accept': ac_dec,
                'test_id': $(btn).parents('.panel-collapse').data('id'),
            },
            beforeSend:function(){
                $(btn).parents('.panel-collapse').addClass('overlay');
            },
            complete:function(){
                $(btn).parents('.panel-collapse').removeClass('overlay');
            },
            success: function(data){
                if(data.message){
                    $(btn).parents('.panel-collapse').find('.testdrive_responce').html('<p>'+data.message+'</p>');
                }
                $(btn).parents('.testdrive_btn_wrap').hide();
                $(btn).parents('.panel-body').find('.form_wrap').hide();
            },
        });
    });

    // (function(window, location) {
    //     window.addEventListener("popstate", function() {
    //       if(location.hash === "#!/stealingyourhistory") {
    //             setTimeout(function(){
    //             	// $('.filter.filter-sidebar').trigger('submit');
    //             },0);
    //       }
    //     }, false);
    // }(window, location));
})(jQuery);

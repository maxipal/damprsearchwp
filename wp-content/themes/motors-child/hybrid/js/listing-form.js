import './listing-form-components';

(function ($) {
	"use strict";

	//reassign templates
	delete VueBase.FormSelectField.render;
	VueBase.FormSelectField.template = '#form-select-field';
	/////////////////////////

	Vue.use(VueBase.Plugin);

	var LayoutMixin = {
		props: {
			field: {type: Object, required: true},
			fieldLabel: {type: String},
			showLabel: {type: Boolean, default: true},
			showHelpText: {type: Boolean, default: true}
		}
	};

	Vue.component('step1-primary-layout', {
		template: '#step1-primary-layout',
		mixins: [LayoutMixin]
	});

	Vue.component('step1-secondary-layout', {
		template: '#step1-secondary-layout',
		mixins: [LayoutMixin]
	});

	var $loader, $message, $button;

	$(document).ready(function () {
		if(jQuery('#listing-form').length){
			var AircraftForm = new Vue({
				el: '#listing-form',

				mixins: [VueBase.FormComponent],

				data: function() {
					return {
						model: {},
						primary: [],
						secondary: [],
						modifications: [],
						id: null,
						selectedPhrases: [],
					}
				},

				mounted: function () {
					$loader = $('.stm-add-a-car-loader');
					$message = $('.stm-add-a-car-message');
					$button = $('button[data-trigger="submit-listing"]');

					this.id = $(this.$el).data('id');
					this.loadForm();
				},

				methods: {

					loadForm: function () {

						// Replace placeholder for custom placeholder
						if(stm_hybrid.is_add_car_page){
							for(var fid in listing_form.fields){
								if(listing_form.fields[fid].attribute == 'features_id'){
									// console.log(listing_form.fields[fid]);
								}
								if(typeof listing_form.fields[fid].attributeInstance !== 'undefined')
									if(typeof listing_form.fields[fid].attributeInstance.placeholder2 !== 'undefined')
										listing_form.fields[fid].placeholder = listing_form.fields[fid].attributeInstance.placeholder2;
							}
						}else{
							for(var fid in listing_form.fields){
								if(typeof listing_form.fields[fid].attributeInstance !== 'undefined')
									if(typeof listing_form.fields[fid].attributeInstance.placeholder2 !== 'undefined')
										listing_form.fields[fid].placeholder = listing_form.fields[fid].attributeInstance.placeholder1;
							}
						}

						this.model = listing_form.data;
						this.setFields(listing_form.fields);
						this.fields.get('videos').component = 'videos-field';
						this.primary = _.where(this.fields.all(), {panel: 'primary'});
						this.secondary = _.where(this.fields.all(), {panel: 'secondary'});
						this.modifications = _.where(this.fields.all(), {panel: 'modifications'});

						if(this.fields.get('phases').value){
							var val = this.fields.get('phases').value.split(', ');
							$(document).find('.stm_phrases input[type=checkbox]').each(function(index, el) {
								if(jQuery.inArray($.trim($(el).val()),val) !== -1){
									$(el).prop('checked',true);
								}
							});

						}


						var sortPrimary = [];

						for(var i in this.primary){
							if(typeof this.primary[i].attribute === 'string'){
								if(this.primary[i].attribute == "condition_id") sortPrimary[0] = this.primary[i];
								if(this.primary[i].attribute == "make_id") sortPrimary[1] = this.primary[i];
								if(this.primary[i].attribute == "serie_id") sortPrimary[2] = this.primary[i];
								if(this.primary[i].attribute == "trim_id") {
									sortPrimary[3] = this.primary[i];
								}
								if(this.primary[i].attribute == "ca-year_id") {
									sortPrimary[4] = this.primary[i];
								}
							}
						}
						this.primary = sortPrimary;

						this.secondary = _.filter(this.secondary, function (field) {
							return field.attribute !== 'location'
						});

					},

					beforeSubmit: function(append){
						if($('#stm_user_info input[name=stm_first_name]').length){
							let stm_first_name = $('#stm_user_info input[name=stm_first_name]').val();
							let stm_last_name = $('#stm_user_info input[name=stm_last_name]').val();
							let stm_phone = $('#stm_user_info input[name=stm_phone]').val();

							$('#stm_user_info input[name=stm_first_name],#stm_user_info input[name=stm_last_name],#stm_user_info input[name=stm_phone]').removeClass('stm_error');

							if(stm_first_name == '' || stm_last_name == '' || stm_phone == ''){
								$('.stm-add-a-car-message').html('')
								if(stm_first_name == '') $('#stm_user_info input[name=stm_first_name]').addClass('stm_error');
								if(stm_last_name == '') $('#stm_user_info input[name=stm_last_name]').addClass('stm_error');
								if(stm_phone == '') $('#stm_user_info input[name=stm_phone]').addClass('stm_error');
								$button.removeClass('disabled');
								return;
							}

							var self = this;

							$.ajax({
								type: 'POST',
								dataType: 'json',
								url: '/wp-admin/admin-ajax.php',
								data: {
									'action': 'stm_update_profile_exdended',
									'first_name': stm_first_name,
									'last_name': stm_last_name,
									'stm_phone': stm_phone
								},
								beforeSend:function(){},
								success: function(data){
									self.submit(append);
								},
								error: function(e){}
							});

						}else this.submit(append);
					},

					submit: function (append) {

						$message.slideUp();
						// if (!this.fields.get('gallery_id').value.length) {
						if (1 === 2) {
							$message.html(stm_hybrid.translations.photoRequired).slideDown();
							setTimeout(function () {
								$button.removeClass('disabled');
							});
							return;
						} else {
							if (this.fields.get('gallery_id').value.length > stm_hybrid.user_photo_upload_limit) {
								$message.html(stm_hybrid.translations.uploadPhotoLimit).slideDown();
								setTimeout(function () {
									$button.removeClass('disabled');
								});
								return;
							}
						}
						$loader.addClass('activated');
						$message.slideUp();
						this.errors.clear();


						if($(this.$el).find('input[value=auction]').prop('checked')){
							this.fields.only('auction')[0].value = true;
						}else{
							this.fields.only('auction')[0].value = false;
						}

						var phases = [];
						$(document).find('.stm_phrases input[type=checkbox]').each(function(index, el) {
							if($(el).prop('checked')){
								phases.push($(el).val());
							}
						});


						let formData = this.formData(this.fields.except('gallery_id', 'brochure_id'));

						if (this.id) {
							formData.append('_method', 'PUT');
						}

						if(phases.length){
							formData.append('phases', phases);
						}

						if($('input[name=stm_car_main_title]').val()){
							formData.append('title', $('input[name=stm_car_main_title]').val());
						}

						if($('input[name=subscription_id]').val()){
							formData.append('subscription_id', $('input[name=subscription_id]').val());
						}
						if($('input[name=subscription_status]').val()){
							formData.append('subscription_status', $('input[name=subscription_status]').val());
						}
						if($('input[name=subscription_expires]').val()){
							formData.append('subscription_expires', $('input[name=subscription_expires]').val());
						}
						if($('input[name=subscription_expires_r]').val()){
							formData.append('subscription_expires_r', $('input[name=subscription_expires_r]').val());
						}
						if($('input[name=subscription_title]').val()){
							formData.append('subscription_title', $('input[name=subscription_title]').val());
						}

						if (append) {
							_.each(append, function (value, key) {
								formData.append(key, value);
							});
						}

						$.ajax({
							type: 'POST',
							url: stm_hybrid.api_url + 'listings' + (this.id ? '/' + this.id : ''),
							data: formData,
							dataType: 'json',
							contentType: false,
							processData: false,
							context: this,
							headers: {
								Authorization: typeof stm_hybrid_auth !== "undefined" ? 'Bearer ' + stm_hybrid_auth.token : null
							},
							success: function (result) {
								this.onSuccess(result)
							},
							error: function (response) {
								this.onError(response)
								$button.removeClass('disabled')
							}
						});
					},
					uploadAttachments: function () {
						$loader.addClass('activated');
						this.errors.clear();
						$message.text(stm_hybrid.translations.uploadingPhotos).slideDown();
						let formData = this.formData(this.fields.only('gallery_id', 'brochure_id'));
						formData.append('_method', 'PUT');



						$.ajax({
							type: 'POST',
							url: stm_hybrid.api_url + 'listings' + (this.id ? '/' + this.id : ''),
							data: formData,
							dataType: 'json',
							contentType: false,
							processData: false,
							context: this,
							headers: {
								Authorization: typeof stm_hybrid_auth !== "undefined" ? 'Bearer ' + stm_hybrid_auth.token : null
							},
							success: function (response) {
								if(stm_hybrid.dealer_pay_per_listing){
									$message.html(stm_hybrid.translations.listingAddedPayRedirect).slideDown();
									this.finishEditing();
									window.location = stm_hybrid.wc_get_checkout_url + '&lid=' + response.data.id;
								}else{
									$message.html(stm_hybrid.translations.listingAddedRedirect).slideDown();
									this.finishEditing();
									if($(this.$el).find('input[value=auction]').prop('checked')){
										window.location = stm_hybrid.user_page_auction_url;
									}else{
										window.location = stm_hybrid.user_page_url;
									}
								}
							},
							error: function (response) {
								this.onError(response);
								$message.slideUp()
							},
							complete: function () {
								$button.removeClass('disabled');
							}
						});
					},

					onSuccess: function(result) {
						$loader.removeClass('activated');
						this.id = result.data.id;

						this.uploadAttachments();
					},

					finishEditing: function() {
						$loader.removeClass('activated');
						this.loadForm();
					},

					onError: function(response) {
						$loader.removeClass('activated');
						switch (response.status) {
							case 422:
								this.setValidationErrors(response.responseJSON.errors);
								break;
						}


						let message = '';
						if (response.responseJSON.message) {
							message += '<div>' + response.responseJSON.message + '</div>';
						}
						_.each(response.responseJSON.errors, function (errors) {
							message += '<div>' + errors.join(' ') + '</div>';
						});
						$message.html(message).slideDown();
					}
				}

			});
		}

		$('button[data-trigger=submit-listing]').on('click', function (event) {
			event.preventDefault();
			var status = $(this).data('status') || 'publish';
			AircraftForm.beforeSubmit({status: status});
		});


		$(document).on('click', '.stm-seller-notes-phrases', function (e) {
			e.preventDefault();
			$('.stm_phrases').toggleClass('activated');
		});

		$('.stm_phrases .button').off('click');

		$(document).on('click', '.stm_phrases .button', function (e) {
			e.preventDefault();
			AircraftForm.selectedPhrases = [];
			$(document).find('.stm_phrases input[type=checkbox]:checked').each(function(index, el) {
				AircraftForm.selectedPhrases.push($(el).val());
			});
			var $textArea = $('.stm-phrases-unit textarea');
			var text = '';
			text = (AircraftForm.selectedPhrases.join(',')).trim();

			$textArea.val(text);
			Base.$emit('content-value', text);
			AircraftForm.selectedPhrases = [];

			$('.stm_phrases').toggleClass('activated');
		});

		$(document).on('click', '.stm_phrases .fa-close', function (e) {
			e.preventDefault();
			$('.stm_phrases').toggleClass('activated');
		});

		//disable motors stuff
		$('.stm-media-car-gallery .stm-placeholder').stop().droppable('destroy');
		$(document).off("mouseenter touchstart", '.stm-media-car-gallery .stm-placeholder .inner .stm-image-preview');
		$('body').off('input', '.stm-video-link-unit input[type="text"]')


	});




	// Auto-complete location by OpenStreetMap
	Vue.component('auto-complete', {
		template: `
			<div class="auto-complete">
				<input v-model="keywords" :placeholder="holder" class="form-control" type="text" v-on:keydown="debounceInput" @focus="focus" @blur="focus" autocomplete="off">
				<div class="pac-container pac-logo" v-if="(focused && this.data.length > 0)">
					<div class="pac-item" v-for="(person, i) in data" v-on:click="searchClicked" :data-lat="person.lat" :data-lng="person.lon">
				    	<span class="pac-icon pac-icon-marker"></span>
				    	<span class="pac-item-query">
				    		<span class="pac-matched" :title="person.display_name">{{ person.display_name }}</span>
				    	</span>
				    </div>

				    <div class="stm-map-location">
						<span>Powered by <strong>Nominatim</strong></span>
						<a data-toggle="modal" href="#stm_pickup_location">
							<img src="https://nominatim.openstreetmap.org/ui/assets/images/osm_logo.120px.png" alt="logo">
						</a>
					</div>
				</div>

				<input name="lat" type="hidden">
				<input name="lng" type="hidden">
			</div>
		`,

		// props: {
		// 	holder: String,
		// 	focused: Boolean
		// },
		methods: {
			debounceInput: _.debounce(function (e) {
				this.filterKey = e.target.value;
				var self = this;
				fetch('https://nominatim.openstreetmap.org/search.php?q='+this.filterKey+'&format=json').then(function(response){
					response.json().then(function(data) {
						self.data = data;
						self.Olddata = data;
					});
				});
			}, 500),
			searchClicked(event) {
				if($(event.target).hasClass('pac-item')) var target = $(event.target);
				else var target = $(event.target).parents('.pac-item');
				var lat = target.data('lat');
				var lng = target.data('lng');
				var name = target.find('.pac-matched').text();

				if(typeof map !== 'undefined'){
					var marker = L.marker([lat,lng]);
					marker.addTo(map);
					window.CenterMarker = marker;
					map.setView(new L.LatLng(lat, lng), 14);
				}

				if(listing_form){
					_.each(listing_form.fields, function (value, key) {
						if(value.attribute == 'location'){
							listing_form.fields[key].value.address = name;
							listing_form.fields[key].value.coordinates = lat+','+lng;
						}
					});
				}


				//$(event.target).parents('.auto-complete').find('input[type=text]').val(name).attr('value',name);
				this.keywords = name;




			},
			focus(event) {
				var self = this;
				// Click work before blur
				setTimeout(function(){
					if(event.type == 'focus'){
						self.focused = true;
						event.target.select();
					}else{
						self.focused = false;
					}
				}, 200);
			},

			setAddressByLatLng(lat,lng){
				let self = this
				$('.stm-location-input-wrap input[name=lat]').val(lat).attr('value',lat);
				$('.stm-location-input-wrap input[name=lng]').val(lng).attr('value',lng);

				//Set address id moved pin
				fetch('https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat='+lat+'&lon='+lng).then(function(response){
					response.json().then(function(data) {
						let address = data.display_name;
						self.input.val(address);
						self.input.attr('value',address);
						self.keywords = address;
						if(listing_form){
							_.each(listing_form.fields, function (value, key) {
								if(value.attribute == 'location'){
									listing_form.fields[key].value.address = address;
								}
							});
						}
					});
				});

				if(listing_form){
					_.each(listing_form.fields, function (value, key) {
						if(value.attribute == 'location'){
							listing_form.fields[key].value.coordinates = lat+','+lng;
						}
					});
				}
			},

			osm_addcar_init_map(lat,lng,zoom){
				let self = this;
				var map = L.DomUtil.get('addcar_location_map');
				if(map != null) map._leaflet_id = null;
				map = L.map('addcar_location_map').setView([lat, lng], zoom);
				map.scrollWheelZoom.disable();
				window.map = map;
				L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'}).addTo(map);
				var marker = L.marker([lat, lng]);
				marker.addTo(map);
				window.CenterMarker = marker;

				var lat = map.getCenter()['lat'];
				var lng = map.getCenter()['lng'];
				self.setAddressByLatLng(lat,lng);

				map.on('move', function (event) {
					if(window.CenterMarker){
						var cnt = map.getCenter();
						window.CenterMarker.setLatLng(cnt);
					}
				});

				map.on('moveend', function(event) {
					var lat = map.getCenter()['lat'];
					var lng = map.getCenter()['lng'];
					self.setAddressByLatLng(lat,lng);
				});
			}

		},
		mounted(){
			this.input = $('.stm-location-input-wrap .auto-complete input[type=text]');
			this.holder = this.$attrs.placeholder;
			// Get saved location
			var _address = null,
				_coordinates = '';

			var self = this;
			if(listing_form){
				_.each(listing_form.fields, function (value, key) {
					if(value.attribute == 'location'){
						if(value.value.address) _address = value.value.address
						if(value.value.coordinates) _coordinates = value.value.coordinates
					}
				});
			}

			if(_coordinates){// Edit car
				if(window.CenterMarker){
					window.CenterMarker.setLatLng(_coordinates.split(','));
					window.map.setView(_coordinates.split(','),14);
				}else{
					var lat = _coordinates.split(',')[0];
					var lng = _coordinates.split(',')[1];
					var zoom = 14;

					$('.stm-location-input-wrap input[name=lat]').val(lat).attr('value',lat);
					$('.stm-location-input-wrap input[name=lng]').val(lng).attr('value',lng);

					self.osm_addcar_init_map(lat,lng,zoom);
				}
			}else{
				// var lat = 40.98138838366699;
				// var lng = 20.06588459014893;
				//
				// self.osm_addcar_init_map(lat,lng,16);
				//
				// if (navigator.geolocation) {
				// 	navigator.geolocation.getCurrentPosition(function(position) {
				// 		lat = position.coords.latitude;
				// 		lng = position.coords.longitude;
				// 		window.map.setView([lat,lng],14);
				// 	}, function(code, message){
				// 		window.map.setView([lat,lng],14);
				// 	});
				// }
			}
			this.keywords = _address
			this.coordinates = _coordinates
			this.keywords = _address

			$('#stm_registered').datetimepicker({
				timepicker:false,
				format: 'd/m/Y',
				lang: stm_lang_code,
				closeOnDateSelect: true
			});


			var $stm_handler = $('#stm_history_label');
			$stm_handler.focus(function () {
				$('.stm-history-popup').removeClass('stm-invisible');
				let top = $stm_handler.offset().top - $stm_handler.parents('.stm-form-1-end-unit').offset().top;
				top = top + $stm_handler.outerHeight() + 20;
				$('.stm-history-popup').css('top', top + 'px');
			});

			$('.stm-history-popup .button').on('click', function (e) {
				e.preventDefault();
				$('.stm-history-popup').addClass('stm-invisible');
				let $stm_checked

				if ($('input[name=stm_chosen_history]:radio:checked').length > 0) {
					$stm_checked = $('input[name=stm_chosen_history]:radio:checked').val();
				} else {
					$stm_checked = '';
				}

				$stm_handler.val($stm_checked);
			});

			$('.stm-history-popup .fa-remove').on('click', function () {
				$('.stm-history-popup').addClass('stm-invisible');
			});

		},
		created() {
			this.keywords = this.value || ''
		},

		data() {
			return {
				coordinates: null,
				input: '',
				focused: false,
				data: [],
				keywords: '',
				holder: ''
			}
		},
	});







})(jQuery);

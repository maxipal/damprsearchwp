(function ($) {
	var terms = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.whitespace,
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		identify: function (term) {
			return term.id;
		},
		remote: {
			url: stm_hybrid.api_url + 'terms/suggestions?s=%QUERY&where[taxonomy][]=body&where[taxonomy][]=make',
			wildcard: '%QUERY',
			transform: function (response) {
				return response.data;
			}
		}
	});

	var users = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		identify: function (user) {
			return user.id;
		},
		prefetch: {
			url: '/user-suggestions/',
			cache: true,
			ttl: 3600000 // 5 minutes
		}
	});

	$('input[data-trigger=suggestions]').each(function () {

		$(this).typeahead({
			hint: true,
			highlight: true,
			minLength: 1
		}, {
			name: 'terms',
			display: 'title',
			source: terms
		}, {
			name: 'users',
			// limit: 100,
			display: 'name',
			source: users
		});

		$(this)
			.on('typeahead:select', function (event, object) {
				if (object.url) {
					location.href = object.url;
					return;
				}

				if (object.taxonomy) {
					location.href = $(this).closest('form').attr('action') + '?filter[' + object.taxonomy + ']=' + object.slug;
				}
			})
	});

})(jQuery);

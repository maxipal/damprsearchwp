import Vue from 'vue'
import VueLazyload from "vue-lazyload";
import { listingImageUrl } from "./includes";
var $ = jQuery;
window.Vue = Vue

Vue.directive('select', {
	inserted: function (el) {
		var options = {
			width: 'auto'
		};

		$(el)
			.select2(options)
			.on('change', function (e) {
				if (e.detail === "vue-directive") {
					return;
				}
				el.dispatchEvent(new CustomEvent("change", {
					detail: "vue-directive"
				}));
				return false;
			})
			.on('reload', function () {
				$(el).select2('destroy').select2(options)
			})
	},
	update: function (el, binding) {
		$(el).addClass('select2-hidden-accessible');
	},
	unbind: function () {
		$(this.el).off().select2('destroy')
	}
});

Vue.use(VueLazyload);
Vue.component('VueGoogleAutocomplete', window.VueGoogleAutocomplete);

//checkbox
Vue.component('stm-checkbox', {
	model: {
		prop: 'checked',
		event: 'input'
	},
	template:
		'<label>' +
		'    <div class="checker">\n' +
		'        <span :class="isChecked ? \'checked\' : \'\'">\n' +
		'            <input style="-webkit-appearance: checkbox;" type="checkbox" v-model="mutableModel" @change="change" :value="value" class="stm-checkbox">\n' +
		'        </span>\n' +
		'    </div>\n' +
		'    <span>{{ label }}</span>' +
		'</label>',
	props: ['value', 'checked', 'label'],
	data: function () {
		return {
			mutableModel: this.checked
		};
	},
	mounted: function () {
		this.$nextTick(function () {
		// Code that will run only after the
		// entire view has been rendered
		})
	},
	created() {
		var self = this;
		jQuery('.stm_wrap_type_inventory').show();
		jQuery(document).on('click','#single_ad,#auction_ad', function(event) {
			event.preventDefault();
			var inv = jQuery(this).parents('.stm_wrap_type_inventory');
			inv.animate({
				opacity: 0},
				400, function() {
				inv.hide();
			});

			if(jQuery(this).attr('id') == 'single_ad'){
				jQuery(document).find('input[value=auction]').prop('checked', false);
				// self.checked = false;
				//self.mutableModel = false;
			}else{
				jQuery(document).find('input[value=auction]').prop('checked', true);
				// self.checked = true;
				jQuery('.stm-form-price-edit').hide();
				//self.mutableModel = true;
			}
		});
	},
	watch: {
		checked: function () {
			this.mutableModel = this.checked || false;
		},
	},
	computed: {
		isChecked: function () {
			if (_.isArray(this.mutableModel)) {
				for(var i in this.mutableModel){
					if(parseInt(this.mutableModel[i]) === this.value){
						return true;
					}
				}
				return false;
			}

			return this.mutableModel;
		},
	},
	methods: {
		change: function () {
			this.$emit('input', this.mutableModel);
		},
	}
});
//radio
Vue.component('stm-radio', {
	model: {
		prop: 'checked',
		event: 'input'
	},
	template:
		'<label>' +
		'	<div class="radio">\n' +
		'		<span>\n' +
		'			<input ' +
		'				style="-webkit-appearance: radio;" ' +
		'				type="radio" ' +
		'				v-model="mutableModel" ' +
		'				@change="change" ' +
		'				:value="value" ' +
		'				class="stm-checkbox" />\n' +
		'		</span>\n' +
		'    </div>\n' +
		'    <span>{{ label }}</span>' +
		'</label>',
	props: ['value', 'checked', 'label'],
	data: function () {
		return {
			mutableModel: this.checked
		};
	},
	mounted: function () {
		this.$nextTick(function () {
			// Code that will run only after the
			// entire view has been rendered
		})
	},
	created() {
		var self = this;
		jQuery('.stm_wrap_type_inventory').show();
		jQuery(document).on('click','#single_ad,#auction_ad', function(event) {
			event.preventDefault();
			var inv = jQuery(this).parents('.stm_wrap_type_inventory');
			inv.animate({
					opacity: 0},
				400, function() {
					inv.hide();
				});
		});
	},
	watch: {
		checked: function () {
			this.mutableModel = this.checked || false;
		},
	},
	computed: {
		isChecked: function () {
			if (_.isArray(this.mutableModel)) {
				for(var i in this.mutableModel){
					if(parseInt(this.mutableModel[i]) === this.value){
						return true;
					}
				}
				return false;
			}

			return this.mutableModel;
		},
	},
	methods: {
		change: function () {
			this.$emit('input', this.mutableModel);
		},
	}
});

Vue.mixin({
	mounted: function () {
		// Motors theme applies uniform plugin for radios and checkboxes
		// and it affects to vue templates
		// We should remove unused wrappers for all rad/check.
		$('.checker :checkbox:not(.stm-checkbox), .checker :radio:not(.stm-checkbox)', this.$el)
			.each(function () {
				var $checker = $(this).closest('.checker');
				$(this).prependTo($(this).closest('label'));
				$checker.remove();
			});
	},
	methods: {
		listingImageUrl,
	},
});


if (typeof (STMListings) == 'undefined') {
	var STMListings = {};
}

(function ($) {
	"use strict";

	var Favorites = STMListings.Favorites = function () {
		$('body.logged-in').on('click', '.stm-listing-favorite, .stm-listing-favorite-action', this.clickUser);
		$('body.stm-user-not-logged-in').on('click', '.stm-listing-favorite, .stm-listing-favorite-action', this.clickGuest);

		this.ids = $.cookie('stm_car_favourites');

		if (this.ids) {
			this.ids = this.ids.split(',');
		} else {
			this.ids = [];
		}

		var _this = this;
		if ($('body').hasClass('logged-in')) {
			$.getJSON('/', { action: 'stm_ajax_user_favourites'}, function (data) {
				_this.ids = data;
				_this.activateLinks();
			});
		} else {
			this.activateLinks();
		}
	};

	Favorites.prototype.clickUser = function (e) {
		e.preventDefault();

		if ($(this).hasClass('disabled')) {
			return false;
		}

		$(this).toggleClass('active');
		var stm_car_add_to = $(this).data('id');

		$.ajax({
			url: ajaxurl,
			type: "POST",
			dataType: 'json',
			data: '&car_id=' + stm_car_add_to + '&action=stm_ajax_add_to_favourites',
			context: this,
			beforeSend: function (data) {
				$(this).addClass('disabled');
			},
			success: function (data) {
				if (data.count) {
					$('.stm-my-favourites span').text(data.count);
				}
				$(this).removeClass('disabled');
			}
		});
	};

	Favorites.prototype.clickGuest = function (e) {
		e.preventDefault();
		location.href = stm_hybrid.user_page_url;
	};

	Favorites.prototype.activateLinks = function (ctx) {
		$.each(this.ids, function (key, value) {
			if (!value) {
				return;
			}

			$('.stm-listing-favorite, .stm-listing-favorite-action', ctx)
				.filter('[data-id=' + value + ']')
				.addClass('active')
				.tooltip('destroy')
				.attr('title', stm_i18n.remove_from_favorites)
				.tooltip()
			;
		});
	};

	window.stm_favourites = new Favorites();

})(jQuery);

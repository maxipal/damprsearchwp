(function ($) {
	"use strict";

		$('form[data-trigger=search-box]').each(function () {
			var filter = new STMListings.Filter(this);
			filter.showCounts = true;
			filter.ajaxDataAppend = {
				filters: 'condition,make,serie,stm_location',
				per_page: 1,
				with: 'seo'
			};

			$(this).on('change', ':input:not(button)', function () {
				filter.ajaxDataAppend['filterWorker'] = true;
				filter.query();
			});

			$(this).on('result', function (event, data) {
				if (data.meta && typeof(data.meta.total) !== 'undefined' && filter.ajaxDataAppend['filterWorker']) {
					$('button span', this).text(data.meta.total);
				}

				if (data.link) {
					if (data.link.indexOf('?') === 0) {
						data.link = '/' + stm_hybrid.listings_base_url + '/' + data.link;
					}
					$(this).attr('action', decodeURI(data.link));
				}
			});

			$(this).data('Filter', filter);
			filter.query();

			// When submitting with empty query params
			// it is better to remove them
			$(this).submit(function (event) {
				event.preventDefault();
				$(this).find('button[type=submit] i').attr('class','stm-preloader').css('color','#ffffff');
				var params = $(this).serialize()
					.replace(/[^=&]+=(&|$)/g, '')
					.replace(/&$/, '');
				// var url_alt = new URL('?' + params, this.action);
				var url = new URL('', this.action);

				// When no lat,lng params is set
				// no reason to pass search radius
				if (!url.searchParams.get('stm_lat') &&
					!url.searchParams.get('stm_lat')) {
					url.searchParams.delete('max_search_radius')
				}

				// Finally forward visitor to clean url
				location.href = url.href;
			});
		});

})(jQuery);

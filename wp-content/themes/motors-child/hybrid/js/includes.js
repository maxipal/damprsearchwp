import thumbor from './inc/thumbor'

/* global thumbor_config, stm_hybrid */
export function listingImageUrl(image, width, height, _crop) {
	const origUrl = stm_hybrid.api_url.replace('/api', '') + 'storage/' + image.directory + '/' + image.filename;

	if (!width && !height) {
		return origUrl
	}

	if (thumbor_config && thumbor_config.url) {
		var op = _crop ? 'resize' : 'fitIn';
		return thumbor
			.setImagePath(image.directory + '/' + image.filename)
			[op](width, height)
			.buildUrl();
	}

	if (stm_hybrid.imgix_url) {
		let crop = _crop ? '&fit=crop' : '';
		return stm_hybrid.imgix_url + '/'
			+ image.directory + '/' + image.filename
			+ '?auto=format'
			+ (width ? '&w=' + width : '')
			+ (height ? '&h=' + height : '')
			+ crop;
	}else{
		if(stm_hybrid.thumb_url){
			return stm_hybrid.thumb_url
				+ '?src=/' + image.directory
				+ '/' + image.filename
				+ '&w=' + width
				+ '&h=' + height
			 	+ '&zc=1&q=90';
		}
	}

	return origUrl
}

export const ListingItemsMixin = {
	methods: {
		getFirstImage: function (gallery) {
			return gallery[0] || false;
		},
		getImageSize: listingImageUrl,
		listingImageUrl,
		openGallery: function (gallery, boxType) {
			let urls = gallery.map(item => {
				if(boxType === 'iframe'){
					return {
						href: item
					};
				}
				return {
					href: this.listingImageUrl(item)
				};
			});

			jQuery.fancybox.open(urls, {
				padding: 0,
				type: boxType
			});
		},
		itemBindings() {
			this.$nextTick(() => {
				stm_favourites.activateLinks(this.$el);
				stm_compare.activateLinks(this.$el);
			});
		},
	},
};

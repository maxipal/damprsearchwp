(function ($) {
    $(".stm_icon_filter_unit").each(function () {

        let iconFilter = new Vue({
            el: this,
            data: {
                terms: [],
                itemsTotal: 1,
                withImage: true,
                taxonomy: $(this).data("taxonomy"),
                limit: $(this).data("per_page")
            },
            created:
                function () {
                    var self = this;
                    var selectedTax = this.taxonomy;
                    self.terms = window.stm_hybrid_icon_filter[selectedTax]
                },
            methods: {
                byImage: function (value) {
                   if (this.withImage)
                    {

                      return _.filter(value, function (o) {
                          return o.image
                        });
                    }
                   else {
                       return value.slice(0,20);
                   }
                },
            },
        });
    });
    setTimeout(function () {
        $(".stm_icon_filter_label").off("click");
    }, 100);
})(jQuery);

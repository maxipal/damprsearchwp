import 'url-polyfill';
import { ListingItemsMixin } from "./includes";

(function ($) {
	"use strict";


	let listingApp = new Vue({
		el: '#listings-result',
		mixins: [ListingItemsMixin],
		data: {
			viewType: 'list',
			listings: [],
			featured: [],
			badges: [],
			alternate: [],
			itemsTotal: 0,
			itemsLoaded: false,
			perPage: null,
			user_image: '',
		},
		mounted() {
			$(this.$el).show();
		},
		methods: {
			stm_replace_curly_brackets( string ) {
				let matches = [];
				matches = string.match(/{(.*?)}/g);
				matches.forEach(function(el, i){
					matches[i] = el.replace(/{/g,'').replace(/}/g,'');
				})
				return matches;
			},
			stm_generate_title (listing, show_labels) {
				let title_from = stm_hybrid.title_frontend;
				let title_return = '';

				if ( title_from !== '' ) {
					let title = this.stm_replace_curly_brackets( title_from );
					let title_counter = 0;
					let terms = listing.get_title;
					if ( title !== '' ) {
						for(var i in  title) {
							let title_part = title[i];
							if ( show_labels && Math.floor(title.length/2) === parseInt(i) ) {
								title_return += '</div>';
							}
							if ( i === '0' && show_labels ) {
								title_return += '<div class="labels">';
							}

							if ( terms ) {
								if ( terms[title_part] && terms[title_part].value ) {
									if ( i === '0' ) {
										title_return += terms[title_part].value;
									} else {
										title_return += ' ' + terms[title_part].value;
									}
								} else {

									// $number_affix = get_post_meta( $post_id, strtolower( $title_part ), true );
									// if ( !empty( $number_affix ) ) {
									// 	$title_return .= ' ' . $number_affix . ' ';
									// }
								}
							} else {
								// $number_affix = get_post_meta( $post_id, strtolower( $title_part ), true );
								// if ( !empty( $number_affix ) ) {
								// 	$title_return .= ' ' . $number_affix . ' ';
								// }
							}
						}
					}
				}

				if ( title_return === '' ) {
					title_return = listing.title;
				}
				return title_return;
			},
			unsetBadge: function (badge) {
				this.$emit('badgeRemoved', badge);
			},
			stmTestDriveCarTitle: function (id, title) {
				stm_test_drive_car_title(id, title);
			},
		}
	});

	function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    }

	$('form[data-trigger=inventory]').each(function () {
		var filter = new STMListings.Filter(this);
		var target = filter.target;

		filter.ajaxDataAppend = listing_query_append;

		listingApp.$on('badgeRemoved', $.proxy(filter.unsetFilter, filter));
		if(listingApp.$refs.pagination)
			listingApp.$refs.pagination.$on('page', $.proxy(filter.paginationClick, filter));

		$(this).on('submit', function (event) {
			event.preventDefault();
			filter.query();
		});

		$(this).on('result', function (event, data) {
			let defaultSorting = 'date_high';
			listingApp.badges = data.badges ? data.badges : [];
			listingApp.listings = data.data;
			listingApp.featured = data.featured ? data.featured : [];
			listingApp.itemsTotal = data.meta.total;
			listingApp.itemsLoaded = !data.initial;
			listingApp.alternate = data.alternate ? data.alternate : [];
			if(listingApp.$refs.pagination)
				listingApp.$refs.pagination.apply(data.meta.last_page, data.meta.current_page);

			// $('.stm_locations').attr('data-el',JSON.stringify(data.ct_loc));
			// $('.stm_locations').stm_locations({
			// 	data: JSON.stringify(data.ct_loc)
			// });

			for(var i in listingApp.listings){
				if(listingApp.listings[i].attributes.mileage.value)
					listingApp.listings[i].attributes.mileage.value = numberWithCommas(listingApp.listings[i].attributes.mileage.value);
			}

			if (typeof(data.pageTitle) !== 'undefined') {
				$('.stm-car-listing-sort-units.stm-car-listing-directory-sort-units .stm-listing-directory-title .title', target).text(data.pageTitle);
			}

			if (data.pageDescription) {
				$('.custom-description').show().html(data.pageDescription);
			}
			else {
				$('.custom-description').hide();
			}

			if (data.meta && typeof(data.meta.total) !== 'undefined') {
				$('.stm-car-listing-sort-units.stm-car-listing-directory-sort-units .stm-listing-directory-title .total > span', target).text(data.meta.total);
			}

			if (data.link) {
				if (data.link.indexOf('?') === 0) {
					data.link = listing.archive_path + data.link;
				}

				var linkData = new URL(window.location.origin + data.link);
				var filterParams = linkData.searchParams;
				var sorting = filterParams.get('sort_order');
				if (sorting === defaultSorting) {
					filterParams.delete('sort_order');
				}
				if (data.meta.current_page !== 1) {
					filterParams.set('page', data.meta.current_page);
				}

				var state = linkData.toString().replace(window.location.origin, '');

				if (state !== location.pathname + location.search) {
					window.history.pushState('', '', decodeURI(state));
				}
			}

			// We also have a blocks rendered in backend
			// since they contain the same information as in Vue rendered blocks
			// they should be removed
			if (!data.initial) {
				$('[data-trigger="vue-remove"]', target).remove();
			}

			$('html title').html(data.title);

			listingApp.$nextTick(function () {
				stm_favourites.activateLinks();
				stm_compare.activateLinks();
			});
		});

		$(this).data('Filter', filter);

		// Since we have requested listing result from PHP
		// and rendered in backend we'll just pass JSON result
		// to process sidebar filters
		listing_result.initial = true;
		filter.appendData(listing_result);
		filter.processFilters(listing_result);

		// Non-Vue pagination click should trigger filter block to reload
		$('.page-numbers a', target).click(function (event) {
			event.preventDefault();
			let page = this.rel;
			filter.paginationClick(page);
		});

		// To make viewType work first time,
		// we should switch to Vue rendered version.
		let removeWatch = listingApp.$watch('viewType', function () {
			listingApp.itemsLoaded = true;
			$('[data-trigger="vue-remove"]', target).remove();
			removeWatch();
		});
	});

})(jQuery);

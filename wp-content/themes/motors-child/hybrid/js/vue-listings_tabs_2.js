import {ListingItemsMixin} from "./includes";

(function ($) {
	$(".stm_listing_tabs_style_2").each(function () {

		new Vue({
			el: this,
			mixins: [ListingItemsMixin],
			data: {
				featured: [],
				recent: [],
				popular: [],
				per_page: $(this).data("per_page"),
				itemsTotal: 1
			},
			created:
				function () {
					var self = this;
					var limit = this.per_page;
					$.getJSON(stm_hybrid.api_url + "listings?auction=0&sort_order=date_high&context=list&per_page=" + limit, function (data) {
						for(var i in data.data){
							if(data.data[i].attributes.mileage.value)
								data.data[i].attributes.mileage.value = self.numberWithCommas(data.data[i].attributes.mileage.value);
						}
						self.recent = data.data;
					});
					$.getJSON(stm_hybrid.api_url + "listings?auction=0&sort_order=popular&context=list&per_page=" + limit, function (data) {
						for(var i in data.data){
							if(data.data[i].attributes.mileage.value)
								data.data[i].attributes.mileage.value = self.numberWithCommas(data.data[i].attributes.mileage.value);
						}
						self.popular = data.data;
					});
				},
			methods: {
				numberWithCommas : function(x) {
			        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
			    },
			}
		})
	});

})(jQuery);

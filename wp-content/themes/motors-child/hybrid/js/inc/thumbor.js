import Thumbor from 'thumbor-js-url-builder'

/* global thumbor_config */
const thumbor = new Thumbor(thumbor_config.key, thumbor_config.url.replace(/\/$/, ''))

export default thumbor

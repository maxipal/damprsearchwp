import Draggable from 'vuedraggable';

Vue.component('form-attachment-field', {
	template: '#form-attachment-field',
	mixins: [VueBase.FormField],
	data: function () {
		return {
			brochure: false,
		}
	},
	methods: {
		clear() {
			this.field.attached = null
			this.field.value = null
		}
	},
});

Vue.component('form-features-field', {
	template: '#form-features-field',
	mixins: [VueBase.FormField],
	data: function () {
		return {
			groups: {},
		}
	},
	created: function () {
		if (!this.field.options) {
			return;
		}

		var vm = this;
		this.field.options.forEach(function (option, key) {
			option.groups.forEach(function (group) {
				if (!vm.groups.hasOwnProperty(group.id)) {
					vm.groups[group.id] = {
						id: group.id,
						title: group.title,
						options: [],
					};
				}

				vm.groups[group.id].options.push(option);
			});
		});
	},
	methods: {
		/*
		 * Set the initial value for the field
		 */
		setInitialValue: function() {
			this.field.value = this.field.value || [];
		},
	}
});

Vue.component('form-gallery-field', {
	name: 'FormGalleryField',
	components: {
		Draggable,
	},
	template: '#form-gallery-field',
	mixins: [VueBase.FormField],
	data: function () {
		return {
			imagesData: [],
			sortable: false,
			isImagesPicked: false
		}
	},
	mounted: function() {
		this.checkIncoming()
	},
	computed: {
		thumbs() {
			let thumbs
			if (!this.isImagesPicked) {
				thumbs = [...Array(5).keys()] //php range() analog
			} else {
				thumbs = []
				this.imagesData.forEach(imageObject => {
					thumbs.push(imageObject.thumb)
				})
			}

			return thumbs
		},
		thumbClasses: function() {
			return this.isImagesPicked ? 'stm-placeholder-generated' : 'stm-placeholder-native'
		},
		firstThumb: function() {
			return this.imagesData[0].thumb.url
		}
	},
	methods: {
		checkIncoming() {
			let thumbsData = this.field.attached
			let imageIds = this.field.value

			if (imageIds.length && thumbsData) {
				imageIds.forEach((imageId, key) => {
					this.imagesData.push({
						thumb: thumbsData[key],
						value: imageId
					})
				})

				this.isImagesPicked = true
			}
		},
		makeThumbFromFile: function(imageFile) {
			let thumb = {url: URL.createObjectURL(imageFile)}
			return thumb
		},
		addImageObject: function(file) {
			this.imagesData.push({
				thumb: this.makeThumbFromFile(file),
				value: file
			})
		},
		removeImage: function(index) {
			if (this.imagesData.length === 1) {
				this.isImagesPicked = false
			}
			this.imagesData.splice(index, 1);
			this.prepareFieldValue();
		},
		onImageFilesPicked: function(e) {
			const imageFiles = e.target.files

			Array.from(imageFiles).forEach(file => {
				this.addImageObject(file)
			})
			if (this.imagesData.length) {
				this.isImagesPicked = true
				this.prepareFieldValue()
			}
			this.resetFileInput()
		},
		prepareFieldValue: function() {
			this.clearFieldValue()
			this.imagesData.forEach(imageData => {
				this.field.value.push(imageData.value)
			});
		},
		clearFieldValue: function() {
			this.field.value.splice(0, this.field.value.length)
		},
		resetFileInput: function() {
			this.$refs.fileInput.value = ''
		}
	}
});

Vue.component('form-location-field', {
	template: '#form-location-field',
	mixins: [ VueBase.FormField ],
	data: function () {
		var lat = '';
		var lng = '';

		if (this.field.value && this.field.value.coordinates) {
			var c = this.field.value.coordinates.split(',');
			lat = c[0];
			lng = c[1];
		}

		return {
			lat: lat,
			lng: lng,
		}
	},

	methods: {
		getAddressData: function (addressData, placeResultData, id) {
			this.field.value.address = placeResultData.formatted_address;
			this.field.value.coordinates = addressData.latitude + ',' + addressData.longitude;
			this.lat = addressData.latitude;
			this.lng = addressData.longitude;
		}
	}

});

Vue.component('form-term-relation-field', {
	template: '#form-term-relation-field',
	mixins: [VueBase.FormField],
	data: function () {
		return {
			options: [],
			depend_ids: [],
			busy: false
		}
	},
	computed: {
		hasAvailableOptions: function () {
			return this.options.length;
		}
	},
	created: function () {
		if (this.field.options) {
			this.options = this.field.options;
		} else {
			this.getOptions()
		}
	},
	methods: {
		getOptions: function () {
			if (this.field.depends.length) {
				if (this.field.depends.length > this.depend_ids.length) {
					return;
				}
			}

			this.busy = true
			jQuery(this.$el).find('.select2-selection__arrow > b').addClass('stm-preloader') // select2 ¯\_(ツ)_/¯

			var self = this;
			var query = {
				'where[taxonomy]': this.field.taxonomy,
				depends: this.depend_ids,
				context: 'fill',
				source: ['title', 'slug','parent_id'],
				'sort[title]': 'asc',
				per_page: 999
			};

			var parents = []
			let childs = []

			jQuery.getJSON(stm_hybrid.api_url + 'terms', query, function (result) {
				self.options.splice(0, self.options.length)

				_.each(result.data, function (term) {
					// childs
					if(typeof term.parent_id !== 'undefined' && term.parent_id){
						childs.push(term)
					}
					if(typeof term.parent_id == 'undefined' || !term.parent_id){ // groups
						parents.push(term)
					}
				})

				let arr = [];
				_.each(parents, function (parent) {
					let parent_id = parent.parent_id;
					let sub = []
					_.each(childs, function (child) {
						if(child.parent_id == parent.id) sub.push(child)
					})
					//if(!sub.length) other.push(parent);
					arr[parent.id] = {
						label: parent.title,
						value: parent.id,
						sub: sub,
					};
					//self.options.push()
				})
				arr.sort(function(a,b){
					if (a.label > b.label) return 1;
					if (a.label < b.label) return -1;
					return 0;
				});

				for(var i in arr){
					var temp = {
						label: arr[i].label,
						value: arr[i].value,
						class: 'is_parent',
					}
					if(arr[i].sub.length){
						temp.class = temp.class + ' disabled has_child';
						temp.disabled = true;
					}
					self.options.push(temp);
					if(arr[i].sub.length){
						for(var j in arr[i].sub){
							var child = {
								label: arr[i].sub[j].title,
								value: arr[i].sub[j].id,
								class: 'child childof_'+arr[i].sub[j].parent_id,
							}
							self.options.push(child);
						}
					}
				}

				self.busy = false
				jQuery(self.$el).find('.select2-selection__arrow > b').removeClass('stm-preloader')
				if(!self.field.value) self.field.value = '';
				// Support for select2 to reload new options
				self.$nextTick(function () {
					if (!this.hasAvailableOptions) {
						this.field.value = null
					}
					if (!self.isFieldValueInOptions()) {
						self.field.value = null
					}
					var select = jQuery(self.$el).find('select')

					let tax = 'single'
					if(typeof self.field.taxonomy !== 'undefined'){
						tax = self.field.taxonomy
					}
					if(!select.val()) select.val('');
					select.find('option[value=""]').removeAttr('disabled');
					select.select2({dropdownCssClass: "showDisabledOption tax_"+tax, templateResult: select2TemplateResult});
				})
			})
		},
		handleDependentChange: function(attribute, value, values) {
			this.depend_ids = _.compact(_.values(values));
			this.getOptions();
		},
		isFieldValueInOptions: function () {
			var self = this
			var inOptions = false

			_.each(this.options, function (option) {
				if (self.field.value === option.value) {
					inOptions = true
				}
			})

			return inOptions
		}
	}
});

Vue.component('form-videos-field', {
    template: '#form-videos-field',
    mixins: [VueBase.FormField],
    data: function () {
        return {
        }
    },
    mounted: function() {
        this.setInitialValue();
    },
    methods: {
        setInitialValue: function() {
            if (!this.field.value.length) {
                this.field.value = ['']
            }
        },
        modifyValue: function(index) {
            let last = index === this.field.value.length - 1;
            if (last) {
                this.addValue()
            } else {
                this.removeValue(index)
            }
        },
        addValue: function() {
            let newIndex = this.field.value.push('') - 1;
            this.focusNew(newIndex)
        },
        removeValue: function(index) {
            this.field.value.splice(index, 1)
        },
        focusNew: function(index) {
            this.$nextTick(function () {
                this.$refs.text[index].focus();
            })
        },
        inputControlClasses: function(index) {
            if (!_.isEmpty(this.field.value[index])) {
                return [
                    index === this.field.value.length - 1 ? 'add' : 'remove',
                    this.field.value[index].indexOf('http://') !== -1 || this.field.value[index].indexOf('https://') !== -1 ? 'active' : ''
                ]
            }
        }
    }
});

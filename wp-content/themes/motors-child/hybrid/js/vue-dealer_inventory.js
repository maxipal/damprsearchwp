import {ListingItemsMixin} from "./includes";

(function ($) {
    $(".dealer-aircrafts").each(function () {
    	var $el = $(this)
    	var view = $el.data('view') ? $el.data('view') : 'grid'

        var dealerPublicInventory = new Vue ({
            el: this,
            mixins: [ListingItemsMixin],
            data: {
                recent: [],
                popular: [],
                perPage: '',
                pageRecentCounter: 1,
                pagePopularCounter: 1,
				last_page: 1,
                viewType: view,
            },
            created:function (){
                this.getPopular();
                this.getRecent();
            },
            mounted() {
                $(this.$el).show();
            },
            methods: {
                stm_replace_curly_brackets( string ) {
                    let matches = [];
                    matches = string.match(/{(.*?)}/g);
                    matches.forEach(function(el, i){
                        matches[i] = el.replace(/{/g,'').replace(/}/g,'');
                    })
                    return matches;
                },
                stm_generate_title (listing, show_labels) {
                    let title_from = stm_hybrid.title_frontend;
                    let title_return = '';

                    if ( title_from !== '' ) {
                        let title = this.stm_replace_curly_brackets( title_from );
                        let title_counter = 0;
                        let terms = listing.get_title;
                        if ( title !== '' ) {
                            for(var i in  title) {
                                let title_part = title[i];
                                if ( show_labels && Math.floor(title.length/2) === parseInt(i) ) {
                                    title_return += '</div>';
                                }
                                if ( i === '0' && show_labels ) {
                                    title_return += '<div class="labels">';
                                }

                                if ( terms ) {
                                    if ( terms[title_part] && terms[title_part].value ) {
                                        if ( i === '0' ) {
                                            title_return += terms[title_part].value;
                                        } else {
                                            title_return += ' ' + terms[title_part].value;
                                        }
                                    } else {

                                        // $number_affix = get_post_meta( $post_id, strtolower( $title_part ), true );
                                        // if ( !empty( $number_affix ) ) {
                                        // 	$title_return .= ' ' . $number_affix . ' ';
                                        // }
                                    }
                                } else {
                                    // $number_affix = get_post_meta( $post_id, strtolower( $title_part ), true );
                                    // if ( !empty( $number_affix ) ) {
                                    // 	$title_return .= ' ' . $number_affix . ' ';
                                    // }
                                }
                            }
                        }
                    }

                    if ( title_return === '' ) {
                        title_return = listing.title;
                    }
                    return title_return;
                },
                getRecent: function(){
                    var self = this;
                    $.getJSON(stm_hybrid.api_url + "listings?where[author_id]=" + stm_hybrid.author_id + "&sort_order=date_high&context=list&page=" + this.pageRecentCounter + '&auction=0', function (data) {
                        self.recent = self.recent.concat(data.data);
                        self.last_page = data.meta.last_page;
                    });
                },
                getPopular: function(){
                    var self = this;
                    $.getJSON(stm_hybrid.api_url + "listings?where[author_id]=" + stm_hybrid.author_id + "&sort_order=popular&context=list&page=" + this.pagePopularCounter + '&auction=0', function (data) {
                        self.popular = self.popular.concat(data.data);
						self.last_page = data.meta.last_page;
                    });
                },
            }
        });
        $('.getRecent').click(function(){
            dealerPublicInventory.getRecent();
        });
        $('.getPopular').click(function(){
            dealerPublicInventory.getPopular();
        });

        $('#mySelect').on('change', function (e) {
            var $optionSelected = $("option:selected", this);
            $optionSelected.tab('show')
        });
    });
	var viewType = $('.stm-modern-view').click(function(){
		viewType.removeClass('active');
		$(this).addClass('active');
	});
})(jQuery);

@setup
$server = isset($server) ? $server : 'staging';
$servers = ['localhost' => '127.0.0.1'];
$deployment = false;
$deployments = [];

if (file_exists('.git/config')) {
	$ini = parse_ini_file('.git/config', true);
	foreach ($ini as $key => $section) {
		if (strpos($key, 'git-ftp ') === 0 && isset($section['url'])) {
			$key = str_replace('git-ftp ', '', $key);
			$deployments[$key] = parse_url($section['url']);
			if (isset($section['user'])) {
				$deployments[$key]['user'] = $section['user'];
			}
			$servers[$key] = $deployments[$key]['user'] . '@' . $deployments[$key]['host'];
		}
	}

	$deployment = isset($deployments[$server]) ? $deployments[$server] : null;
}
@endsetup

@servers($servers)

@task('deploy-code', ['on' => 'localhost'])
	git ftp push -s {{ $server }}
@endtask

@task('build-console', ['on' => 'localhost'])
	cd console-src
	yarn
	yarn build --mode={{ $server }}
	cd ../
	zip -q -r console.zip ./console
	scp console.zip {{ $servers[$server] . ':' . rtrim($deployment['path'], '/') }}/console.zip
	rm -rf console.zip ./console
@endtask

@task('deploy-console', ['on' => $server])
	cd {{ $deployment['path'] }}
	rm -rf ./console
	unzip -q console.zip
	rm -rf console.zip
@endtask

@task('build-theme', ['on' => 'localhost'])
	cd wp-content/themes/motors-child
	yarn
	yarn prod
	zip -q -r dist.zip ./dist && echo "Assets archived"
	scp dist.zip {{ $servers[$server] . ':' . rtrim($deployment['path'], '/') }}/wp-content/themes/motors-child/dist.zip && echo "Assets uploaded"
	rm -rf dist.zip && echo "Assets archive removed locally"
@endtask

@task('deploy-theme', ['on' => $server])
	cd {{ $deployment['path'] }}/wp-content/themes/motors-child
	rm -rf ./dist && echo "Old assets removed"
	unzip -q dist.zip && echo "Assets archive extracted"
	rm -rf dist.zip && echo "Assets archive removed"
@endtask

@task('deploy-remote', ['on' => $server])
	cd {{ $deployment['path'] }}
	wp --allow-root total-cache querystring
	wp --allow-root total-cache flush minify
	wp --allow-root total-cache flush object
	wp --allow-root total-cache opcache_flush local
@endtask

@story('deploy')
	deploy-code
	build-theme
	deploy-theme
	deploy-remote
	@if (empty($noconsole))
	build-console
	deploy-console
	@endif
@endstory

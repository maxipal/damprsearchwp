module.exports = {
  baseUrl: process.env.BASE_URL || "/",
  outputDir: "../console",
  chainWebpack: config => {
    config.optimization.delete("splitChunks");
  }
};

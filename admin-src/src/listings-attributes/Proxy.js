import Proxy from "../proxies/Proxy";

class AttributesProxy extends Proxy {
  /**
   * @param {Object} parameters The query parameters.
   */
  constructor(parameters = {}) {
    super("listing-attributes", parameters);
  }

  bulkDestroy(ids) {
    return this.submit("post", `/${this.endpoint}/bulk-destroy`, { ids });
  }
}

export default AttributesProxy;

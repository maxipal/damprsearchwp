const base = "listings-attributes";

const routes = [
  {
    path: `/${base}`,
    name: `${base}.index`,
    component: () => import("./Index.vue"),
    meta: {
      navigation: "Attributes",
      order: 2,
      auth: true
    }
  },
  {
    path: `/${base}/create`,
    name: `${base}.create`,
    component: () => import("./Create.vue"),
    meta: {
      auth: true
    }
  },
  {
    path: `/${base}/:id`,
    name: `${base}.edit`,
    component: () => import("./Edit.vue"),
    meta: {
      auth: true
    }
  }
];

export default routes;

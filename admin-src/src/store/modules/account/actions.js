/* ============
 * Actions for the account module
 * ============
 *
 * The actions that are available on the
 * account module.
 */

import Transformer from "@/transformers/AccountTransformer";
import AccountProxy from "@/proxies/AccountProxy";
import * as types from "./mutation-types";

export const find = ({ commit }) => {
  new AccountProxy()
    .get()
    .then(response => {
      commit(types.FIND, Transformer.fetch(response));
    })
    .catch(() => {
      console.warn("User request failed...");
    });
};

export default {
  find
};

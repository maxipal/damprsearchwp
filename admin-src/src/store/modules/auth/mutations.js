/* ============
 * Mutations for the auth module
 * ============
 *
 * The mutations that are available on the
 * account module.
 */

import Vue from "vue";
import { CHECK, REGISTER, LOGIN, LOGOUT } from "./mutation-types";

const prefix = process.env.VUE_APP_STORAGE_PREFIX;

/* eslint-disable no-param-reassign */
export default {
  [CHECK](state) {
    state.authenticated = !!localStorage.getItem(`${prefix}.user-token`);
    if (state.authenticated) {
      state.expires = localStorage.getItem(`${prefix}.user-token-expires`);
      Vue.$http.defaults.headers.common.Authorization = `Bearer ${localStorage.getItem(
        `${prefix}.user-token`
      )}`;
    }
  },

  [REGISTER]() {
    //
  },

  [LOGIN](state, { token, expiresIn }) {
    state.authenticated = true;
    state.expires = Date.now() + expiresIn * 1000;
    localStorage.setItem(`${prefix}.user-token`, token);
    localStorage.setItem(`${prefix}.user-token-expires`, state.expires);
    Vue.$http.defaults.headers.common.Authorization = `Bearer ${token}`;
  },

  [LOGOUT](state) {
    state.authenticated = false;
    localStorage.removeItem(`${prefix}.user-token`);
    localStorage.removeItem(`${prefix}.user-token-expires`);
    Vue.$http.defaults.headers.common.Authorization = "";
  }
};

/* ============
 * Actions for the auth module
 * ============
 *
 * The actions that are available on the
 * auth module.
 */

import Vue from "vue";
import store from "@/store";
import * as types from "./mutation-types";
import AuthProxy from "../../../proxies/AuthProxy";

let timer;

function initRefresh(context) {
  if (!context.state.authenticated) {
    return;
  }

  const diff = context.state.expires - Date.now();

  if (diff < 0) {
    context.dispatch("logout");
    return;
  }

  // Refresh token 5 seconds before expiring
  // But not less than zero seconds
  const timeout = Math.max(0, diff - 5 * 1000);

  clearTimeout(timer);
  timer = setTimeout(() => {
    new AuthProxy().refresh().then(({ token, expires_in: expiresIn }) => {
      context.commit(types.LOGIN, { token, expiresIn });
      initRefresh(context);
    });
  }, timeout);
}

export const check = context => {
  context.commit(types.CHECK);
  initRefresh(context);
};

export const register = ({ commit }) => {
  /*
   * Normally you would use a proxy to register the user:
   *
   * new Proxy()
   *  .register(payload)
   *  .then((response) => {
   *    commit(types.REGISTER, response);
   *  })
   *  .catch(() => {
   *    console.log('Request failed...');
   *  });
   */
  commit(types.LOGIN, "RandomGeneratedToken");
  Vue.router.push({
    name: "home.index"
  });
};

export const login = (context, payload) => {
  context.commit(types.LOGIN, payload);
  initRefresh(context);
  store.dispatch("account/find");

  Vue.router.push({
    path: "/"
  });
};

export const logout = ({ commit }) => {
  commit(types.LOGOUT);
  clearTimeout(timer);
  Vue.router.push({
    name: "login.index"
  });
};

export default {
  check,
  register,
  login,
  logout
};

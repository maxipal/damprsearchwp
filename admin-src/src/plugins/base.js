import Vue from "vue";
import vSelect from "vue-select";
import VueTimers from "vue-timers";
import { BasePlugin } from "stylemix-base";
import InlineLayout from "../components/fields/InlineLayout.vue";
import FormRelationField from "../components/fields/FormRelationField.vue";
import FormAttachmentField from "../components/fields/FormAttachmentField.vue";
import FormVideosField from "../components/fields/FormVideosField.vue";
import FormGalleryField from "../components/fields/FormGalleryField.vue";
import FormFeaturesField from "../components/fields/FormFeaturesField.vue";
import FormLocationField from "../components/fields/FormLocationField.vue";
import FormTermRelationField from "../components/fields/FormTermRelationField.vue";
import FormEditorField from "../components/fields/FormEditorField.vue";
import DataBoolean from "../components/data/DataBoolean.vue";

Vue.use(VueTimers);
Vue.use(BasePlugin, {
  defaultLayout: "horizontal"
});
Vue.component("v-select", vSelect);
Vue.component("inline-layout", InlineLayout);
Vue.component("form-relation-field", FormRelationField);
Vue.component("form-attachment-field", FormAttachmentField);
Vue.component("data-boolean", DataBoolean);
Vue.component("form-videos-field", FormVideosField);
Vue.component("form-gallery-field", FormGalleryField);
Vue.component("form-location-field", FormLocationField);
Vue.component("form-features-field", FormFeaturesField);
Vue.component("form-attachment-field", FormAttachmentField);
Vue.component("form-term-relation-field", FormTermRelationField);
Vue.component("form-editor-field", FormEditorField);

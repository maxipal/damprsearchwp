const base = "settings";

const routes = [
  {
    path: `/${base}/seo`,
    name: `${base}.seo`,
    component: () => import("./SEO.vue"),
    meta: {
      navigation: "SEO",
      order: 4,
      auth: true
    }
  }
];
export default routes;

import { FormComponent } from "stylemix-base";
import isPlainObject from "lodash-es/isPlainObject";
import isFunction from "lodash-es/isFunction";

export default {
  mixins: [FormComponent],

  props: {
    id: {}
  },

  data() {
    return {
      model: {}
    };
  },

  computed: {
    updating() {
      return !!this.id;
    }
  },

  methods: {
    loadForm(ProxyParam) {
      let promise;

      const proxy = isFunction(ProxyParam) ? new ProxyParam() : ProxyParam;

      if (this.updating) {
        promise = proxy.edit(this.id);
      } else {
        promise = proxy.create();
      }

      return promise.then(result => {
        this.setFields(result.fields);
        this.model = isPlainObject(result.data) ? result.data : {};
        return result;
      });
    },

    save(Proxy, data = null) {
      const formDataFinal = data || this.formData(this.fields.all());
      let promise;

      if (this.updating) {
        promise = new Proxy().update(this.id, formDataFinal);
      } else {
        promise = new Proxy().store(formDataFinal);
      }

      return promise
        .then(result => {
          this.onSaved(result);
          return result;
        })
        .catch(response => {
          this.onSaveError(response);
        });
    },

    onSaved(result) {
      this.$emit("saved", result);
    },

    onSaveError(response) {
      if (response.status === 422) {
        this.setValidationErrors(response.data.errors);
        this.$emit("invalid", response.data);
        this.$toast.error(response.data.message);
      }
    },

    cancel() {
      this.$emit("cancelled");
    }
  }
};

export default {
  props: {
    attribute: { type: String, required: true },
    config: { type: Object, required: false, default: () => {} },
    item: { type: Object, required: true }
  }
};

/* ============
 * Routes File
 * ============
 *
 * The routes and redirects are defined in this file.
 */

let routes = [];

/**
 * Module system
 * Scan for routes.js file in each first level directories
 */
const modules = require.context("../", true, /routes\.js/);
modules.keys().forEach(module => {
  routes = routes.concat(modules(module).default);
});

routes = routes.concat([
  // Login
  {
    path: "/login",
    name: "login.index",
    component: () => import("@/views/Login.vue"),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  {
    path: "/",
    redirect: "/listings"
  },

  {
    path: "/*",
    redirect: "/listings"
  }
]);

const result = routes;

export default result;

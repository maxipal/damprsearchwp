const routes = [
  {
    path: "/import/listings",
    name: "import-listings",
    component: () => import("./Aircraft.vue"),
    meta: {
      navigation: "Import listings",
      order: 3,
      auth: true
    }
  },
  {
    path: "/import/categories",
    name: "import-categories",
    component: () => import("./Dependencies.vue"),
    meta: {
      navigation: "Import dependencies",
      order: 4,
      auth: true
    }
  }
];

export default routes;

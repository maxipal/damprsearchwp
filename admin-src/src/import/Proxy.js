import Proxy from "../proxies/Proxy";

class ImportProcessProxy extends Proxy {
  /**
   * The constructor for the ImportProcessProxy.
   *
   * @param {Object} parameters The query parameters.
   */
  constructor(parameters = {}) {
    super("import-processes", parameters);
  }
}

export default ImportProcessProxy;

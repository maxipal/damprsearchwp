import { FormComponent } from "stylemix-base";
import VueTimers from "vue-timers/mixin";
import findIndex from "lodash-es/findIndex";
import ImportProcess from "./Process.vue";
import ImportProcessProxy from "./Proxy";

export default {
  mixins: [FormComponent, VueTimers],
  data() {
    return {
      model: {},
      items: []
    };
  },
  components: {
    ImportProcess
  },
  computed: {},
  mounted() {
    this.getForm();
    this.load();
  },
  timers: {
    load: {
      time: 5000,
      autostart: true,
      repeat: true
    }
  },
  methods: {
    getForm() {
      new ImportProcessProxy()
        .setParameter("processor", this.processor)
        .create()
        .then(result => {
          this.setFields(result.fields);
        });
    },
    submit() {
      const data = this.formData();
      data.append("processor", this.processor);

      new ImportProcessProxy()
        .setParameter("processor", this.processor)
        .store(data)
        .then(result => {
          this.items.unshift(result.data);
          this.getForm();
        })
        .catch(response => {
          if (response.status === 422) {
            this.setValidationErrors(response.data.errors);
            this.$emit("invalid", response.data);
          }
        });
    },
    load() {
      new ImportProcessProxy()
        .setParameter("processor", this.processor)
        .all({ ignoreProgressBar: true })
        .then(result => {
          this.items = result.data;
        });
    },
    deleted(process) {
      const index = findIndex(this.items, { id: process.id });
      this.items.splice(index, 1);
    }
  }
};

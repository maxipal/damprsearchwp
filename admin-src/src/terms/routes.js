const base = "terms";

const routes = [
  {
    path: `/${base}`,
    name: `${base}.taxonomies`,
    component: () => import("./Taxonomies.vue"),
    meta: {
      navigation: "Terms",
      order: 2,
      auth: true
    },
    children: [
      {
        name: `${base}.index`,
        path: "taxonomy/:taxonomy",
        component: () => import("./Index.vue")
      }
    ]
  },
  {
    path: `/${base}/:taxonomy/create`,
    name: `${base}.create`,
    component: () => import("./Create.vue"),
    meta: {
      auth: true
    }
  },
  {
    path: `/${base}/:id`,
    name: `${base}.edit`,
    component: () => import("./Edit.vue"),
    meta: {
      auth: true
    }
  }
];

export default routes;

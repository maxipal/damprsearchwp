import Proxy from "../proxies/Proxy";

class TermsProxy extends Proxy {
  /**
   * @param {Object} parameters The query parameters.
   */
  constructor(parameters = {}) {
    super("terms", parameters);
  }

  taxonomies() {
    return this.submit("get", `/${this.endpoint}/taxonomies`);
  }

  bulkDestroy(ids) {
    return this.submit("post", `/${this.endpoint}/bulk-destroy`, { ids });
  }
}

export default TermsProxy;

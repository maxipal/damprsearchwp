import Proxy from "../proxies/Proxy";

class ListingProxy extends Proxy {
  /**
   * @param {Object} parameters The query parameters.
   */
  constructor(parameters = {}) {
    super("listings", parameters);
  }

  bulkUpdate(ids, data) {
    return this.submit("put", `/${this.endpoint}/bulk-update`, { ids, data });
  }

  bulkDestroy(ids) {
    return this.submit("post", `/${this.endpoint}/bulk-destroy`, { ids });
  }
}

export default ListingProxy;

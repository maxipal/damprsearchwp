const base = "listings";

const routes = [
  {
    path: `/${base}`,
    name: `${base}.index`,
    component: () => import("./Index.vue"),
    meta: {
      navigation: "Listings",
      order: 1,
      auth: true
    }
  },
  {
    path: `/${base}/create`,
    name: `${base}.create`,
    component: () => import("./Create.vue"),
    meta: {
      auth: true
    }
  },
  {
    path: `/${base}/:id`,
    name: `${base}.edit`,
    component: () => import("./Edit.vue"),
    meta: {
      auth: true
    }
  }
];

export default routes;
